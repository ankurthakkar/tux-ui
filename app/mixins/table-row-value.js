import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';

export default Mixin.create({
  row: undefined,
  column: {},

  computedValue: computed('column', 'row', function() {
    let row = this.get('row');
    let column = this.get('column');
    let valuePath = column.get('valuePath');

    if (valuePath && valuePath !== 'record.undefined') {
      return row.get(column.get('valuePath'));
    }

    let modelName = row.get('record.constructor.modelName');
    let configValuePaths = column.get('valuePaths');

    valuePath = configValuePaths.findBy('modelName', modelName).valuePath;
    return row.get(`record.${valuePath}`);
  })
});
