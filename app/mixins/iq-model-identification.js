import Mixin from '@ember/object/mixin';
import { assert } from '@ember/debug';
import { isPresent, isEmpty } from '@ember/utils';

const REQUIRED_PROPERTIES = [
  'iqModelName',
  'iqModelForeignKey',
  'iqModelRelationship'
];

export default Mixin.create({
  // e.g. `iq-driver`
  iqModelName: null,

  // canonical `id` key (e.g. `externalId`)
  iqModelForeignKey: null,

  // e.g. `iqDriver`; this is how you get to the IQ model
  iqModelRelationship: null,

  init() {
    this._super(...arguments);

    REQUIRED_PROPERTIES.forEach((propertyName) => {
      let value = this.get(propertyName);
      if (isPresent(value)) return;
      assert(`iq-model-identification: '${propertyName}' property required`);
    });
  },

  normalizeArrayResponse() {
    let normalized = this._super(...arguments);

    normalized.included = normalized.included || [];
    normalized.data.forEach((payload) => {
      this._addInverseRelationship(payload, normalized.included);
    });

    return normalized;
  },

  normalizeSingleResponse() {
    let normalized = this._super(...arguments);

    normalized.included = normalized.included || [];
    this._addInverseRelationship(normalized.data, normalized.included)

    return normalized;
  },

  _addInverseRelationship(payload, included) {
    let iqModelName = this.get('iqModelName');
    let iqModelForeignKey = this.get('iqModelForeignKey');

    let iqModelId;
    if (iqModelForeignKey === 'id') {
      iqModelId = payload.id;
    } else {
      iqModelId = payload.attributes[iqModelForeignKey];
    }

    if (isEmpty(iqModelId)) {
      // don't have data needed to connect, so don't try
      return;
    }

    included.push({
      id: iqModelId,
      type: iqModelName,
      attributes: {},
      relationships: {}
    });
  },

  normalize(typeClass, hash) {
    let iqModelName = this.get('iqModelName');
    let iqModelForeignKey = this.get('iqModelForeignKey');
    let iqModelRelationship = this.get('iqModelRelationship');

    let iqModelId;
    if (iqModelForeignKey === 'id') {
      iqModelId = hash.id;
    } else {
      iqModelId = hash.attributes[iqModelForeignKey];
    }

    let normalized = this._super(...arguments);

    if (isEmpty(iqModelId)) {
      // we don't have the data required to connect, so don't try
      return normalized;
    }

    normalized.data.relationships[iqModelRelationship] = {
      data: {
        id: iqModelId,
        type: iqModelName
      }
    };

    return normalized;
  }});
