import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';

export default Mixin.create({
  otpLabel: computed('otp', function() {
    let otp = this.get('otp');

    switch(otp) {
      case 'O':
        return 'On Time';
      case 'D':
        return 'In Danger';
      case 'L': 
        return 'Late';
      case 'E':
        return 'Early';
    }
  })
});
