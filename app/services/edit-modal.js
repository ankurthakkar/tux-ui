import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { computed, get, set } from '@ember/object';
import { isPresent, isBlank, isNone } from '@ember/utils';
import { task, timeout } from 'ember-concurrency';
import lodash from 'lodash';
import saveRecords from '../utils/save-records';
import Inflector from 'ember-inflector';

const { first, last, isUndefined } = lodash;

const UNDO_DEBOUNCE_MS = 500;

export default Service.extend({
  router: service('router'),
  store: service(),

  editableRecords: null,
  editComponent: null,
  saveRecordFunction: null,

  isLinkMode: false,
  isSearching: false,
  hasApplied: false,
  filter: '',
  undoHistory: null,
  isLastUndoStateCommitted: true,
  errors: null,

  init() {
    this._super(...arguments);

    this.set('editableRecords', []);
    this.set('undoHistory', []);
    this.set('errors', []);
  },

  isOpen: computed.equal('router.currentRouteName', 'dashboard.modals.edit-form'),

  isDirty: computed('editableRecords.{@each.hasDirtyAttributes,@each.isForceDirty}', function() {
    let records = this.get('editableRecords');
    return records.any(record => {
      return record.get('hasDirtyAttributes') ||
        record.get('isForceDirty');
    });
  }),

  canUndo: computed('undoHistory.lastObject.[]', function() {
    return isPresent(this.get('undoHistory.lastObject'));
  }),

  commitLastUndoState: task(function * () {
    yield timeout(UNDO_DEBOUNCE_MS);
    this.set('isLastUndoStateCommitted', true);
  }).restartable(),

  getLastUndoState() {
    let lastUndoState = this.get('undoHistory.lastObject');
    let isLastUndoStateCommitted = this.get('isLastUndoStateCommitted');

    if (isLastUndoStateCommitted) {
      lastUndoState = this.get('undoHistory').pushObject([]);
      this.set('isLastUndoStateCommitted', false);
    }

    return lastUndoState;
  },

  setLastUndoState(record, valuePath) {
    let lastUndoState = this.getLastUndoState();
    let recordChanges = lastUndoState.find(changes => changes.record === record);

    if (isBlank(recordChanges)) {
      recordChanges = lastUndoState.pushObject({ record: record, properties: {} });
    }

    if (isBlank(get(recordChanges, `properties.${valuePath}`))) {
      set(recordChanges, `properties.${valuePath}`, get(record, valuePath));
    }

    this.get('commitLastUndoState').perform();
  },

  open(editComponent, models, saveRecordFunction) {
    this.set('editComponent', editComponent);
    this.set('editableRecords', models);
    this.set('saveRecordFunction', saveRecordFunction);

    let router = this.get('router');
    router.transitionTo('dashboard.modals.edit-form');
  },

  close() {
    this.reset();

    let router = this.get('router');
    router.transitionTo('dashboard');
  },

  setRecordValue(record, valuePath, value, options) {
    let { hasMany, belongsTo } = record.constructor.relationshipNames;
    let isHasMany = hasMany.includes(valuePath);
    let isBelongsTo = belongsTo.includes(first(valuePath.split('.')));

    if (isHasMany) {
      this.setLastUndoState(options.record, options.valuePath);
      options.record.set(options.valuePath, value);

      // Link Mode does not support hasMany records.
      return;
    }

    if (isBelongsTo) {
      record = get(record, first(valuePath.split('.'))).content;
      valuePath = last(valuePath.split('.'));
    }

    if (!isNone(options) && options.useOptionRecord) {
      this.setLastUndoState(options.record, options.valuePath);
      options.record.set(options.valuePath, value);
    }
    else {
      this.setLastUndoState(record, valuePath);
      record.set(valuePath, value);
    }
    // this.setLastUndoState(record, valuePath);
    // record.set(valuePath, value);

    if (this.get('isLinkMode')) {
      let records = this.get('editableRecords');

      records.forEach(record => {
        if (isBelongsTo) {
          record = get(record, first(valuePath.split('.'))).content;
        }

        // valuePath may be set to belongsTo model valuePath if isBelongsTo.
        this.setLastUndoState(record, valuePath);
        record.set(valuePath, value);
      });
    }
  },

  pushRecordValue(record, valuePath, modelNameDefault) {
    let inflector = new Inflector(Inflector.defaultRules);
    let modelName = !isUndefined(modelNameDefault) ? modelNameDefault : inflector.singularize(valuePath);
    let newRecord = this.get('store').createRecord(modelName);

    record.set('isForceDirty', true);
    get(record, valuePath).pushObject(newRecord);
    return newRecord;
  },

  pullRecordValue(record, valuePath, removeRecord) {
    record.set('isForceDirty', true);
    get(record, valuePath).removeObject(removeRecord);
  },

  undo() {
    let lastUndoState = this.get('undoHistory').popObject();

    lastUndoState.forEach(({ record, properties }) => {
      record.setProperties(properties);
    });

    this.set('isLastUndoStateCommitted', true);
  },

  undoAll() {
    let undoHistory = this.get('undoHistory');
    while (isPresent(undoHistory)) this.undo();
  },

  toggleLinkMode() {
    this.toggleProperty('isLinkMode');
  },

  toggleSearchMode() {
    this.toggleProperty('isSearching');

    if (this.get('isSearching') === false) {
      this.set('filter', '');
    }
  },

  apply() {
    // TODO: Integrate against impact API.

    this.setProperties({
      hasApplied: true,
      undoHistory: [],
      isLastUndoStateCommitted: true
    });
  },

  commit() {
    let records = this.get('editableRecords');
    let saveRecordFunction = this.get('saveRecordFunction');

    this.set('errors', []);

    if (!isUndefined(saveRecordFunction) && saveRecordFunction !== null) {
      saveRecordFunction(records).catch(e => {
        this.get('errors').pushObject(e.message);
      });
    }
    else {
      saveRecords(records).catch(e => {
        this.get('errors').pushObject(e.errors.firstObject.detail);
      });
    }

    this.reset();
  },

  cancel() {
    this.set('hasApplied', false);
  },

  reset() {
    this.undoAll();
    this.setProperties({
      isLinkMode: false,
      isSearching: false,
      hasApplied: false,
      filter: '',
      errors: []
    });
  }
});
