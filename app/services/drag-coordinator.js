import Service from '@ember/service';
import { set } from '@ember/object';
import { isBlank } from '@ember/utils';

export default Service.extend({

  sourceRows: null,
  widgetType: null,

  init() {
    this._super(...arguments);
    this.set('sourceRows', {});
  },

  // called when a widget registers dragStart
  setSourceRow(dragId, row, widgetType) {
    set(this.get('sourceRows'), dragId, row);
    this.set('widgetType', widgetType);
  },

  // called when a widget registers drop
  getSourceRow(dragId) {
    if (isBlank(dragId)) return;
    let sourceRow = this.get(`sourceRows.${dragId}`);
    this.set('sourceRows', {});
    return sourceRow;
  },

  // called when a widget registers dragEnd
  clearSourceRows() {
    this.set('sourceRows', {});
    this.set('widgetType', null);
  }
});
