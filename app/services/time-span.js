import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { run } from '@ember/runloop';
import { computed } from '@ember/object';

export default Service.extend({
  workspace: service(),

  minusValue: 0,
  plusValue: 0,

  intervalTimer: null,
  timeInterval: 900000,   // 15 minutes

  isTimeSpanRunning: computed.alias('intervalTimer'),

  startInterval() {
    // Run the update now.
    this._updateWorkspace();

    // Schedule the update to run every X minutes thereafter.
    this.set('intervalTimer', setInterval(() => {
      this._updateWorkspace();
    }, this.get('timeInterval')));
  },

  stopInterval() {
    clearInterval(this.get('intervalTimer'));
    this.set('intervalTimer', null);
  },

  /* Input unit is hours. */
  setSpan(minusValue, plusValue) {
    this.set('minusValue', minusValue * 3600000);
    this.set('plusValue', plusValue * 3600000);
    this._resetSpan();
  },

  /* Input unit is milliseconds. */
  setSpanMillis(minusValue, plusValue) {
    this.set('minusValue', minusValue);
    this.set('plusValue', plusValue);
    this._resetSpan();
  },

  _updateWorkspace() {
    let now = Date.now();
    let startDate = new Date(now - this.get('minusValue'));
    let endDate = new Date(now + this.get('plusValue'));

    let changeSet = {};
    changeSet.startDate = startDate;
    changeSet.endDate = endDate;

    run.scheduleOnce('afterRender', () => {
      this.get('workspace').setProperties(changeSet);
    });
  },

  _resetSpan() {
    this.stopInterval();
    this.startInterval();
  }
});
