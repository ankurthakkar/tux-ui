import Service, { inject as service } from '@ember/service';
import { run } from '@ember/runloop';
import ENV from 'adept-iq/config/environment';
import RSVP from 'rsvp';
import lodash from 'lodash';
import polyline from '@mapbox/polyline';
import { makeArray } from '@ember/array';
import { isPresent } from '@ember/utils';


const TIME_FILTERING_ENABLED = false;

const SOCKET_URL_BASE = ENV.API.streamingService.host;

// `loadAsync` tells UI not to wait for this on initial load
const SUBSCRIPTION_TOPICS = [
  { topic: 'routeWidget', loadAsync: true },
  { topic: 'tripWidget', loadAsync: true },
  { topic: 'stopWidget', loadAsync: true },
  { topic: 'avlWidget', loadAsync: true },
  { topic: 'navigationInfoWidget', loadAsync: true },
  { topic: 'routeExecEventsWidget', loadAsync: true }
];

const EVENT_HANDLERS = {
  open: 'onSocketOpen',
  close: 'onSocketClose',
  error: 'onSocketError',
  message: 'onSocketMessage'
};

const ROUTE_RELATIONSHIP_TYPES = [
  'cluster',
  'driver',
  'driverBreak',
  'driverShift',
  'schedule',
  'provider',
  'vehicleType',
  'vehicle',
  'vehicleRoute'
];

const TRIP_RELATIONSHIP_TYPES = [
  'route',
  'cluster',
  'stop',
  'rider',
  'schedule',
  'fareType'
];

const STOP_RELATIONSHIP_TYPES = [
  'place',
  'trip',
  'cluster',
  'route'
];

// eslint-disable no-console
const logger = {
  warn: console.warn,
  debug: console.debug
};
// eslint-enable no-console

export default Service.extend({
  store: service(),
  websockets: service(),
  workspaceContext: service(),

  sockets: null,

  init() {
    this._super(...arguments);
    this.set('sockets', []);
  },

  connect(startDate, endDate) {
    this.disconnect();

    let sessionId = this.generateUUID();
    let promises = [];

    let sockets = SUBSCRIPTION_TOPICS.map(({ topic, loadAsync }) => {
      let socketUrl = this.socketUrlFor(topic, sessionId, startDate, endDate);
      let socket = this.get('websockets').socketFor(socketUrl);
      let deferred = RSVP.defer();

      

      // initial load only waits sync topics
      if (!loadAsync) {
        promises.push(deferred.promise);
      }

      // build and cache handler functions (so we can unsubscribe on close)
      let handlers = Object.entries(EVENT_HANDLERS).reduce((obj, entry) => {
        let [eventName, handlerName] = entry;
        obj[eventName] = (event) => {
          // augment each call with topic
          run.schedule('actions', this, handlerName, topic, event, deferred);
        };
        return obj;
      }, {});

      // subscribe to events
      Object.entries(handlers).forEach(([ eventName, handler ]) => {
        socket.on(eventName, handler);
      });

      return {
        topic,
        socket,
        handlers
      };
    });

    this.set('sockets', sockets);



    return RSVP.allSettled(promises);
  },

  generateUUID() {
    return Math.random().toString(36).substr(2, 9);
  },

  socketUrlFor(topic, sessionId, startDate, endDate) {
    let queryTerms = [
      `topic=${topic}`,
      `consumerGroup=_${sessionId}`
    ];

    if (TIME_FILTERING_ENABLED) {
      if (topic === 'tripWidget') {
        let filterString = [
          `requestTime=ge=${startDate.toISOString()}`,
          `requestTime=le=${endDate.toISOString()}`
        ].join('%26');

        queryTerms.push(`filter=${filterString}`);
      }

      if (topic === 'routeWidget') {
        let filterString = [
          `plannedStartTime=lt=${endDate.toISOString()}`,
          `plannedEndTime=gt=${startDate.toISOString()}`
        ].join('%26');

        queryTerms.push(`filter=${filterString}`);
      }
    }

    let queryString = queryTerms.join('&');

    return `${SOCKET_URL_BASE}/?${queryString}`;
  },

  disconnect() {
    this.get('sockets').forEach(({ socket, handlers }) => {
      Object.entries(handlers).forEach(({ eventName, handler }) => {
        socket.off(eventName, handler);
      });

      socket.close();
    });

    this.set('sockets', []);
  },

  onSocketOpen(topic, event) {
    logger.debug('socket open', topic, event);
  },

  onSocketClose(topic, event) {
    logger.debug('socket close', topic, event);
  },

  onSocketError(topic, event, deferred) {
    logger.debug('socket error', topic, event);
    deferred.reject(event);
  },

  onSocketMessage(topic, event, deferred) {
    logger.debug('socket message', topic, event);

    if (event.data) {
      let payload;

      try {
        payload = JSON.parse(event.data);
      } catch(err) {
        throw `bad topic json payload: ${event.data}`;
      }

      this.onTopicPayload(payload);
    }

    deferred.resolve();
  },

  // this is a good entry point for stubbing a socket payload
  onTopicPayload(payload) {
    let normalizedPayloads = makeArray(payload).reduce((arr, payload) => {
      let payloads = makeArray(this.unpackTopicPayload(payload));
      arr.push(...payloads);
      return arr;
    }, []);

    this.get('workspaceContext').pushPayloads(normalizedPayloads);
  },

  unpackTopicPayload(payload) {
    switch (payload.data.type) {
      case 'route':
        return this.unpackRouteTopicPayload(payload);
      case 'trip':
        return this.unpackTripTopicPayload(payload);
      case 'stop':
        return this.unpackStopTopicPayload(payload);
      case 'avl':
        return this.unpackAVLTopicPayload(payload);
      case 'navigationInfo':
        return this.unpackNavigationInfoPayload(payload);
      case 'routeExecEvents':
        return this.unpackRouteExecEventPayload(payload);
      default:
        throw `unhandled topic payload type '${payload.data.type}'`;
    }
  },

  unpackRouteTopicPayload(payload) {
    let normalizedPayloads = [];
    payload.included.forEach((includedPayload) => {
      if (!ROUTE_RELATIONSHIP_TYPES.includes(includedPayload.type)) {
        logger.warn('unhandled included payload', includedPayload);
        return;
      }

      // these are all ds-types; don't need to change IDs
      let normalizePayload = this._normalizePayload({ data: includedPayload });
      normalizedPayloads.push(normalizePayload);
    });

    let { stopPolylines } = payload.data.attributes;
    stopPolylines.forEach((obj) => {
      if (!obj.polyline) return;
      obj.polyline = polyline.decode(obj.polyline);
    });

    delete payload.included;

    normalizedPayloads.push(this._normalizePayload(payload));

    return normalizedPayloads;
  },

  unpackTripTopicPayload(payload) {
    let normalizedPayloads = [];

    payload.included.forEach((includedPayload) => {
      if (!TRIP_RELATIONSHIP_TYPES.includes(includedPayload.type)) {
        logger.warn('unhandled included payload', includedPayload);
        return;
      }

      if (includedPayload.type === 'rider') {
        includedPayload.type = 'bs-rider';

        let { rmsRiderId } = includedPayload.attributes;
        delete includedPayload.attributes.rmsRiderId;

        includedPayload.attributes.riderId = rmsRiderId;
      }

      if (includedPayload.type === 'stop') {
        includedPayload.type = 'bs-stop';
      }

      let normalizePayload = this._normalizePayload({ data: includedPayload });
      normalizedPayloads.push(normalizePayload);
    });

    delete payload.included;

    let bsPayload = lodash.cloneDeep(payload);
    bsPayload.data.type = 'bs-booking';

    let normalizedPayload = this._normalizePayload(bsPayload);
    normalizedPayloads.push(normalizedPayload);

    if (payload.data.attributes.dispatchTripPKId) {
      let dsPayload = lodash.cloneDeep(payload);
      let externalId = dsPayload.data.id;
      dsPayload.data.id = dsPayload.data.attributes.dispatchTripPKId;
      dsPayload.data.attributes.externalId = externalId;

      // these are bs-type models
      delete dsPayload.data.relationships.rider;
      delete dsPayload.data.relationships.pick;
      delete dsPayload.data.relationships.drop;

      normalizedPayloads.push(this._normalizePayload(dsPayload));
    }

    return normalizedPayloads;
  },

  unpackStopTopicPayload(payload) {
    let normalizedPayloads = [];
    let dispatchTripId;

    payload.included.forEach((includedPayload) => {
      if (!STOP_RELATIONSHIP_TYPES.includes(includedPayload.type)) {
        logger.warn('unhandled included payload', includedPayload);
        return;
      }

      if (includedPayload.type === 'trip') {
        includedPayload.type = 'bs-booking';

        // need to save this, it's only way to connect ds-stop to ds-trip!
        dispatchTripId = includedPayload.attributes.dispatchTripPKId;
      }

      if (includedPayload.type === 'place') {
        // TODO: identify with `ds-place` model (need to add dispatchId)
        includedPayload.type = 'bs-location';
      }

      let normalizePayload = this._normalizePayload({ data: includedPayload });
      normalizedPayloads.push(normalizePayload);
    });

    delete payload.included;

    let bsPayload = lodash.cloneDeep(payload);
    bsPayload.data.type = 'bs-stop';

    // rename relationships; these are really bs-types
    let { place, trip } = bsPayload.data.relationships;

    if (place) {
      place.data.type = 'location';
      bsPayload.data.relationships.location = place;
      delete bsPayload.data.relationships.place;
    }

    if (trip) {
      // trip.data.type = 'booking';
      // bsPayload.data.relationships.booking = trip;
      delete bsPayload.data.relationships.trip;
    }

    normalizedPayloads.push(this._normalizePayload(bsPayload));

    if (payload.data.attributes.dispatchStopId) {
      let dsPayload = lodash.cloneDeep(payload);
      let externalId = dsPayload.data.id;
      dsPayload.data.id = dsPayload.data.attributes.dispatchStopId;
      dsPayload.data.attributes.externalId = externalId;

      // these are bs-type models
      delete dsPayload.data.relationships.place;
      delete dsPayload.data.relationships.trip;

      if (dispatchTripId) {
        dsPayload.data.relationships.trip = {
          data: {
            id: dispatchTripId,
            type: 'trip'
          }
        };
      }

      normalizedPayloads.push(this._normalizePayload(dsPayload));
    }

    return normalizedPayloads;
  },

  unpackAVLTopicPayload(payload) {
    payload.data.type = 'avl-location';

    let { lat, lng } = payload.data.attributes.location.coord;
    logger.debug('avl', payload.data.id, lat, lng);

    return this._normalizePayload(payload);
  },

  unpackNavigationInfoPayload(payload) {
    let routeId = payload.data.id;
    let encodedPolyline = payload.data.attributes.polyline;

    // route will be created if non-existent, so show a warning
    let route = this.get('store').peekRecord('route', routeId);
    if (!route) {
      logger.warn(`received navigationInfo for unknown route '${routeId}'`);
    }

    return this._normalizePayload({
      data: {
        id: routeId,
        type: 'route',
        attributes: {
          navigationPolyline: encodedPolyline
        }
      }
    });
  },

  unpackRouteExecEventPayload(payload) {
    let { routeId, tripId, type } = payload.data.attributes;
    let stopId = payload.data.attributes.stopPointId;

    let routeAttributes = {};
    let tripAttributes = {};
    let stopAttributes = {};

    switch (type) {
      case 'No Show': {
        logger.debug(`routeExec ${type} tripId=${tripId}`)
        tripAttributes.status = type;
        break;
      }
      case 'Arrive':
      case 'Depart': {
        logger.debug(`routeExec ${type} stopId=${stopId}`)
        stopAttributes.status = type;
        break;
      }
      case 'Accept':
      case 'Reject':
      case 'OnBreak':
      case 'OffBreak': {
        logger.debug(`routeExec ${type} routeId=${routeId}`)
        routeAttributes.status = type;
        break;
      }
      // these are currently defined but unused
      case 'OnStop':
      case 'Complete':
      case 'work': // TODO: check if this should be capitalized
      case 'ETA':
      case 'AVL':
      case 'Pullout':
      case 'Pullin':
      default:
        break;
    }

    let normalizedPayloads = [];

    if (routeId) {
      let { otpRouteStatus, otpRouteValue } = payload.data.attributes;

      if (isPresent(otpRouteStatus)) {
        routeAttributes.otp = otpRouteStatus;
      }

      if (isPresent(otpRouteValue)) {
        routeAttributes.otpValue = otpRouteValue;
      }

      let routePayload = {
        data: {
          id: routeId,
          type: 'route',
          attributes: routeAttributes
        }
      };

      let normalizedRoutePayload = this._normalizePayload(routePayload);
      normalizedPayloads.push(normalizedRoutePayload);
    }

    if (tripId) {
      let { otpTripStatus, otpTripValue } = payload.data.attributes;

      if (isPresent(otpTripStatus)) {
        tripAttributes.otp = otpTripStatus;
      }

      if (isPresent(otpTripValue)) {
        tripAttributes.otpValue = otpTripValue;
      }

      let tripPayload = {
        data: {
          id: tripId,
          type: 'trip',
          attributes: tripAttributes
        }
      };

      let normalizedTripPayload = this._normalizePayload(tripPayload);
      normalizedPayloads.push(normalizedTripPayload);
    }

    if (stopId) {
      let { otpStopStatus, otpStopValue } = payload.data.attributes;

      if (isPresent(otpStopStatus)) {
        stopAttributes.otp = otpStopStatus;
      }

      if (isPresent(otpStopValue)) {
        stopAttributes.otpValue = otpStopValue;
      }

      let stopPayload = {
        data: {
          id: stopId,
          type: 'stop',
          attributes: stopAttributes
        }
      };

      let normalizedStopPayload = this._normalizePayload(stopPayload);
      normalizedPayloads.push(normalizedStopPayload);
    }

    return normalizedPayloads;
  },

  _normalizePayload(payload) {
    let store = this.get('store');
    let { id, type } = payload.data;

    let modelClass = store.modelFor(type);
    let serializer = store.serializerFor(type);
    return serializer.normalizeResponse(store, modelClass, payload, id, 'findRecord');
  }
});
