import Service from '@ember/service';

export default Service.extend({
  vehicleInfoData() {
    let vehicleInfoData = {
      "data":{
         "type":"vehicleInfo",
         "id":"",
         "attributes":{
            "state":"active",
            "callsign":"",
            "device":{
               "hardwareId":null,
               "type":"",
               "OS":"",
               "appVersion":""
            }
         }
      }
   }

    return vehicleInfoData;
  },
  prepareVehicleInfoData(vehicleInfoData,record){
    vehicleInfoData.data.id = record.get('name');
    vehicleInfoData.data.attributes.callsign = record.get('name');
    return vehicleInfoData;
  }
});
