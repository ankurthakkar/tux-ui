import Service from '@ember/service';
import Evented from '@ember/object/evented';
import { run } from '@ember/runloop';

const FRAMES_PER_SECOND = 30;

export default Service.extend(Evented, {
  fps: FRAMES_PER_SECOND,

  init() {
    this._super(...arguments);

    setInterval(() => {
      run.scheduleOnce('afterRender', this, 'tick');
    }, 1000 / FRAMES_PER_SECOND)
  },

  tick() {
    this.trigger('tick');
  },
});
