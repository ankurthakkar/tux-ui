// import Ember from 'ember';
import Service from '@ember/service';
import { inject as service } from '@ember/service';
import ENV from 'adept-iq/config/environment';
import tomtom from 'tomtom';
import { insideZone } from 'adept-iq/utils/zone-validation';


export default Service.extend({
  ajax: service(),
  errorMessage: service(),
  activeGeocode: false,
  activeMarker: null,
  activePickMarker: null,
  activeDropMarker: null,
  oldPickMarker: null,
  oldDropMarker: null,
  oldMarker: null,
  markerType: '',
  latlng: null,
  tomtomAddress: null,
  zoom: 12,
  currentRecord: null,

  addressSearchResults(results) {
    const record = this.get('currentRecord.locations.firstObject');

    if (results.length > 0) {
      const latLng = results.firstObject.position;

      this.set('latlng', {lat: latLng.lat, lng: latLng.lon});
      const tomtomMarker = tomtom.L.marker(this.get('latlng'));

      if(this.get('markerType') === 'pick') {
        this.set('oldPickMarker', this.get('activePickMarker'));
        this.set('activePickMarker', tomtomMarker);

      }else if(this.get('markerType') === 'drop') {

        this.set('oldDropMarker', this.get('activeDropMarker'));
        this.set('activeDropMarker', tomtomMarker);
      } else {

        this.set('oldMarker', this.get('activeMarker'));
        this.set('activeMarker', tomtomMarker);
      }
    }
    else if (record !== undefined && record.lat !== 0 && record.lng !== 0) {

      this.set('latlng', {lat: record.lat, lng: record.lng});
      const tomtomMarker = tomtom.L.marker(this.get('latlng'));

      if(this.get('markerType') === 'pick') {
        this.set('oldPickMarker', this.get('activePickMarker'));
        this.set('activePickMarker', tomtomMarker);

      } else if(this.get('markerType') === 'drop') {
        this.set('oldDropMarker', this.get('activeDropMarker'));
        this.set('activeDropMarker', tomtomMarker);
      } else {

        this.set('oldMarker', this.get('activeMarker'));
        this.set('activeMarker', tomtomMarker);
      }
    }
  },

  findLocationForRecord(record) {

    if (record !== undefined) {
      this.set('currentRecord', record);
      if(record.get('streetName') !== undefined) {
        if (record.get('streetNumber') !== undefined || record.get('streetName') !== undefined ||
          record.get('city') !== null) {
          tomtom.fuzzySearch()
            .key(ENV.tomtom.search.searchKey)
            .query(`${record.get('streetNumber')} ${record.get('streetName')} ${record.get('city')}`)
            .countrySet(ENV.tomtom.search.countrySet)
            .typeahead(false)
            .limit(1)
            .center(ENV.tomtom.search.center)
            .radius(ENV.tomtom.search.radius)
            .callback(this.addressSearchResults.bind(this))
            .go();
        }


      } else {
        if (record.get('streetNumber') !== undefined || record.get('streetAddress') !== undefined ||
          record.get('subRegion') !== null) {
          tomtom.fuzzySearch()
            .key(ENV.tomtom.search.searchKey)
            .query(`${record.get('streetNumber')} ${record.get('streetAddress')} ${record.get('subRegion')}`)
            .countrySet(ENV.tomtom.search.countrySet)
            .typeahead(false)
            .limit(1)
            .center(ENV.tomtom.search.center)
            .radius(ENV.tomtom.search.radius)
            .callback(this.addressSearchResults.bind(this))
            .go();
        }

      }
    }
  },

  activateGeocode(activeGeocode, record, markerType) {

    this.findLocationForRecord(record);
    this.set('activeGeocode', activeGeocode);
    this.set('markerType',markerType);
  },

  deactivateGeocode() {
    this.set('activeGeocode', false);
    this.set('tomtomAddress', null);
    this.set('activeMarker', null);
    this.set('oldMarker', null);
    this.set('markerType', '');
  },

  reverseGeocodeResult(result) {
      const latlng = this.get('latlng');

      result.position = {
        lat: parseFloat(latlng.lat.toFixed(5)),
        lon: parseFloat(latlng.lng.toFixed(5))
      };
      this.set('tomtomAddress', result);
  },

  saveNewGeocode(latlng) {
    this.set('latlng', latlng);

    // if the point is outside the valid zone poolygon
    // return error and exit
    if (!insideZone([ latlng.lat, latlng.lng ])) {
      this.get('errorMessage').pushError({ detail:'Invalid Zone' });
      return;
    }


    const tomtomMarker = tomtom.L.marker(latlng);

    tomtom.reverseGeocode()
      .key(ENV.tomtom.search.searchKey)
      .position(`${latlng.lat},${latlng.lng}`)
      .go()
      .then(this.reverseGeocodeResult.bind(this));

      if(this.get('markerType') === 'pick') {
        this.set('oldPickMarker', this.get('activePickMarker'));
        this.set('activePickMarker', tomtomMarker);

      } else if(this.get('markerType')  === 'drop') {
        this.set('oldDropMarker', this.get('activeDropMarker'));
        this.set('activeDropMarker', tomtomMarker);
      } else {

        this.set('oldMarker', this.get('activeMarker'));
        this.set('activeMarker', tomtomMarker);
      }


  },

  // geonode is part of DDS' service used by the scheduling engine
  getGeonode(queryString) {
    return this.get('ajax').request('/geocode', {
      host: ENV.API.geocodeService.host,
      method: 'GET',
      data: {
        query: queryString,
        resultCount: 1
      }
    });
  }
});
