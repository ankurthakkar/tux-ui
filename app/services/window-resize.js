import Service from '@ember/service';
import Evented from '@ember/object/evented';
import { debounce } from '@ember/runloop';

export default Service.extend(Evented, {
  debounceInterval: 250,
  _onResize: undefined,

  init() {
    this._super(...arguments);
    this._onResize = (event) => {
      let debounceInterval = this.get('debounceInterval');
      debounce(this, '_resizeHandler', event, debounceInterval);
    };
    this._initialiseResizeListener();
  },

  destroy() {
    this._super(...arguments);
    this._destroyResizeListener();
  },

  _initialiseResizeListener() {
    window.addEventListener('resize', this._onResize);
  },

  _destroyResizeListener() {
    window.removeEventListener('resize', this._onResize);
  },

  _resizeHandler(event) {
    this.trigger('didResize', event);
  }
});
