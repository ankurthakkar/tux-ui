import Service from '@ember/service';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isPresent  } from '@ember/utils';
import RSVP from 'rsvp';
import moment from 'moment';
import { makeArray } from '@ember/array';

const DEFAULT_SIZE = 'small';
const DEFAULT_TILE_SIZE = 100;
const DEFAULT_TILE_SPACING = 4;

const logger = () => null;

const STATE_MACHINE = {
  defaultStateId: 'default',

  // TODO: relocate hooks to separated modules
  states: [
    {
      id: 'default',
      hooks: []
    },
    {
      id: 'editWorkspace',
      isSideDrawerVisible: true,
      hooks: [
        {
          from: '*',
          action() {
            // this allows us to roll back on cancel
            this.get('dashboard').makeWidgetsSavepoint();

            // set tooltip
            this.get('tooltip').setProperties({
              tip: 'Edit mode:',
              primaryActionText: 'Keep Changes',
              secondaryActionText: 'Discard Changes',
              primaryAction: () => {
                this.get('dashboard').save().then(() => {
                  this.popState('editWorkspace');
                });
              },
              secondaryAction: () => {
                this.get('dashboard').rollbackWidgets();

                // Allow time for the GridStack instance to update the grid with the
                // rolled back widget configs before popping edit workspace state.
                setTimeout(() => {
                  this.popState('editWorkspace');
                }, 15);
              }
            });
          }
        },
        {
          to: '*',
          action() {
            this.get('tooltip').reset();
          }
        }
      ]
    },
    {
      id: 'editRecords',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'filterColumnWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'gaugeWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'donutChartWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'barChartWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'boxWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'metricsColumnWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'filterMapWidget',
      isSideDrawerVisible: true,
      hooks: []
    },
    {
      id: 'addNewMessage',
      isSideDrawerVisible: true,
      hooks: [{
        to: '*',
        action() {
          this.get('tooltip').reset();
        }
      }]
    },
    {
      id: 'openWorkspace',
      hooks: [{
        to: '*',
        action() {
          this.get('tooltip').reset();
        }
      }]
    },
    {
      id: 'saveWorkspaceAs',
      hooks: [{
        to: '*',
        action() {
          this.get('tooltip').reset();
        }
      }]
    },
    {
      id: 'deleteWorkspace',
      hooks: [{
        from: '*',
        action() {
          this.get('tooltip').setProperties({
            tip: 'Permanently delete workspace?',
            primaryActionText: 'Delete Workspace',
            secondaryActionText: 'Keep Workspace',
            primaryAction: () => {
              this.popState('deleteWorkspace').then(() => {
                let dashboard = this.get('dashboard');
                return dashboard.destroyRecord().then(() => {
                  this.get('router').transitionTo('index');
                });
              });
            },

            secondaryAction: () => {
              this.popState('deleteWorkspace');
            }
          })
        }
      },
      {
        to: '*',
        action() {
          this.get('tooltip').reset();
        }
      }]
    },
    {
      id: 'closeSchedule',
      hooks: []
    },
    {
      id: 'routeActivityLog',
      isSideDrawerVisible: true
    }
  ]
};

export default Service.extend({
  activeContext: service(),
  router: service(),
  scheduleGeneration: service(),
  store: service(),
  tooltip: service(),
  workspaceContext: service(),

  isLightMode: false,

  size: DEFAULT_SIZE,
  tileSize: DEFAULT_TILE_SIZE,
  tileSpacing: DEFAULT_TILE_SPACING,

  isDragging: false,
  isResizing: false,

  contentHeight: null,
  contentWidth: null,

  stack: null,
  dashboard: null,

  isSideDrawerVisible: computed.alias('stack.lastObject.isSideDrawerVisible'),

  topState: computed.alias('stack.lastObject.state'),
  topStateDisplayName: computed.alias('stack.lastObject.options.displayName'),
  topOptions: computed.alias('stack.lastObject.options'),
  stackDepth: computed.alias('stack.length'),

  isEditingTimeline: false,
  isEditing: computed.equal('topState', 'editWorkspace'),
  isGlobalSpinnerVisible: computed.alias('scheduleGeneration.isRunning'),

  isDashboardPickerOpen: computed.equal('topState', 'openWorkspace'),
  isDashboardSaveAsOpen: computed.equal('topState', 'saveWorkspaceAs'),
  isClosingSchedule: computed.equal('topState', 'closeSchedule'),

  gridWidth: computed('contentWidth', 'tileSize', function () {
    return Math.floor(this.get('contentWidth') / this.get('tileSize'));
  }),

  gridHeight: computed('contentHeight', 'tileSize', function () {
    return Math.floor(this.get('contentHeight') / this.get('tileSize'));
  }),

  startDate: computed('dashboard.{startDate,referenceDate}', {
    get(/* key */) {
      let startDate = this.get('dashboard.startDate');
      let referenceDate = this.get('dashboard.referenceDate');
      return moment()
        .startOf('day')
        .add(startDate - referenceDate, 'ms')
        .toDate();
    },
    set(key, value) {
      let referenceDate = moment().startOf('day').toDate();

      this.set('dashboard.startDate', value);
      this.set('dashboard.referenceDate', referenceDate);

      return value;
    }
  }),

  endDate: computed('dashboard.{endDate,referenceDate}', {
    get(/* key */) {
      let endDate = this.get('dashboard.endDate');
      let referenceDate = this.get('dashboard.referenceDate');
      return moment()
        .startOf('day')
        .add(endDate - referenceDate, 'ms')
        .toDate();
    },
    set(key, value) {
      let referenceDate = moment().startOf('day').toDate();

      this.set('dashboard.endDate', value);
      this.set('dashboard.referenceDate', referenceDate);

      return value;
    }
  }),

  init() {
    this._super(...arguments);
    this.set('stack', ['default']);
  },

  reset() {
    this.setProperties({
      isLightMode: false,
      size: DEFAULT_SIZE,
      tileSize: DEFAULT_TILE_SIZE
    });

    this.resetForDashboardChange();
  },

  resetForDashboardChange() {
    this.setProperties({
      isDragging: false,
      isResizing: false,
    });

    this.set('stack', [{ state: 'default' }]);
  },

  pushState(newStateId, options={}) {
    let previousStateId = this.get('topState');
    let previousState = STATE_MACHINE.states.findBy('id', previousStateId);

    logger('covering', previousStateId);

    // TODO: allow for promise actions
    if (previousState) {
      makeArray(previousState.hooks).filter(({ to }) => {
        return to === newStateId || to === '*';
      }).forEach(({ action }) => {
        action.call(this, this);
      });
    }

    // TODO: check if we are allowed to push state based on top state
    let newState = STATE_MACHINE.states.findBy('id', newStateId);
    if (!newState) throw `unknown state with id '${newStateId}'`;

    logger('pushing', newStateId);
    makeArray(newState.hooks).filter(({ from }) => {
      return from === previousStateId || from === '*';
    }).forEach(({ action }) => {
      action.call(this, this);
    });

    this.get('stack').pushObject({
      state: newStateId,
      isSideDrawerVisible: newState.isSideDrawerVisible,
      options
    });

    return RSVP.Promise.resolve();
  },

  popState(stateId) {
    if (this.get('stackDepth') === 0) {
      throw `cannot pop state: stack is empty`;
    }

    let previousStateId = this.get('topState');
    let previousState = STATE_MACHINE.states.findBy('id', previousStateId);

    if (isPresent(stateId) && stateId !== previousStateId) {
      throw `cannot pop state ${stateId}: top state is ${previousStateId}`;
    }

    logger('popping', previousStateId);
    this.get('stack').popObject();

    let newStateId = this.get('topState');
    let newState = STATE_MACHINE.states.findBy('id', newStateId);

    // TODO: allow for promise actions
    makeArray(previousState.hooks).filter(({ to }) => {
      return to === newStateId || to === '*';
    }).forEach(({ action }) => {
      action.call(this, this);
    });

    logger('uncovering', newStateId);

    // TODO: allow for promise actions
    if (newState) {
      makeArray(newState.hooks).filter(({ from }) => {
        return from === previousStateId || from === '*';
      }).forEach(({ action }) => {
        action.call(this, this);
      });
    }

    return RSVP.Promise.resolve();
  }
});
