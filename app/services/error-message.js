import Service from '@ember/service';
import { computed } from '@ember/object';
import { isBlank } from '@ember/utils';

export default Service.extend({
  messages: null,
  warnings: null,
  infos: null,

  message: computed('messages.[]', function() {
    if (isBlank(this.get('messages'))) {
      return;
    }
    return this.get('messages').firstObject;
  }),

  warning: computed('warnings.[]', function() {
    if (isBlank(this.get('warnings'))) {
      return;
    }
    return this.get('warnings').firstObject;
  }),

  info: computed('infos.[]', function() {
    if (isBlank(this.get('infos'))) {
      return;
    }
    return this.get('infos').firstObject;
  }),

  pushError(error) {
    this.get('messages').pushObject(error);
  },

  shiftError() {
    this.get('messages').shiftObject();
  },

  pushWarning(warning) {
    this.get('warnings').pushObject(warning);
  },

  shiftWarning() {
    this.get('warnings').shiftObject();
  },

  pushInfo(info) {
    this.get('infos').pushObject(info);
  },

  shiftInfo() {
    this.get('infos').shiftObject();
  },

  flushMessageQueues() {
    this.get('messages').clear();
    this.get('warnings').clear();
    this.get('infos').clear();
  },

  init() {
    this.set('messages', []);
    this.set('warnings', []);
    this.set('infos', []);

    this._super(...arguments);
  }
});
