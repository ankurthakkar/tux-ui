import Service from '@ember/service';
import Evented from '@ember/object/evented';
import { computed, observer } from '@ember/object';
import { debounce, run } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import ENV from 'adept-iq/config/environment';
import { uuidIn, dateLte, dateGte } from 'adept-iq/config/filter-types';
import { serverRelationships } from 'adept-iq/config/server-relationships';
import { breadthFirstSearch } from 'adept-iq/utils/graph';
import { buildQueryParams } from 'adept-iq/utils/rql';
import { buildCompareFunction } from 'adept-iq/utils/sorts';

import {
  activeContextGraph,
  activeContextNodes
} from 'adept-iq/config/active-context-graph';

import {
  computeActiveContext,
  computeImplicitContext
} from 'adept-iq/utils/local-traversal';

import {
  buildFilterFunction,
  buildCompoundFilterNode,
  buildValueFilterNode
} from 'adept-iq/utils/filters';

import _ from 'lodash';
import { makeArray } from '@ember/array';

export default Service.extend(Evented, {
  store: service(),
  workspace: service(),
  workspaceContext: service(),

  checkedItems: null,

  activeData: null,
  activeDataPromise: null,

  implicitData: null,

  implicitStops: computed('implicitData.[]', function() {
    let implicitData = this.get('implicitData') || [];
    return implicitData.filterBy('modelName', 'iq-stop').mapBy('record');
  }),

  onStartDateChange: observer('workspace.startDate', function() {
    debounce(this, 'refreshAll', 300);
  }),

  onEndDateChange: observer('workspace.endDate', function() {
    debounce(this, 'refreshAll', 300);
  }),

  init() {
    this._super(...arguments);
    this.set('checkedItems', []);
    this.set('activeData', []);
    this.set('implicitData', []);

    this.get('workspaceContext').on('change', (modelNames, deferred) => {
      run.schedule('actions', this, 'onWorkspaceContextChange', modelNames, deferred);
    });

    this.get('workspaceContext').on('clear', (deferred) => {
      run.schedule('actions', this, 'onWorkspaceContextClear', deferred);
    });
  },

  query(modelName, params={}, extra={}) {
    if (ENV.APP.localFilteringEnabled) {
      return this.queryLocal(modelName, params);
    }

    return this.queryServer(modelName, params, extra);
  },

  queryLocal(modelName, params={}) {
    let compareFn = buildCompareFunction(params.sorts);
    let filterFn = buildFilterFunction(params.filter);

    return this.getActiveData().then((activeData) => {
      return activeData[modelName].filter(filterFn).sort(compareFn);
    });
  },

  queryServer(modelName, params={}, extra={}) {
    let { filter, includes } = this.buildActiveContextParams(modelName);

    let extendedFilter =
      buildCompoundFilterNode('and', [filter, params.filter]);

    let extendedIncludes = [];
    if (!isEmpty(includes)) {
      extendedIncludes.addObjects(includes);
    }

    if (!isEmpty(params.includes)) {
      extendedIncludes.addObjects(params.includes);
    }

    let extendedParams = _.merge({}, params, {
      filter: extendedFilter,
      includes: extendedIncludes
    });

    let queryParams = buildQueryParams(extendedParams);
    queryParams = _.merge({}, queryParams, extra);
    return this.get('store').query(modelName, queryParams);
  },

  getActiveData() {
    let promise = this.get('activeDataPromise');
    if (promise) return promise;

    let flattenData = (data) => {
      return Object.entries(data).reduce((arr, [modelName, records]) => {
        records.forEach((record) => {
          arr.push({ modelName, record });
        });
        return arr;
      }, []);
    };

    promise = this.getWorkspaceData().then((workspaceData) => {
      let selectedData = this.get('checkedItems');

      let activeData = computeActiveContext(workspaceData, selectedData);
      let implicitData = computeImplicitContext(workspaceData, selectedData);

      // these are flattened for binding & filtering
      this.set('activeData', flattenData(activeData));
      this.set('implicitData', flattenData(implicitData));

      // TODO: it's weird that implicitData gets set in here but not returned;
      // need to reconcile that there are two varieties of active data

      return activeData;
    });

    this.set('activeDataPromise', promise);
    return promise;
  },

  getWorkspaceData() {
    let promise = this.get('workspaceContext').getData().then((workspaceData) => {
      return workspaceData;
    });

    return promise;
  },

  setCheckedRecords(modelName, records) {
    let items = this.get('checkedItems').rejectBy('modelName', modelName);

    records.forEach((record) => {
      items.pushObject({
        modelName,
        record
      })
    });

    this.set('checkedItems', items);
    this.invalidateActiveData();
    this.triggerRelatedModelsRefresh(modelName);
  },

  triggerRelatedModelsRefresh(modelName) {
    let modelNode = activeContextNodes.findBy('modelName', modelName);

    // queue the neighbours of this modelName's active context node
    let queue = makeArray(modelNode.links).map(({ nodeId }) => {
      return activeContextGraph[nodeId];
    });

    // don't revisit originating node
    let visited = [modelNode];

    let getNeighbours = ({ links }) => {
      return makeArray(links).map(({ nodeId }) => activeContextGraph[nodeId]);
    };

    let modelNames = [];
    let visitNode = ({ modelName }) => {
      modelNames.push(modelName);
    };

    // trigger refresh events for related models
    breadthFirstSearch({
      queue,
      visited,
      getNeighbours,
      visitNode
    });

    this.trigger('refresh', modelNames);
  },

  refreshTableContent(modelName) {
    let modelNames = makeArray(modelName);

    if (isEmpty(modelNames)) {
      modelNames = activeContextNodes.mapBy('modelName');
    }

    modelNames.forEach((name) => {
      this.trigger('refresh', name);
    });
  },

  clearTableContent(modelName) {
    let modelNames = makeArray(modelName);

    if (isEmpty(modelNames)) {
      modelNames = activeContextNodes.mapBy('modelName');
    }

    this.trigger('clear', modelNames);
  },

  invalidateActiveData() {
    this.set('activeData', []);
    this.set('activeDataPromise', null);
  },

  // only used for server-side queries
  buildActiveContextParams(modelName) {
    let filters = [];
    let includes = [];

    serverRelationships[modelName].forEach((relationship) => {
      let items = this.get('checkedItems');
      let relatedItems = items.filterBy('modelName', relationship.modelName);

      if (isEmpty(relatedItems)) return;

      let path;

      // push necessary includes for filter
      if (relationship.type === 'belongsTo') {
        path = `${relationship.path}Id`;

        let pathSegments = relationship.path.split('.');

        if (pathSegments.length > 1) {
          let parentPath = pathSegments.slice(0, -1).join('.');
          includes.push(parentPath);
        }
      }

      if (relationship.type === 'hasMany') {
        path = `${relationship.path}.id`;
        includes.push(relationship.path);
      }

      // build filter node
      let values = relatedItems.mapBy('record.id');
      let filter = buildValueFilterNode(uuidIn, path, values);

      filters.push(filter);
    });

    let filter = buildCompoundFilterNode('and', filters);

    return { filter, includes };
  },

  buildTimeFilter(modelName) {
    let filters = [];
    let node = activeContextGraph[modelName];

    // select entities that overlap time window, not just that are contained

    if (node.leftTimeConstraint) {
      let endDate = this.get('workspace.endDate').toISOString();
      let filter =
        buildValueFilterNode(dateLte, node.leftTimeConstraint, [endDate]);
      filters.push(filter);
    }

    if (node.rightTimeConstraint) {
      let startDate = this.get('workspace.startDate').toISOString();
      let filter =
        buildValueFilterNode(dateGte, node.rightTimeConstraint, [startDate]);
      filters.push(filter);
    }

    return buildCompoundFilterNode('and', filters);
  },

  refreshAll() {
    this.invalidateActiveData();
    this.refreshTableContent();
  },

  clearCheckedItems() {
    this.set('checkedItems', []);
    this.invalidateActiveData();
    this.refreshTableContent();
  },

  onWorkspaceContextChange(modelNames, deferred) {
    this.invalidateActiveData();

    // TODO: try not to refresh everything
    this.refreshTableContent(modelNames);

    // TODO: wait until refresh is done to resolve
    deferred.resolve();
  },

  onWorkspaceContextClear(deferred) {
    this.clearTableContent();

    // TODO: wait until all widgets are cleared
    deferred.resolve();
  }
});
