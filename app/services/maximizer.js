import Service from '@ember/service';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Service.extend({
  router: service('router'),
  workspace: service(),

  dashboard: computed.alias('workspace.dashboard'),

  maximizedWidget: null,

  maximize(widget) {
    this.set('maximizedWidget', widget);
    let router = this.get('router');
    router.transitionTo('maximized');
  },

  minimize() {
    this.set('maximizedWidget', null);
    let router = this.get('router');
    router.transitionTo('dashboard', this.get('dashboard'));
  }
});
