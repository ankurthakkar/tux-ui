import Service from '@ember/service';

export default Service.extend({
  tip: null,
  primaryActionText: null,
  secondaryActionText: null,
  primaryAction: null,
  secondaryAction: null,

  reset() {
    this.setProperties({
      tip: null,
      primaryActionText: null,
      secondaryActionText: null,
      primaryAction: null,
      secondaryAction: null
    });
  }
})
