import Service, { inject as service } from '@ember/service';
import Evented from '@ember/object/evented';
import { makeArray } from '@ember/array';
import { activeContextNodes } from 'adept-iq/config/active-context-graph';
import RSVP from 'rsvp';
import { run } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import { observer } from '@ember/object';

export default Service.extend(Evented, {
  socket: service(),
  store: service(),
  workspace: service(),

  invalidatedModelNames: null,
  workspaceData: null,

  init() {
    this._super(...arguments);
    this.set('invalidatedModelNames', []);

    this.onDatesChange();
  },

  getData() {
    let store = this.get('store');

    let startTime = this.get('workspace.startDate').getTime();
    let endTime = this.get('workspace.endDate').getTime();

    let promise = RSVP.all([
      this.loadAPIData(),
      this.connectAndLoadSocketData()
    ]).then(() => {
      return activeContextNodes.reduce((obj, node) => {
        let peekedRecords = store.peekAll(node.modelName);

        if (node.isActive) {
          obj[node.modelName] = peekedRecords.filter((record) => {
            let leftTime, rightTime;

            if (node.leftTimeConstraint) {
              let date = record.get(node.leftTimeConstraint);
              leftTime = date ? date.getTime() : null;
            }

            if (node.rightTimeConstraint) {
              let date = record.get(node.rightTimeConstraint);
              rightTime = date ? date.getTime() : null;
            }

            if (leftTime && leftTime > endTime) return false;
            if (rightTime && rightTime < startTime) return false;
            return true;
          });
        } else {
          obj[node.modelName] = peekedRecords.toArray();
        }

        return obj;
      }, {});
    });

    promise.then((data) => {
      let flattenedWorkspaceData =
        Object.entries(data).reduce((arr, [modelName, records]) => {
          records.forEach((record) => {
            arr.push({ modelName, record });
          });
          return arr;
        }, []);
      this.set('workspaceData', flattenedWorkspaceData);
    })

    return promise
  },

  loadAPIData() {
    let promise = this.get('loadAPIDataPromise');
    if (promise) return promise;

    promise = RSVP.allSettled([
      // dispatch
      this.store.findAll('break-type'),
      this.store.findAll('no-show-reason-code'),
      this.store.findAll('cancel-type'),
      this.store.findAll('agency-marker'),
      this.store.findAll('travel-need-type'),

      // rider mgmt
      this.store.findAll('rms-rider'),
      this.store.findAll('rms-travel-need-type'),
      this.store.findAll('rms-passenger-type'),
      this.store.findAll('rms-eligibility-type'),

      // scheduling
      this.store.findAll('ss-driver'),
      this.store.findAll('ss-vehicle'),
      this.store.findAll('ss-vehicle-type'),
      this.store.findAll('ss-vehicle-capacity-config'),
      this.store.findAll('ss-vehicle-capacity-type'),
      this.store.findAll('ss-shift-break'),
      this.store.findAll('bs-subscription'),
      this.store.findAll('bs-subscription-recurrence-pattern'),
      this.store.findAll('bs-subscription-travel-need'),
      this.store.findAll('bs-subscription-exclusion')
    ]);

    this.set('loadAPIDataPromise', promise);

    return promise;
  },

  connectAndLoadSocketData() {
    let promise = this.get('loadSocketDataPromise');
    if (promise) return promise;

    let startDate = this.get('workspace.startDate');
    let endDate = this.get('workspace.endDate')
    promise = this.get('socket').connect(startDate, endDate);

    this.set('loadSocketDataPromise', promise);
    return promise;
  },

  invalidateSocketData() {
    this.set('loadSocketDataPromise', null);
  },

  pushPayloads(payloads) {
    let store = this.get('store');

    // compute which model types are going to change
    let modelNames = payloads.reduce((arr, payload) => {
      arr.addObject(payload.data.type);

      makeArray(payload.included).forEach((includedPayload) => {
        arr.addObject(includedPayload.type);
      });

      return arr;
    }, []);

    // count how many records of each affected model type there are
    let modelCounts = modelNames.map((modelName) => {
      return {
        modelName,
        count: store.peekAll(modelName).length
      };
    });

    payloads.forEach((payload) => {
      store.push(payload);
    });

    // compare and issue change events iff a model count changed
    modelCounts.forEach(({ modelName, count }) => {
      let newCount = store.peekAll(modelName).length;
      if (count !== newCount) {
        this.queueForContextChangeEvent(modelName);
      }
    });

    // coalesce multiple pushes into a single context change event
    run.scheduleOnce('actions', this, 'triggerContextChangeEvent');
  },

  // for Ember-Data store compatibility
  pushPayload(_, payload) {
    this.pushPayloads([payload]);
  },

  queueForContextChangeEvent(modelName) {
    this.get('invalidatedModelNames').addObject(modelName);
  },

  triggerContextChangeEvent() {
    let invalidatedModelNames = this.get('invalidatedModelNames');
    if (isEmpty(invalidatedModelNames)) return RSVP.resolve();

    let deferred = RSVP.defer();

    this.trigger('change', invalidatedModelNames.slice(), deferred);

    invalidatedModelNames.clear();

    return deferred.promise;
  },

  onDatesChange: observer('workspace.{startDate,endDate}', function() {
    this.invalidateSocketData();

    activeContextNodes.forEach(({ modelName }) => {
      this.queueForContextChangeEvent(modelName);
    });

    run.scheduleOnce('actions', this, 'triggerContextChangeEvent');
  })
});
