import Service, { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import moment from 'moment';
import { task } from 'ember-concurrency';
import { isEmpty } from '@ember/utils';

// TODO: derive from agency
const BOOKING_WINDOW_DAYS = 1;
const TIME_ZONE = 'America/Los_Angeles';

export default Service.extend({
  activeContext: service(),
  errorMessage: service(),
  store: service(),
  tooltip: service(),
  workspace: service(),

  isRunning: false,

  task: task(function*() {
    return yield this.performTask();
  }).drop(),

  performTask() {
    let schedules;
    let dates = this.getScheduleDates();

    this.set('isRunning', true);

    return this.fetchDSSchedulesForDates(dates).then((_schedules) => {
      schedules = _schedules;
      return this.confirmScheduleDeletion(schedules);
    }).then(() => {
      let scheduleDeletions = RSVP.all(schedules.map((schedule) => {
        return schedule.destroyRecord();
      }));
      return RSVP.hash({
        ssSchedules: this.fetchOrCreateSSSchedulesForDates(dates),
        scheduleDeletions
      });
    }).then(({ ssSchedules }) => {
      return RSVP.all(ssSchedules.map((ssSchedule) => {
        return ssSchedule.importBookings().then(() => {
          return ssSchedule.regenerateSchedule();
        }).then(() => {
          return ssSchedule.exportSchedule();
        });
      }));
    }).then(() => {
      // no promise to return here; refresh extends well beyond task
      this.get('activeContext').refreshAll();
      this.set('isRunning', false);
    }).catch((err) => {
      this.set('isRunning', false);
      if (!err || !err.message) return;
      this.get('errorMessage').pushError({
        title: 'Error generating schedule',
        detail: err.message
      });
    });
  },

  getScheduleDates() {
    let endDate = this.get('workspace.endDate');
    let tomorrow = moment.tz(TIME_ZONE).startOf('day').add(1, 'day');
    let maxDate = tomorrow.clone().add(BOOKING_WINDOW_DAYS - 1, 'day');
    let maxTime = Math.min(endDate.valueOf(), maxDate.valueOf());

    // regenerate all days after today and within end time and booking window
    let dates = [];
    let date = tomorrow;
    while (date.valueOf() <= maxTime) {
      dates.push(date);
      date = date.clone().add(1, 'day');
    }

    return dates;
  },

  fetchOrCreateSSSchedulesForDates(dates) {
    let ssSchedulesQuery = this.get('activeContext').queryServer('ss-schedule');

    return ssSchedulesQuery.then((ssSchedules) => {
      return RSVP.all(dates.map((date) => {
        let dateStart = moment.tz(date, TIME_ZONE).startOf('day');
        let dateEnd = moment.tz(date, TIME_ZONE).endOf('day');

        let ssSchedule = ssSchedules.find((ssSchedule) => {
          let scheduleStart = moment.tz(ssSchedule.get('start'), TIME_ZONE);
          return scheduleStart.startOf('day').valueOf() === dateStart.valueOf();
        });

        if (ssSchedule) {
          return RSVP.resolve(ssSchedule);
        }

        // ensure name is unique
        let baseName = dateStart.format('YYYY-MM-DD');
        let name = baseName;
        for(let i = 1; ; i++) {
          if (!ssSchedules.findBy('name', name)) break;
          name = `${baseName} (${i})`;
        }

        ssSchedule = this.get('store').createRecord('ss-schedule', {
          start: dateStart.toDate(),
          end: dateEnd.toDate(),
          name
        });

        return ssSchedule.save();
      }));
    });
  },

  fetchDSSchedulesForDates(dates) {
    // TODO: add node to filter for future-only and ditch days altogether
    let schedulesQuery = this.get('activeContext').queryServer('schedule');

    return schedulesQuery.then((schedules) => {
      return dates.reduce((arr, date) => {
        let dateStart = moment.tz(date, TIME_ZONE).startOf('day');

        let schedule = schedules.find((schedule) => {
          let scheduleStart = moment.tz(schedule.get('startTime'), TIME_ZONE);
          return scheduleStart.startOf('day').valueOf() === dateStart.valueOf();
        });

        if (schedule) {
          arr.push(schedule);
        }

        return arr;
      }, []);
    });
  },

  confirmScheduleDeletion(schedules) {
    if (isEmpty(schedules)) return RSVP.resolve();
    let tooltip = this.get('tooltip');

    this.set('isRunning', false);

    return new RSVP.Promise((resolve, reject) => {
      this.get('tooltip').setProperties({
        tip: 'Regenerating the schedule may result in loss of data. Proceed?',
        primaryActionText: 'Confirm',
        secondaryActionText: 'Cancel',
        primaryAction: () => {
          tooltip.reset();
          this.set('isRunning', true);
          resolve();
        },
        secondaryAction: () => {
          tooltip.reset();
          reject();
        }
      })
    });
  }
});
