import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { set } from '@ember/object';
import { isBlank, isPresent, isEmpty, isNone } from '@ember/utils';
import lodash from 'lodash';
import Inflector from 'ember-inflector';
import { columnTypesHash } from 'adept-iq/config/column-types';
import { filterTypesHash } from 'adept-iq/config/filter-types';
import LayersConfig from 'adept-iq/config/map-layers';

import AgencyMarker from 'adept-iq/classes/markers/agency';
import StopMarker from 'adept-iq/classes/markers/stop';
import VehicleMarker from 'adept-iq/classes/markers/vehicle';
import RoutePolyline from 'adept-iq/classes/polylines/route';
import TripPolyline from 'adept-iq/classes/polylines/trip';

import {
  buildCompoundFilterNode,
  buildValueFilterNode,
  buildFilterFunction,
  testFilterValues,
} from 'adept-iq/utils/filters';

import computeOrderedIds from 'adept-iq/pods/components/generic-widgets/column-widget/utils/compute-ordered-ids';

const { flatten } = lodash;

const DEFAULT_LATLNG = [46.0479, -118.3537]; // DDS Canada

export default Service.extend({
  activeContext: service(),
  animationTimer: service(),
  store: service(),
  editModal: service(),
  workspace: service(),

  isSearchEnabled: false,
  searchText: '',

  config: LayersConfig,

  contextMenu: null,
  contextMenuOptions: null,
  contextMenuPosition: null,
  contextMenuRecord: null,

  init() {
    this._super(...arguments);

    this.set('contextMenuOptions', [{
      name: 'Edit',
      action: (model) => {
        let inflector = new Inflector(Inflector.defaultRules);
        let modelName = inflector.pluralize(model.constructor.modelName);
        let component = `iq-widgets/${modelName}-form-widget`;

        this.get('editModal').open(component, [model]);
      }
    }]);
  },

  isFiltered: computed('layers', function() {
    return !!this.get('layers').find(({ labels }) => {
      return !!labels.find((label) => {
        let { filterTypeId, filterValues } = label;
        let filterType = filterTypesHash[filterTypeId];
        return testFilterValues(filterType, filterValues);
      });
    });
  }),

  // merge dashboard's `mapState` on top of layer config defaults
  layers: computed('workspace.dashboard.mapState', function() {
    let config = lodash.cloneDeep(LayersConfig);
    let mapState = this.get('workspace.dashboard.mapState');
    if (!mapState) return config;

    Object.entries(mapState).forEach(([layerId, layerState]) => {
      let layer = config.findBy('id', layerId);
      if (!layer) return;

      ['isVisible', 'opacity'].forEach((property) => {
        if (isNone(layerState[property])) return;
        layer[property] = layerState[property];
      });

      if (layerState.types) {
        Object.entries(layerState.types).forEach(([typeId, typeState]) => {
          let type = layer.types.findBy('id', typeId);
          if (!type) return;

          ['isVisible'].forEach((property) => {
            if (isNone(typeState[property])) return;
            type[property] = typeState[property];
          });
        });
      }

      if (layerState.labels) {
        Object.entries(layerState.labels).forEach(([labelId, labelState]) => {
          let label = layer.labels.findBy('id', labelId);
          if (!label) return;

          ['isVisible', 'index', 'filterTypeId', 'filterValues'].forEach((property) => {
            if (isNone(labelState[property])) return;
            label[property] = labelState[property];
          });
        });
      }

      layer.labels = layer.labels || [];

      // ensure every label has index
      let orderedIds = computeOrderedIds(layer.labels);
      orderedIds.forEach((labelId, index) => {
        let label = layer.labels.findBy('id', labelId);
        label.index = index;
      });
    });

    return config;
  }),

  lat: computed('activeContext.implicitStops', function() {
    let lats = this.get('activeContext.implicitStops').mapBy('lat');
    let lat = this.computeCenter(lats);

    return isNaN(lat) ? DEFAULT_LATLNG[0] : lat;
  }),

  lng: computed('activeContext.implicitStops', function() {
    let lngs = this.get('activeContext.implicitStops').mapBy('lng');
    let lng = this.computeCenter(lngs);

    return isNaN(lng) ? DEFAULT_LATLNG[1] : lng;
  }),

  polylines: computed(
    'activeContext.implicitData',
    'layers',
    'searchText',
    function() {
      // each item is a store record, with `otp` if so endowed
      let items = this.get('activeContext.implicitData');
      let searchText = this.get('searchText');
      let layers = this.get('layers').filterBy('type', 'polylines');

      let layerPolylines = layers.map(layer => {
        if (layer.isVisible === false) return [];

        let layerItems = items.filterBy('modelName', layer.modelName);
        let layerRecords = layerItems.mapBy('record');

        let filterNode = buildCompoundFilterNode('and', [
          this.buildSearchFilterNode(layer, searchText),
          this.buildColumnFilterNode(layer)
        ]);

        let filter = buildFilterFunction(filterNode);

        let stopsIsVisible = this.get('layers').findBy('id', 'stops').isVisible;

        return layer.types.map(type => {
          if (type.isVisible === false) return [];

          let typeRecords = type.valueKey ?
            layerRecords.filterBy(type.valueKey, type.id) :
            layerRecords;

          let filteredRecords = typeRecords.filter(filter);

          return filteredRecords.reduce((arr, record) => {
            let polyline = this.polylineObjectFor(layer, type, record);
            let label = this.getLabel(record, layer);

            let otpStatus = '';
            // Include Routes when otpStatus is mocked or provided.
            if (layer.id === 'trips') {
              otpStatus = this._getOTP_ODLE_styleSuffix(record);
            }

            polyline.setProperties({
              style: `${type.style}${otpStatus}`,
              type: type.id,
              label: label,
              opacity: layer.opacity,
              otpStatus
            });

            if (layer.id === 'trips') {
              polyline.set('stopsIsVisible', stopsIsVisible);
            }

            arr.push(polyline);
            return arr;
          }, []);
        });
      });

      return flatten(flatten(layerPolylines));
    }
  ),

  markers: computed(
    'activeContext.implicitData',
    'layers',
    'searchText',
    function() {
      let items = this.get('activeContext.implicitData');
      let searchText = this.get('searchText');
      let layers = this.get('layers').filterBy('type', 'markers');

      let layerMarkers = layers.map(layer => {
        if (layer.isVisible === false) return [];

        let layerRecords;
        if (layer.isActive) {
          layerRecords = items.filterBy('modelName', layer.modelName)
            .mapBy('record');
        } else {
          // TODO: query `workspace-context` instead of store
          layerRecords = this.get('store').peekAll(layer.modelName);
        }

        let filterNode = buildCompoundFilterNode('and', [
          this.buildSearchFilterNode(layer, searchText),
          this.buildColumnFilterNode(layer)
        ]);

        let filter = buildFilterFunction(filterNode);

        return layer.types.map(type => {
          if (type.isVisible === false) return [];

          let typeRecords = layerRecords;

          if (type.valueKey) {
            typeRecords = layerRecords.filter(record => {
              return record.get(type.valueKey) === type.id;
            });
          }

          let filteredRecords = typeRecords.filter(filter);
          return filteredRecords.reduce((arr, record) => {
            let marker = this.markerObjectFor(layer, type, record);

            // TODO: move this to marker model
            let markerStyle = type.style;
            if (layer.modelName === 'iq-stop') {
              markerStyle += this._getOTPstyleSuffix(record, type.id);
            }

            marker.setProperties({
              style: markerStyle,
              type: type.id,
              label: this.getLabel(record, layer),
              opacity: layer.opacity,
              isActive: layer.isActive
            });

            arr.push(marker);
            return arr;
          }, []);
        });
      });
      return flatten(flatten(layerMarkers));
    }
  ),

  // TODO: this logic should be encapsulated in a data join
  markerObjectFor(layer, layerType, record) {
    if (!this._markerCache) {
      this._markerCache = {};
    }

    let id = `${layer.id}-${layerType.id}-${record.get('id')}`
    let marker = this._markerCache[id];

    if (!marker) {
      let markerClass = this.markerClassForLayer(layer);
      marker = markerClass.create({
        id,
        record,
        animationTimer: this.get('animationTimer')
      });
      this._markerCache[id] = marker;
    }

    return marker;
  },

  polylineObjectFor(layer, layerType, record) {
    if (!this._polylineCache) {
      this._polylineCache = {};
    }

    let id = `${layer.id}-${layerType.id}-${record.get('id')}`
    let polyline = this._polylineCache[id];

    if (!polyline) {
      let polylineClass = this.polylineClassForLayer(layer);
      polyline = polylineClass.create({
        id,
        record,
        animationTimer: this.get('animationTimer')
      });
      this._polylineCache[id] = polyline;
    }

    return polyline;
  },

  markerClassForLayer(layer) {
    switch (layer.id) {
      case 'agency':
        return AgencyMarker;
      case 'vehicles':
        return VehicleMarker;
      case 'stops':
        return StopMarker;
      default:
        throw `unhandled layer id '${layer.id}'`;
    }
  },

  polylineClassForLayer(layer) {
    switch (layer.id) {
      case 'routes':
        return RoutePolyline;
      case 'trips':
        return TripPolyline;
      default:
        throw `unhandled layer id '${layer.id}'`;
    }
  },

  buildSearchFilterNode(layer, searchText) {
    if (isBlank(searchText) || !layer.isVisible) return null;
    if (!layer.isVisible) return null;

    let filterNodes = layer.labels.reduce((arr, label) => {
      if (!label.isSearchable) return arr;

      // TODO: determine if we shuld be searching hidden labels
      // if (!label.isVisible) return arr;

      let filterType = columnTypesHash[label.type].searchFilterType;
      let filterNode =
        buildValueFilterNode(filterType, label.valuePath, searchText);

      arr.push(filterNode);
      return arr;
    }, []);

    return buildCompoundFilterNode('or', filterNodes);
  },

  buildColumnFilterNode(layer) {
    if (!layer.isVisible) return null;

    let filterNodes = layer.labels.reduce((arr, label) => {
      if (!label.isVisible || !label.isSearchable) return arr;

      let filterType = filterTypesHash[label.filterTypeId];
      let filterValues = label.filterValues;

      if (!testFilterValues(filterType, filterValues)) return arr;

      filterValues = label.filterValues.map(filterType.parseValue);

      let filterNode =
        buildValueFilterNode(filterType, label.valuePath, filterValues);

      arr.push(filterNode);
      return arr;
    }, []);

    return buildCompoundFilterNode('and', filterNodes);
  },

  computeCenter(values) {
    if (isEmpty(values)) return NaN;

    let [min, max] = values.reduce(([min, max], value) => {
      if (isPresent(value)) {
        min = Math.max(min, value);
        max = Math.min(max, value);
      }

      return [min, max];
    }, [-Infinity, Infinity]);

    // will return NaN if min and max are infinite
    return (min + max) / 2;
  },

  getLabel(record, layer) {
    let lines = [];
    let sortedLabels = layer.labels.sort((a,b) => a.index - b.index);

    sortedLabels.forEach(label => {
      if (!label.isVisible) return;
      lines.push(`${label.label} ${record.get(label.valuePath)}`);
    });

    return `${lines.join('<br/>')}`;
  },

  setLayerVisibility(layerId, isVisible) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        isVisible
      }
    });
  },

  setLayerOpacity(layerId, opacity) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        opacity
      }
    });
  },

  setLayerTypeVisibility(layerId, typeId, isVisible) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        types: {
          [typeId]: {
            isVisible
          }
        }
      }
    });
  },

  setLayerLabelVisibility(layerId, labelId, isVisible) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        labels: {
          [labelId]: {
            isVisible
          }
        }
      }
    });
  },

  setLayerLabelFilterType(layerId, labelId, filterTypeId) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        labels: {
          [labelId]: {
            filterTypeId
          }
        }
      }
    });
  },

  setLayerLabelFilterValues(layerId, labelId, filterValues) {
    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        labels: {
          [labelId]: {
            filterValues
          }
        }
      }
    });
  },

  setLayerLabelsOrder(layerId, labelIds) {
    let labels = labelIds.reduce((obj, labelId, index) => {
      obj[labelId] = { index };
      return obj;
    }, {});

    this.get('workspace.dashboard').mergeMapState({
      [layerId]: {
        labels
      }
    });
  },

  setContextMenuPosition(e) {
    this.set('contextMenuPosition', function() {
      return {
        style: {
          top: e.originalEvent.clientY,
          left: e.originalEvent.clientX
        }
      }
    });
  },

  onContextMenuOptionClick(option) {
    let record = this.get('contextMenuRecord');

    this.get('contextMenu').actions.close();

    if (option.action) {
      option.action(record);
    }
  },

  onLayerClick(/* modelName, id, e */) {},

  onLayerDblClick(modelName, id/*, e*/) {
    let record = this.get('store').peekRecord(modelName, id);
    this.get('activeContext').setCheckedRecords(modelName, [record]);
  },

  onLayerContextMenu(modelName, id, e) {
    let record = this.get('store').peekRecord(modelName, id);

    this.setContextMenuPosition(e);
    this.set('contextMenuRecord', record);
    this.get('contextMenu').actions.open()
  },

  _getOTPstyleSuffix(record, typeId) {
    let otp = record.get('otp'), suffix = '';
    if (typeId !== 'pick' && typeId !== 'drop') return suffix;
    if (otp === 'L') {
      suffix += 'Late';
    } else if (otp === 'D') {
      suffix += 'Danger';
    }
    return suffix;
  },

  _getOTP_ODLE_styleSuffix(record) {
    switch (record.get('otpStatus')) {
      case 'L':
        return 'Late';
      case 'D':
        return 'Danger';
      default:
        return '';
    }
  }
});
