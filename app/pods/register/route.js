import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { Promise } from 'rsvp';
import fetch from 'fetch';
import ENV from '../../config/environment';

export default Route.extend({
  session: service(),

  model({ invitationId }) {
    if (isBlank(invitationId)) return;

    return this.store.findRecord('invitation', invitationId).catch(e => {
      if (e.errors.firstObject.status !== '404') throw(e);
    });
  },

  setupController(controller, model) {
    controller.set('data', { invitation: model });
  },

  makeRequest(url, options) {
    return new Promise((resolve, reject) => {
      fetch(url, options).then(response => {
        response.json().then(body => {
          if (!response.ok) {
            reject(body);
          } else {
            resolve(body);
          }
        });
      }).catch(reject);
    });
  },

  actions: {
    register(userName, password) {
      let { host } = ENV.API.ssoService;

      let url = host + '/signup';
      let data = {
        userName,
        password,
        displayName: userName
      };

      let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      };

      this.makeRequest(url, options).then(tokenPayload => {
        let session = this.get('session');
        let authenticator = 'authenticator:sso';

        session.authenticate(authenticator, userName, password, tokenPayload).then(() => {
          this.transitionTo('index');
        });
      }).catch(e => {
        this.set('controller.data.errors', [e.message]);
      });
    }
  }
});
