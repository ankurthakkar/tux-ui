import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  queryParams: ['invitationId'],
  invitationId: undefined,

  data: undefined,

  invitation: computed.alias('data.invitation'),
  errors: computed.alias('data.errors')
});
