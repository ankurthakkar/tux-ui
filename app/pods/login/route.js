import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';

export default Route.extend({
  session: service(),
  errorMessage: service(),

  setupController(controller/*, model*/) {
    controller.set('data', {});
  },

  actions: {
    login(userName, password) {
      let session = this.get('session');
      let authenticator = 'authenticator:sso';

      session.authenticate(authenticator, userName, password).then(() => {
        this.get('errorMessage').flushMessageQueues();
        this.transitionTo('index');
      }).catch(e => {
        let message;

        switch (e.message) {
        case 'Unauthorized':
          message = 'Email or password is not correct.';
          break;
        case 'Disabled':
          message = 'Your account is disabled. Please contact your system administrator.';
          break;
        }

        if (isPresent(message)) {
          return this.set('controller.data.errors', [message]);
        }

        throw(e);
      });
    }
  }
});
