import Controller from '@ember/controller';

export default Controller.extend({
  classNames: ['create-dashboard-wrapper'],
  actions: {
    toggleModal() {
      this.transitionToRoute('index');
    }
  }
});
