import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  tooltip: service(),
  workspace: service(),

  model() {
    return this.store.createRecord('dashboard')
  },

  actions: {
    createDashboard(dashboard) {
      dashboard.save().then(() => {
        this.get('tooltip').reset();
        this.transitionTo('dashboard', dashboard).then(() => {
          let workspace = this.get('workspace');
          workspace.pushState('editWorkspace');
        });
      });
    }
  }
});
