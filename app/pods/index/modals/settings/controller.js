import Controller from '@ember/controller';
import { computed } from '@ember/object'
import { inject as service } from '@ember/service';

const TILE_SIZE_OPTIONS = [100, 150, 200];
const TILE_SIZE_LABELS = {
  '100': 'Small',
  '150': 'Medium',
  '200': 'Large'
};

const TILE_SPACING_OPTIONS = [2, 4, 6];
const TILE_SPACING_LABELS = {
  '2': 'Small',
  '4': 'Medium',
  '6': 'Large'
};

const COLOR_MODE_OPTIONS = ["light", "dark"];
const COLOR_MODE_LABELS = {
  "light": "Light mode",
  "dark": "Dark mode"
}


export default Controller.extend({
  workspace: service(),

  tileSizeOptions: TILE_SIZE_OPTIONS,
  tileSizeLabels: TILE_SIZE_LABELS,

  tileSpacingOptions: TILE_SPACING_OPTIONS,
  tileSpacingLabels: TILE_SPACING_LABELS,

  colorModeOptions: COLOR_MODE_OPTIONS,
  colorModeLabels: COLOR_MODE_LABELS,

  selectedTileSize: computed.alias('workspace.tileSize'),
  selectedTileSpacing: computed.alias('workspace.tileSpacing'),

  selectedColorMode: computed('workspace.isLightMode', function() {
    let isLight = this.get('workspace.isLightMode');
    if (isLight) {
      return 'light';
    } else {
      return 'dark';
    }
  }),

  actions: {
    toggleModal() {
      this.transitionToRoute('index');
    },

    didSelectColorMode(colorMode) {
      if (colorMode === "dark") {
        this.set('workspace.isLightMode', false);
      } else {
        this.set('workspace.isLightMode', true);
      }
    },
  }
});
