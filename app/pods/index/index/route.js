import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

const DEFAULT_DASHBOARD_ID = 'workspaces-default/default';

export default Route.extend({
  session: service(),

  beforeModel(transition) {
    // temporary fix until authenticated routes are implemented
    if (!this.get('session.isAuthenticated')) {
      transition.abort();
      return this.transitionTo('login');
    }
  },

  model() {
    return new RSVP.Promise((resolve, reject) => {
      this.fetchLastDashboard().then((dashboard) => {
        if (dashboard) {
          resolve(dashboard);
          return;
        }

        // try to get default dashboard instead
        this.store.findAll('dashboard').then((dashboards) => {
          let dashboard = dashboards.findBy('id', DEFAULT_DASHBOARD_ID);

          if (!dashboard) {
            // default is missing, use whatever we can find
            dashboard = dashboards.get('firstObject');
          }

          resolve(dashboard);
        });
      });
    });
  },

  fetchLastDashboard() {
    let lastDashboardId = localStorage.getItem('lastDashboardId');

    if (!lastDashboardId) {
      return RSVP.resolve(null);
    }

    return new RSVP.Promise((resolve) => {
      this.store.findRecord('dashboard', lastDashboardId)
        .then(resolve)
        .catch(() => {
          // don't try this again
          localStorage.setItem('lastDashboardId', null);
          resolve(null);
        });
    });
  },

  afterModel(dashboard) {
    if (!dashboard) {
      return this.transitionTo('index.modals.create-dashboard');
    }

    this.transitionTo('dashboard', dashboard);
  }
});
