import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: ['map-legend-filter'],
  mapContext: service(),

  layer: null,

  actions: {
    setVisibility(type, event) {
      this.get('mapContext').setLayerTypeVisibility(
        this.get('layer.id'),
        type.id,
        event.target.checked
      );
    }
  }
});
