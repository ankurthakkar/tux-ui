import Component from '@ember/component';
import { computed } from '@ember/object';
import { run } from '@ember/runloop';
import { inject as service } from '@ember/service';
import $ from 'jquery';

const RESIZE_CURSOR_OFFSET = -2;
const SIDE_DRAWER_SHRUNKEN_WIDTH = 20;
const AUTO_CLOSE_MODAL_WIDTH = 213;

export default Component.extend({
  workspace: service(),

  isShrunken: false,
  lastModalWidth: null,
  sideDrawerTitle: computed.alias('workspace.topStateDisplayName'),

  containerClassNames: computed('workspace.topState', function() {
    let topState = this.get('workspace.topState');
    switch(topState) {
      case 'editWorkspace':
        return ['ember-modal-dialog', 'side-drawer', 'add-widget-drawer'];
      case 'filterColumnWidget':
      case 'filterMapWidget':
        return ['ember-modal-dialog', 'side-drawer', 'filter-drawer'];
      default:
        return ['ember-modal-dialog', 'side-drawer'];
    }
  }),

  actions: {
    resizeModal(position) {
      if (this.get('isShrunken')) {
        return
      }

      let windowWidth = $(window).width();

      // Resize cursor offset ensures cursor is centered on handle.
      let modalWidth = windowWidth - position.x + RESIZE_CURSOR_OFFSET;

      run.scheduleOnce('afterRender', () => {
        if (windowWidth - modalWidth > AUTO_CLOSE_MODAL_WIDTH) {
          $('.side-drawer').css('right', `${modalWidth}px`);
        } else {
          this.send('onShrinkClick');
        }
      });
    },

    onCloseClick() {
      this.get('workspace').popState();
    },

    onShrinkClick() {
      let isShrunken = this.get('isShrunken');

      if (isShrunken) {
        // Let's restore the old modal width
        let modalWidth = this.get('lastModalWidth');
        $('.side-drawer').css('right', `${modalWidth}px`);
        $('.side-drawer').css('left', `0px`);
      } else {
        // Shrink the modal and save the old minWidth
        let currentModalWidth = parseInt($('.side-drawer').css('right'));
        this.set('lastModalWidth', currentModalWidth);

        let windowWidth = $(window).width();
        let rightPosition = windowWidth - SIDE_DRAWER_SHRUNKEN_WIDTH;
        // this has to be the former modal width, minus the side drawer side
        let leftPosition = 0 - parseInt(currentModalWidth) + SIDE_DRAWER_SHRUNKEN_WIDTH;
        $('.side-drawer').css('right', `${rightPosition}px`);
        $('.side-drawer').css('left', `${leftPosition}px`);
      }
      this.toggleProperty('isShrunken');
    }
  }
});
