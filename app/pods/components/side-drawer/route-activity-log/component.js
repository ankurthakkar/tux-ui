import Component from '@ember/component';
import Table from 'ember-light-table';
import moment from 'moment';
import lodash from 'lodash';
import { columns, routeEventTypes } from './config';
import { faker } from 'ember-cli-mirage';

export default Component.extend({
  classNames: ['route-activity-log'],

  columns,

  route: null,
  table: null,

  isLoading: false,

  init() {
    this._super(...arguments);

    let columns = this.get('columns');

    let routeEvents = [];
    for (let i = 0; i < 30; i++) {
      let { activity, detailsTemplate } = lodash.sample(routeEventTypes);

      routeEvents.push({
        activity,
        details: this.interpolateDetails(detailsTemplate),
        date: moment().subtract(30 * i, 'minutes'),
        user: `${faker.name.firstName()} ${faker.name.lastName()}`
      });
    }

    let table = new Table(columns, routeEvents, {
      enableSync: this.get('enableSync')
    });

    this.set('table', table);
  },

  interpolateDetails(details) {
    let matches;
    do {
      matches = /^(.*)<(\w+)>(.*)$/.exec(details);

      if (matches) {
        let value = this.fakeValueFor(matches[2]);
        details = `${matches[1]}${value}${matches[3]}`;
      }
    } while (matches)

    return details;
  },

  fakeValueFor(tag) {
    switch(tag) {
      case 'tripId':
        return `#${parseInt(Math.random()*1000)}`;
      case 'driverName':
        return `${faker.name.firstName()} ${faker.name.lastName()}`;
      case 'vehicleName':
        return `#${parseInt(Math.random()*100)}`;
      case 'odometerValue':
        return `${parseInt(Math.random()*100000)}km`;
      case 'lat':
      case 'lng':
        return Math.random()*100;
      case 'cannedMessage':
        return `"${faker.lorem.sentence()}"`;
      case 'date':
        return moment(faker.date.past()).format('YYYY-MM-DD HH:MM a');
      default:
        return '';
    }
  }
});
