import moment from 'moment'

const TIME_FORMAT = 'YYYY-MM-DD hh:mmA';

export const columns = [{
  label: 'Activity',
  valuePath: 'activity',
}, {
  label: 'Time',
  valuePath: 'date',
  format: (date) => moment(date).format(TIME_FORMAT)
}, {
  label: 'Details',
  valuePath: 'details',
}, {
  label: 'User',
  valuePath: 'user'
}];

export const routeEventTypes = [{
  activity: 'Create',
  detailsTemplate: 'Route created'
}, {
  activity: 'Add Trip',
  detailsTemplate: 'Trip <tripId> added'
}, {
  activity: 'Start',
  detailsTemplate: 'Route Started. Driver <driverName>. Vehicle <vehicleName>. Odometer <odometerValue> Lat <lat>. Long <lng>.'
}, {
  activity: 'No Show Trip',
  detailsTemplate: 'Trip <tripId> no-showed. No-show reason <reason>.'
}, {
  activity: 'Remove Vehicle',
  detailsTemplate: 'Vehicle <vehicleName> removed.'
}, {
  activity: 'Add Vehicle',
  detailsTemplate: 'Vehicle <vehicleName> added.'
}, {
  activity: 'Cancel Trip',
  detailsTemplate: 'Trip <tripId> cancelled. Cancel reason <reason>.'
}, {
  activity: 'Transfer Trip',
  detailsTemplate: 'Trip <tripId> transferred to route <routeName>.'
}, {
  activity: 'Waitlist Trip',
  detailsTemplate: 'Trip <tripId> placed on waitlist.'
}, {
  activity: 'Message Received',
  detailsTemplate: 'Message <cannedMessage> received from vehicle <vehicleName>.'
}, {
  activity: 'Message Sent',
  detailsTemplate: 'Message <cannedMessage> sent to vehicle <vehicleName>.'
}, {
  activity: 'End',
  detailsTemplate: 'Route ended. Driver <driverName>. Vehicle <vehicleName>. Odometer <odometerValue>. Lat <lat>. Long <lng>.'
}, {
  activity: 'Edit',
  detailsTemplate: 'Route edited. Route\'s start odometer changed from <odometerValue> to <odometerValue>.'
}, {
  activity: 'Close',
  detailsTemplate: 'Route\'s date of <date> was closed.'
}, {
  activity: 'Arrive Stop',
  detailsTemplate: 'Trip <tripId> pickup stop arrived.'
}, {
  activity: 'Depart Stop',
  detailsTemplate: 'Trip <tripId> pickup stop departed.'
}];
