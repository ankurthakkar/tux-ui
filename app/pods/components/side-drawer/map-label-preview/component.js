import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['map-label-preview'],
  labels: null,

  // This computed property is required, as Ember is not updating the list order
  // when the passed in `labels` property changes order.
  labelData: computed('labels', 'labels.@each.index', function() {
    return this.get('labels');
  })
});
