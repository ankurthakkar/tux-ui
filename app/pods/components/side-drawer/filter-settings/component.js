import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { filterTypes }  from 'adept-iq/config/filter-types';

export default Component.extend({
  workspace: service(),

  classNames: ['filter-settings'],

  filterTypes,

  columns: null,
  widget: null,

  dataColumns: computed('columns.[]', function() {
    let columns = this.get('columns');
    return columns.slice(1, columns.length - 1);
  }),

  actions: {
    onReorderItems(tableColumns) {
      let columns = tableColumns.reduce((obj, column, index) => {
        let id = column.get('id');
        obj[id] = { index };
        return obj;
      }, {});

      this.get('widget').mergeState({ columns });
    },

    onClickVisibilityCheckbox(column, event) {
      let id = column.get('id');
      let columns = {};
      columns[id] = { hidden: !event.target.checked };
      this.get('widget').mergeState({ columns });
    },

    onSelectFilterType(column, filterType) {
      let id = column.get('id');
      let columns = {};
      columns[id] = { filterTypeId: filterType.id }
      this.get('widget').mergeState({ columns })

      // TODO: focus on filter value input
    },

    onFilterValueChange(column, index, event) {
      let filterValues = column.get('filterValues') || [];
      filterValues = filterValues.slice();
      filterValues[index] = event.target.value;

      let id = column.get('id');
      let columns = {};
      columns[id] = { filterValues };

      this.get('widget').mergeState({ columns });
    },

    clearFilters() {
      let columns = this.get('columns').reduce((obj, column) => {
        obj[column.id] = {
          filterTypeId: null,
          filterValues: []
        };

        return obj;
      }, {});

      this.get('widget').mergeState({ columns });
    }
  }
});
