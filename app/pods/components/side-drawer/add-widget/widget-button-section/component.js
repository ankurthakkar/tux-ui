import Component from '@ember/component';

export default Component.extend({
  classNames: ['add-widget-section'],
  displayButtons: true,
  widgetSection: null,
  isOpen: true,

  actions: {
    onSectionButtonClick() {
      // expand / contract the button
      this.toggleProperty('displayButtons');
    },
    onWidgetButtonClick(widgetType) {
      this.get('addWidget')(widgetType);
    },
    onHeaderClick() {
      this.toggleProperty('isOpen');
    }
  }
});
