import Component from '@ember/component';
import { inject as service } from '@ember/service';
import widgetGroups from 'adept-iq/config/widget-groups';
import { computeWidgetPlacements } from 'adept-iq/utils/widget-placement';

export default Component.extend({
  workspace: service(),
  store: service(),
  classNames: ['add-widget'],

  widgetSections: widgetGroups,

  actions: {
    addWidget(widgetType) {
      // TODO: move this logic into dashboard model after grid settings have
      // have been migrated
      let dashboard = this.get('workspace.dashboard');
      let gridWidth = this.get('workspace.gridWidth');
      let { minWidth, minHeight, defaultWidth, defaultHeight } = widgetType;

      let locations =
        computeWidgetPlacements(dashboard, gridWidth, minWidth, minHeight);

      // might want to rank locations rather than taking the first available
      let { x, y, maxWidth, maxHeight } = locations[0];

      dashboard.addWidget({
        typeId: widgetType.id,
        width: Math.min(defaultWidth, maxWidth),
        height: Math.min(defaultHeight, maxHeight),
        x,
        y
      });
    }
  }
});
