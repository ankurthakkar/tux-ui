import Component from '@ember/component';
import config from './config';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import RSVP from 'rsvp';

export default Component.extend({
  workspace: service(),
  tooltip: service(),
  store: service(),
  errorMessage: service(),

  sections: config.sections,

  templates: null,
  selectedTemplate: null,

  draftMessages: computed.alias('workspace.topOptions.draftMessages'),

  didInsertElement() {
    this.fetchTemplates().then((templates) => {
      this.set('templates', templates);
      this.set('selectedTemplate', templates.get('firstObject'));
    });
  },

  fetchTemplates() {
    let store = this.get('store');
    let templates = store.peekAll('tm-canned-message-template');

    if (isEmpty(templates)) {
      return store.findAll('tm-canned-message-template');
    }

    return RSVP.resolve(templates);
  },

  sendMessage() {
    let draftMessages = this.get('draftMessages');

    draftMessages.forEach((message) => {
      message.set('body', this.get('selectedTemplate.body'));
    });

    return RSVP.all(draftMessages.map((message) => {
      return message.save();
    })).then(() => {
      this.get('workspace').popState();
    }).catch((err) => {
      this.get('errorMessage').pushError({
        detail: err.message
      });
    });
  },

  cancelMessage() {
    this.get('tooltip').reset();
  },

  actions: {
    onSendMessageButtonClick() {
      let tooltip = this.get('tooltip');

      tooltip.setProperties({
        tip: 'Send new message?',
        primaryActionText: 'Confirm',
        secondaryActionText: 'Cancel',
        primaryAction: () => {
          tooltip.reset();
          this.sendMessage();
        },
        secondaryAction: () => {
          toolbar.reset();
          this.cancelMessage();
        }
      });
    },

    onTemplateSelect(template) {
      this.set('selectedTemplate', template);
    },

    onCloseButtonClick() {
      this.get('workspace').popState();
    }
  }
});
