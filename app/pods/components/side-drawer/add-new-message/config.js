
export default {
  modelName: 'tm-canned-message-template',

  sections: [{
    title: 'Message content',
    fields: [{
      id: 'title',
      type: 'enum',
      label: 'Title',
      valuePath: 'messageNr',
      cellDesc: 'title',
      editable: true,
      hidden: false,
      extra: {
        allowMultiple: false
      }
    },
    {
      id: 'content',
      type: 'text',
      label: 'Content',
      valuePath: 'body',
      cellDesc: 'content',
      editable: false,
      hidden: false
    },
  ]
  }]
};
