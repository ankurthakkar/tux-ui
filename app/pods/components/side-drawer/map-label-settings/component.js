import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, get, set } from '@ember/object';
import { columnTypesHash } from 'adept-iq/config/column-types';
import { filterTypesHash } from 'adept-iq/config/filter-types';

export default Component.extend({
  mapContext: service(),

  classNames: ['filter-settings'],

  columnTypesHash,
  filterTypesHash,

  layer: null,

  sortedLabels: computed('layer.labels', function() {
    let labels = this.get('layer.labels');
    let sortedLabels = labels.sortBy('index');
    return sortedLabels;
  }),

  actions: {
    onReorderItems(labels) {
      let layer = this.get('layer');
      let labelIds = labels.mapBy('id');
      this.get('mapContext').setLayerLabelsOrder(layer.id, labelIds);
    },

    setVisibility(label, event) {
      let layer = this.get('layer');
      let layerId = get(layer, 'id');
      let labelId = get(label, 'id');

      this.get('mapContext')
        .setLayerLabelVisibility(layerId, labelId, event.target.checked);
    },

    onSelectFilterType(label, filterType) {
      let layer = this.get('layer');
      let layerId = get(layer, 'id');
      let labelId = get(label, 'id');
      let filterTypeId = get(filterType, 'id');

      this.get('mapContext')
        .setLayerLabelFilterType(layerId, labelId, filterTypeId);
    },

    onFilterValueChange(label, index, event) {
      let layer = this.get('layer');
      let layerId = get(layer, 'id');
      let labelId = get(label, 'id');
      let filterValues = get(label, 'filterValues') || [];

      // treat filter values as immutable
      filterValues = filterValues.slice();
      filterValues[index] = event.target.value;

      this.get('mapContext')
        .setLayerLabelFilterValues(layerId, labelId, filterValues);
    }
  }
});
