const COLUMNS = {
  routeColumns: {
    status: {
      id: 'status',
      config: {
        id: 'status',
        index: 5,
        type: 'text',
        label: 'Status',
        valuePath: 'status',
        editable: true,
        hidden: false,
        format: 'HH:mm A',
        defaultWidth: 75
      },
      filterTypeId: 'stringEq',
      filterValues: ['Scheduled']
    }
  },
  tripColumns: {
    status: {
      id: 'status',
      config: {
        id: 'status',
        index: 5,
        type: 'text',
        label: 'Status',
        valuePath: 'status',
        editable: true,
        hidden: false,
        defaultWidth: 75
      },
      filterTypeId: 'stringEq',
      filterValues: ['Scheduled']
    }
  }
}

const BOX_COLOR = {
  orange: 2,
  red: 4
};

export default [
  {
    name: 'Trips',
    modelName: 'iq-trip',
    columns: COLUMNS.tripColumns,
    displayOptions: {
      selected: {
        id: 'otp',
        name: 'OTP Status',
        label: 'OTP',
        valueKey: 'otp',
        valueCategory: {
          label: 'Late',
          value: 'L'
        },
        valueCategories: [{
          label: 'On Time',
          value: 'O'
        },{
          label: 'Late',
          value: 'L'
        },{
          label: 'In Danger',
          value: 'D'
        },{
          label: 'Early',
          value: 'E'
        }],
        metricOption: {
          id: 'count',
          name: 'Count'
        },
        metricOptions: [{
          id: 'count',
          name: 'Count'
        }]
      },
      options: [{
        id: 'otp',
        name: 'OTP Status',
        label: 'OTP',
        valueKey: 'otp',
        valueCategories: [{
          label: 'On Time',
          value: 'O'
        },{
          label: 'Late',
          value: 'L'
        },{
          label: 'In Danger',
          value: 'D'
        },{
          label: 'Early',
          value: 'E'
        }],
        metricOptions: [{
          id: 'count',
          name: 'Count'
        }]
      }]
    },
    title: '',
    boxColor: BOX_COLOR
  },
  {
    name: 'Routes',
    modelName: 'route',
    columns: COLUMNS.tripColumns,
    displayOptions: {
      selected: {
        id: 'otp',
        name: 'OTP Status',
        label: 'OTP',
        valueKey: 'otp',
        valueCategory: {
          label: 'Late',
          value: 'L'
        },
        valueCategories: [{
          label: 'On Time',
          value: 'O'
        },{
          label: 'Late',
          value: 'L'
        },{
          label: 'In Danger',
          value: 'D'
        },{
          label: 'Early',
          value: 'E'
        }],
        metricOption: {
          id: 'count',
          name: 'Count'
        },
        metricOptions: [{
          id: 'count',
          name: 'Count'
        }]
      },
      options: [{
        id: 'otp',
        name: 'OTP Status',
        label: 'OTP',
        valueKey: 'otp',
        valueCategories: [{
          label: 'On Time',
          value: 'O'
        },{
          label: 'Late',
          value: 'L'
        },{
          label: 'In Danger',
          value: 'D'
        },{
          label: 'Early',
          value: 'E'
        }],
        metricOptions: [{
          id: 'count',
          name: 'Count'
        }]
      }]
    },
    title: '',
    boxColor: BOX_COLOR
  }
]
