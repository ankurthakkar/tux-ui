import Component from '@ember/component';
import { set } from '@ember/object';
import { isPresent } from '@ember/utils';
import BOX_WIDGET_OPTIONS from './config';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';

export default Component.extend({
  widget: null,

  dataType: null,
  sourceOptions: null,

  init() {
    this._super(...arguments);
    this.set('sourceOptions', BOX_WIDGET_OPTIONS);
    let dataType = this.get('widget.state.dataType');
    this.set('dataType', dataType);
    if (isPresent(dataType)) {
      this.set('filterColumns',
                Object.values(dataType.columns).map((col) => new FilterColumn(col)));
    }
  },

  actions: {
    onTypeSelect(dataType) {
      this.set('dataType', dataType);
      this.set('filterColumns',
                Object.values(dataType.columns).map((col) => new FilterColumn(col)));
      this.get('widget').mergeState({ dataType });
    },

    setDisplayOptionVisibility(selected) {
      this.set('dataType.displayOptions.selected', selected);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    setDisplayOptionMetric(selected) {
      this.set('dataType.displayOptions.selected.metricOption', selected);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    setDisplayOptionValueCategory(selected) {
      this.set('dataType.displayOptions.selected.valueCategory', selected);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onFilterValueChange(column, index, event) {
      let filterValues = column.get('filterValues') || [];
      filterValues = filterValues.slice();
      filterValues[index] = event.target.value;
      // Update the displayed filter column.
      set(column, 'filterValues', filterValues);

      let id = column.get('id');
      // Update the column config.
      set(this.get('dataType'), `columns.${id}.filterValues`, filterValues);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onSelectFilterType(column, filterType) {
      // Update the displayed filter column.
      set(column, 'filterTypeId', filterType.id);

      let id = column.get('id');
      // Update the column config.
      set(this.get('dataType'), `columns.${id}.filterTypeId`, filterType.id);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onTitleInput(data) {
      this.set('dataType.title', data);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    setBoxMetriclabel(label) {
      this.set('dataType.boxMetricLabel', label);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onGoColorChange(color, data) {
      data = parseInt(data)
      if (data) {
        set(this.get('dataType'), `boxColor.${color}`, data);
        this.get('widget').mergeState({ dataType: this.get('dataType') });
      }
    }
  }
})
