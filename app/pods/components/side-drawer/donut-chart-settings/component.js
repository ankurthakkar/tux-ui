import Component from '@ember/component';
import { set } from '@ember/object';
import { isPresent } from '@ember/utils';
import DONUT_CHART_OPTIONS from './config';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';

export default Component.extend({
  widget: null,

  dataType: null,
  sourceOptions: null,

  init() {
    this._super(...arguments);
    this.set('sourceOptions', DONUT_CHART_OPTIONS);
    let dataType = this.get('widget.state.dataType');
    this.set('dataType', dataType);
    if (isPresent(dataType)) {
      this.set('filterColumns',
                Object.values(dataType.columns).map((col) => new FilterColumn(col)));
    }
  },

  actions: {
    onTypeSelect(dataType) {
      this.set('dataType', dataType);
      this.set('filterColumns',
                Object.values(dataType.columns).map((col) => new FilterColumn(col)));
      this.get('widget').mergeState({ dataType });
    },

    setVisualOptionVisibility(selected, event) {
      let dataType = this.get('dataType');
      set(selected, 'isChecked', event.target.checked);
      this.get('widget').mergeState({ dataType });
    },

    onTitleInput(data) {
      this.set('dataType.title', data);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    setDisplayOptionVisibility(displaySet, selected) {
      set(displaySet, 'selected', selected);
      // Don't want to merge `valueCategories`.
      set(this.get('widget'), 'state.dataType', this.get('dataType'))
      this.get('widget').notifyPropertyChange('state');
    },

    setSortOrder(selected) {
      this.set('dataType.sortOrder.selected', selected);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    setLegendPlacement(selected) {
      this.set('dataType.legendPlacement.selected', selected);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onFilterValueChange(column, index, event) {
      let filterValues = column.get('filterValues') || [];
      filterValues = filterValues.slice();
      filterValues[index] = event.target.value;
      // Update the displayed filter column.
      set(column, 'filterValues', filterValues);

      let id = column.get('id');
      // Update the column config.
      set(this.get('dataType'), `columns.${id}.filterValues`, filterValues);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },

    onSelectFilterType(column, filterType) {
      // Update the displayed filter column.
      set(column, 'filterTypeId', filterType.id);

      let id = column.get('id');
      // Update the column config.
      set(this.get('dataType'), `columns.${id}.filterTypeId`, filterType.id);
      this.get('widget').mergeState({ dataType: this.get('dataType') });
    },
  }
});
