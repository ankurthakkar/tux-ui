import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { isNone, isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['map-filter'],
  mapContext: service(),

  // map-context layers are ephemeral, so use `id` for selection state
  selectedLayerId: null,

  layers: computed.alias('mapContext.layers'),

  selectedLayer: computed('layers', 'selectedLayerId', function() {
    let layerId = this.get('selectedLayerId');
    if (isNone(layerId)) return null;

    let layers = this.get('layers');
    if (isEmpty(layers)) return null;

    return layers.findBy('id', layerId);
  }),

  isAgencyLayer: computed.equal('selectedLayerId', 'agency'),
  disabled: computed.not('selectedLayerId'),

  didInsertElement() {
    this._super(...arguments);

    let layers = this.get('layers');
    if (isEmpty(layers)) return;

    this.selectLayer(layers[0]);
  },

  selectLayer(layer) {
    this.set('selectedLayerId', layer.id);
  },

  actions: {
    onLayerSelect(layer) {
      this.selectLayer(layer);
    },

    onLayerVisibilityCheck(event) {
      let layer = this.get('selectedLayer');
      this.get('mapContext').setLayerVisibility(layer.id, event.target.checked);
    },

    onTransparencySliderChange(value) {
      let layer = this.get('selectedLayer');
      this.get('mapContext').setLayerOpacity(layer.id, value)
    }
  },
});
