import Component from '@ember/component';
import { computed, observer } from '@ember/object';
import { debounce, run } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { widgetTypesHash } from 'adept-iq/config/widget-types';

const CLIP_COLUMNS_IN_NORMAL_MODE = false;
const RESIZE_DEBOUNCE_TIME = 250;

export default Component.extend({
  workspace: service(),

  classNames: ['tile-dashboard'],

  dashboard: null,

  widgetTypesHash,

  isEditing: computed.alias('workspace.isEditing'),
  isDragging: computed.alias('workspace.isDragging'),
  isResizing: computed.alias('workspace.isResizing'),

  tileSize: computed.alias('workspace.tileSize'),
  tileSpacing: computed.alias('workspace.tileSpacing'),

  contentWidth: computed.alias('workspace.contentWidth'),
  contentHeight: computed.alias('workspace.contentHeight'),

  modeClass: computed('isEditing', function() {
    let mode = this.get('isEditing') ? 'edit' : 'normal';
    return `dashboard-mode-${mode}`;
  }),

  tileSizeClass: computed('tileSize', function() {
    return 'tile-size-' + this.get('tileSize');
  }),

  tileSpacingClass: computed('tileSpacing', function() {
    return 'tile-spacing-' + this.get('tileSpacing');
  }),

  colsClass: computed(
    'dashboard.widgets.@each.{x,width}',
    'isEditing',
    'gridWidth', function() {

    let cols = this.get('gridWidth');
    let widgets = this.get('dashboard.widgets') || [];

    if (!this.get('isEditing') && CLIP_COLUMNS_IN_NORMAL_MODE) {
      let maxWidth = widgets.reduce((max, widget) => {
        let x = widget.get('x');
        let width = widget.get('width');
        return Math.max(max, x + width);
      }, 0);

      cols = Math.min(cols, maxWidth);
    }

    return `cols-${cols}`;
  }),

  rowsClass: computed(
    'widgets.@each.{y,height}',
    'gridHeight', function() {

    let gridHeight = this.get('gridHeight');
    let widgets = this.get('dashboard.widgets') || [];

    let rows = widgets.reduce((max, widget) => {
      let y = widget.get('y');
      let height = widget.get('height');
      return Math.max(max, y + height);
    }, gridHeight);

    return `rows-${rows}`;
  }),

  gridWidth: computed.alias('workspace.gridWidth'),
  gridHeight: computed.alias('workspace.gridHeight'),

  didInsertElement() {
    this._onResize = () => {
      if (this.get('isDragging') || this.get('isResizing')) return;
      debounce(this, 'updateContentDimensions', RESIZE_DEBOUNCE_TIME);
    };

    this._resizeObserver = new ResizeObserver(this._onResize);
    this._resizeObserver.observe(this.$()[0]);

    this.updateContentDimensions();
    this.restoreStaticGridSetting();
  },

  willDestroyElement() {
    this._resizeObserver.disconnect();
    this._resizeObserver = null;
    this._onResize = null;
  },

  restoreWidgetLayout() {
    // unfortunately, we have to drop down to the GridStack level here;
    // the changes we make will trigger `onGridstackChange` actions

    let widgets = this.get('dashboard.widgets');
    if (isEmpty(widgets)) return;

    run.scheduleOnce('afterRender', () => {
      let grid = this.$('.grid-stack').data('gridstack');

      widgets.forEach((widget) => {
        let id = widget.get('id');
        let el = this.$(`.grid-stack-item[id="${id}"]`);

        let currentX = parseInt(el.attr('data-gs-x'));
        let currentY = parseInt(el.attr('data-gs-y'));
        let x = widget.get('x');
        let y = widget.get('y');

        if (currentX !== x || currentY !== y) {
          grid.move(el, x, y);
        }

        let currentWidth = parseInt(el.attr('data-gs-width'));
        let currentHeight = parseInt(el.attr('data-gs-height'));
        let width = widget.get('width');
        let height = widget.get('height');

        if (currentWidth !== width || currentHeight !== height) {
          grid.resize(el, width, height);
        }
      });
    });
  },

  restoreStaticGridSetting() {
    // gridstack resets to non-static when widgets are added or removed;
    // this happens even when dashboard model changes, so always set here
    this.$('.grid-stack')
      .data('gridstack')
      .setStatic(!this.get('isEditing'));
  },

  updateContentDimensions() {
    let content = this.$();
    if (!content) return;
    this.set('contentWidth', content.innerWidth());
    this.set('contentHeight', content.innerHeight());
  },

  onTileGeometryChange: observer('tileSize', 'tileSpacing', function () {
    run.scheduleOnce('afterRender', this, 'restoreWidgetLayout');
  }),

  onGridWidthChange: observer('gridWidth', function () {
    // in editing mode, we do not respond to window size
    if (this.get('isEditing')) return;

    let debounceInterval = this.get('windowResize.debounceInterval');
    debounce(this, 'restoreWidgetLayout', debounceInterval, false);
  }),

  onIsEditingChange: observer('isEditing', function() {
    if (this.get('isEditing') === false) {
      this.restoreWidgetLayout();
    }
  }),

  actions: {
    onGridStackChange(event, items) {
      this.restoreStaticGridSetting();

      if (!items || !this.get('isEditing')) return;

      let widgets = this.get('dashboard.widgets');

      items.forEach((gridItem) => {
        let { id, x, y, width, height } = gridItem;

        let widget = widgets.findBy('id', `${id}`);
        if (!widget) return;

        widget.setProperties({ x, y, width, height });
      });
    },

    onXButtonClick(widget) {
      this.get('dashboard').removeWidget(widget);
    }
  }
});
