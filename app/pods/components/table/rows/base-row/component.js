import BaseRow from 'ember-light-table/components/lt-row';

export default BaseRow.extend({
  classNames: ['table-base-row']
});
