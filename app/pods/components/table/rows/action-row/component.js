import BaseRow from '../base-row/component';
import { computed } from '@ember/object';

export default BaseRow.extend({
  classNames: ['table-action-row'],
  classNameBindings: ['status'],

  status: computed.alias('row.content.record.status'),
  modelName: computed.alias('row.content.constructor.modelName'),

  mouseDown() {
    let row = this.get('row');
     let rows = this.get('table.rows');
     rows.setEach('selected', false);
     row.set('selected', true);
  },

  didInsertElement() {
    this.$().on('contextmenu', (event) => {
      event.preventDefault();

      if (event.target.type !== 'checkbox') {
        let extra = this.get('extra');
        if (extra.onRightClick) {
          extra.onRightClick(this.get('row'), event);
        }
      }
    });
  }
});
