import ActionRow from '../action-row/component';
import { computed } from '@ember/object';

export default ActionRow.extend({
  classNameBindings: ['alertCategory'],

  alertCategory: computed('row.otp', function() {
    let otp = this.get('otp');
    if (otp === 'L') {
      return 'otp-danger';
    } else if (otp === 'D') {
      return 'otp-warning';
    }
    return '';
  }),

  // override with sub-class-specific CP
  otp: computed.alias('row.otp')
});
