import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['table-base-column'],

  sortIconProperty: computed('column.{sortable,sorted,ascending}', function () {
    let sorted = this.get('column.sorted');

    if (sorted) {
      let ascending = this.get('column.ascending');
      return ascending ? 'iconAscending' : 'iconDescending';
    }
  })
});
