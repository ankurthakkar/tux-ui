import BaseColumn from '../base-column/component';

export default BaseColumn.extend({
  classNames: ['table-filter-capable-column']
});
