import BaseColumn from '../base-column/component';
import { computed } from '@ember/object';

export default BaseColumn.extend({
  classNames: ['table-check-box-column'],

  isChecked: computed('table.rows.@each.isChecked', function() {
    let rows = this.get('table.rows');

    // it's weird to see checkbox when no records are shown
    if (!rows || rows.length === 0) return false;

    return rows.isEvery('isChecked');
  }),

  actions: {
    onToggleCheckbox(event) {
      let rows = this.get('table.rows');
      rows.setEach('isChecked', event.target.checked);

      let onRowCheckboxToggle = this.get('tableActions.onRowCheckboxToggle')
      onRowCheckboxToggle(null, event.target.checked, rows);
    }
  }
});
