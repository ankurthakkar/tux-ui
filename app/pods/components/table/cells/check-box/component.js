import BaseCell from '../base-cell/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default BaseCell.extend({
  classNames: ['table-check-box-cell'],

  activeContext: service(),

  isChecked: computed(
    'activeContext.checkedItems.[]',
    'row.refreshCounter',
    function() {

    let checkedItems = this.get('activeContext.checkedItems');

    let row = this.get('row');
    let record = row.get('record');

    if (!record) return false;

    let modelName = row.get('record').constructor.modelName;
    let modelId = row.get('record.id');

    return checkedItems.any(item => {
      return item.modelName === modelName && item.record.get('id') === modelId;
    });
  }),

  actions: {
    onToggleCheckbox(event) {
      let row = this.get('row');
      let rows = this.get('table.rows');
      let lastCheckedRow = rows.findBy('isLastChecked', true);
      let affectedRows = [];

      if (event.shiftKey && lastCheckedRow) {
        let lastCheckedIndex = rows.indexOf(lastCheckedRow);
        let checkedIndex = rows.indexOf(row);
        let start = Math.min(checkedIndex, lastCheckedIndex);
        let end = Math.max(checkedIndex, lastCheckedIndex);

        for (let i = start; i <= end; i++) {
          rows.objectAt(i).set('isChecked', event.target.checked);
          affectedRows.push(rows.objectAt(i));
        }
      } else {
        row.set('isChecked', event.target.checked);
        affectedRows.push(row);
      }

      rows.setEach('isLastChecked', false);
      row.set('isLastChecked', true);

      let onRowCheckboxToggle = this.get('tableActions.onRowCheckboxToggle');
      onRowCheckboxToggle(row, event.target.checked, affectedRows);
    }
  }
});
