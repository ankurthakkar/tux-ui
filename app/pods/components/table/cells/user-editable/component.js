import { inject as service } from '@ember/service';
import BaseCell from '../base-cell/component';
import { run } from '@ember/runloop';
import saveRecords from '../../../../../utils/save-records';
import TableRowValueMixin from '../../../../../mixins/table-row-value';
import moment from 'moment';

const KEYCODE_TAB = 9;
const KEYCODE_ENTER = 13;
const KEYCODE_ESCAPE = 27;

export default BaseCell.extend(TableRowValueMixin, {
  classNames: ['table-user-editable-cell'],
  errorMessage: service(),
  editModal: service(),
  isEditing: false,
  buffer: null,

  doubleClick() {
    this.startEditing();
  },

  startEditing() {
    if (this.get('isEditing')) return;

    this.set('isEditing', true);

    let row = this.get('row');
    let valuePath = this.get('column.valuePath');
    let value = row.get(valuePath);

    let type = this.get('column.columnType');
    let format = this.get('column.format');
    let buffer = this.formatInputValue(value, type, format);

    this.set('buffer', buffer);

    run.scheduleOnce('afterRender', () => {
      this.$('input').focus();
    });
  },

  formatInputValue(value, type, format) {
    switch (type.id) {
      case 'date':
      case 'datetime':
      case 'datetimeflatpickr':
      case 'time':
        return format(value);
      default:
        return value.toString();
    }
  },

  parseInputValue(value, type, parse, originalValue) {
    switch (type.id) {
      case 'date':
        return new Date(value);
      case 'datetime':
      case 'datetimeflatpickr':
      case 'time':
        return parse(value, originalValue);
      default:
        return value;
    }
  },

  validate(value) {
    let valid = true;
    let config = this.get('column.config');

    if (config.validation === undefined) {
      return true;
    }

    const minValue = config.validation.minValue;
    const maxValue = config.validation.maxValue;
    const type = config.validation.type;

    if (minValue !== undefined && maxValue !== undefined) {
      switch(type) {
        case 'text': {
          if (value.length < minValue || value.length > maxValue) {
            let message = '';

            if (minValue === maxValue) {
              message = `${config.label} must be ${minValue} characters in length.`
            }
            else {
              message = `${config.label} must be between ${minValue} and ${maxValue} characters in length.`
            }
            valid = false;
            this.get('errorMessage').pushWarning({ detail: message});
          }

          break;
        }
        case 'phoneNumber': {
          let numOnly = value.replace(/[^0-9]/g,'');

          if (numOnly.length < minValue || numOnly.length > maxValue) {
            let message = '';

            if (minValue === maxValue) {
              message = `${config.label} must have ${minValue} digits.`
            }
            else {
              message = `${config.label} must be between ${minValue} and ${maxValue} digits.`
            }
            valid = false;
            this.get('errorMessage').pushWarning({ detail: message});
          }

          break;
        }
        case 'date': {
          let valueMoment = moment(value);
          let minValueMoment = minValue === 'now' ? moment() : moment(minValue);
          let maxValueMoment = maxValue === 'now' ? moment() : moment(maxValue);
          let message = '';

          if (!valueMoment.isBetween(minValueMoment, maxValueMoment, null, '[]')) {
            valid = false;
            message = `${config.label} must be between ${minValueMoment.format('YYYY-MM-DD')} and ${maxValueMoment.format('YYYY-MM-DD')}.`
            this.get('errorMessage').pushWarning({ detail: message});
          }

          break;
        }

        default: {
          valid = false;
          break;
        }
      }
    }

    return valid;
  },

  saveChanges() {
    if (!this.get('isEditing')) return;

    this.set('isEditing', false);

    let buffer = this.get('buffer');
    let type = this.get('column.columnType');
    let parse = this.get('column.parse');

    let row = this.get('row');
    let valuePath = this.get('column.valuePath');
    let origialValue = row.get(valuePath);

    let newValue = this.parseInputValue(buffer, type, parse, origialValue);

    if (this.validate(newValue)) {
      row.set(valuePath, newValue);
      saveRecords([row.record]);
    }
  },

  discardChanges() {
    if (!this.get('isEditing')) return;
    this.set('isEditing', false);
  },

  actions: {
    onInput(text) {
      this.set('buffer', text);
    },

    onKeydown(keyCode) {
      if (keyCode === KEYCODE_ESCAPE) {
        this.discardChanges();
      }

      if (keyCode === KEYCODE_TAB || keyCode === KEYCODE_ENTER) {
        this.saveChanges();
      }
    },

    onBlur() {
      this.saveChanges();
    }
  }
});
