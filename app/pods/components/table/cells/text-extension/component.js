import BaseCell from 'ember-light-table/components/cells/base';
import { computed } from '@ember/object';
import TableRowValueMixin from '../../../../../mixins/table-row-value';

export default BaseCell.extend(TableRowValueMixin, {
  textContent: computed('value', function () {
    const value = this.get('value.values');

    if (!value) return '';

    const length = value.filter(val => val.className === 'valueItem').length;

    // show the actual value when there is only one item
    if (length > 1) {
      return `${length} ${this.get('column.cellDesc')}`;
    }
    else if (length === 1) {
      return value.firstObject.value;
    }
    else {
      return '';
    }
  }),

  actions: {
    showDropdown(dropdown, event) {
      if (this.value.values.length > 1) {
        dropdown.actions.open(event);
      }
    },

    hideDropdown(dropdown, event) {
      if (this.value.values.length > 1) {
        dropdown.actions.close(event);
      }
    },
  }
});
