import BaseCell from '../base-cell/component';
import { computed } from '@ember/object';
import { run } from '@ember/runloop';
import { copy } from '@ember/object/internals';
import $ from 'jquery';
import saveRecords from '../../../../../utils/save-records';

const KEYCODE_TAB = 9;
const KEYCODE_ENTER = 13;
const KEYCODE_ESCAPE = 27;

export default BaseCell.extend({
  classNames: ['table-user-editable-enum-cell'],

  isEditing: false,
  value: null,
  dropdownClass: null,
  dropdownAPI: null,

  options: computed.alias('column.options'),
  allowMultiple: computed.alias('column.allowMultiple'),

  didInsertElement() {
    this._super(...arguments);

    this.set('dropdownClass', `${this.elementId}-dropdown`);
  },

  doubleClick() {
    this.startEditing();
  },

  startEditing() {
    if (this.get('isEditing')) return;

    this.set('isEditing', true);

    let row = this.get('row');
    let valuePath = this.get('column.valuePath');
    let value = row.get(valuePath);

    this.set('value', copy(value));

    run.scheduleOnce('afterRender', () => {
      this.get('dropdownAPI').actions.open();

      // Close on click outside.
      $(document).on('click', (e) => {
        run.scheduleOnce('afterRender', () => {
          let componentElements = [
            this.element,
            $(this.get('dropdownClass')).get(0)
          ];

          let isClickInside = componentElements.any(element => {
            return element && element.contains(e.target)
          });

          if (isClickInside) return;

          this.saveChanges();

          $(document).off('click');
        })
      });
    });
  },

  saveChanges() {
    if (!this.get('isEditing')) return;

    this.set('isEditing', false);

    let row = this.get('row');
    let valuePath = this.get('column.valuePath');
    let value = this.get('value');

    row.set(valuePath, value);

    saveRecords([row.content.record]);
  },

  discardChanges() {
    if (!this.get('isEditing')) return;

    this.set('isEditing', false);

    let row = this.get('row');
    let valuePath = this.get('column.valuePath');
    let value = row.get(valuePath);

    this.set('value', copy(value));
  },

  actions: {
    onChange(value) {
      this.set('value', value);

      if (this.get('allowMultiple') === false) {
        this.saveChanges();
      }
    },

    onKeydown(api, event) {
      let { keyCode } = event;

      // Allow default enter to select and escape to close drop down when open.
      if (api.isOpen) {
        event.stopPropagation();
        return;
      }

      if (keyCode === KEYCODE_ESCAPE) {
        this.discardChanges();
      }

      if (keyCode === KEYCODE_TAB || keyCode === KEYCODE_ENTER) {
        this.saveChanges();
      }
    },

    onDidInsertDropdownElement(dropdownAPI) {
      this.set('dropdownAPI', dropdownAPI);
    }
  }
});
