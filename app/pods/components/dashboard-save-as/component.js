import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import { isEmpty, isPresent } from '@ember/utils';

const DEFAULT_NAME = 'Untitled';

export default Component.extend({
  workspace: service(),
  store: service(),
  router: service(),

  classNames: ['dashboard-save-as'],

  name: null,

  didInsertElement() {
    this.get('findUnusedNameTask').perform().then((name) => {
      this.set('name', name);
    });
  },

  findUnusedNameTask: task(function *() {
    let baseName = this.get('workspace.dashboard.name')
    let revisionNumber;

    if (isPresent(baseName)) {
      let regEx = /^(.*)\s+\((\d+)\)\s*$/;
      let matches = regEx.exec(baseName);

      if (matches) {
        baseName = matches[1];
        revisionNumber = parseInt(matches[2]);
      }
    }

    if (isEmpty(baseName)) {
      baseName = DEFAULT_NAME;
      revisionNumber = 1;
    }

    if (!revisionNumber) {
      revisionNumber = 1;
    }

    let dashboards = yield this.get('store').findAll('dashboard');

    let name = baseName;

    for (let i = revisionNumber; dashboards.findBy('name', name); i++) {
      name = `${baseName} (${i})`;
    }

    return name;
  }),

  saveDashboardTask: task(function *(dashboard) {
    yield dashboard.save();
  }),

  actions: {
    onXButtonClick() {
      this.get('workspace').popState('saveWorkspaceAs');
    },

    onSaveButtonClick() {
      let dashboard = this.get('workspace.dashboard');

      let options = dashboard.getOptionsForClone();
      options.name = this.get('name');

      let clone = this.get('store').createRecord('dashboard', options);

      this.get('saveDashboardTask').perform(clone).then(() => {
        this.get('workspace').popState('saveWorkspaceAs').then(() => {
          this.get('router').transitionTo('dashboard', clone);
        });
      });
    },

    onCancelButtonClick() {
      this.get('workspace').popState('saveWorkspaceAs');
    }
  }
});
