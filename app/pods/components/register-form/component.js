import Component from '@ember/component';
import { computed } from '@ember/object';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations(
  {
    password: {
      validators: [
        validator('presence', true),
        validator('confirmation', {
          on: 'password'
        })
      ]
    },
    confirmPassword: validator('confirmation', {
      on: 'password',
      message: 'Passwords do not match'
    }),
    acceptTerms: validator(value => value === true)
  }, {
    debounce: 500
  }
);

export default Component.extend(Validations, {
  invitaton: undefined,
  errors: undefined,

  password: undefined,
  confirmPassword: undefined,
  acceptTerms: false,

  isValid: computed.alias('validations.isValid'),

  actions: {
    onSubmit(email, password, e) {
      e.preventDefault();
      this.get('onSubmit')(email, password);
    }
  }
});
