import Component from '@ember/component';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default Component.extend({
  errors: undefined,

  email: undefined,
  password: undefined,

  isValid: computed('email', 'password', function() {
    let email = this.get('email');
    let password = this.get('password');

    return isPresent(email) && isPresent(password);
  }),

  actions: {
    onSubmit(email, password, e) {
      e.preventDefault();
      this.get('onSubmit')(email, password);
    }
  }
});
