import EmberCLIChart from 'ember-cli-chart/components/ember-chart';

export default EmberCLIChart.extend({
  updateChart() {
    let chart   = this.get('chart');
    let data    = this.get('data');
    let options = this.get('options');
    let animate = this.get('animate');


    if (chart) {
      chart.config.data = data;
      chart.options = options;
      if (animate) {
        chart.update();
      } else {
        chart.update(0);
      }
    }
  }
});
