import Component from '@ember/component';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default Component.extend({
  errors: undefined,

  email: undefined,

  isValid: computed('email', function() {
    let email = this.get('email');

    return isPresent(email);
  }),

  actions: {
    onSubmit(email, e) {
      e.preventDefault();
      this.get('onSubmit')(email);
    }
  }
});
