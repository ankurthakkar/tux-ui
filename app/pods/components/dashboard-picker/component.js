import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  store: service(),
  router: service(),
  workspace: service(),

  classNames: ['dashboard-picker'],

  dashboards: null,

  didInsertElement() {
    this._super(...arguments);
    this.get('loadDashboardsTask').perform();
  },

  loadDashboardsTask: task(function * () {
    let dashboards = yield this.get('store').findAll('dashboard');
    this.set('dashboards', dashboards);
  }),

  roles: computed('dashboards.[]', function() {
    let dashboards = this.get('dashboards');
    if (isEmpty(dashboards)) return [];

    return dashboards.mapBy('role').uniq().sort();
  }),

  sections: computed('dashboards.[]', 'roles.[]', function() {
    let dashboards = this.get('dashboards');
    let roles = this.get('roles');

    if (isEmpty(dashboards) || isEmpty(roles)) return [];

    return roles.map((role) => {
      return {
        title: role,
        dashboards: dashboards.filterBy('role', role).sortBy('name')
      };
    });
  }),

  actions: {
    onXButtonClick() {
      this.get('workspace').popState('openWorkspace');
    },

    onOpenButtonClick(dashboard) {
      this.get('workspace').popState('openWorkspace').then(() => {
        this.get('router').transitionTo('dashboard', dashboard);
      });
    },

    onReloadButtonClick(dashboard) {
      this.get('workspace').popState('openWorkspace');
      dashboard.reload();
    }
  }
});
