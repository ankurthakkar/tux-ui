import Component from '@ember/component';
import { alias } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed, observer } from '@ember/object';
import { run } from '@ember/runloop';
import $ from 'jquery';


export default Component.extend({
  editModal: service(),
  tooltip: service(),
  workspace: service(),
  session: service(),
  errorMessage: service(),
  scheduleGeneration: service(),

  classNames: ['row', 'no-gutters', 'header-wrapper'],
  classNameBindings: ['isLightMode:is-light:is-dark'],

  dashboards: null,
  createDashboard: null,
  transitionTo: null,

  isEditing: alias('workspace.isEditing'),
  isLightMode: alias('workspace.isLightMode'),
  isAuthenticated: alias('session.isAuthenticated'),

  abridgedDetail: computed('errorMessage.message', function() {
    let lastChar = this.get('errorMessage').message.detail.length - 1;
    return '... ' + this.get('errorMessage').message.detail.slice(lastChar - 80,lastChar);
  }),

  errorMessageInfoContentChange: observer('errorMessage.info', function() {
    run.later(function() {
      if ($(".text-info-multi").length > 0) {
        $(".text-info-multi").height( $(".text-info-multi")[0].scrollHeight );
      }
    }, 500);

  }),

  actions: {
    onNewWorkspaceClick(dropdown) {
      dropdown.actions.close();
      this.get('createDashboard')();
    },

    onEditWorkspaceClick(dropdown) {
      let workspace = this.get('workspace');

      dropdown.actions.close();
      if (this.get('isEditing')) return;

      let displayName = "Add Widgets";
      workspace.pushState('editWorkspace', {displayName}).then(() => {
        // Q: state pushed?
      }).catch(() => {
        // denied access
      });
    },

    onSaveWorkspaceClick(dropdown) {
      dropdown.actions.close();

      let dashboard = this.get('workspace.dashboard');
      return dashboard.save();
    },

    onSaveWorkspaceAsClick(dropdown) {
      dropdown.actions.close();
      this.get('workspace').pushState('saveWorkspaceAs');
    },

    onDeleteWorkspaceClick(dropdown) {
      dropdown.actions.close();
      this.get('workspace').pushState('deleteWorkspace');
    },

    onOpenWorkspaceClick(dropdown) {
      dropdown.actions.close();
      this.get('workspace').pushState('openWorkspace');
    },

    toggleTimelinePicker() {
      let workspace = this.get('workspace');
      workspace.toggleProperty('isEditingTimeline');
    },

    onGenerateScheduleClick(dropdown) {
      dropdown.actions.close();
      this.get('scheduleGeneration.task').perform();
    },

    transitionToModalAndClose(dropdown, modal) {
      dropdown.actions.close();
      this.transitionTo(`index.modals.${modal}`);
    },

    toggleLayoutType(dropdown) {
      dropdown.actions.close();
      this.toggleProperty('isLightMode');
    },

    onEditApplyCancel() {
      this.get('editModal').cancel();
    },

    onEditApplyCommit() {
      this.get('editModal').commit();
    },

    onErrorDismiss() {
      this.get('errorMessage').shiftError();
    },

    onWarningDismiss() {
      this.get('errorMessage').shiftWarning();
    },

    onInfoDismiss() {
      this.get('errorMessage').shiftInfo();
    },

    onCloseScheduleClick(dropdown) {
      dropdown.actions.close();
      this.get('workspace').pushState('closeSchedule');
    }
  }
});
