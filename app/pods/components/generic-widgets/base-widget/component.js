import Component from '@ember/component';
import { computed } from '@ember/object';
import { debounce } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default Component.extend({
  workspace: service(),

  classNames: ['dashboard-widget'],
  classNameBindings: ['disabled'],

  // override with per-widget static configuration
  config: null,

  // widget model (see: `app/classes/widget.js`)
  widget: null,

  width: computed.alias('widget.width'),
  height: computed.alias('widget.height'),
  state: computed.alias('widget.state'),

  isEditing: computed.alias('workspace.isEditing'),
  disabled: computed.readOnly('isEditing'),

  didInsertElement() {
    this._super(...arguments);

    this._resizeObserver = new ResizeObserver(() => {
      debounce(this, 'onWidgetResize', 250);
    });
    this._resizeObserver.observe(this.$()[0]);
  },

  willDestroyElement() {
    this._super(...arguments);

    if (this._resizeObserver) {
      this._resizeObserver.disconnect();
      this._resizeObserver = null;
    }
  },

  // override this to resize 3rd party integrations (e.g. map)
  onWidgetResize() {}
});
