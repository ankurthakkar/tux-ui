import Component from '@ember/component';
import { computed, set } from '@ember/object';
import { isPresent, isNone } from '@ember/utils';
import { isArray } from '@ember/array';
import { copy } from '@ember/object/internals';
import { assert } from '@ember/debug';

export default Component.extend({
  classNames: ['form-widget'],
  layoutName: 'components/generic-widgets/form-widget',

  editableRecords: null,

  editableSections: null,
  service: null,

  title: computed('editModal.editComponent', function() {
    let editComponent = this.get('editModal.editComponent');
    if (editComponent === "iq-widgets/new-trip-form-widget") {
      return "Add Trip";
    } else if(editComponent === "iq-widgets/assign-trip-2-route-widget") {
      return "Assign Trip to Route";
    }
  }),

  readOnlyFields: computed('editableRecords.[]', function () {
    let editableSections = this.get("editableSections");
    let readOnlyFields = editableSections.find(x => x.title === "Read Only");

    var readOnlyItems = {};

    if (!isNone(readOnlyFields)) {
      for(var i = 0; i < readOnlyFields.fields.length; i++) {
        let o = readOnlyFields.fields[i];
        readOnlyItems[o.id] = o
      }
    }

    return readOnlyItems
  }),

  init() {
    this._super(...arguments);

    assert('{{generic-widgets/form-widget}} requires extending components to provide an `editableSections` array', isArray(this.get('editableSections')));
    assert('{{generic-widgets/form-widget}} requires extending components to provide a `service` service', isPresent(this.get('service')));
    assert('{{generic-widgets/form-widget}} requires an `editableRecords` array', isArray(this.get('editableRecords')));
  },

  filteredSections: computed(
    'editableSections',
    'service.filter',
    function() {
      let sections = copy(this.get('editableSections'), true);
      let filter = this.get('service.filter');

      return sections.map(section => {
        set(section, 'fields', section.fields.filter(field => {
          return new RegExp(`^${filter}`, 'i').test(field.label);
        }));


        return section;
      }).filter(section => section.fields.length > 0);
    }
  ),

  actions: {
    onCellValueChange(record, valuePath, value, options ) {
      this.get('service').setRecordValue(record, valuePath, value, options);

      if(valuePath  === 'vehicleType' || valuePath === 'noShowReason' || valuePath === 'assignRoute') {
        record.set('isForceDirty', true);
        record.set(valuePath,value);
      }
    },

    onCellValueAppend(record, valuePath, modelName) {
      return this.get('service').pushRecordValue(record, valuePath, modelName);
    },

    onCellValueRemove(record, valuePath, modelName, options) {
      this.get('service').pullRecordValue(record, valuePath, options.record);
    },

    onCloseClick() {
      this.get('service').close();
    },

    onUndoClick() {
      this.get('service').undo();
    },

    onLinkToggle() {
      this.get('service').toggleLinkMode();
    },

    onSearchToggle() {
      this.get('service').toggleSearchMode();
    },

    onApplyClick() {
      this.get('service').apply();
    },

    onItemButtonClick(item){
      alert('Clicked button');
    }
  }
});
