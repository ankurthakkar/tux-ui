import Component from '@ember/component';

export default Component.extend({
  tagName: '',

  section: null,

  isOpen: true,

  actions: {
    onHeaderClick() {
      this.toggleProperty('isOpen');
    }
  }
});
