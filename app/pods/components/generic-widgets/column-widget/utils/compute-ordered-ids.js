import { isPresent } from '@ember/utils';

/**
 * Returns a sorted list of ids from a partially indexed collection.
 *
 * @param {Array} items
 * @param {string} idKey
 * @param {string} indexKey
 */
export default function(items, idKey='id', indexKey='index') {
  let orderedIds = [];

  items.forEach((item) => {
    let id = item[idKey];
    let index = item[indexKey];

    // no index specified; pass
    if (!isPresent(index)) return;

    if (!Number.isInteger(index)) {
      throw `item with id='${id}' has invalid index '${index}'`;
    }

    if (isPresent(orderedIds[index])) {
      throw `item with id='${id}' has duplicate index '${index}'`;
    }

    orderedIds[index] = id;
  });

  // interleave other items
  let i = 0;
  items.reject((item) => orderedIds.includes(item[idKey]))
    .forEach((item) => {
      while (isPresent(orderedIds[i])) i++;
      orderedIds[i] = item[idKey];
    });

  // remove any spaces that are left
  return orderedIds.compact();
}
