import Row from 'ember-light-table/classes/Row';
import { computed } from '@ember/object';

export default class CheckboxRow extends Row {}

CheckboxRow.reopen({
  isChecked: false,
  isLastChecked: false,
  isSelected: false,

  // FIXME: refactor this away
  // increment to force refresh on checkbox cell's `isChecked` property
  refreshCounter: 0,

  record: computed.alias('content'),
  id: computed.alias('content.id'),
});
