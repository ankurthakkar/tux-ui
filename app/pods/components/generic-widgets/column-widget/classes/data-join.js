import DataJoin from 'adept-iq/utils/data-join';
import CheckboxRow from './checkbox-row';

export default DataJoin.extend({
  table: null,

  initialize(record) {
    return new CheckboxRow(record);
  },

  join(/* collection */) {
    this._super(...arguments);

    // update light-table
    let rows = this.get('nodes');
    let tableRows = this.get('table.rows');
    tableRows.clear();
    tableRows.pushObjects(rows);
  },

  enter(row) {
    row.incrementProperty('refreshCounter');
  },

  exit(row) {
    // `isChecked` state is preserved on exit
    row.setProperties({
      isSelected: false,
      isLastChecked: false
    });
  },

  release(row) {
    row.destroy();
  }
});
