import Column from 'ember-light-table/classes/Column';
import columnTypes from 'adept-iq/config/column-types';
import { nullFilter, filterTypesHash } from 'adept-iq/config/filter-types';
import { testFilterValues, buildValueFilterNode } from 'adept-iq/utils/filters';
import { computed } from '@ember/object';

export default class FilterColumn extends Column {}

FilterColumn.reopen({
  id: null,
  config: null,

  filterTypeId: null,
  filterValues: null,

  valuePathMap: computed('valuePaths', function() {
    let valuePaths = this.get('valuePaths');
    if (!valuePaths) return {};

    let valuePathMap = valuePaths.reduce((obj, { modelName, valuePath }) => {
      obj[modelName] = valuePath;
      return obj;
    }, {});

    return valuePathMap;
  }),

  columnType: computed('config.type', function() {
    let columnTypeId = this.get('config.type');
    let columnType = columnTypes.findBy('id', columnTypeId);

    if (!columnType) {
      throw `no column type with id '${columnTypeId}'`;
    }

    return columnType;
  }),

  filterType: computed('filterTypeId', function() {
    let filterTypeId = this.get('filterTypeId');
    let filterType = filterTypesHash[filterTypeId];

    if (!filterType) {
      return nullFilter;
    }

    return filterType;
  }),

  filterTypes: computed('columnType', function() {
    let columnType = this.get('columnType');
    return columnType.filterTypes;
  }),

  isFiltered: computed('filterType', 'filterValues', function() {
    let filterType = this.get('filterType');
    let filterValues = this.get('filterValues');
    return testFilterValues(filterType, filterValues);
  }),

  filterNode: computed(
    'filterType',
    'filterValues',
    'config.valuePath',
    function() {

    let filterType = this.get('filterType');
    let filterValues = this.get('filterValues') || [];
    if (!testFilterValues(filterType, filterValues)) return null;

    filterValues = filterValues.map(filterType.parseValue);

    let valuePath = this.get('config.valuePath');
    return buildValueFilterNode(filterType, valuePath, filterValues);
  })
});
