import BaseWidget from '../base-widget/component';
import { computed, observer } from '@ember/object';
import { debounce, run } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { isEmpty, isPresent } from '@ember/utils';
import Table from 'ember-light-table';
import moment from 'moment';
import DataJoin from './classes/data-join';
import FilterColumn from './classes/filter-column';
import computeOrderedIds from './utils/compute-ordered-ids';
import ENV from 'adept-iq/config/environment'
import { task, timeout, didCancel } from 'ember-concurrency';

const DEFAULT_COLUMN_WIDTH = 100;
const MIN_COLUMN_WIDTH = 50;
const CHECKBOX_COLUMN_WIDTH = 30;
const REFRESH_DATA_DEBOUNCE_TIME = 250;

const toPixels = (x) => `${x}px`;
const fromPixels = (x) => parseInt(x.split('px')[0]);

export default BaseWidget.extend({
  workspace: service(),
  maximizer: service(),

  classNames: ['column-widget'],

  // overload this with a static config object
  config: null,

  limit: 40,
  canLoadMore: true,
  paginationEnabled: ENV.APP.paginationEnabled,

  tableActions: null,
  singleActions: null,
  bulkActions: null,

  records: null,
  searchText: '',
  isSearchEnabled: false,

  table: null,
  dataJoin: null,

  lastRowClicked: false,
  inlineRowMenu: undefined,
  inlineRowMenuPosition: undefined,

  sortId: computed.alias('widget.state.sortId'),
  sortAsc: computed.alias('widget.state.sortAsc'),

  tileSize: computed.alias('workspace.tileSize'),
  tileSpacing: computed.alias('workspace.tileSpacing'),

  isLoading: computed.alias('fetchDataTask.isRunning'),
  isError: computed.alias('fetchDataTask.last.isError'),

  showBulkActions: computed.gt('checkedRowsCount', 1),
  showSingleActions: computed.equal('showBulkActions', false),

  checkedRowsCount: computed('table.rows.@each.isChecked', function() {
    return this.get('table.rows').filterBy('isChecked').length;
  }),

  isFiltered: computed('table.columns.@each.{isFiltered}', function() {
    return this.get('table.columns').isAny('isFiltered');
  }),

  tableColumns: computed('config.columns', function() {
    let tableColumns = [{
      id: 'checkbox',
      valuePath: 'checked',
      cellComponent: 'table/cells/check-box',
      component: 'table/columns/check-box',
      sortable: false,
      width: toPixels(CHECKBOX_COLUMN_WIDTH)
    }];

    let columnConfigs = this.get('config.columns');
    let orderedColumnIds = computeOrderedIds(columnConfigs);

    orderedColumnIds.forEach((id) => {
      let config = columnConfigs.findBy('id', id);

      let tableColumn = {
        id: id,
        component: 'table/columns/filter-capable',
        label: config.label,
        cellDesc: config.cellDesc,
        valuePath: `record.${config.valuePath}`,
        valuePaths: config.valuePaths,
        hidden: config.hidden,
        resizable: true,
        minResizeWidth: MIN_COLUMN_WIDTH,
        width: toPixels(config.defaultWidth || DEFAULT_COLUMN_WIDTH),
        config
      };

      if (config.editable) {
        if (config.type === 'enum') {
          tableColumn.cellComponent = 'table/cells/user-editable-enum';
          tableColumn.options = config.options;
          tableColumn.allowMultiple = config.allowMultiple;
        } else {
          tableColumn.cellComponent = 'table/cells/user-editable';
        }
      } else {
        if (config.type === 'enum') {
          tableColumn.cellComponent = 'table/cells/text-extension';
        }
        else {
          tableColumn.cellComponent = 'table/cells/base-cell';
        }
      }

      if (config.highlightable) {
        tableColumn.cellClassNames = ['highlightable'];
      }

      if (['date', 'datetime', 'time', 'datetimeflatpickr'].includes(config.type) && config.format) {
        tableColumn.format = (value) => {
          return isEmpty(value) ? value : moment(value).format(config.format);
        };

        switch (config.type) {
          case 'date':
          case 'datetimeflatpickr':
          case 'datetime':
            tableColumn.parse = (value) => {
              return moment(value, config.format).toDate();
            };
            break;
          case 'time':
            tableColumn.parse = (timeString, originalValue) => {
              let date = moment(originalValue).format('YYYY-MM-DD');
              let newValue = moment(`${date} ${timeString}`, 'YYYY-MM-DD hh:mm A').toDate();
              return newValue;
            };
           break;
        }
      }

      tableColumns.push(tableColumn);
    });

    // fills the rest of the space with an empty column
    tableColumns.push({
      id: 'spacer',
      width: '100%',
      sortable: false,
      isResizable: false
    });

    return tableColumns;
  }),

  init() {
    this._super(...arguments);

    let table = new Table([], [], {
      enableSync: false
    });

    this.setProperties({
      table,
      dataJoin: DataJoin.create({ table }),
      records: []
    });
  },

  didInsertElement() {
    this._super(...arguments);

    if (!this.get('config')) {
      throw 'static config property not found';
    }

    this.onTableColumnsChange();
    this.onWidgetStateChange();
    this.onRecordsChange();

    run.scheduleOnce('afterRender', this, 'resizeBody');
    run.scheduleOnce('afterRender', this, 'refreshData');
  },

  willDestroyElement() {
    this._super(...arguments);
    this.get('dataJoin').clear();
  },

  setTableColumns() {
    let table = this.get('table');
    let tableColumns = this.get('tableColumns');
    table.set('columns', tableColumns.map((col) => new FilterColumn(col)));
  },

  updateTableColumns() {
    let state = this.get('widget.state') || {};
    let config = this.get('config') || {};

    let columns = this.get('table.columns');
    let shouldResize = false;

    let scrollLeft;
    let body = this.$('.column-widget-body');
    if (body) {
      scrollLeft = this.$('.column-widget-body').scrollLeft();
    }

    columns.forEach((column) => {
      let sortId = state.sortId || config.defaultSortId;

      if (isPresent(sortId) && sortId === column.get('id')) {
        let sortAsc;
        if (isPresent(state.sortAsc)) {
          sortAsc = state.sortAsc;
        } else if (isPresent(config.defaultSortAsc)) {
          sortAsc = config.defaultSortAsc;
        } else {
          sortAsc = true;
        }

        column.set('sorted', true);
        column.set('ascending', sortAsc);
      }
    });

    let columnsHash = state.columns || {};
    Object.entries(columnsHash).forEach(([id, columnState]) => {
      let column = columns.findBy('id', id);
      if (!column) return;

      if (isPresent(columnState.hidden)) {
        column.set('hidden', columnState.hidden);
        shouldResize = true;
      }

      if (isPresent(columnState.index)) {
        // +/- 1 accounts for checkbox column
        let oldIndex = columns.indexOf(column) - 1;

        if (columnState.index !== oldIndex) {
          let swapColumn = columns.objectAt(1 + columnState.index)
          columns.replace(1 + columnState.index, 1, [column]);
          columns.replace(1 + oldIndex, 1, [swapColumn])
          shouldResize = true;
        }
      }

      let wasFiltered = column.get('isFiltered');

      ['filterTypeId', 'filterValues'].forEach((prop) => {
        // allow null value to overwrite
        if (!columnState.hasOwnProperty(prop)) return;

        column.set(prop, columnState[prop]);
      });

      if (wasFiltered || column.get('isFiltered')) {
        run.scheduleOnce('afterRender', this, 'refreshData');
      }

      if (isPresent(columnState.width)) {
        column.set('width', toPixels(columnState.width));
      }
    });

    if (shouldResize) {
      // resize at end of _next_ loop so that all changes are applied
      run.next(() => {
        run.scheduleOnce('afterRender', this, 'resizeBody');

        if (isPresent(scrollLeft)) {
          // wait an additional loop before restoring scroll position
          run.next(() => {
            run.scheduleOnce('afterRender', this, 'setScrollLeft', scrollLeft);
          });
        }
      });
    }
  },

  resizeBody() {
    let tableWidth = this.$('.lt-head-wrap table').width();
    let widgetWidth = this.$('.column-widget-body').width();
    let width = Math.max(tableWidth, widgetWidth);
    this.$('.ember-light-table').width(width);
  },

  refreshData() {
    this.clearData();

    this.get('fetchDataTask')
      .perform()
      .catch((err) => {

        if (!didCancel(err)) {
          throw err;
        }

        // TODO: log error
      });
  },

  clearData() {
    // this.get('records').clear();
    this.set('canLoadMore', true);
  },

  fetchMoreData() {
    this.get('fetchDataTask')
      .perform()
      .catch(() => {
        // TODO: log error
      });
  },

  setScrollLeft(scrollLeft) {
    this.$('.column-widget-body').scrollLeft(scrollLeft);
  },

  fetchDataTask: task(function* () {
    if (this.get('canLoadMore') === false) return;

    let records = yield this.fetchDataQuery();

    this.set('records', records);

    if (!this.get('paginationEnabled') ||
      records.get('length') < this.get('limit')) {

      this.set('canLoadMore', false);
    }
  }).restartable(),

  onRecordsChange: observer('records', function() {
    this.get('dataJoin').join(this.get('records'));
    run.scheduleOnce('afterRender', this, 'resizeBody');
  }),

  onTableColumnsChange: observer('tableColumns', function() {
    run.scheduleOnce('afterRender', this, 'setTableColumns');
    run.scheduleOnce('afterRender', this, 'updateTableColumns');
  }),

  onWidthChange: observer('width', function() {
    run.scheduleOnce('afterRender', this, 'resizeBody');
  }),

  onWidgetStateChange: observer('widget.state', function() {
    run.scheduleOnce('afterRender', this, 'updateTableColumns');
  }),

  actions: {
    onSearchTextChange(searchText) {
      let previousSearchText = this.get('searchText');
      if (searchText === previousSearchText) return;

      this.set('searchText', searchText);
      debounce(this, 'refreshData', 250);
    },

    onColumnClick(column) {
      if (!column.sorted) return;

      this.get('widget').mergeState({
        sortId: column.get('id'),
        sortAsc: column.get('ascending')
      });

      run.scheduleOnce('afterRender', this, 'refreshData');
    },

    onColumnResize(column, pixels) {
      let scrollLeft = this.$('.column-widget-body').scrollLeft();
      let id = column.get('id');
      let columns = {};
      columns[id] = { width: fromPixels(pixels) };
      this.get('widget').mergeState({ columns });

      run.scheduleOnce('afterRender', this, 'resizeBody');
      run.next(() => {
        run.scheduleOnce('afterRender', this, 'setScrollLeft', scrollLeft);
      });
    },

    onRowCheckboxToggle() {},

    onSearchButtonClick() {
      this.send('onSearchTextChange','');
      this.toggleProperty('isSearchEnabled');

      run.scheduleOnce('afterRender', () => {
        if (this.get('isSearchEnabled')) {
          this.$('.column-widget-search-box').focus();
        }
      })
    },

    onRemoveSearchClick() {
      this.send('onSearchTextChange','');
      this.toggleProperty('isSearchEnabled');
    },

    onHeaderDoubleClick() {
      let maximizer = this.get('maximizer');
      if (maximizer.maximizedWidget === this.get('widget')) {
        maximizer.minimize();
        return;
      }
      maximizer.maximize(this.get('widget'));
    },

    onExitMaximizedClick() {
      let maximizer = this.get('maximizer');
      maximizer.minimize();
    },

    onFilterButtonClick() {
      let columns = this.get('table.columns');
      let workspace = this.get('workspace');
      let widget = this.get('widget');
      let config = this.get('config');

      let topState = workspace.get('topState');
      while (topState === 'filterColumnWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = `${config.title} Filters`;

      workspace.pushState('filterColumnWidget', {
        columns,
        widget,
        displayName
      });
    },

    setInlineRowMenu(dropdown) {
      this.set('inlineRowMenu', dropdown);
    },

    onRowClick(row, event) {
      this.set('inlineRowMenuPosition', function() {
        return {
          style: {
            top: event.clientY,
            left: event.clientX
          }
        }
      });

      this.set('lastRowClicked', row.get('record'));
      this.get('inlineRowMenu').actions.open();
    },

    onScrolledToBottom() {
      run.scheduleOnce('actions', this, 'fetchMoreData');
    },

    onTableActionClick(action, dropdown) {
      dropdown.actions.close();
      if (action.action) {
        action.action();
      }
    },

    onSingleRowAction(action) {
      let model = this.get('lastRowClicked');

      this.get('inlineRowMenu').actions.close();

      if (action.action) {
        action.action(model);
      }
    },

    onBulkRowAction(action) {
      let checkedRows = this.get('table.rows').filterBy('isChecked');
      let models = [];
      checkedRows.forEach((row) => {
        models.push(row.get('record'));
      });

      this.get('inlineRowMenu').actions.close();

      if (action.action) {
        action.action(models);
      }
    }
  }
});
