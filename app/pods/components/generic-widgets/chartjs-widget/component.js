import BaseWidget from '../base-widget/component';
import { computed } from '@ember/object';

export default BaseWidget.extend({
  classNames: ['analytics-widget'],
  
  colors: computed('', function() {
    return [
      '#D6E1EA',
      '#B8AFC0',
      '#D3DDE6',
      '#BADCF8',
      '#E3EBF0',
      '#CFD5DD',
      '#D6DEE3',
    ]
  }),

  warningColor: computed('', function() {
    return '#BADCF8';
  }),

  urgentColor: computed('', function() {
    return '#B8AFC0';
  }),
});
