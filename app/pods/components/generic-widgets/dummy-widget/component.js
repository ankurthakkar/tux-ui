import BaseWidget from '../base-widget/component';
import { computed } from '@ember/object';

export default BaseWidget.extend({
  classNames: ['dummy-widget'],
  classNameBindings: ['color'],

  color: computed.readOnly('widget.state.color')
});
