import BaseWidget from '../base-widget/component';
import { computed, observer } from '@ember/object';
import { run } from '@ember/runloop';
import tomtom from 'tomtom';
import ENV from 'adept-iq/config/environment';

export default BaseWidget.extend({
  classNames: ['map-widget'],
  layoutName: 'components/generic-widgets/map-widget',

  lat: 0,
  lng: 0,
  zoom: 12,

  map: null,

  // tomtom will only accept an element ID, so create a unique one
  mapElementId: computed('elementId', function () {
    let elementId = this.get('elementId');
    return `${elementId}-map`;
  }),

  didInsertElement() {
    this._super(...arguments);

    let mapElementId = this.get('mapElementId');
    let lat = this.get('lat');
    let lng = this.get('lng');
    let zoom = this.get('zoom');

    run.scheduleOnce('afterRender', () => {
      let map = tomtom.map(mapElementId, {
        zoom,
        center: [lat, lng],
        key: ENV.tomtom.key,
        source: ENV.tomtom.source,
        basePath: ENV.tomtom.basePath,
        styleUrlMapping: ENV.tomtom.styleUrlMapping
      });

      this.set('map', map);
      this.didInitializeMap(map);
    });
  },

  // overload in sub-class
  didInitializeMap(/* map */) {},

  willDestroyElement() {
    this._super(...arguments);
    this.get('map').remove();
  },

  onWidgetResize() {
    this._super(...arguments);

    let map = this.get('map');
    if (!map) return;

    run.scheduleOnce('afterRender', () => {
      map.invalidateSize();
    });
  },

  onLatLngOrZoomChange: observer('lat', 'lng', 'zoom', function() {
    run.scheduleOnce('actions', this, 'updateMapView')
  }),

  updateMapView() {
    let lat = this.get('lat');
    let lng = this.get('lng');
    let zoom = this.get('zoom');
    let map = this.get('map');
    if (!map) return;

    run.scheduleOnce('afterRender', () => {
      map.setView([lat, lng], zoom);
    });
  }
});
