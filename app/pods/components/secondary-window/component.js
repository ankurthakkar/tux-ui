import Component from '@ember/component';

export default Component.extend({
  classNames: ['secondary-window'],

  title: null,
  isLoading: false,

  onXButtonClick: null,

  actions: {
    onXButtonClick() {
      this.get('onXButtonClick')();
    }
  }
});
