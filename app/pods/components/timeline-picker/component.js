import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  workspace: service(),
  timeSpan: service(),

  classNames: ['timeline-picker'],

  zoomPercent: 50,

  minZoomPercent: 25,
  maxZoomPercent: 100,
  zoomPercentStep: 1,

  proposedStartDate: null,
  proposedEndDate: null,
  isRelative: null,

  fieldValidity: null,

  areFieldsValid: computed('fieldValidity.{startDate,endDate,calendar}', function() {
    // body
    let fv = this.get('fieldValidity');
    for (let state in fv) {
      if (fv[state] === false) {
        return false;
      }
    }
    return true;
  }),

  isAbsoluteMode: true,

  minusValue: computed('timeSpan.minusValue', function () {
    return this.get('timeSpan.minusValue');
  }),

  plusValue: computed('timeSpan.plusValue', function () {
    return this.get('timeSpan.plusValue');
  }),

  canChooseRelative: computed('proposedStartDate', 'proposedEndDate', function() {
    let start = this.get('proposedStartDate').getTime();
    let end = this.get('proposedEndDate').getTime();
    let now = Date.now();
    if ( now <= start || now > end) {
      return false;
    }
    return true;
  }),

  updateTimeSpanService(minusValue, plusValue) {
    this.get('timeSpan').setSpanMillis(minusValue, plusValue);
  },

  stopTimeSpanService() {
    this.get('timeSpan').stopInterval();
  },

  init() {
    this._super(...arguments);
    let startDate = this.get('workspace.startDate');
    let proposedStartDate = new Date
    proposedStartDate.setTime(startDate.getTime());
    this.set('proposedStartDate', proposedStartDate);

    let endDate = this.get('workspace.endDate');
    let proposedEndDate = new Date
    proposedEndDate.setTime(endDate.getTime());
    this.set('proposedEndDate', proposedEndDate);

    this.set('isRelative', this.get('timeSpan.isTimeSpanRunning'));

    this.set('fieldValidity', {});
  },

  actions: {
    onZoomPercentChange(zoomPercent) {
      this.set('zoomPercent', parseInt(zoomPercent));
    },

    checkValidity() {
      return this.get('areFieldsValid');
    },

    onCloseButtonClick() {
      this.send('close');
    },

    onApplyButtonClick() {
      if (this.get('areFieldsValid')) {
        let workspaceStartDate = this.get('workspace.startDate');
        let newStartDate = this.get('proposedStartDate');

        let workspaceEndDate = this.get('workspace.endDate');
        let newEndDate = this.get('proposedEndDate');

        if (this.get('isRelative') && this.get('canChooseRelative')) {
          let now = Date.now();
          this.updateTimeSpanService(now - newStartDate.getTime(), newEndDate.getTime() - now);
        } else {
          this.stopTimeSpanService();
          if (workspaceStartDate.getTime() !== newStartDate.getTime()) {
            this.set('workspace.startDate', newStartDate);
          }
          if (workspaceEndDate.getTime() !== newEndDate.getTime()) {
            this.set('workspace.endDate', newEndDate);
          }
        }
        this.send('close');
      }
    },

    onChangeAbsoluteCheckBox() {
      this.toggleProperty('isRelative');
    },

    close() {
      let workspace = this.get('workspace');
      workspace.toggleProperty('isEditingTimeline');
    }
  }
});
