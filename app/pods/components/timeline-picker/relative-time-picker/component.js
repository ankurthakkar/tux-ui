import Component from '@ember/component';

export default Component.extend({
  tagName: '',

  minusValue: null,
  plusValue: null,

  updateTimeline() {
    if (!(this.get('minusValue') === null || this.get('plusValue') === null)) {
      this.sendAction('onSpanChange', this.get('minusValue'), this.get('plusValue')); // eslint-disable-line ember/closure-actions
    }
  },

  actions: {
    onUpdateMinusValue(value) {
      this.set('minusValue', value);
      this.updateTimeline();
    },

    onUpdatePlusValue(value) {
      this.set('plusValue', value);
      this.updateTimeline();
    },
  }
});
