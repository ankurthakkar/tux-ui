import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: 'span',
  classNames: ['timeline-relative-time-picker'],
  classNameBindings: ['isValid', 'isInvalid', 'isDirty'],

  value: null,

  isValid: computed('value', function() {
    // The <input type="number"> will return '' when the input is invalid.
    // The <input type="number"> will allow floating point numbers: decimal point and engineering notation.
    return /^[0-9]+$/.test(this.get('value'));
  }),

  isInvalid: computed.not('isValid'),

  actions: {
    setValue(/* event */) {
      let val = this.$('input').val();
      this.set('value', val);
      this.set('isDirty', true);
    },

    update(event) {
      if (event.key === 'Enter' || event.type === 'blur') {
        this.set('isDirty', false);
        if (this.get('isValid')) {
          this.sendAction('onUpdateValue', this.get('value')); // eslint-disable-line ember/closure-actions
        } else {
          this.sendAction('onUpdateValue', null); // eslint-disable-line ember/closure-actions
        }
      }
    }
  }
});
