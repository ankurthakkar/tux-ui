import Component from '@ember/component';
import { computed } from '@ember/object';
import moment from 'moment';

export default Component.extend({
  classNames: ['time-picker-wrapper'],
  classNameBindings: ['isValid', 'isInvalid', 'isDirty'],

  timePickerOptions: computed(
    'startDate',
    'endDate',
    'editingDateProperty', function() {

    return {
      dropdown: true,
      interval: 30,
      scrollbar: true,
      defaultTime: this.get(this.get('editingDateProperty'))
    }
  }),

  isValid: computed('fieldValidityObject.{startDate,endDate}', function() {
    let fieldValidityObj = this.get('fieldValidityObject');
    let editingDateProperty = this.get('editingDateProperty');
    if (fieldValidityObj[editingDateProperty] === false) {
      return false;
    }
    return true;
  }),

  startDate: null,
  endDate: null,
  editingDateProperty: null,
  fieldValidityObject: null,

  timeString: null,

  actions: {
    onTimeChange(selectedTime) {
      let propertyName = this.get('editingDateProperty');
      let editingDate = this.get(propertyName);

      let selectedDate = new Date(editingDate.getTime());
      selectedDate.setHours(selectedTime.getHours());
      selectedDate.setMinutes(selectedTime.getMinutes());

      // do validation here!
      if (editingDate.getTime() === selectedDate.getTime()) {
        // nothing changed
        this.set(`fieldValidityObject.${propertyName}`, true);
      } else if (propertyName === "startDate") {
        let endDate = this.get('endDate');

        if (selectedDate > endDate) {
          this.set(`fieldValidityObject.${propertyName}`, false);
          this.set('startDate', selectedDate);
        } else {
          this.set(`fieldValidityObject.${propertyName}`, true);
          this.set('startDate', selectedDate);
        }
      } else {
        // endDate being set
        let startDate = this.get('startDate');

        if (selectedDate < startDate) {
          this.set(`fieldValidityObject.${propertyName}`, false);
          this.set('endDate', selectedDate);
        } else {
          this.set(`fieldValidityObject.${propertyName}`, true);
          this.set('endDate', selectedDate);
        }
      }

      this.get('isValid');
      this.get('fieldValidityCheck')();
    },

    onLastHourButtonClick(dropdown) {
      dropdown.actions.close();

      let end = moment().startOf('minute');
      let start = end.clone().subtract(1, 'hour');
      this.setDates(start.toDate(), end.toDate());
    },

    onNowButtonClick(dropdown) {
      dropdown.actions.close();

      let start = moment();
      let end = start.clone().add(1, 'hour');
      this.setDates(start.toDate(), end.toDate());
    },

    onNextTwoHoursButtonClick(dropdown) {
      dropdown.actions.close();

      let start = moment();
      let end = start.clone().add(2, 'hour');
      this.setDates(start.toDate(), end.toDate());
    },

    onDoneButtonClick(dropdown) {
      dropdown.actions.close();
    }
  }
});
