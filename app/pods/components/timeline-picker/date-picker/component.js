import Component from '@ember/component';
import moment from 'moment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEncodedDate, parseDate } from 'adept-iq/utils/encoding';
import { typeOf } from '@ember/utils';

const DATE_FORMAT = 'ddd YYYY-MM-DD';
const KEY_CODE_ENTER = 13;
const INVALID_DATE_STRING = 'Invalid';

export default Component.extend({
  workspace: service(),

  classNames: ['timeline-date-picker'],
  classNameBindings: ['isValid', 'isInvalid', 'isDirty'],

  startDate: null,
  endDate: null,

  fieldValidityObject: null,

  dateString: null,
  rangeStart: null,
  rangeEnd: null,

  isValid: computed.readOnly('isDateStringValid'),
  isInvalid: computed.not('isValid'),
  isDirty: computed.readOnly('isDateStringDirty'),

  isDateStringValid: computed('dateString', function() {
    let dateString = this.get('dateString');
    return this.validateDateString(dateString);
  }),

  isDateStringDirty: computed('dateString', 'startDate', 'endDate', function() {
    let dateString = this.get('dateString');
    if (!this.validateDateString(dateString)) return true;

    // take the provided dates, build a date string, then parse it
    let startDate = this.get('startDate');
    let endDate = this.get('endDate');
    let effectiveDateString = this.buildDateString(startDate, endDate);
    let [effectiveStartDate, effectiveEndDate] =
      this.parseDateString(effectiveDateString);

    // compare against the parsed values from the current time string
    let [encodedStartDate, encodedEndDate] = this.parseDateString(dateString);

    // string is dirty when parsed times don't match
    return effectiveStartDate.getTime() !== encodedStartDate.getTime() ||
      effectiveEndDate.getTime() !== encodedEndDate.getTime();
  }),

  isRangeStartDirty: computed('rangeStart', 'startDate', function() {
    let rangeStart = this.get('rangeStart');
    if (!rangeStart) return true;

    let startDate = this.get('startDate');
    let start = moment(startDate).startOf('day');

    return start.valueOf() !== rangeStart.valueOf();
  }),

  isRangeEndDirty: computed('rangeStart', 'rangeEnd', 'endDate', function() {
    let rangeStart = this.get('rangeStart');
    if (!rangeStart) return true;

    let rangeEnd = this.get('rangeEnd');
    let endDate = this.get('endDate');
    let end = moment(endDate).startOf('day');

    // it's ok for no end to be specified, so long as window is single date
    if (!rangeEnd) {
      return end.valueOf() !== rangeStart.valueOf();
    }

    return end.valueOf() !== rangeEnd.valueOf();
  }),

  center: computed('startDate', function() {
    let startDate = this.get('startDate');
    return moment(startDate).startOf('day');
  }),

  didReceiveAttrs() {
    this._super(...arguments);

    let startDate = this.get('startDate');
    let endDate = this.get('endDate');

    if (this.get('isDateStringDirty')) {
      let dateString = this.buildDateString(startDate, endDate);
      this.set('dateString', dateString);
    }

    // check this before rangeStart!
    if (this.get('isRangeEndDirty')) {
      let rangeEnd = moment(endDate).startOf('day');
      this.set('rangeEnd', rangeEnd);
    }

    if (this.get('isRangeStartDirty')) {
      let rangeStart = moment(startDate).startOf('day');
      this.set('rangeStart', rangeStart);
    }
  },

  buildDateString(startDate, endDate) {
    let start = moment(startDate);
    let end = moment(endDate);

    if (!start.isValid() || !end.isValid()) return INVALID_DATE_STRING;

    start = start.startOf('day');
    end = end.startOf('day');

    if (start.valueOf() === end.valueOf()) {
      return start.format(DATE_FORMAT);
    }

    return `${start.format(DATE_FORMAT)} to ${end.format(DATE_FORMAT)}`;
  },

  validateDateString(dateString) {
    if (typeOf(dateString) !== 'string') return false;

    if (dateString.includes(' to ')) {
      let arr = dateString.split(' to ');
      if (arr.length !== 2) return false;
      if (!arr.every(isEncodedDate)) return false;

      let [startDate, endDate] = arr.map(parseDate);
      return startDate.getTime() <= endDate.getTime();
    }

    return isEncodedDate(dateString);
  },

  parseDateString(dateString) {
    let start, end;

    if (dateString.includes(' to ')) {
      let [startString, endString] = dateString.split(' to ');
      start = moment(startString).startOf('day');
      end = moment(endString).endOf('day');
    } else {
      start = moment(dateString).startOf('day');
      end = start.clone().endOf('day');
    }

    return [start.toDate(), end.toDate()]
  },

  checkValidity(startDate, endDate) {
    if (startDate > endDate) {
      return false;
    } else {
      return true;
    }
  },

  setDates(startDate, endDate) {
    // first: get the hours of the existing dates and apply those
    let existingStart = this.get('startDate');
    startDate.setHours(existingStart.getHours());
    startDate.setMinutes(existingStart.getMinutes());
    let existingEnd = this.get('endDate');
    endDate.setHours(existingEnd.getHours());
    endDate.setMinutes(existingEnd.getMinutes());

    this.set('fieldValidityObject.calendar', this.checkValidity(startDate, endDate));

    this.set('startDate', startDate);
    this.set('endDate', endDate);
  },

  actions: {
    onInputTextChange(text) {
      this.set('dateString', text);
    },

    onKeydown(keyCode) {
      if (keyCode === KEY_CODE_ENTER && this.get('isDateStringValid')) {
        let dateString = this.get('dateString');
        let [startDate, endDate] = this.parseDateString(dateString);
        this.setDates(startDate, endDate);
      }
    },

    onYesterdayButtonClick(dropdown) {
      dropdown.actions.close();

      let start = moment().startOf('day').subtract(1, 'day');
      let end = start.clone().endOf('day');
      this.setDates(start.toDate(), end.toDate());
    },

    onTodayButtonClick(dropdown) {
      dropdown.actions.close();

      let start = moment().startOf('day');
      let end = start.clone().endOf('day');
      this.setDates(start.toDate(), end.toDate());
    },

    onTomorrowButtonClick(dropdown) {
      dropdown.actions.close();

      let start = moment().startOf('day').add(1, 'day');
      let end = start.clone().endOf('day');
      this.setDates(start.toDate(), end.toDate());
    },

    onRangeSelect(range) {
      this.set('rangeStart', range.start);
      this.set('rangeEnd', range.end);

      let end;
      if (range.end) {
        end = range.end.clone().endOf('day');
      } else {
        end = range.start.clone().endOf('day');
      }

      this.setDates(range.start.toDate(), end.toDate());
    },

    onDoneButtonClick(dropdown) {
      dropdown.actions.close();
    }
  }
});
