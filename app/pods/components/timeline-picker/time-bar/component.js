import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, observer } from '@ember/object';
import { run } from '@ember/runloop';

export default Component.extend({
  workspace: service(),

  classNames: ['bar'],
  /* one 24-hour day */
  numIncrements: 24,
  startDateAbbrev: computed('startDate', function() {
    return this.get('startDate').toDateString();
  }),
  didInsertElement() {
    this._super(...arguments);
    let parentEl = this.$();
    // To keep the zoomPercent related to the original size.
    this.set('baseLineWidth', parentEl.width());
    parentEl.css('width', `${parentEl.width() * this.get('zoomPercent') / 100}px`);
    this.set('parentWidth', parentEl.width());
    this.resetAllElements();
  },
  setDates(startDate, endDate) {
    run.scheduleOnce('afterRender', this, 'onDatesChange', startDate, endDate);
  },
  resetAllElements() {
    this.insertTimeScale();
    this.insertTimeIncrements();
    this.setLeftHandle();
    this.setRightHandle();
    this.setNowMarker();
    this.insertOperatingBox();
  },
  insertTimeScale() {
    let rowEl = this.$('#time-scale-row');
    // Clear out the old cells before we add new ones.
    rowEl.empty();
    let numIncrements = this.get('numIncrements');
    let cellWidth = Math.floor(this.get('parentWidth') / numIncrements);
    let startEndCell = `<td style="width:${cellWidth / 2}px"></td>`;
    rowEl.append(startEndCell);
    for (let i = 1; i < numIncrements; i++) {
      let cell = `<td class="time-scale-cell" style="width:${cellWidth}px">${i}:00</td>`;
      rowEl.append(cell);
    }
    rowEl.append(startEndCell);
  },
  insertTimeIncrements() {
    let rowEl = this.$('#increment-row');
    rowEl.empty();
    let numIncrements = this.get('numIncrements');
    let cellWidth = Math.floor(this.get('parentWidth') / numIncrements);
    let cell = `<td class="time-bar-increment" style="width:${cellWidth}px"></td>`;
    for (let i = 0; i < numIncrements; i++) {
      rowEl.append(cell);
    }
  },
  setLeftHandle() {
    let leftHandle = this.$('.time-bar-handle-left');
    let startDate = this.get('startDate');
    let decimalTime = startDate.getHours() + (startDate.getMinutes() / 60);
    let leftPosition = Math.floor((this.get('parentWidth') / 24) * decimalTime);
    leftHandle.css('left', `${leftPosition}px`);
  },
  setRightHandle() {
    let rightHandle = this.$('.time-bar-handle-right');
    let offset = rightHandle.width();
    let endDate = this.get('endDate');
    let decimalTime = endDate.getHours() + (endDate.getMinutes() / 60);
    let rightPosition = Math.floor((this.get('parentWidth') / 24) * decimalTime) - offset;
    rightHandle.css('left', `${rightPosition}px`);
  },
  setNowMarker() {
    let newMarker = this.$('.time-bar-now-marker');
    // Arbitrarily picking 12:00PM,
    let leftPosition = Math.floor((this.get('parentWidth') / 24) * 12);
    newMarker.css('left', `${leftPosition}px`);
  },
  insertOperatingBox() {
    let operatingBox = this.$('.time-bar-operating-box');
    // The agency begins operations at 9:00am.
    let leftPosition = Math.floor((this.get('parentWidth') / 24) * 9);
    // THe agency operates 8 hours / day.
    let boxWidth = Math.floor((this.get('parentWidth') / 24) * 8);
    operatingBox.css('left', `${leftPosition}px`);
    operatingBox.css('width', `${boxWidth}px`);
  },
  startDateObserver: observer('startDate', function() {
    this.setLeftHandle();
  }),
  endDateObserver: observer('endDate', function() {
    this.setRightHandle();
  }),
  zoomObserver: observer('zoomPercent', function() {
    let parentEl = this.$();
    parentEl.css('width', `${this.get('baseLineWidth') * this.get('zoomPercent') / 100}px`);
    this.set('parentWidth', parentEl.width());
    this.resetAllElements();
  }),
  dragEnd(event) {
    let target = this.$(`#${event.originalEvent.target.id}`);
    let startingPosition = parseInt(target.css('left').replace(/px/,''));
    let newPosition = startingPosition + event.originalEvent.offsetX;
    switch (event.originalEvent.target.id) {
      case 'left-handle': {
        // Gives a fractional hour.
        let startDate = new Date(this.get('startDate').getTime());
        let newTime = this.convertTime(newPosition / (this.get('parentWidth') / 24));
        startDate.setHours(newTime.hours, newTime.minutes,0);
        this.setDates(startDate, this.get('endDate'));
        break;
      }
      case 'right-handle': {
        let endDate = new Date(this.get('endDate').getTime());
        let newTime = this.convertTime(newPosition / (this.get('parentWidth') / 24));
        endDate.setHours(newTime.hours, newTime.minutes,0);
        this.setDates(this.get('startDate'), endDate);
        break;
      }
      case 'now-marker': {
        target.css('left', `${newPosition}px`);
        break;
      }
    }
  },
  convertTime(decimal) {
    let hours = Math.floor(decimal / 24);
    let minutes = Math.floor(decimal * 60);
    return { hours:hours, minutes: minutes };
  }
});
