import Component from '@ember/component';

const NUMBER_FORMAT = /^\d*\.*\d*$/;

export default Component.extend({
  classNames: ['form-components-number'],

  value: null,
  disabled: false,

  actions: {
    onInput(value) {
      // Reset input value to prior value unless changed value matches format.
      if (!NUMBER_FORMAT.test(value)) {
        return this.notifyPropertyChange('value');
      }

      this.get('onChange')(value);
    }
  }
});
