import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';
import ENV from '../../../../config/environment';
import { isNone } from '@ember/utils';

const DAILY_OPTIONS = {
  everyDay: 'Every Day',
  weekDay: 'Every WeekDay'
};

const FREQUENCY_OPTIONS = {
  daily: 'daily',
  weekly: 'weekly',
  monthly: 'monthly'
};

const END_OPTIONS = {
  noEnd: {type: 'noEnd', label: 'No end date'},
  endAfter: {type: 'endAfter', label: 'End after'},
  endBy: {type: 'endBy', label: 'End by'}
};

const WOM_OPTIONS = {
  first: 'first',
  second: 'second',
  third: 'third',
  fourth: 'fourth',
  last: 'last'
};

const MONTHLY_TYPE_OPTIONS = {
  dayOfMonth: 'dayOfMonth',
  weekOfMonth: 'weekOfMonth'
};

const DAY_OF_WEEK_OPTIONS = {
  sunday: 'sunday',
  monday: 'monday',
  tuesday: 'tuesday',
  wednesday: 'wednesday',
  thursday: 'thursday',
  friday: 'friday',
  saturday: 'saturday'
};

export default Component.extend({
  classNames: ['form-components-subscriptions'],
  editModal: service(),
  value: null,
  disabled: false,
  dayOfWeeks: null,
  recurring: false,

  subscriptions: computed.readOnly('value'),
  recurringPath: computed.readOnly('extra.recurringPath'),
  dailyConfigPath: computed.readOnly('extra.dailyConfigPath'),
  availabilityType: computed.readOnly('valuePath'),
  startTimePath: computed.readOnly('extra.startTimePath'),
  endTimePath: computed.readOnly('extra.endTimePath'),
  shiftStartPath: computed.readOnly('extra.shiftStartPath'),
  shiftEndPath: computed.readOnly('extra.shiftEndPath'),
  sundayPath: computed.readOnly('extra.sundayPath'),

  mondayPath: computed.readOnly('extra.mondayPath'),
  tuesdayPath: computed.readOnly('extra.tuesdayPath'),
  wednesdayPath: computed.readOnly('extra.wednesdayPath'),
  thursdayPath: computed.readOnly('extra.thursdayPath'),
  fridayPath: computed.readOnly('extra.fridayPath'),
  saturdayPath: computed.readOnly('extra.saturdayPath'),

  startDatePath: computed.readOnly('extra.startDateTimePath'),
  endDatePath: computed.readOnly('extra.endDateTimePath'),
  exStartDatePath: computed.readOnly('extra.excludeStartDateTimePath'),
  exEndDatePath: computed.readOnly('extra.excludeEndDateTimePath'),
  selectedWOMPath: computed.readOnly('extra.selectedWOMPath'),
  maxOccurencesPath: computed.readOnly('extra.maxOccurencesPath'),
  separationCountPath: computed.readOnly('extra.separationCountPath'),
  dayOfMonthPath: computed.readOnly('extra.dayOfMonthPath'),

  init() {

    this._super(...arguments);

    let recurring = this.get(`record.${this.get('valuePath')}.firstObject.${this.get('recurringPath')}`);
    this.set('dayOfWeeks', [DAY_OF_WEEK_OPTIONS.sunday,DAY_OF_WEEK_OPTIONS.monday,
      DAY_OF_WEEK_OPTIONS.tuesday,DAY_OF_WEEK_OPTIONS.wednesday,DAY_OF_WEEK_OPTIONS.thursday,
      DAY_OF_WEEK_OPTIONS.friday,DAY_OF_WEEK_OPTIONS.saturday]);
    this.set('dateTimePlaceHolder', ENV.dateTimeFlatPickr);

    if (recurring) {
      this.set('recurring', true);
    }
    else {
      this.set('recurring', false);
    }


    this.set('frequencyOptions',[FREQUENCY_OPTIONS.daily, FREQUENCY_OPTIONS.weekly, FREQUENCY_OPTIONS.monthly]);

    this.set(this.get('dailyConfigPath'), FREQUENCY_OPTIONS.daily);
    this.set('dailyDaysOptions',[DAILY_OPTIONS.everyDay, DAILY_OPTIONS.weekDay]);
    this.subscriptions.forEach(subscription => {
      // setup daily days
      if (subscription.isEveryDay) {
        subscription.set('dailyDays', DAILY_OPTIONS.everyDay);
      }
      else {
        subscription.set('dailyDays', DAILY_OPTIONS.weekDay);
      }

      // setup exclude range dates
      subscription.set('excludeRangeDate',
        [subscription.get(this.get('exStartDatePath')), subscription.get(this.get('exEndDatePath'))]);
      // setup end options
      subscription.set('endOptions', [END_OPTIONS.noEnd, END_OPTIONS.endAfter, END_OPTIONS.endBy]);
      // setup what is the end option selected
      if (!isNone(subscription.get(this.get('endDatePath')))) {
        subscription.set('endSelected', END_OPTIONS.endBy);
      }
      else if (!isNone(subscription.get(this.get('maxOccurencesPath')))) {
        subscription.set('endSelected', END_OPTIONS.endAfter);
      }
      else {
        subscription.set('endSelected', END_OPTIONS.noEnd);
      }
      // setup monthlyType
      if (!isNone(subscription.get(this.get('dayOfMonthPath')))) {
        subscription.set('monthlyType', MONTHLY_TYPE_OPTIONS.dayOfMonth);
      }
      else {
        subscription.set('monthlyType', MONTHLY_TYPE_OPTIONS.weekOfMonth);
      }
    });

    this.set('weekOfMonthOptions', [WOM_OPTIONS.first, WOM_OPTIONS.second, WOM_OPTIONS.third, WOM_OPTIONS.fourth, WOM_OPTIONS.last]);
    this.set('datePlaceHolder', ENV.dateTimeFormat.dateMoment);
    this.set('timePlaceHolder', ENV.dateTimeFormat.timeMoment);
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);
  },

  frequencyOptionChange(record) {
    const frequency = record.get(this.get('dailyConfigPath'));

    if (frequency === FREQUENCY_OPTIONS.daily || frequency === FREQUENCY_OPTIONS.weekly) {
      record.set(this.get('separationCountPath'), 1);
    }
    else if (frequency === FREQUENCY_OPTIONS.monthly) {
      let daysOfWeeks = this.get('dayOfWeeks');

      daysOfWeeks.forEach(day => {
        record.set(day, false);
      });
    }
  },

  actions: {
    onInput(record, valuePath, monthlyType, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
      record.set('monthlyType', monthlyType);
    },

    onChangeDate(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value[0], options);
    },

    onChangeRangeDate(record, startPath, endPath, value) {
      if (value.length === 2) {
        let options = { record, valuePath: startPath };

        this.get('onChange')(value[0], options);
        options = { record, valuePath: endPath };
        this.get('onChange')(value[1], options);
        this.set('record.isForceDirty', true);
      }
    },

    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        // date format is for database so should not be from config
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }
    },

    onChangeTime(record, valuePath, value) {
      const momentTime = moment(value[0]);
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(momentTime.format('HH:mm:ss'), options);
    },

    onCheckboxChange(record, valuePath) {
      let switchState = record.get(valuePath);
      let options = { record, valuePath };

      switchState = !switchState;
      this.set('record.isForceDirty', true);
      this.get('onChange')(switchState, options);
    },

    onDOWChange(record, values) {
      let daysOfWeeks = this.get('dayOfWeeks');

      daysOfWeeks.forEach(day => {
        record.set(day, false);
      });
      switch(values) {
        case DAY_OF_WEEK_OPTIONS.sunday:
          record.set('sunday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.monday:
          record.set('monday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.tuesday:
          record.set('tuesday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.wednesday:
          record.set('wednesday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.thursday:
          record.set('thursday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.friday:
          record.set('friday', true);
          break;
        case DAY_OF_WEEK_OPTIONS.saturday:
          record.set('saturday', true);
      }

      this.set('record.isForceDirty', true);
    },

    onAddNewAvailability() {
      this.get('onCreate')();
    },

    // onRecurringChange(record, value){
    //   record.set('recurring',value);
    //   this.set('recurring',value);
    // },

    onFrequencyChange(record, value){
      this.set('frequency',value);

      let valuePath = this.get('dailyConfigPath');
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
      this.frequencyOptionChange(record);
    },

    onDailyOptionsChange(record, value){
      record.set('dailyDays',value);

      this.frequencyOptionChange(record);
    },

    onEndOptionChange(record, value) {
      record.set('endSelected', value);

      if (value === END_OPTIONS.endBy) {
        // null out max occurences
        let valuePath = this.get('maxOccurencesPath');
        let options = { record, valuePath };

        this.get('onChange')(null, options);
      }
      else if (value === END_OPTIONS.endAfter) {
        // null out end date
        let valuePath = this.get('endDatePath');
        let options = { record, valuePath };

        this.get('onChange')(null, options);
      }
      else {
        // no end option
        let valuePath = this.get('endDatePath');
        let options = { record, valuePath };

        // null out end date
        this.get('onChange')(null, options);

        // null out max occurences
        valuePath = this.get('maxOccurencesPath');
        options = { record, valuePath };
        this.get('onChange')(null, options);
        this.set('record.isForceDirty', true);
      }

      this.set('record.isForceDirty', true);
    },

    onWOMChange(record, value){
      let valuePath = this.get('selectedWOMPath');
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onEnableSubscriptionChange(record) {
      this.toggleProperty('recurring');
      record.set(this.get('recurringPath'), this.get('recurring'));

      // let options = { record, valuePath };

      // value = this.get('enableSubscription') ? 1 : 0;
      // this.set('record.isForceDirty', true);
      // this.get('onChange')(value, options);
    },

    onMonthlyTypeChange(record, value) {
      if (value === MONTHLY_TYPE_OPTIONS.dayOfMonth) {
        let valuePath = this.get('selectedWOMPath');
        let options = { record, valuePath };

        this.get('onChange')(null, options);
        record.set(this.get('dayOfMonthPath'), 1);
      }
      else {
        let valuePath = this.get('dayOfMonthPath');
        let options = { record, valuePath };

        this.get('onChange')(null, options);
        record.set(this.get('selectedWOMPath'), WOM_OPTIONS.first);
      }
      this.set('record.isForceDirty', true);
    }
  }
});
