import Component from '@ember/component';
import { computed, observer } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';
import ENV from '../../../../config/environment';

import tomtom from 'tomtom';
import { task } from 'ember-concurrency';
import $ from 'jquery';



function cleanFieldString(text) {
  return (typeof text === 'undefined') ? '' : text;
}

export default Component.extend({
  classNames: ['form-components-availabilities'],
  editModal: service(),
  value: null,
  disabled: false,
  dayOfWeeks: null,
  store: service(),

  availabilities: computed.readOnly('value'),
  availabilityType: computed.readOnly('valuePath'),
  startTimePath: computed.readOnly('extra.startTimePath'),
  endTimePath: computed.readOnly('extra.endTimePath'),
  shiftStartPath: computed.readOnly('extra.shiftStartPath'),
  shiftEndPath: computed.readOnly('extra.shiftEndPath'),
  sundayPath: computed.readOnly('extra.sundayPath'),

  mondayPath: computed.readOnly('extra.mondayPath'),
  tuesdayPath: computed.readOnly('extra.tuesdayPath'),
  wednesdayPath: computed.readOnly('extra.wednesdayPath'),
  thursdayPath: computed.readOnly('extra.thursdayPath'),
  fridayPath: computed.readOnly('extra.fridayPath'),
  saturdayPath: computed.readOnly('extra.saturdayPath'),
  shiftBreaksPath: computed.readOnly('extra.shiftBreaksPath'),
  shiftBreakTypePath: computed.readOnly('extra.shiftBreakTypePath'),
  shiftBreaks: null,
  breakTypes:null,

  addressOptions: null,

  addresses: computed.readOnly('value'),
  addressType: computed.readOnly('extra.addressvaluePath'),
  aliasPath: computed.readOnly('extra.aliasPath'),
  notesPath: computed.readOnly('extra.notesPath'),
  streetNumberPath: computed.readOnly('extra.streetNumberPath'),
  streetNamePath: computed.readOnly('extra.streetNamePath'),
  localityPath: computed.readOnly('extra.localityPath'),
  regionPath: computed.readOnly('extra.regionPath'),
  subRegionPath: computed.readOnly('extra.subRegionPath'),
  postalCodePath: computed.readOnly('extra.postalCodePath'),
  countryPath: computed.readOnly('extra.countryPath'),
  locationsPath: 'latlng',
  addressRecord: null,

  selected: null,

  geocodeAddressChange: observer('geocode.tomtomAddress', function() {
    const record = this.get('addressRecord');
    const tomtomAddress = this.get('geocode.tomtomAddress');
    this.convertTTAddressToModel(record, tomtomAddress);
    this.disableAddressSearchOnMap(record, record.type);
  }),



  validateAddresses() {
    const records = this.get('addresses');

    this.set('editModal.errors', []);

    records.forEach((record) => {
      if (
            (record.lat === 0 ||
             record.lng === 0)) {
          this.get('editModal.errors').pushObject('Address does not have lat/lng. Please select position on the map.');
      }
    });

  },

  addressSearchResults(results) {
    this.set('addressOptions', results);
  },

  searchAddress: task(function * (term) {
    yield tomtom.fuzzySearch()
      .key(ENV.tomtom.search.searchKey)
      .query(term)
      .countrySet(ENV.tomtom.search.countrySet)
      .typeahead(true)
      .center(ENV.tomtom.search.center)
      .radius(ENV.tomtom.search.radius)
      .callback(this.addressSearchResults.bind(this))
      .go();
  }),


  convertTTAddressToModel: function(record, ttAddress) {

    if((ttAddress.id === undefined && ttAddress.id === null)
       || (ttAddress.position !== undefined && ttAddress.position !== null)) {
      let addressvaluePath = this.get('streetNumberPath');
      let options = { record, addressvaluePath };

      this.set('record.isForceDirty', true);

      this.get('onChange')(cleanFieldString(ttAddress.address.streetNumber), options);

      options.addressvaluePath = this.get('streetNamePath');
      this.get('onChange')(cleanFieldString(ttAddress.address.streetName), options);

      options.addressvaluePath = this.get('localityPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.municipality), options);

      options.addressvaluePath = this.get('regionPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countrySubdivisionName), options);

      options.addressvaluePath = this.get('subRegionPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countrySecondarySubdivision), options);

      options.addressvaluePath = this.get('postalCodePath');
      this.get('onChange')(cleanFieldString(ttAddress.address.postalCode), options);

      options.addressvaluePath = this.get('countryPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countryCode), options);

      options.addressvaluePath = this.get('aliasPath');
      this.get('onChange')('', options);


      record.set('lat', ttAddress.position.lat);
      record.set('lng', ttAddress.position.lon);

      this.get('geocode').getGeonode(ttAddress.address.freeformAddress)
        .then((result) => {
          if (result.data.length > 0) {
            record.set('geoNode', result.data[0].geonode);
            //this.set('addressRecord', record);

          }
        });
    } else {
      let addressvaluePath = this.get('streetNumberPath');
    let options = { record, addressvaluePath };

    this.set('record.isForceDirty', true);

    this.get('onChange')(cleanFieldString(ttAddress.streetNumber), options);

    options.addressvaluePath = this.get('streetNamePath');
    this.get('onChange')(cleanFieldString(ttAddress.streetAddress), options);

    options.addressvaluePath = this.get('localityPath');
    this.get('onChange')(cleanFieldString(ttAddress.locality), options);

    options.addressvaluePath = this.get('regionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySubdivisionName), options);

    options.addressvaluePath = this.get('subRegionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySecondarySubdivision), options);

    options.addressvaluePath = this.get('postalCodePath');
    this.get('onChange')(cleanFieldString(ttAddress.postalCode), options);

    options.addressvaluePath = this.get('countryPath');
    this.get('onChange')(cleanFieldString(ttAddress.countryCode), options);

    options.addressvaluePath = this.get('aliasPath');
    this.get('onChange')(cleanFieldString(ttAddress.alias), options);

    options.addressvaluePath = this.get('notesPath');
    this.get('onChange')(cleanFieldString(ttAddress.notes), options);

    record.set('lat', ttAddress.locations.firstObject.lat);
    record.set('lng', ttAddress.locations.firstObject.lng);

    this.get('geocode').getGeonode(ttAddress.tomtomFormattedAddress.address.freeformAddress)
      .then((result) => {
        if (result.data.length > 0) {
          record.set('geoNode', result.data[0].geonode);
        }

      });
    }
    this.get('geocode').activateGeocode(false, record ,record.type);
    this.validateAddresses()
    this.set('addressOptions', this.get('record').get('rider.riderAddresses'));

  },

  clearAddressModel: function(record) {
    let addressvaluePath = this.get('streetNumberPath');
    let options = { record, addressvaluePath };

    this.get('onChange')('', options);

    options.addressvaluePath = this.get('streetNamePath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('localityPath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('regionPath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('subRegionPath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('postalCodePath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('countryPath');
    this.get('onChange')('', options);

    options.addressvaluePath = this.get('aliasPath');
    this.get('onChange')('', options);

    record.set('lat', 0);
    record.set('lng', 0);
  },

  enableAddressSearchOnMap(record, type) {
    $('html,body,.tomtom-map').addClass('custom-cursor');
    this.get('geocode').activateGeocode(true, record ,type);
    this.set('addressRecord', record);
  },

  disableAddressSearchOnMap(record, type) {
    $('html,body,.tomtom-map').removeClass('custom-cursor');
    this.get('geocode').activateGeocode(false,record,type);
    this.set('addressRecord', null);
  },


  init() {
    this.set('dayOfWeeks', ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']);
    this.set('datePlaceHolder', ENV.dateTimeFormat.dateMoment);
    this.set('timePlaceHolder', ENV.dateTimeFormat.timeMoment);
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);
    this.set('timeFormat', ENV.dateTimeFormat.timeFlatPickr);
    this._super(...arguments);
    this.set('shiftBreaks',this.record.get('shiftBreaks'));
  },

  didInsertElement: function() {
    const breakTypeModelName = this.get('extra.breakTypeModelName');

    this.set('breakTypes', this.get('store').peekAll(breakTypeModelName));
  },

  getAvailibilityBreaks(record){
    return record.get('shiftBreaks');
  },


  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onChangeDate(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value[0], options);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        // $(`#${datePicker.element.id}`).focus();
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        // date format is for database so should not be from config
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }

      // $(datePicker.altInput.className).select();
      // $(datePicker.element).select();
      // $('.effective-start').select();
    },

    onChangeTime(record, valuePath, value) {
      const momentTime = moment(value[0]);
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(momentTime.format('HH:mm:ss'), options);
    },

    onCheckboxChange(record, valuePath) {
      let switchState = record.get(valuePath);
      let options = { record, valuePath };

      switchState = !switchState;
      this.set('record.isForceDirty', true);
      this.get('onChange')(switchState, options);
    },
    onDOWChange(record, values) {
      record.set('sunday', values.includes('sunday'));
      record.set('monday', values.includes('monday'));
      record.set('tuesday', values.includes('tuesday'));
      record.set('wednesday', values.includes('wednesday'));
      record.set('thursday', values.includes('thursday'));
      record.set('friday', values.includes('friday'));
      record.set('saturday', values.includes('saturday'));
      this.set('record.isForceDirty', true);
    },

    onAddNewAvailability() {
      this.get('onCreate')();
    },

    onRemoveAvailability(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },

    onAddNewBreak(record, valuePath, modelName) {
      //const newAddress = this.get('editModal').pushRecordValue(record., 'locations', 'bs-location');

      const shiftBreak = this.get('editModal').pushRecordValue(record, valuePath, modelName);
      shiftBreak.set('breakType', this.get('breakTypes.firstObject'));
      //this.get('editModal').pushRecordValue(record, 'shiftBreaks', 'ss-shift-break');
    },

    onbreakTypeChange(record, valuePath, value){
      record.set(valuePath, value);
      this.record.set(valuePath, value);

      this.set('record.isForceDirty', true);

    },

    onRemoveBreak(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },

    onAddressSelected(record, valuePath, value) {
      this.convertTTAddressToModel(record, value);
    },

    onAliasAddressSelected(record, valuePath, value) {
      this.convertTTAliasAddressToModel(record, value);
    },

    onPickOnMap(record) {
      const activeGeocode = this.get('geocode.activeGeocode');
      if (activeGeocode) {
        this.disableAddressSearchOnMap(record, record.type);
      }
      else {
        this.enableAddressSearchOnMap(record, record.type);
      }
    },

    handleKeydown(record, valuePath, dropdown, e) {
      if (e.keyCode !== 13) { return; }

      let text = e.target.value;
      let options = { record, valuePath: 'streetAddress' };
      // address has been forced without geocodes
      // this.set('record.isForceDirty', true);
      this.clearAddressModel(record);

      // this.set('record.isForceDirty', true);
      this.get('onChange')(text, options);
      this.set('addressRecord', record);
      this.validateAddresses();
    }
  }
});
