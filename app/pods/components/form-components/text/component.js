import Component from '@ember/component';

export default Component.extend({
  classNames: ['form-components-text'],

  value: null,
  disabled: false,

  actions: {
    onInput(value) {
      this.get('onChange')(value.trim());
    }
  }
});
