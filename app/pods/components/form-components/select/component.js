import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['form-components-select'],

  value: null,
  disabled: false,
  extra: null,
  store: service(),
  dataFromModel: false,

  options: computed.alias('extra.options'),

  init(){
    this._super(...arguments);
    if(this.get('extra.optionModelName') !== undefined ) {
      this.set('dataFromModel',true);

      this.set('options', this.get('store').peekAll(this.get('extra.optionModelName')));
    }
  },

  actions: {
    onChange(option) {
      this.set('selected', option)
    }
  }
});
