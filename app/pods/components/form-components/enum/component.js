import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['form-components-select'],

  value: null,
  disabled: false,
  extra: null,
  store: service(),
  dataFromModel: false,

  options: computed.alias('extra.options'),
  allowMultiple: computed.alias('extra.allowMultiple'),
  optionModelName: computed.alias('extra.optionModelName'),


  init(){
    this._super(...arguments);
    if(this.get('extra.optionModelName') !== undefined ) {
      this.set('dataFromModel',true);

      this.set('options', this.get('store').peekAll(this.get('extra.optionModelName')));

    }
  },

  selected: computed('value', 'options', function() {
    if(this.get('extra.optionModelName') !== undefined ) {
      if(this.get('value')  === undefined || this.get('value.id')  === undefined) {
        this.set('value',this.get('options').firstObject);
      }
      return this.get('options').find(option => {
        return option.id === this.get('value.id');
    })
    }
  }),

  actions: {
    onChange(option) {
      this.get('onChange')(option);
    }
  }
});
