import moment from 'moment';
import Component from '@ember/component';
import ENV from '../../../../config/environment';

export default Component.extend({
  classNames: ['form-components-datetimeflatpickr'],

  value: null,
  disabled: false,
  defaultDate: null,
  maxDate: null,
  minDate: null,

  init(){
    this._super(...arguments);

    this.set('minDate', this.get('value'));
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);
    this.set('datePlaceholder', ENV.dateTimeFormat.dateMoment);
  },
  actions: {
    onChangeDate(value) {
      this.get('onChange')(value[0]);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }
    }
  }
});
