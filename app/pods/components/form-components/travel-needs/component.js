import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import _ from 'lodash';

// status of travel needs within the UI
const STATUS_TYPE_EXISTING = 'existing'; // already part of rider
const STATUS_TYPE_AVAILABLE = 'available'; // available for rider to add
const STATUS_TYPE_ADDED = 'added'; // it will be added to rider once it is saved


export default Component.extend({
  classNames: ['form-components-travel-need', 'data-test-form-travel-need'],
  store: service(),
  editModal: service(),
  value: null,
  disabled: false,
  pcaState: false,
  serviceAnimalState: false,
  travelNeedTypes: null,
  passengerTypes: null,

  travelNeeds: computed.readOnly('value'),
  countPath: computed.readOnly('extra.countPath'),
  travelNeedTypePath: computed.readOnly('extra.travelNeedTypePath'),
  passengerTypePath: computed.readOnly('extra.passengerTypePath'),
  modelName: computed.readOnly('field.modelName'),

  equipments: computed('record.{equipments,newTravelNeeds}', function() {
    this.get('record.equipments').map((travelNeed) => {
      travelNeed.set('status', STATUS_TYPE_EXISTING);
    });

    this.get('record.newTravelNeeds').map((travelNeed) => {
      travelNeed.set('status', STATUS_TYPE_AVAILABLE);
    });
    return _.concat(this.get('record.equipments'), this.get('record.newTravelNeeds'));
  }),

  newEquipments: computed('record.newTravelNeeds', function() {
    return this.get('record.newTravelNeeds').filter(travelNeed => {
      return travelNeed.get('passengerType.isClient') && !travelNeed.get('travelNeedType.isServiceAnimal') &&
        travelNeed.get('status') === STATUS_TYPE_AVAILABLE;
    });
  }),

  extraPassengers: computed('record.{extraPassengers,newTravelNeeds}', function() {
    const tempArray = _.concat(this.get('record.extraPassengers'), this.get('record.newTravelNeeds'));
    const extraPassObjects = [];

    // arrange in specific order
    extraPassObjects.push(tempArray.find(travelNeed => travelNeed.get('travelNeedType.isServiceAnimal')));
    extraPassObjects.push(tempArray.find(travelNeed => travelNeed.get('passengerType.isPca')));
    extraPassObjects.push(tempArray.find(travelNeed => travelNeed.get('passengerType.isCompanion')));

    return extraPassObjects;
  }),

  init: function() {
    this._super(...arguments);

    const travelNeedTypeModelName = this.get('extra.travelNeedTypeModelName');
    const passengerTypeModelName = this.get('extra.passengerTypeModelName');

    this.set('travelNeedTypes', this.get('store').peekAll(travelNeedTypeModelName));
    this.set('passengerTypes', this.get('store').peekAll(passengerTypeModelName));

    this.setupNewEquipments();
    this.setupNewExtraPassengers();
  },

  clientPassengerType: function() {
    const passengerTypes = this.get('passengerTypes');

    return passengerTypes.find(types => types.isClient);
  },

  serviceAnimalTravelNeedType: function() {
    const travelNeedTypes = this.get('travelNeedTypes');

    return travelNeedTypes.find(types => types.isServiceAnimal);
  },

  ambulatoryTravelNeedType: function() {
    const travelNeedTypes = this.get('travelNeedTypes');

    return travelNeedTypes.find(types => types.isAmbulatory);
  },

  pcaPassengerType: function() {
    const passengerTypes = this.get('passengerTypes');

    return passengerTypes.find(types => types.isPca);
  },

  companionPassengerType: function() {
    const passengerTypes = this.get('passengerTypes');

    return passengerTypes.find(types => types.isCompanion);
  },

  setupNewEquipments: function() {
    const currentTravelNeeds = this.get('record.equipments');

    const newTravelNeeds = [];

    // add other travel need types that this person does not have
    // with empty count
    this.get('travelNeedTypes').map(travelNeedType => {
      if (!travelNeedType.isServiceAnimal &&
          _.findIndex(currentTravelNeeds, travelNeed =>{
        return travelNeed.get('travelNeedType.id') === travelNeedType.id;
      }) < 0) {
        let newRecord = this.get('store').createRecord(this.get('modelName'));
        newRecord.set('travelNeedType', travelNeedType);
        newRecord.set('passengerType', this.clientPassengerType());

        newTravelNeeds.push(newRecord);
      }
    });

    this.set('record.newTravelNeeds', newTravelNeeds);
  },

  setupNewExtraPassengers: function() {
    const currentTravelNeeds = this.get('record.extraPassengers');

    const newTravelNeeds = [];

    let retVal;

    // add service animal travel need type if this rider does not have
    // in current travel needs. leave count as empty
    retVal = currentTravelNeeds.find(travelNeed => travelNeed.get('travelNeedType.isServiceAnimal'));

    if (_.isUndefined(retVal)) {
      let newRecord = this.get('store').createRecord(this.get('modelName'));

      this.set('serviceAnimalState', false);
      newRecord.set('travelNeedType', this.serviceAnimalTravelNeedType());
      newRecord.set('passengerType', this.clientPassengerType());

      newTravelNeeds.push(newRecord);
    }
    else {
      this.set('serviceAnimalState', retVal.get('count') > 0);
    }

    // add pca passenger type for this rider if it does not have
    // in current travel needs. leave count as empty
    retVal = currentTravelNeeds.find((travelNeed) => {
        return travelNeed.get('passengerType.isPca');
    });

    if (_.isUndefined(retVal)) {
      let newRecord = this.get('store').createRecord(this.get('modelName'));

      this.set('pcaState', false);
      newRecord.set('travelNeedType', this.ambulatoryTravelNeedType());
      newRecord.set('passengerType', this.pcaPassengerType());

      newTravelNeeds.push(newRecord);
    }
    else {
      this.set('pcaState', retVal.get('count') > 0);
    }

    // add companion passenger type for this rider if it does not have
    // in current travel needs. leave count as empty
    retVal = currentTravelNeeds.find(travelNeed => travelNeed.get('passengerType.isCompanion'));

    if (_.isUndefined(retVal)) {
      let newRecord = this.get('store').createRecord(this.get('modelName'));

      newRecord.set('travelNeedType', this.ambulatoryTravelNeedType());
      newRecord.set('passengerType', this.companionPassengerType());

      newTravelNeeds.push(newRecord);
    }

    this.get('record.newTravelNeeds').addObjects(newTravelNeeds);
  },

  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(parseInt(value), options);
    },

    onAddNewTravelNeed(record) {
      const travelNeed = record.get('newTravelNeeds').find(travelNeed => {
        return travelNeed.status === STATUS_TYPE_AVAILABLE &&
          travelNeed.isEquipment;
      });

      if (travelNeed !== undefined) {
        travelNeed.set('status', STATUS_TYPE_ADDED);
      }
    },

    onTravelNeedTypeChange(oldTravelNeed, newTravelNeed) {
      oldTravelNeed.set('status', STATUS_TYPE_AVAILABLE);
      oldTravelNeed.set('count', 0);

      newTravelNeed.set('status', STATUS_TYPE_ADDED);
    },

    onRemoveTravelNeed(record, valuePath, value) {
      let options = { record, valuePath };

      if (record !== undefined) {
        record.set('status', STATUS_TYPE_AVAILABLE);
      }

      this.get('onRemove')(value, options);
    },

    onPCAChange(record, valuePath, value) {
      this.toggleProperty('pcaState');
      let options = { record, valuePath };

      value = this.get('pcaState') ? 1 : 0;
      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onServiceAnimalChange(record, valuePath, value) {
      this.toggleProperty('serviceAnimalState');
      let options = { record, valuePath };

      value = this.get('serviceAnimalState') ? 1 : 0;
      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onPassengerTypeChange(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onChange')(value, options);
    },
  }
});
