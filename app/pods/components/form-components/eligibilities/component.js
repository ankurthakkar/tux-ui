import moment from 'moment';
import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import ENV from '../../../../config/environment';

export default Component.extend({
  classNames: ['form-components-eligibilities'],
  store: service(),
  value: null,
  eligibilityTypes: null,

  eligibilities: computed.readOnly('value'),
  fromPath: computed.readOnly('extra.fromPath'),
  toPath: computed.readOnly('extra.toPath'),
  eligibilityTypePath: computed.readOnly('extra.eligibilityTypePath'),

  init() {
    this.set('datePlaceHolder', ENV.dateTimeFormat.dateMoment);
    this.set('timePlaceHolder', ENV.dateTimeFormat.timeMoment);
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);
    this.set('timeFormat', ENV.dateTimeFormat.timeFlatPickr);
    this._super(...arguments);
  },

  didInsertElement: function() {
      const eligibilityTypeModelName = this.get('extra.eligibilityTypeModelName');

      this.set('eligibilityTypes', this.get('store').peekAll(eligibilityTypeModelName));
  },

  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onChange')(value, options);
    },

    onChangeDate(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value[0], options);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        // $(`#${datePicker.element.id}`).focus();
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        // date format is for database so should not be from config
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }

      // $(datePicker.altInput.className).select();
      // $(datePicker.element).select();
      // $('.effective-start').select();
    },

    onAddNewEligibility() {
      const eligibility = this.get('onCreate')();

      eligibility.set('eligibilityType', this.get('eligibilityTypes.firstObject'));
    },

    onRemoveEligibility(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },
    onEligibilityTypeChange(record, valuePath, value) {
      record.set(valuePath, value);
    }
  }
});
