import moment from 'moment';
import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import ENV from '../../../../config/environment';
import { isArray } from '@ember/array';
import { assert } from '@ember/debug';

/* 
  This component is based off the "eligibilities" and "places" components

  It generates a form of text fields based on a config that supplies "rows" in a collection
  For each key in a "row" object,
  it pulls out the column key
  and supplies a cell label and an input field from that key/value pair

  The intent is that a serializer would pull a model from an API,
  translate it into the desired form configuration object
  and supply new records based on these arbitrary fields

  TODO: hook this component/a model into a Mirage serializer
  to supply a backend and multiple records on page load

  Sample config entries:
  app/pods/components/iq-widgets/edit-settings-form-widget/config.js
  {
    title: 'Wifi Network',
    fields: [{
      id: 'wifiNetwork',
      type: 'multientry',
      label: '',
      valuePath: 'wifiNetwork',
      modelName: 'rms-address',
      isMultiType: true,
      extra: {
        rows: [
          {password: 'default', networkName: 'default'}
        ]
      }
    }]},
*/

export default Component.extend({
  classNames: ['form-components-multientry'],
  store: service(),
  value: null,
  editableRecords: null,
  editableSections: null,
  service: null,

  columns: computed('extra.rows.firstObject', function(){
    let row = this.get('extra.rows.firstObject');
    return Object.keys(row);
  }),

  rows: computed.readOnly('extra.rows'),
  model: null,
  apiService: null,

  init() {
    this._super(...arguments);
  },

  didInsertElement: function() {
    // todo: Add insert-element function
  },

  actions: {
    onInput(model) {
      // do something with typed values
    },

    onClose(model) {
    },

    onAddNew(model, valuePath, modelName) {
      // create a new table item
      const newItem = this.get('editModal').pushRecordValue(model, valuePath, modelName);
      this.get('editModal').pushRecordValue(newItem, 'multientry-catchall', 'rms-location');
    },

    onRemove(model) {
      // remove an item from a table
      // alert('Remove me', model)
      // let options = { record, valuePath };
      // this.get('onRemove')(value, options);
    }
  }
});
