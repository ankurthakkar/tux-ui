import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed } from '@ember/object';
import { task, timeout } from 'ember-concurrency';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: ['form-components-select'],
  store: service(),
  editModal: service(),
  value: null,
  disabled: false,
  extra: null,

  options: null,

  allowMultiple: computed.alias('extra.allowMultiple'),
  optionSearchPath: computed.alias('extra.optionSearchPath'),
  optionModelName: computed.alias('extra.optionModelName'),
  optionSearchEnabled: computed.alias('extra.optionSearchEnabled'),
  optionTempRecordSelected: computed.alias('extra.optionTempRecordSelected'),
  optionFilterFunction: computed.alias('extra.optionFilterFunction'),

  includes: computed('extra.optionIncludes', function() {
    return this.get('extra.optionIncludes') !== undefined ? this.get('extra.optionIncludes') : '';
  }),

  init() {
    const selectedPath = this.get('extra.optionSelectedPath');

    if (!this.get('optionSearchEnabled')) {
      if (isEmpty(selectedPath)) {
        this.set('selected', this.get(`value`));
      }
      else {
        this.set('selected', this.get(`value.${selectedPath}`));
      }
    }
    else {
      let object = {};
      object[this.get('optionSearchPath')] = this.get('value');
      this.set('selected', object);
    }

    if (!this.get('optionSearchEnabled')) {
      this.loadEnum();
    }

    this._super(...arguments);
  },

  loadEnum() {
    this.store.findAll(this.get('optionModelName'))
      .then((results) => {
        this.searchResults(results);
      });
  },

  searchResults(results) {
    if (this.get('optionFilterFunction')) {
      let filteredResults = results.filter((result) => {
      let available = this.get('record')[this.get('optionFilterFunction')](result);

        return available;
      });

      this.set('options', filteredResults);
    }
    else {
      this.set('options', results);
    }
  },

  // will do this when optionSearchEnabled is true
  // otherwise it will do loadEnum
  search: task(function * (term) {
    yield timeout(500);

    const query = {
      filter: `ilike(${this.get('optionSearchPath')},'%${term}%')`,
      include: this.get('includes')
    };

    yield this.store.query(this.get('optionModelName'), query)
      .then((results) => {
        this.searchResults(results);
      });
  }),

  actions: {
    onSelected(newRecord) {
      this.set('selected', newRecord);

      if (this.get('optionSearchEnabled')) {
        this.set(`record.${this.get('valuePath')}`,newRecord.get(this.get('optionSearchPath')));
        this.set(`record.${this.get('optionTempRecordSelected')}`, newRecord);
      }
      else {
        this.set(`record.${this.get('valuePath')}`, newRecord);
      }

      this.set('record.isForceDirty', true);
    },

    handleKeydown(dropdown, e) {
      if (e.keyCode !== 13) { return; }

      // TODO: add code to store data
      //  to correct api host when kafka topic is done.
    }
  }
});
