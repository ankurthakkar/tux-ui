import Component from '@ember/component';
import moment from 'moment';

const TIME_FORMAT = /^\d\d:\d\d (AM|PM)$/;

export default Component.extend({
  classNames: ['form-components-time'],

  value: null,
  disabled: false,

  actions: {
    onInput(value) {
      if (!TIME_FORMAT.test(value)) return;

      let date = moment(this.get('value')).format('YYYY-MM-DD');
      let newValue = moment(`${date} ${value}`, 'YYYY-MM-DD hh:mm A').toDate();

      this.get('onChange')(newValue);
    }
  }
});
