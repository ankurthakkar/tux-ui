import Component from '@ember/component';
import moment from 'moment';
import { isEmpty } from '@ember/utils';

const DATE_FORMAT = /^\d\d\d\d-\d\d-\d\d$/;

export default Component.extend({
  classNames: ['form-components-date'],

  value: null,
  disabled: false,

  init() {
    this._super(...arguments);

    // forces this component to show Invalid date instead
    // of the current date
    if (isEmpty(this.value)) {
      this.value = '';
    }
  },

  actions: {
    onInput(value) {
      if (!DATE_FORMAT.test(value)) return;

      this.get('onChange')(moment(value, 'YYYY-MM-DD').toDate());
    }
  }
});
