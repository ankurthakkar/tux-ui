import Component from '@ember/component';
import ENV from '../../../../config/environment';

export default Component.extend({
  classNames: ['form-components-datetimeflatpickr'],

  value: null,
  disabled: false,
  defaultDate: null,
  maxDate:null,

  init(){
    this._super(...arguments);

    this.set('timeFormat', ENV.dateTimeFormat.timeFlatPickr);
    this.set('timePlaceholder', ENV.dateTimeFormat.timeMoment);
  },
  actions: {
    onChangeTime() {
      // required by flatpickr
    },
    onClose(currentValue) {
      let currValue =  new Date(this.get('value'));

      // change the date part only
      currValue.setHours(currentValue[0].getHours());
      currValue.setMinutes(currentValue[0].getMinutes());

      this.get('onChange')(currValue);
    }
  }
});
