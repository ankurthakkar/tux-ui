import Component from '@ember/component';
import { computed } from '@ember/object';
import tomtom from 'tomtom';
import ENV from 'adept-iq/config/environment';
import { task } from 'ember-concurrency';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import { isEmpty } from '@ember/utils';
import { insideZone } from 'adept-iq/utils/zone-validation';

function cleanFieldString(text) {
  return (typeof text === 'undefined') ? '' : text;
}

export default Component.extend({
  classNames: ['form-components-places'],
  editModal: service(),
  geocode: service(),
  errorMessage: service(),
  value: null,
  disabled: false,
  addressOptions: null,

  addresses: computed.readOnly('value'),
  addressType: computed.readOnly('valuePath'),
  aliasPath: computed.readOnly('extra.aliasPath'),
  notesPath: computed.readOnly('extra.notesPath'),
  streetNumberPath: computed.readOnly('extra.streetNumberPath'),
  streetAddressPath: computed.readOnly('extra.streetAddressPath'),
  localityPath: computed.readOnly('extra.localityPath'),
  regionPath: computed.readOnly('extra.regionPath'),
  subRegionPath: computed.readOnly('extra.subRegionPath'),
  postalCodePath: computed.readOnly('extra.postalCodePath'),
  countryPath: computed.readOnly('extra.countryPath'),
  locationsPath: 'locations.firstObject.latlng',
  addressRecord: null,

  selected: null,

  geocodeAddressChange() {
    const record = this.get('addressRecord');
    const tomtomAddress = this.get('geocode.tomtomAddress');

    if (!isEmpty(tomtomAddress)) {
      this.convertTTAddressToModel(record, tomtomAddress);
      this.disableAddressSearchOnMap();
    }
  },

  validateAddresses() {
    const records = this.get('addresses');

    this.set('editModal.errors', []);

    records.forEach((record) => {
      if (!isEmpty(record.streetAddress) > 0 &&
            (record.locations.firstObject.lat === 0 ||
             record.locations.firstObject.lng === 0)) {
          this.get('editModal.errors').pushObject('Address does not have lat/lng. Please select position on the map.');
      }
    });
  },


  addressSearchResults(results) {
    this.set('addressOptions', results);
  },

  searchAddress: task(function * (term) {
    yield tomtom.fuzzySearch()
      .key(ENV.tomtom.search.searchKey)
      .query(term)
      .countrySet(ENV.tomtom.search.countrySet)
      .typeahead(true)
      .center(ENV.tomtom.search.center)
      .radius(ENV.tomtom.search.radius)
      .callback(this.addressSearchResults.bind(this))
      .go();
  }),

  isAddressAtMax: computed('value', function() {
    const length = this.get('value').length;
    let isAtMax = false;

    if (this.get('addressType') === 'primaryAddresses' && length >= 1) {
      isAtMax = true;
    }

    return isAtMax;
  }),

  convertTTAddressToModel: function(record, ttAddress) {
    let valuePath = this.get('streetNumberPath');
    let options = { record, valuePath };

    this.set('record.isForceDirty', true);

    this.get('onChange')(cleanFieldString(ttAddress.address.streetNumber), options);

    options.valuePath = this.get('streetAddressPath');
    this.get('onChange')(cleanFieldString(ttAddress.address.streetName), options);

    options.valuePath = this.get('localityPath');
    this.get('onChange')(cleanFieldString(ttAddress.address.municipality), options);

    options.valuePath = this.get('regionPath');
    this.get('onChange')(cleanFieldString(ttAddress.address.countrySubdivisionName), options);

    options.valuePath = this.get('subRegionPath');
    this.get('onChange')(cleanFieldString(ttAddress.address.countrySecondarySubdivision), options);

    options.valuePath = this.get('postalCodePath');
    this.get('onChange')(cleanFieldString(ttAddress.address.postalCode), options);

    options.valuePath = this.get('countryPath');
    this.get('onChange')(cleanFieldString(ttAddress.address.countryCode), options);

    record.set('locations.firstObject.lat', ttAddress.position.lat);
    record.set('locations.firstObject.lng', ttAddress.position.lon);

    this.get('geocode').getGeonode(ttAddress.address.freeformAddress)
      .then((result) => {
        if (result.data.length > 0) {
          record.set('locations.firstObject.geoNode', result.data[0].geonode);
        }
      });

    this.validateAddresses();
  },

  clearAddressModel: function(record) {
    let valuePath = this.get('streetNumberPath');
    let options = { record, valuePath };

    this.get('onChange')('', options);

    options.valuePath = this.get('streetAddressPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('localityPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('regionPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('subRegionPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('postalCodePath');
    this.get('onChange')('', options);

    options.valuePath = this.get('countryPath');
    this.get('onChange')('', options);

    record.set('locations.firstObject.lat', 0);
    record.set('locations.firstObject.lng', 0);
  },

  enableAddressSearchOnMap(record) {
    $('html,body,.tomtom-map').addClass('custom-cursor');
    this.get('geocode').activateGeocode(true, record, 'pick');
    this.set('addressRecord', record);
    this.addObserver('geocode.tomtomAddress', this.geocodeAddressChange);
  },

  disableAddressSearchOnMap() {
    $('html,body,.tomtom-map').removeClass('custom-cursor');
    this.get('geocode').deactivateGeocode();
    this.set('addressRecord', null);
    this.removeObserver('geocode.tomtomAddress', this.geocodeAddressChange);
  },

  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onAddressSelected(record, valuePath, value) {
      // if the point is outside the valid zone poolygon
      // return error and exit
      if (!insideZone([ value.position.lat, value.position.lon ])) {
        this.get('errorMessage').pushError({ detail:'Invalid Zone' });
        return;
      }

      this.convertTTAddressToModel(record, value);
    },

    onPickOnMap(record) {
      const activeGeocode = this.get('geocode.activeGeocode');

      if (activeGeocode) {
        this.disableAddressSearchOnMap();
      }
      else {
        this.enableAddressSearchOnMap(record);
      }
    },

    onAddNewAddress(record, valuePath, modelName) {
      const newAddress = this.get('editModal').pushRecordValue(record, valuePath, modelName);
      this.get('editModal').pushRecordValue(newAddress, 'locations', 'rms-location');
    },

    onRemoveAddress(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },

    handleKeydown(record, valuePath, dropdown, e) {
      if (e.keyCode !== 13) { return; }

      let text = e.target.value;
      let options = { record, valuePath: 'streetAddress' };
      // address has been forced without geocodes
      // this.set('record.isForceDirty', true);
      this.clearAddressModel(record);

      // this.set('record.isForceDirty', true);
      this.get('onChange')(text, options);
      this.set('addressRecord', record);
      this.validateAddresses();
    }
  }
});
