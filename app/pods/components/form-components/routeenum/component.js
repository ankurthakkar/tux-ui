import { inject as service } from '@ember/service';
import Component from '@ember/component';
import { computed, get } from '@ember/object';
import { isRouteAssignable } from 'adept-iq/utils/isRouteAssignable';

export default Component.extend({
  classNames: ['form-components-select'],

  value: null,
  disabled: false,
  extra: null,
  store: service(),
  dataFromModel: false,

  options: computed.alias('extra.options'),
  allowMultiple: computed.alias('extra.allowMultiple'),
  optionModelName: computed.alias('extra.optionModelName'),


  init(){
    this._super(...arguments);

    if(this.get('extra.optionModelName') !== undefined ) {
      this.set('dataFromModel',true);

      let routeOptions = this.get('store').peekAll(this.get('extra.optionModelName'));
      let assignableRoute = [];

      routeOptions.forEach(function(route) {
        if(isRouteAssignable(route)) {
          assignableRoute.push(route);
        }
      });

      this.set('options', assignableRoute);
    }
  },

  selected: computed('value', 'options', function() {
    if(this.get('extra.optionModelName') !== undefined ) {

      return this.get('options').find(option => {
        return option.id === this.get('value.id');
    })
    }
  }),

  actions: {
    onChange(option) {
      this.get('onChange')(option);
    }
  }
});
