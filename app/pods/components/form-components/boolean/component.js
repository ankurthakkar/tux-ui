import Component from '@ember/component';
import { computed, get } from '@ember/object';

export default Component.extend({
  classNames: ['form-components-boolean'],

  value: null,
  disabled: false,

  options: computed.alias('extra.options'),

  selected: computed('value', 'options', function() {
    return this.get('options').find(option => {
      return option.value === this.get('value');
    })
  }),

  actions: {
    onChange(option) {
      this.get('onChange')(get(option, 'value'));
    }
  }
});
