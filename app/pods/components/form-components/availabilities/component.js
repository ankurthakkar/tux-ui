import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';
import ENV from '../../../../config/environment';


export default Component.extend({
  classNames: ['form-components-availabilities'],
  editModal: service(),
  value: null,
  disabled: false,
  dayOfWeeks: null,

  availabilities: computed.readOnly('value'),
  availabilityType: computed.readOnly('valuePath'),
  startTimePath: computed.readOnly('extra.startTimePath'),
  endTimePath: computed.readOnly('extra.endTimePath'),
  shiftStartPath: computed.readOnly('extra.shiftStartPath'),
  shiftEndPath: computed.readOnly('extra.shiftEndPath'),
  sundayPath: computed.readOnly('extra.sundayPath'),

  mondayPath: computed.readOnly('extra.mondayPath'),
  tuesdayPath: computed.readOnly('extra.tuesdayPath'),
  wednesdayPath: computed.readOnly('extra.wednesdayPath'),
  thursdayPath: computed.readOnly('extra.thursdayPath'),
  fridayPath: computed.readOnly('extra.fridayPath'),
  saturdayPath: computed.readOnly('extra.saturdayPath'),

  init() {
    this.set('dayOfWeeks', ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']);
    this.set('datePlaceHolder', ENV.dateTimeFormat.dateMoment);
    this.set('timePlaceHolder', ENV.dateTimeFormat.timeMoment);
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);
    this.set('timeFormat', ENV.dateTimeFormat.timeFlatPickr);
    this._super(...arguments);
  },


  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onChangeDate(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value[0], options);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        // $(`#${datePicker.element.id}`).focus();
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        // date format is for database so should not be from config
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }

      // $(datePicker.altInput.className).select();
      // $(datePicker.element).select();
      // $('.effective-start').select();
    },

    onChangeTime(record, valuePath, value) {
      const momentTime = moment(value[0]);
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(momentTime.format('HH:mm:ss'), options);
    },

    onCheckboxChange(record, valuePath) {
      let switchState = record.get(valuePath);
      let options = { record, valuePath };

      switchState = !switchState;
      this.set('record.isForceDirty', true);
      this.get('onChange')(switchState, options);
    },
    onDOWChange(record, values) {
      record.set('sunday', values.includes('sunday'));
      record.set('monday', values.includes('monday'));
      record.set('tuesday', values.includes('tuesday'));
      record.set('wednesday', values.includes('wednesday'));
      record.set('thursday', values.includes('thursday'));
      record.set('friday', values.includes('friday'));
      record.set('saturday', values.includes('saturday'));
      this.set('record.isForceDirty', true);
    },

    onAddNewAvailability() {
      this.get('onCreate')();
    },

    onRemoveAvailability(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },
  }
});
