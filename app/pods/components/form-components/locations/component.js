import Component from '@ember/component';
import { computed } from '@ember/object';
import tomtom from 'tomtom';
import ENV from 'adept-iq/config/environment';
import { task } from 'ember-concurrency';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import { isEmpty, isNone } from '@ember/utils';
import { insideZone } from 'adept-iq/utils/zone-validation';
import EmberObject from '@ember/object';

function cleanFieldString(text) {
  return (typeof text === 'undefined') ? '' : text;
}

export default Component.extend({
  classNames: ['form-components-places'],
  editModal: service(),
  geocode: service(),
  errorMessage: service(),
  value: null,
  disabled: false,
  addressOptions: null,

  // addresses: computed.readOnly('value'),
  addressType: computed.readOnly('valuePath'),
  aliasPath: computed.readOnly('extra.aliasPath'),
  notesPath: computed.readOnly('extra.notesPath'),
  streetNumberPath: computed.readOnly('extra.streetNumberPath'),
  streetNamePath: computed.readOnly('extra.streetNamePath'),
  localityPath: computed.readOnly('extra.localityPath'),
  regionPath: computed.readOnly('extra.regionPath'),
  subRegionPath: computed.readOnly('extra.subRegionPath'),
  postalCodePath: computed.readOnly('extra.postalCodePath'),
  countryPath: computed.readOnly('extra.countryPath'),
  defaultOptionsPath: computed.readOnly('extra.defaultOptionsPath'),
  useOptionRecord: computed.readOnly('extra.useOptionRecord'),
  useRecordWithId: computed.readOnly('extra.useRecordWithId'),
  locationsPath: 'latlng',
  addressRecord: null,

  selected: null,

  geocodeAddressChange() {
    const record = this.get('addressRecord');
    const tomtomAddress = this.get('geocode.tomtomAddress');

    if (!isEmpty(tomtomAddress)) {
      this.convertTTAddressToModel(record, tomtomAddress);
      this.disableAddressSearchOnMap(record, record.type);
    }
  },

  validateAddresses() {
    const records = this.get('addresses');

    this.set('editModal.errors', []);

    records.forEach((record) => {
      if (
            (record.get('lat') === 0 ||
             record.get('lng') === 0)) {
          this.get('editModal.errors').pushObject('Address does not have lat/lng. Please select position on the map.');
      }
    });

  },

  init() {
    this._super(...arguments);

    this.set('addresses', this.get('value'));

    const defaultOptionsPath = this.get('defaultOptionsPath');

    if(this.get('record') !== null &&
      this.get('record') !== undefined &&
      !isNone(defaultOptionsPath) &&
      this.get('record').get(defaultOptionsPath) !== null &&
      this.get('record').get(defaultOptionsPath) !== undefined) {
      this.set('aliasOptions',this.get('record').get(defaultOptionsPath));
      this.set('addressOptions',this.get('record').get(defaultOptionsPath));
    }
  },

  addressSearchResults(results) {
    const emberResults = [];
    let defaultOptions = this.get('record').get(this.get('defaultOptionsPath'));

    results.forEach(result => {
      emberResults.push(EmberObject.create(result));
    });

    defaultOptions.forEach(option => {
      emberResults.push(option);
    });

    this.set('addressOptions', emberResults);
  },

  searchAddress: task(function * (term) {
    yield tomtom.fuzzySearch()
      .key(ENV.tomtom.search.searchKey)
      .query(term)
      .countrySet(ENV.tomtom.search.countrySet)
      .typeahead(true)
      .center(ENV.tomtom.search.center)
      .radius(ENV.tomtom.search.radius)
      .callback(this.addressSearchResults.bind(this))
      .go();
  }),

  convertTTAddressToModel: function(record, ttAddress) {
    let useOptionRecord = this.get('useOptionRecord');

    // if((ttAddress.id === undefined && ttAddress.id === null)
    //    || (ttAddress.position !== undefined && ttAddress.position !== null)) {
    if(isNone(ttAddress.id)) {
      let valuePath = this.get('streetNumberPath');
      let options = { record, valuePath, useOptionRecord };

      this.set('record.isForceDirty', true);

      this.get('onChange')(cleanFieldString(ttAddress.address.streetNumber), options);

      options.valuePath = this.get('streetNamePath');
      this.get('onChange')(cleanFieldString(ttAddress.address.streetName), options);

      options.valuePath = this.get('localityPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.municipality), options);

      options.valuePath = this.get('regionPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countrySubdivisionName), options);

      options.valuePath = this.get('subRegionPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countrySecondarySubdivision), options);

      options.valuePath = this.get('postalCodePath');
      this.get('onChange')(cleanFieldString(ttAddress.address.postalCode), options);

      options.valuePath = this.get('countryPath');
      this.get('onChange')(cleanFieldString(ttAddress.address.countryCode), options);

      options.valuePath = this.get('aliasPath');
      this.get('onChange')('', options);


      record.set('lat', ttAddress.position.lat);
      record.set('lng', ttAddress.position.lon);
      record.set('geoNode', ttAddress.position.geoNode);

      this.get('geocode').getGeonode(ttAddress.address.freeformAddress)
        .then((result) => {
          if (result.data.length > 0) {
            record.set('geoNode', result.data[0].geonode);
          }
        });
    } else {
      let valuePath = this.get('streetNumberPath');
    let options = { record, valuePath, useOptionRecord };

    this.set('record.isForceDirty', true);

    this.get('onChange')(cleanFieldString(ttAddress.streetNumber), options);

    options.valuePath = this.get('streetNamePath');
    this.get('onChange')(cleanFieldString(ttAddress.streetAddress), options);

    options.valuePath = this.get('localityPath');
    this.get('onChange')(cleanFieldString(ttAddress.locality), options);

    options.valuePath = this.get('regionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySubdivisionName), options);

    options.valuePath = this.get('subRegionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySecondarySubdivision), options);

    options.valuePath = this.get('postalCodePath');
    this.get('onChange')(cleanFieldString(ttAddress.postalCode), options);

    options.valuePath = this.get('countryPath');
    this.get('onChange')(cleanFieldString(ttAddress.countryCode), options);

    options.valuePath = this.get('aliasPath');
    this.get('onChange')(cleanFieldString(ttAddress.alias), options);

    options.valuePath = this.get('notesPath');
    this.get('onChange')(cleanFieldString(ttAddress.notes), options);

    record.set('lat', ttAddress.locations.firstObject.lat);
    record.set('lng', ttAddress.locations.firstObject.lng);
    record.set('geoNode', ttAddress.locations.firstObject.geoNode);

    this.get('geocode').getGeonode(ttAddress.tomtomFormattedAddress.address.freeformAddress)
      .then((result) => {
        if (result.data.length > 0) {
          record.set('geoNode', result.data[0].geonode);
        }

      });
    }
    this.get('geocode').activateGeocode(false, record ,record.type);
    this.validateAddresses();
    this.set('addressOptions',this.get('record').get(this.get('defaultOptionsPath')));
  },

  convertTTAliasAddressToModel: function(record, ttAddress) {

    let valuePath = this.get('streetNumberPath');
    let options = { record, valuePath };

    this.set('record.isForceDirty', true);

    this.get('onChange')(cleanFieldString(ttAddress.streetNumber), options);

    options.valuePath = this.get('streetNamePath');
    this.get('onChange')(cleanFieldString(ttAddress.streetAddress), options);

    options.valuePath = this.get('localityPath');
    this.get('onChange')(cleanFieldString(ttAddress.municipality), options);

    options.valuePath = this.get('regionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySubdivisionName), options);

    options.valuePath = this.get('subRegionPath');
    this.get('onChange')(cleanFieldString(ttAddress.countrySecondarySubdivision), options);

    options.valuePath = this.get('postalCodePath');
    this.get('onChange')(cleanFieldString(ttAddress.postalCode), options);

    options.valuePath = this.get('countryPath');
    this.get('onChange')(cleanFieldString(ttAddress.countryCode), options);

    options.valuePath = this.get('aliasPath');
    this.get('onChange')(cleanFieldString(ttAddress.alias), options);

    record.set('lat', ttAddress.locations.firstObject.lat);
    record.set('lng', ttAddress.locations.firstObject.lng);

    this.get('geocode').getGeonode(ttAddress.tomtomFormattedAddress.address.freeformAddress)
      .then((result) => {
        if (result.data.length > 0) {
          record.set('geoNode', result.data[0].geonode);
        }
      });


    this.validateAddresses();
  },

  clearAddressModel: function(record) {
    let valuePath = this.get('streetNumberPath');
    let options = { record, valuePath };

    this.get('onChange')('', options);

    options.valuePath = this.get('streetNamePath');
    this.get('onChange')('', options);

    options.valuePath = this.get('localityPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('regionPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('subRegionPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('postalCodePath');
    this.get('onChange')('', options);

    options.valuePath = this.get('countryPath');
    this.get('onChange')('', options);

    options.valuePath = this.get('aliasPath');
    this.get('onChange')('', options);

    record.set('lat', 0);
    record.set('lng', 0);
  },

  enableAddressSearchOnMap(record, type) {
    $('html,body,.tomtom-map').addClass('custom-cursor');
    this.get('geocode').activateGeocode(true, record ,type);
    this.set('addressRecord', record);
    this.addObserver('geocode.tomtomAddress', this.geocodeAddressChange);
  },

  disableAddressSearchOnMap(record, type) {
    $('html,body,.tomtom-map').removeClass('custom-cursor');
    this.get('geocode').activateGeocode(false,record,type);
    this.set('addressRecord', null);
    this.removeObserver('geocode.tomtomAddress', this.geocodeAddressChange);
  },

  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onAddressSelected(record, valuePath, value) {
      if (this.get('useRecordWithId') && !isNone(value.get('id'))) {
        this.set('record.place', value);
        this.set('addresses', [value]);
      }
      else {
         if (!insideZone([ value.get('position.lat'), value.get('position.lon') ])) {
          this.get('errorMessage').pushError({ detail:'Invalid Zone' });
          return;
        }

        this.convertTTAddressToModel(record, value);
      }
    },

    onAliasAddressSelected(record, valuePath, value) {
      this.convertTTAliasAddressToModel(record, value);
    },

    onPickOnMap(record) {
      const activeGeocode = this.get('geocode.activeGeocode');
      if (activeGeocode) {
        this.disableAddressSearchOnMap(record, record.type);
      }
      else {
        this.enableAddressSearchOnMap(record, record.type);
      }
    },

    onAddNewAddress(record, valuePath, modelName) {
      const newAddress = this.get('editModal').pushRecordValue(record, valuePath, modelName);
      this.get('editModal').pushRecordValue(newAddress, 'locations', 'rms-location');
    },

    onRemoveAddress(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },

    handleKeydown(record, valuePath, dropdown, e) {
      if (e.keyCode !== 13) { return; }

      let text = e.target.value;
      let options = { record, valuePath: 'streetAddress' };
      // address has been forced without geocodes
      // this.set('record.isForceDirty', true);
      this.clearAddressModel(record);

      // this.set('record.isForceDirty', true);
      this.get('onChange')(text, options);
      this.set('addressRecord', record);
      this.validateAddresses();
    }
  }
});
