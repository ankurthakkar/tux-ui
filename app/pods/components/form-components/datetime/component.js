import Component from '@ember/component';
import moment from 'moment';

const DATETIME_FORMAT = /^\d\d\d\d-\d\d-\d\d \d\d:\d\d (AM|PM)$/;

export default Component.extend({
  classNames: ['form-components-datetime'],

  value: null,
  disabled: false,

  actions: {
    onInput(value) {
      if (!DATETIME_FORMAT.test(value)) return;

      this.get('onChange')(moment(value, 'YYYY-MM-DD hh:mm A').toDate());
    }
  }
});
