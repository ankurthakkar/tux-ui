import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import moment from 'moment';
import ENV from '../../../../config/environment';
import { isNone } from '@ember/utils';

export default Component.extend({
  classNames: ['form-components-subscriptions'],
  editModal: service(),
  value: null,
  disabled: false,
  dayOfWeeks: null,

  subscriptions: computed.readOnly('value'),
  recurringPath: computed.readOnly('extra.recurringPath'),
  availabilityType: computed.readOnly('valuePath'),
  startTimePath: computed.readOnly('extra.startTimePath'),
  endTimePath: computed.readOnly('extra.endTimePath'),
  shiftStartPath: computed.readOnly('extra.shiftStartPath'),
  shiftEndPath: computed.readOnly('extra.shiftEndPath'),
  sundayPath: computed.readOnly('extra.sundayPath'),

  mondayPath: computed.readOnly('extra.mondayPath'),
  tuesdayPath: computed.readOnly('extra.tuesdayPath'),
  wednesdayPath: computed.readOnly('extra.wednesdayPath'),
  thursdayPath: computed.readOnly('extra.thursdayPath'),
  fridayPath: computed.readOnly('extra.fridayPath'),
  saturdayPath: computed.readOnly('extra.saturdayPath'),

  startDatePath: computed.readOnly('extra.startDateTimePath'),
  endDatePath: computed.readOnly('extra.endDateTimePath'),
  exStartDatePath: computed.readOnly('extra.excludeStartDateTimePath'),
  exEndDatePath: computed.readOnly('extra.excludeEndDateTimePath'),

  init() {

    this._super(...arguments);

    let recurring = this.get(`record.${this.get('valuePath')}.firstObject.${this.get('recurringPath')}`);
    this.set('dayOfWeeks', ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']);
    this.set('dateTimePlaceHolder', ENV.dateTimeFlatPickr);
    this.set('recurringOptions',['yes','no']);
    if (!isNone(recurring) || recurring === 'yes') {
      this.set('recurring','yes');
    }
    else {
      this.set('recurring','no');
    }

    this.set('frequencyOptions',['daily','weekly','montly']);
    this.set('frequency','daily');
    this.set('dailyDaysOptions',['Every Day','Every WeekDay']);
    this.set('dailyDays','Every Day');

    this.set('weekOfMonthOptions', ['first','second','Third','fourth','last']);
    this.set('dayOfWeekOfMonthOptions', ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']);

    this.set('datePlaceHolder', ENV.dateTimeFormat.dateMoment);
    this.set('timePlaceHolder', ENV.dateTimeFormat.timeMoment);
    this.set('dateFormat', ENV.dateTimeFormat.dateFlatPickr);

//    this.set('monthFrequency', ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']);

  },


  actions: {
    onInput(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value, options);
    },

    onChangeDate(record, valuePath, value) {
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(value[0], options);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        // $(`#${datePicker.element.id}`).focus();
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        // date format is for database so should not be from config
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }

      // $(datePicker.altInput.className).select();
      // $(datePicker.element).select();
      // $('.effective-start').select();
    },

    onChangeTime(record, valuePath, value) {
      const momentTime = moment(value[0]);
      let options = { record, valuePath };

      this.set('record.isForceDirty', true);
      this.get('onChange')(momentTime.format('HH:mm:ss'), options);
    },

    onCheckboxChange(record, valuePath) {
      let switchState = record.get(valuePath);
      let options = { record, valuePath };

      switchState = !switchState;
      this.set('record.isForceDirty', true);
      this.get('onChange')(switchState, options);
    },
    onDOWChange(record, values) {
      record.set('sunday', values.includes('sunday'));
      record.set('monday', values.includes('monday'));
      record.set('tuesday', values.includes('tuesday'));
      record.set('wednesday', values.includes('wednesday'));
      record.set('thursday', values.includes('thursday'));
      record.set('friday', values.includes('friday'));
      record.set('saturday', values.includes('saturday'));
    },

    onAddNewAvailability() {
      this.get('onCreate')();
    },

    onRecurringChange(record, value){
      record.set('recurring',value);
      this.set('recurring',value);
    },

    onFrequencyChange(record, value){
      record.set('frequency',value);
      this.set('frequency',value);
    },

    onRemoveAvailability(record, valuePath, value) {
      let options = { record, valuePath };
      this.get('onRemove')(value, options);
    },

    onDailyDaysChange(record, value){
      record.set('dailyDays',value);
    },
    onDailyOptionsChange(record, value){
      record.set('dailyDays',value);
    },
    onWOMChange(record, value){
      record.set('dailyDays',value);
    },
    onDOWOMChange(record, value){
      record.set('dailyDays',value);
    }

  }
});
