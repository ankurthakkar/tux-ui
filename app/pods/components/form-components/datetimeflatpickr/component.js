import moment from 'moment';
import Component from '@ember/component';
import ENV from '../../../../config/environment';

export default Component.extend({
  classNames: ['form-components-datetimeflatpickr'],

  value: null,
  disabled: false,
  defaultDate: null,
  maxDate:null,

  init(){
    this._super(...arguments);
    this.set('dateFormat', ENV.dateTimeFlatPickr);
  },
  actions: {
    onChangeDate(value) {
      this.get('onChange')(value[0]);
    },
    onClose(currentValue, stringValue, datePicker) {
      const currentValueMoment = moment(currentValue[0]);
      const newValueMoment = moment(stringValue);

      if (!newValueMoment._isValid) {
        // $(`#${datePicker.element.id}`).focus();
        this.set('editModal.errors', ['Date entered is invalid.']);
      }

      if (datePicker.config.allowInput && datePicker._input.value &&
          newValueMoment._isValid &&
          !currentValueMoment.isSame(newValueMoment)) {
        datePicker.setDate(newValueMoment.format('YYYY-MM-DD'), true);
      }

      // $(datePicker.altInput.className).select();
      // $(datePicker.element).select();
      // $('.effective-start').select();
    }
  }
});
