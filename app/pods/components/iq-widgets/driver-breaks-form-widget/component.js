import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { isNone } from '@ember/utils';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),
  errorMessage: service(),

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);
    records.forEach(record => {
      let promisedStartMoment = moment(record.get('promisedStart'));
      let promisedEndMoment = moment(record.get('promisedEnd'));

      if (isNone(record.get('breakType.id'))) {
        valid = false;
        this.get('editModal.errors').pushObject('Must select a break type.');
      }

      if (promisedStartMoment.isSame(promisedEndMoment)) {
        valid = false;
        this.get('editModal.errors').pushObject('Break Start and Break End cannot be the same.');
      }
    });

    return valid;
  },

  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      onUndoClick() {
        let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }
    }
});
