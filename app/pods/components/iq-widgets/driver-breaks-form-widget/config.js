export default {
  editableSections: [{
    title: 'Read Only',
    fields: [{
      id: 'id',
      type: 'text',
      label: 'ID',
      valuePath: 'id',
      editable: false
    }]
  }, {
    title: 'Editable',
    fields: [{
      id: 'type',
      type: 'searchable-enum',
      label: 'Type',
      valuePath: 'breakType',
      editable: true,
      extra: {
        optionModelName: 'break-type',
        optionSearchPath: 'name',
        optionSelectedPath: 'firstObject',
        optionIncludes: '',
        optionSearchEnabled: false
      }
    }, {
      id: 'place',
      type: 'locations',
      label: '',
      valuePath: 'places',
      modelName: 'place',
      isMultiType: true,
      extra: {
        aliasPath: 'alias',
        notesPath: 'notes',
        streetNumberPath: 'streetNumber',
        streetNamePath: 'streetAddress',
        localityPath: 'locality',
        regionPath: 'region',
        subRegionPath: 'subRegion',
        postalCodePath: 'postalCode',
        countryPath: 'country',
        defaultOptionsPath: 'route.garages',
        useRecordWithId: true,
        useOptionRecord: true
      }
    }, {
      id: 'promisedStart',
      type: 'datetimeflatpickr',
      label: 'Break Start',
      valuePath: 'promisedStart'
    }, {
      id: 'promisedEnd',
      type: 'datetimeflatpickr',
      label: 'Break End',
      valuePath: 'promisedEnd'
    }]
  }]
};
