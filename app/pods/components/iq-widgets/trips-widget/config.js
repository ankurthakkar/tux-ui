export default {
  component: 'iq-widgets/trips-widget',
  rowComponent: 'table/rows/otp-formatted-row',
  modelName: 'iq-trip',

  defaultSortId: 'id',
  defaultSortAsc: false,

  title: 'Trips',

  columns: [
  {
    id: 'id',
    type: 'uuid',
    label: 'ID',
    valuePath: 'id',
    valuePreview: '1',
    hidden: false,
    isMapLabelVisible: true,
    searchable: true,
    defaultWidth: 60
  },
  {
    id: 'riderFirstName',
    type: 'text',
    label: 'Passenger First Name',
    valuePath: 'bsBooking.rider.firstName',
    hidden: false,
    searchable: true,
    defaultWidth: 75
  },
  {
    id: 'riderLastName',
    type: 'text',
    label: 'Passenger Last Name',
    valuePath: 'bsBooking.rider.lastName',
    hidden: false,
    searchable: true,
    defaultWidth: 75
  },
  {
    id: 'riderPhone',
    type: 'text',
    label: 'Passenger Phone Number',
    valuePath: 'bsBooking.rider.phone',
    hidden: false,
    searchable: true,
    defaultWidth: 70
  },
  {
    id: 'externalId',
    type: 'text',
    label: 'Passenger ID',
    valuePath: 'dsTrip.externalId',
    editable: true,
    hidden: true,
    defaultWidth: 60
  },
  {
    id: 'tripType',
    type: 'text',
    label: 'Type',
    valuePath: 'dsTrip.type',
    editable: true,
    hidden: true,
    defaultWidth: 60
  },
  {
    id: 'routeName',
    type: 'text',
    label: 'Route Name',
    valuePath: 'dsTrip.cluster.route.name',
    hidden: false,
    searchable: true,
    defaultWidth: 70
  },
  {
    id: 'status',
    type: 'text',
    label: 'Status',
    valuePath: 'status',
    valuePreview: 'Planned',
    hidden: false,
    isMapLabelVisible: true,
    defaultWidth: 75
  },
  {
    id: 'statusReason',
    type: 'text',
    label: 'Status Reason',
    valuePath: 'bsBooking.statusReason',
    hidden: false,
    defaultWidth: 75
  },
  {
    id: 'statusNotes',
    type: 'text',
    label: 'Status Notes',
    valuePath: 'bsBooking.statusNotes',
    hidden: false,
    defaultWidth: 75
  },
  {
    id: 'otp',
    type: 'text',
    label: 'OTP Status',
    valuePath: 'otpLabel',
    valuePreview: 'On Time',
    hidden: false,
    highlightable: true
  },
  {
    id: 'promisedTime',
    type: 'datetime',
    label: 'Promise Time',
    valuePath: 'bsBooking.promisedTime',
    valuePreview: '09:00 AM',
    editable: true,
    format: 'YYYY-MM-DD hh:mm A',
    defaultWidth: 50
  },
  {
    id: 'requestedTime',
    type: 'time',
    label: 'Request Time',
    valuePath: 'bsBooking.requestedTime',
    valuePreview: '08:00 AM',
    editable: true,
    hidden: false,
    format: 'HH:mm A',
    defaultWidth: 50
  },
  {
    id: 'anchorETA',
    type: 'time',
    label: 'Anchor ETA',
    valuePath: 'dsTrip.anchorStop.eta',
    valuePreview: '16',
    hidden: false,
    format: 'HH:mm A',
    defaultWidth: 50
  },
  {
    id: 'serviceStartTime',
    type: 'date',
    label: 'Service Start Time',
    valuePath: 'dsTrip.serviceWindowStartTime',
    valuePreview: '?',
    editable: true,
    hidden: true,
    format: 'HH:mm A',
    defaultWidth: 50
  },
  {
    id: 'serviceEndTime',
    type: 'date',
    label: 'Service End Time',
    valuePath: 'dsTrip.serviceWindowEndTime',
    valuePreview: '?',
    editable: true,
    hidden: true,
    format: 'HH:mm A',
    defaultWidth: 50
  },
  {
    id: 'anchor',
    type: 'text',
    label: 'Anchor',
    valuePath: 'bsBooking.anchor',
    valuePreview: 'Pick',
    editable: true,
    hidden: true,
    defaultWidth: 60
  },
  {
    id: 'pickupaddress',
    type: 'text',
    label: 'Pickup Address',
    valuePath: 'bsBooking.pick.location.streetAddress',
    editable: true,
    hidden: true,
    defaultWidth: 80
  },
  {
    id: 'pickupCity',
    type: 'text',
    label: 'Pickup City',
    valuePath: 'bsBooking.pick.location.city',
    editable: true,
    hidden: true,
    defaultWidth: 80
  },
  {
    id: 'dropaddress',
    type: 'text',
    label: 'Drop off Address',
    valuePath: 'bsBooking.drop.location.streetAddress',
    editable: true,
    hidden: true,
    defaultWidth: 80
  },
  {
    id: 'dropCity',
    type: 'text',
    label: 'Drop off Address',
    valuePath: 'bsBooking.drop.location.city',
    editable: true,
    hidden: true,
    defaultWidth: 80
  },
  {
    id: 'passengerNotes',
    type: 'text',
    label: 'Passenger Notes',
    valuePath: 'bsBooking.rider.notes',
    editable: true,
    hidden: true,
    searchable: true,
    defaultWidth: 80
  },
  {
    id: 'notes',
    type: 'text',
    label: 'Driver Notes',
    valuePath: 'dsTrip.notes',
    editable: true,
    hidden: true,
    searchable: true,
    defaultWidth: 50
  },
  {
    id: 'travelNeeds',
    type: 'number',
    label: 'Travel Needs',
    valuePath: 'dsTrip.noOfTravelNeeds',
    editable: true,
    hidden: true,
    defaultWidth: 50
  },
    //STUB FOR AVA DEMO
  {
    id: 'plannedMiles',
    type: 'number',
    label: 'Planned Trip Miles',
    valuePath: 'dsTrip.plannedMiles',
    editable: true,
    hidden: true,
    defaultWidth: 50
  },
    //STUB FOR AVA DEMO
  {
    id: 'actualMiles',
    type: 'number',
    label: 'Actual Trip Miles',
    valuePath: 'dsTrip.actualMiles',
    editable: true,
    hidden: true,
    defaultWidth: 50
  },

  {
    id: 'fare',
    type: 'text',
    label: 'Fare',
    valuePath: 'dsTrip.fare',
    editable: true,
    hidden: true,
    defaultWidth: 50
  },
  {
    id: 'paymentType',
    type: 'text',
    label: 'Payment Type',
    valuePath: 'dsTrip.fareType.name',
    valuePreview: 'Single',
    editable: true,
    hidden: true,
    defaultWidth: 50
  }
  ]
}
