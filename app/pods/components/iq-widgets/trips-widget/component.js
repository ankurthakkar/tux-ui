import IQUXColumnWidget from '../column-widget/component';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { findRowRecordByElement, findRowElement } from 'adept-iq/utils/find-row-record';
import { isBlank } from '@ember/utils';
import ENV from 'adept-iq/config/environment';

const INCLUDES = [
  'rider',
  'cluster.route',
  'stops',
  'stops.place',
  'rider.travelNeeds',
  'rider.travelNeeds.travelNeedType',
  'cluster',
  'cluster.route,stops.stopEvents',
  'fareType',
  'serviceWindow'
];

export default IQUXColumnWidget.extend({
  dragCoordinator: service(),
  store: service(),
  classNames: ['trips-widget'],
  editModal: service(),
  ajax: service(),
  booking:service(),
  errorMessage: service(),

  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,
  session: service(),


  noShowComponent: 'iq-widgets/noshow-event-form-widget',
  newTripComponent: 'iq-widgets/edit-trip-form-widget',
  cancelTripComponent: 'iq-widgets/cancel-type-form-widget',
  assignTrip2RouteComponent: 'iq-widgets/assign-trip-2-route-widget',

  routeexecevent: service(),

  saveTripRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        let bookingData =  this.get('booking').bookingData();
        let preparedBookingData =  this.get('booking').prepareBookingData(bookingData,record);
        preparedBookingData.data.id = record.get('id');

        let session = this.get('session');
        var json = JSON.stringify(preparedBookingData);

        return this.get('ajax').put(ENV.API.avlmBookingService.host+'/production/booking/'+record.get('id'), {
          method: 'PUT',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('bs-booking');
        }).catch(function(error) {
         reject(error);
        });
      })
    });
  },


  saveCancelRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {

        let cancelData =  this.get('routeexecevent').cancelData();

        let prepareCancelData =  this.get('routeexecevent').prepareTripCancelData(cancelData,record);

        var json = JSON.stringify(prepareCancelData);
        let session = this.get('session');

        return this.get('ajax').delete(ENV.API.avlmBookingService.host+'/production/cancel', {
          method: 'DELETE',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('route');
          me.sendMessagesToVehicles(record,'No Show');
        }).catch(function(error) {
          reject(error);
        });
      })
    });
  },

  sendMessageAssignTrip2RouteFunction(record, status, dsTripId) {


    let assignedDriver = record.get('assignRoute.assignedDriver');
    let assignedVehicle = record.get('assignRoute.assignedVehicle');

    let message = ` Trip ${status} for ${record.get('bsBooking.rider.firstName')} ${record.get('bsBooking.rider.firstName')},
                Pickup: ${record.get('bsBooking.pick.location.streetAddress')} , ${record.get('bsBooking.requestedTime')} ,
                Dropoff: ${record.get('bsBooking.drop.location.streetAddress')} `;

    let draftMessage = this.get('store').createRecord('tm-canned-message', {
      driverId: assignedDriver.get('externalId'),
      vehicleId: assignedVehicle.get('name'),
      routeId: `${record.get('assignRoute.id')}`,
      replyToMsgId:`${dsTripId}`,
      body:message
    });
    return draftMessage.save();
  },

  sendMessageTripMovefromRouteFunction(record, status, dsTripId) {

    let assignedDriver = record.get('dsTrip.cluster.route.assignedDriver');
    let assignedVehicle = record.get('dsTrip.cluster.route.assignedVehicle');


    let message = ` Trip ${status} for ${record.get('bsBooking.rider.firstName')} ${record.get('bsBooking.rider.firstName')},
                Pickup: ${record.get('bsBooking.pick.location.streetAddress')} , ${record.get('bsBooking.requestedTime')} ,
                Dropoff: ${record.get('bsBooking.drop.location.streetAddress')} `;

    let draftMessage = this.get('store').createRecord('tm-canned-message', {
      driverId: assignedDriver.get('externalId'),
      vehicleId: assignedVehicle.get('name'),
      routeId: `${record.get('dsTrip.cluster.route.id')}`,
      replyToMsgId:`${dsTripId}`,
      body:message
    });

    return draftMessage.save();
  },


  sendMessagesToVehicles(record, status) {

    let assignedDriver = record.get('dsTrip.cluster.route.assignedDriver');
    let assignedVehicle = record.get('dsTrip.cluster.route.assignedVehicle');
    let message = ` ${status} action performed by dispatch for ${record.get('bsBooking.rider.firstName')} ${record.get('bsBooking.rider.firstName')},
                Pick-up: ${record.get('bsBooking.pick.location.streetAddress')} , ${record.get('bsBooking.requestedTime')} `;

    let draftMessage = this.get('store').createRecord('tm-canned-message', {
      // TODO: should this be `driver.externalId`?
      driverId: assignedDriver.get('externalId'),
      vehicleId: assignedVehicle.get('name'),
      replyToMsgId:`${record.get('id')}`,
      body:message
      // TODO: how do we get the current route ID for a vehicle?
      // routeId: driver.get('routeId')
    });
    return draftMessage.save();
  },
  saveAssignTrip2RouteFunction(records) {
    let me = this;
    let bsBookingId = '';
    let dsTripId = '';
    let updateRecord = null;

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        bsBookingId = record.get('id');
        updateRecord = record;
        let bookingScheduleData =  this.get('booking').bookingScheduleData();

        bookingScheduleData.data.id = bsBookingId;// external id is BS booking id
        bookingScheduleData.included = []; // a work around because service rejects this request without it.

        let session = this.get('session');
        var json = JSON.stringify(bookingScheduleData);

        return this.get('ajax').put(ENV.API.bookingService.host+'/booking/'+bsBookingId+'/export/', {
          method: 'PUT',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {

          record.set('isForceDirty', false);
          dsTripId = response.data[0].id;

          return this.get('ajax').put(ENV.API.dispatchService.host+`/route/`+record.get('assignRoute.id')+'/tripTransfer/'+dsTripId, {
            method: 'PUT',
            contentType: "application/json",
            headers: {
              "Authorization": `Bearer ${session.data.authenticated.token}`
            }
          }).then((response) => {
            record.set('isForceDirty', false);
            me.get('activeContext').refreshTableContent('trip');
            me.get('errorMessage').pushInfo({ detail: `Trip '${bsBookingId}' successfully transferred to Route '${record.get('assignRoute.name')}'`});
            me.sendMessageAssignTrip2RouteFunction(record,'Added',dsTripId);
          }).catch(function(error) {
            updateRecord.set('isForceDirty', false);
            me.get('activeContext').refreshTableContent('trip');
            me.get('errorMessage').pushInfo({ detail: `Trip '${bsBookingId}' successfully transferred to Route '${record.get('assignRoute.name')}'`});
            me.sendMessageAssignTrip2RouteFunction(record,'Added',dsTripId);
            //reject(error);
          });
        }).catch(function(error) {
          reject(error);
        });
      })
    });
  },


  saveMoveTrip2RouteFunction(records) {
    let me = this;
    let bsBookingId = '';
    let dsTripId = '';

    return new Promise((resolve, reject) => {
      let session = this.get('session');
      records.forEach(record => {
          bsBookingId = record.get('id');
          dsTripId = record.get('dsTrip.id');

          return this.get('ajax').put(ENV.API.dispatchService.host+`/route/`+record.get('assignRoute.id')+'/tripTransfer/'+dsTripId, {
            method: 'PUT',
            contentType: "application/json",
            headers: {
              "Authorization": `Bearer ${session.data.authenticated.token}`
            }
          }).then((response) => {
            record.set('isForceDirty', false);
            me.get('activeContext').refreshTableContent('trip');
            me.get('errorMessage').pushInfo({ detail: `Trip '${bsBookingId}' successfully transferred to Route '${record.get('assignRoute.name')}'`});
            me.sendMessageTripMovefromRouteFunction(record,'Remove',dsTripId);
            me.sendMessageAssignTrip2RouteFunction(record,'Added',dsTripId);
          }).catch(function(error) {
            reject(error);
            me.sendMessageTripMovefromRouteFunction(record,'Remove',dsTripId);
            me.sendMessageAssignTrip2RouteFunction(record,'Added',dsTripId);
          });
        })
    });
  },
  saveNoShowRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        let noShowData =  this.get('routeexecevent').noShowData();
        let prepareNoShowData =  this.get('routeexecevent').prepareTripNoShowData(noShowData,record);

        var json = JSON.stringify(prepareNoShowData);
        let session = this.get('session');

        return this.get('ajax').post(ENV.API.avlmService.host+'/production/route-exec-event', {
          method: 'POST',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('route');
          me.sendMessagesToVehicles(record,'No Show');
        }).catch(function(error) {
          reject(error);
        });
      })
    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'Table Action',
      action: function() {
        alert('I still work');
      }
    }]);


    //TODO
    this.set('scheduleTripAction', {
      name: 'Assign to Route',
        action: (model) => {
          model.set('assignRoute',null);
          let assignTrip2RouteComponent = this.get('assignTrip2RouteComponent');
          this.get('editModal').open(assignTrip2RouteComponent, [model], this.get('saveAssignTrip2RouteFunction').bind(this));
        }
      });

    this.set('noShowTripAction', {
      name: 'No Show',
      action: (model) => {

        let noShowReasonCodes = this.get('store').peekAll('no-show-reason-code');
        model.set('noShowReason',noShowReasonCodes.firstObject);
        model.set('timestamp',new Date());

        let noShowComponent = this.get('noShowComponent');
        this.get('editModal').open(noShowComponent, [model], this.get('saveNoShowRecordFunction').bind(this));
      }
    });

    this.set('cancelTripAction', {
      name: 'Cancel',
      action: (model) => {
        let cancelTypes = this.get('store').peekAll('cancel-type');
        model.set('cancelType',cancelTypes.firstObject);
        model.set('timestamp',new Date());

        let cancelTripComponent = this.get('cancelTripComponent');
        this.get('editModal').open(cancelTripComponent, [model], this.get('saveCancelRecordFunction').bind(this));
      }
    });

    this.set('editTripAction', {
      name: 'Edit',
      action: (model) => {
          console.log('model ',model);
          console.log('bs-leg',model);
          let updateBooking = model.get('bsBooking');

          console.log('updateBooking',updateBooking);

          let leg = updateBooking.get('leg');

          //DEMO HACK
          let segment;
          if (leg) {
            let segment = leg.get('segment');
          }

          let rider = leg.get('rider');
          this.store.findRecord('rms-rider',rider.get('riderId')).then((bookingRider) => {

          let originlocations = [];
          let origin = leg.get('origin');
          origin.set('type','pick');
          originlocations.push(origin);

          let destinationlocations = [];
          let destination = leg.get('destination');
          destination.set('type','drop');
          destinationlocations.push(destination);

          let subscriptions = [];
          let subscription = this.get('store').createRecord('bs-leg-subscription');
          subscriptions.push(subscription);

          updateBooking.set('originlocations', originlocations);
          updateBooking.set('destinationlocations', destinationlocations);

          updateBooking.set('subscriptions', subscriptions);
          updateBooking.set('riderAddresses', bookingRider.get( 'riderAddresses'));
          updateBooking.set('eligibilities', bookingRider.get('eligibilities'));

          if (leg) {
            updateBooking.set('requestedTime', leg.get('requestTime'));
            updateBooking.set('anchor', leg.get('anchor'));
            updateBooking.set('legTravelNeeds', leg.get('legTravelNeeds'));
          }

          if (segment) {
            updateBooking.set('fareType', segment.get('fareType'));
            updateBooking.set('fare', segment.get('fare'));
          }

          console.log('Prepare updateBooking',updateBooking);

          this.get('records').pushObject(updateBooking);
          let newTripComponent = this.get('newTripComponent');
          this.get('editModal').open(newTripComponent, [updateBooking], this.get('saveTripRecordFunction').bind(this));
            });


          }
        });


    this.set('moveTripAction',
      {
        name: 'Move Trip',
        action: (model) => {
          model.set('assignRoute',null);
          let assignTrip2RouteComponent = this.get('assignTrip2RouteComponent');
          this.get('editModal').open(assignTrip2RouteComponent, [model], this.get('saveMoveTrip2RouteFunction').bind(this));
        }
      });
    this.set('waitlistTripAction',
      {
        name: 'Waitlist',
        action: (/* model */) => {
      }
    });
    this.set('viewActivityLogAction',
      {
        name: 'Activity Log',
        action: (/* model */) => {
      }
    });
    this.set('singleActions', [
      {
        name: 'Move Trip',
        action: (model) => {
          model.set('assignRoute',null);
          let assignTrip2RouteComponent = this.get('assignTrip2RouteComponent');
          this.get('editModal').open(assignTrip2RouteComponent, [model], this.get('saveMoveTrip2RouteFunction').bind(this));
        }
      },
      {
        name: 'Waitlist',
        action: (/*model*/) => {


        }
      },
      {
        name: 'Assign to Route',
        action: (model) => {
          model.set('assignRoute',null);
          let assignTrip2RouteComponent = this.get('assignTrip2RouteComponent');
          this.get('editModal').open(assignTrip2RouteComponent, [model], this.get('saveAssignTrip2RouteFunction').bind(this));
        }
      },
      {
        name: 'No Show',
        action: (model) => {

          let noShowReasonCodes = this.get('store').peekAll('no-show-reason-code');
          model.set('noShowReason',noShowReasonCodes.firstObject);
          model.set('timestamp',new Date());

          let noShowComponent = this.get('noShowComponent');
          this.get('editModal').open(noShowComponent, [model], this.get('saveNoShowRecordFunction').bind(this));

        }
      },
      {

        name: 'Cancel Trip',
        action: (model) => {
          let cancelTypes = this.get('store').peekAll('cancel-type');
          model.set('cancelType',cancelTypes.firstObject);
          model.set('timestamp',new Date());

          let cancelTripComponent = this.get('cancelTripComponent');
          this.get('editModal').open(cancelTripComponent, [model], this.get('saveCancelRecordFunction').bind(this));

        }
      },
      {

        name: 'Edit',
        action: (model) => {
          console.log('model ',model);
          console.log('bs-leg',model);

          let updateBooking = model.get('bsBooking');

          console.log('updateBooking',updateBooking);

          let leg = updateBooking.get('leg');
          if (leg) {
            let segment = leg.get('segment');
          }
          let rider = leg.get('rider');
          this.store.findRecord('rms-rider',rider.get('riderId')).then((bookingRider) => {

          let originlocations = [];
          let origin = leg.get('origin');
          origin.set('type','pick');
          originlocations.push(origin);

          let destinationlocations = [];
          let destination = leg.get('destination');
          destination.set('type','drop');
          destinationlocations.push(destination);

          let subscriptions = [];
          let subscription = this.get('store').createRecord('bs-leg-subscription');
          subscriptions.push(subscription);

          updateBooking.set('originlocations', originlocations);
          updateBooking.set('destinationlocations', destinationlocations);
          updateBooking.set('requestedTime', leg.get('requestTime'));
          updateBooking.set('subscriptions', subscriptions);
          updateBooking.set('anchor', leg.get('anchor'));
          updateBooking.set('fare', segment.get('fare'));
          updateBooking.set('legTravelNeeds', leg.get('legTravelNeeds'));
          updateBooking.set('riderAddresses', bookingRider.get( 'riderAddresses'));
          updateBooking.set('eligibilities', bookingRider.get('eligibilities'));
          updateBooking.set('fareType', segment.get('fareType'));

          console.log('Prepare updateBooking',updateBooking);

          this.get('records').pushObject(updateBooking);
          let newTripComponent = this.get('newTripComponent');
          this.get('editModal').open(newTripComponent, [updateBooking], this.get('saveTripRecordFunction').bind(this));
            });
        }
      },
    {
      name: 'View Activity Log',
      action: (/* models */) => {

      }
    }]);


    this.set('bulkActions', [{
      name: 'Bulk Edit',
      action: (/* models */) => {

      }
    },
    {
      name: 'Activity Log',
      action: (/* models */) => {

      }
    }]);
  },

  rowGroup: computed('row', function() {
    return this.get('table.rows');
  }).readOnly(),

  dragOver(event) {
    event.preventDefault();
    if (this.get('dragCoordinator.widgetType') !== 'routes') return;
    let rowElement = findRowElement(event.target);
    // User might drop onto widget's other elements.
    if (isBlank(rowElement) || isBlank(rowElement.id)) return;
    this.$(`#${rowElement.id}`).addClass('drag-target');
  },

  dragLeave(event) {
    event.preventDefault();

    let rowElement = findRowElement(event.target);
    if (isBlank(rowElement) || isBlank(rowElement.id)) return;
    this.$(`#${rowElement.id}`).removeClass('drag-target');
  },

  drop(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.get('dragCoordinator.widgetType') !== 'routes') return;

    let rowElement = findRowElement(event.target);
    if (isBlank(rowElement) || isBlank(rowElement.id)) return;
    this.$(`#${rowElement.id}`).removeClass('drag-target');

    let sourceRowRecord = this.get('dragCoordinator').getSourceRow(event.dataTransfer.getData('text'));
    if (isBlank(sourceRowRecord)) return;

    let targetRowRecord = findRowRecordByElement(event.target, this.get('rowGroup'));

    this.assignCluster(targetRowRecord, sourceRowRecord);
  },

  assignCluster(tripRow, routeRow) {
    let tripRecord = tripRow.get('record');
    let routeRecord = routeRow.get('record');
    if (routeRecord.constructor.modelName !== 'route') {
      return;
    }

    let cluster = routeRecord.get('clusters').firstObject;
    if (isBlank(cluster)) {
      cluster = this.get('store').createRecord('cluster', {
        route: routeRecord
      });
      tripRecord.set('cluster', cluster);
      cluster.save();
    } else {
      tripRecord.set('cluster', cluster);
      tripRecord.save();
    }
  },
  //TODO
  singleActions: computed('table.rows.@each.{selected,status}', function() {
    let scheduleTripAction = this.get('scheduleTripAction');
    let noShowTripAction = this.get('noShowTripAction');
    let cancelTripAction = this.get('cancelTripAction');
    let editTripAction = this.get('editTripAction');
    let moveTripAction = this.get('moveTripAction');
    let waitlistTripAction = this.get('waitlistTripAction');
    let viewActivityLogAction = this.get('viewActivityLogAction');


    let row = this.get('table.rows');
    if (!row) return [];

    switch(row.get('status')) {
      case 'Scheduled': {
        return [moveTripAction, waitlistTripAction, noShowTripAction, cancelTripAction, editTripAction, viewActivityLogAction];
      }
      case 'WaitList': {
        return [scheduleTripAction, cancelTripAction, editTripAction , viewActivityLogAction];
      }
      default: {
        return [viewActivityLogAction];
      }

    }
  })
});
