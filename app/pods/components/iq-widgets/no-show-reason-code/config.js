export default {

  editableSections: [ {
    title: 'Read Only',
    fields: [{
      id: 'vehicle',
      type: 'text',
      label: 'Vehicle',
      valuePath: 'assignedVehicle.id',
      editable: false
    }]
  },{
    title: 'Editable',
    fields: [{
      id: 'timestamp',
      type: 'datetime',
      label: 'Timestamp',
      valuePath: 'timestamp'
    },
    {
      id: 'odometer',
      type: 'text',
      label: 'Odometer',
      valuePath: 'odometer'
    }]
  }]
};
