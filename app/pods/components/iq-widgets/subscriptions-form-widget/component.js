import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { isNone } from '@ember/utils';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),
  errorMessage: service(),
  store: service(),

  init() {

    this._super(...arguments);

    let records = this.get('editableRecords');

    records.forEach(record => {
      if (record.get('exclusions').length === 0) {
        let exclusionRecord = this.get('store').createRecord('bs-subscription-exclusion');

        exclusionRecord.set('subscription', record);
        record.get('exclusions').pushObject(exclusionRecord);
      }
    });

  },

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);

    records.forEach(record => {
      const requestedTime = moment(record.get('requestedTime'));
      let recurrencePatterns = record.recurrencePatterns;

      if (isNone(requestedTime)) {
        valid = false;
        this.get('editModal.errors').pushObject('Date should be there');
      }

      recurrencePatterns.forEach(recurrencePattern => {
        if (recurrencePattern.type === 'monthly') {
          if (isNone(recurrencePattern.weekOfMonth) &&
            isNone(recurrencePattern.dayOfMonth)) {
            valid = false;
            this.get('editModal.errors').pushObject('Day of month and week of month cannot be both blank.');
          }
          else if (!isNone(recurrencePattern.weekOfMonth) &&
            (recurrencePattern.selectedDOWs.length < 1)) {
            valid = false;
            this.get('editModal.errors').pushObject('Select day of the week.');
          }
        }
      });

    });

    return valid;
  },

  actions: {
    // override undo because we will have to deal with undoing created
    // models for addresses, travel needs, and eligibility later.
    // IQUX-510
    onUndoClick() {
      let lastUndoState = this.get('editModal.undoHistory').popObject();

      if (lastUndoState === null) {
        let records = this.get('editableRecords');

        records.forEach(record => {
          record.set('isForceDirty', false);
        });

      }
      else {
        lastUndoState.forEach(({ record, properties }) => {
          record.setProperties(properties);
        });
      }

      this.set('isLastUndoStateCommitted', true);
    },

    onApplyClick() {
      if (this.validateRecords()) {
        this.get('service').apply();
      }
    }
  }
});
