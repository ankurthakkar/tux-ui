import tomtom from 'tomtom';
import IconPaths from 'adept-iq/config/icon-paths';

export default {
  location: {
    icon: tomtom.L.svgIcon({
      icon: {
        icon: 'fa fa-building',
        iconSize: [32, 37],
        iconAnchor: [16, 2],
        style: {
          color: '#fff'
        },
        noPlainSVG: true,
      }
    })
  },
  locationLate: {
    icon: tomtom.L.svgIcon({
      icon: {
        icon: 'fa fa-building',
        iconSize: [32, 37],
        iconAnchor: [16, 2],
        style: {
          color: '#fff'
        },
        noPlainSVG: true,
      },
      background: {
        icon: 'icon-marker-red'
      }
    })
  },
  locationDanger: {
    icon: tomtom.L.svgIcon({
      icon: {
        icon: 'fa fa-building',
        iconSize: [32, 37],
        iconAnchor: [16, 2],
        style: {
          color: '#fff'
        },
        noPlainSVG: true,
      },
      background: {
        icon: 'icon-marker-orange'
      }
    })
  },
  pick: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.future,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  pickLate: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.futureLate,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  pickDanger: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.futureInDanger,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  drop: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.future,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  dropLate: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.futureLate,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  dropDanger: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.futureInDanger,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  break: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.stops.break,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  vehicle: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.vehicles.vehicle,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  vehicleDriving: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.vehicles.atWork,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  vehicleLate: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.vehicles.vehicle,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  },
  vehicleDanger: {
    icon: tomtom.L.icon({
        iconUrl: IconPaths.vehicles.vehicle,
        iconSize: [32, 32],
        iconAnchor: [16, 16],
    })
  }
};
