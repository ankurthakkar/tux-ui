import IconPaths from 'adept-iq/config/icon-paths';
import tomtom from 'tomtom';

export default {
  tripArrowPlanned: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrow.ontime,
      iconSize: [20, 20],
      iconAnchor: [20, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  },
  tripArrowPlannedDanger: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrow.danger,
      iconSize: [20, 20],
      iconAnchor: [20, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  },
  tripArrowPlannedLate: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrow.late,
      iconSize: [20, 20],
      iconAnchor: [20, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  },
  tripArrowPlannedOffset: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrowOffset.ontime,
      iconSize: [35, 20],
      iconAnchor: [35, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  },
  tripArrowPlannedOffsetLate: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrowOffset.late,
      iconSize: [35, 20],
      iconAnchor: [35, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  },
  tripArrowPlannedOffsetDanger: {
    icon: tomtom.L.icon({
      iconUrl: IconPaths.tripArrowOffset.danger,
      iconSize: [35, 20],
      iconAnchor: [35, 10]
    }),
    rotationAngle: null,
    rotationOrigin: 'center right'
  }
}