import DataJoin from 'adept-iq/utils/data-join';
import Evented from '@ember/object/evented';
import tomtom from 'tomtom';

export default DataJoin.extend(Evented, {
  map: null,

  bindMouseEvents(layer) {
    layer.on('click', e => {
      let { modelName, modelId, isActive } = e.target.options;
      if (!isActive) return;

      this.trigger('click', modelName, modelId, e);
    });

    layer.on('dblclick', e => {
      // Prevent double clicks on polylines from triggering map zoom.
      tomtom.L.DomEvent.stop(e);

      let { modelName, modelId, isActive } = e.target.options;
      if (!isActive) return;

      this.trigger('dblClick', modelName, modelId, e);
    });

    layer.on('contextmenu', e => {
      let { modelName, modelId, isActive } = e.target.options;
      if (!isActive) return;

      this.trigger('contextMenu', modelName, modelId, e);
    });
  }
});
