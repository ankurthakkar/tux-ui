import DataJoin from './base';
import tomtom from 'tomtom';
import DEFAULTS from '../config/polyline';
import { isEmpty } from '@ember/utils';
import createArrow from '../decorations/arrow';
import ARROWS from '../config/arrow';
import lodash from 'lodash';

export default DataJoin.extend({
  enter(node) {
    let points = node.get('points');
    if (isEmpty(points)) return;

    this.createPolylineFor(node);
  },

  update(node) {
    let points = node.get('points');
    let polyline = node.get('polyline');

    if (isEmpty(points)) {
      if (polyline) {
        this.removePolylineFor(node);
      }
      return;
    }

    if (!polyline) {
      this.createPolylineFor(node);
    }

    this.updatePolylineFor(node);
  },

  exit(node) {
    if (node.get('polyline')) {
      this.removePolylineFor(node);
    }
  },

  createPolylineFor(node) {
    let label = node.get('label');
    let record = node.get('record');
    let style = node.get('style');
    let defaultOptions = DEFAULTS[style] || {};
    let nodeOptions = node.get('options') || {};
    let modelName = record.constructor.modelName;

    let options = lodash.merge(defaultOptions, nodeOptions, {
      label,
      opacity: node.get('opacity'),

      // for active context clicks
      isActive: node.get('isActive'),
      modelId: record.get('id'),
      modelName
    });

    let points = node.get('points');
    let polyline = tomtom.L.polyline(points, options);

    polyline.bindPopup(label, { closeButton: false });

    this.bindMouseEvents(polyline);

    node.set('polyline', polyline);

    let map = this.get('map');
    polyline.addTo(map);

    let arrowStyle = node.get('arrowStyle');
    if (arrowStyle && points.length > 1) {
      let arrowPoints = points.slice(points.length - 2, points.length);
      let arrowOptions = ARROWS[arrowStyle];

      let arrow = createArrow(arrowPoints, arrowOptions);
      arrow.addTo(map);
      node.set('arrow', arrow);
    }
  },

  updatePolylineFor(node) {
    let opacity = node.get('opacity');

    let polyline = node.get('polyline');
    if (polyline) {
      let points = node.get('points');
      let label = node.get('label');
      let style = node.get('style');

      let defaultOptions = DEFAULTS[style] || {};
      let nodeOptions = node.get('options') || {};
      let options = lodash.merge(defaultOptions, nodeOptions, {
        opacity
      });

      // polyline does not have `setOpacity` method
      polyline.setLatLngs(points);
      polyline.setStyle(options);

      polyline._popup.setContent(label);
      polyline.setTooltipContent(label);
    }

    let arrow = node.get('arrow');
    if (arrow) {
      let arrowStyle = node.get('arrowStyle');

      // this enables otp colour changes
      arrow.setIcon(ARROWS[arrowStyle].icon);
      arrow.setOpacity(opacity);
    }
  },

  removePolylineFor(node) {
    let polyline = node.get('polyline');
    let arrow = node.get('arrow');

    if (polyline) {
      polyline.remove();
      node.set('polyline', null);
    }

    if (arrow) {
      arrow.remove()
      node.set('arrow', null);
    }
  }
});
