import DataJoin from './base';
import { isNone } from '@ember/utils';
import tomtom from 'tomtom';
import DEFAULTS from '../config/marker';
import lodash from 'lodash';

export default DataJoin.extend({
  enter(node) {
    let lat = node.get('lat');
    let lng = node.get('lng');
    if (isNone(lat) || isNone(lng)) return;

    this.createMarkerFor(node);
  },

  update(node) {
    let lat = node.get('lat');
    let lng = node.get('lng');
    let marker = node.get('marker');

    if (isNone(lat) || isNone(lng)) {
      if (marker) {
        this.removeMarkerFor(node);
      }
      return;
    }

    if (!marker) {
      this.createMarkerFor(node);
    }

    this.updateMarkerFor(node);
  },

  exit(node) {
    if (node.get('marker')) {
      this.removeMarkerFor(node);
    }
  },

  createMarkerFor(node) {
    let label = node.get('label');
    let record = node.get('record');

    let style = node.get('style');
    let defaultOptions = DEFAULTS[style] || {};
    let nodeOptions = node.get('options') || {};

    let options = lodash.merge(defaultOptions, nodeOptions, {
      label,
      opacity: node.get('opacity'),

      // for active context clicks
      isActive: node.get('isActive'),
      modelName: record.constructor.modelName,
      modelId: record.get('id'),
    });

    let lat = node.get('lat');
    let lng = node.get('lng');
    let marker = tomtom.L.marker([lat, lng], options);

    marker.bindPopup(label, { closeButton: false });

    this.bindMouseEvents(marker);

    node.set('marker', marker);

    let map = this.get('map');
    marker.addTo(map);

    return marker;
  },

  updateMarkerFor(node) {
    let marker = node.get('marker');
    if (!marker) return;

    let lat = node.get('lat');
    let lng = node.get('lng');
    let opacity = node.get('opacity');
    let label = node.get('label');

    marker.setLatLng([lat, lng]);
    marker.setOpacity(opacity);

    marker._popup.setContent(label);
    marker.setTooltipContent(label);
  },

  removeMarkerFor(node) {
    let marker = node.get('marker');
    if (marker) {
      marker.remove();
      node.set('marker', null);
    }
  }
});
