import tomtom from 'tomtom';

export default function(points, arrowOptions) {
  let arrow = tomtom.L.marker(points[1], arrowOptions);

  // relies on `vendor/leaflet/leaflet.rotatedMarker.js`
  arrow.setRotationAngle(getBearing(points) - 90);

  return arrow;
}

// SOURCE: https://www.igismap.com/formula-to-find-bearing-or-heading-angle-between-two-points-latitude-longitude/
function getBearing(points) {
  let X = Math.cos(toRadians(points[1][0])) * Math.sin(toRadians(points[1][1] - points[0][1]));
  let Y = (Math.cos(toRadians(points[0][0])) * Math.sin(toRadians(points[1][0])))
          - (Math.sin(toRadians(points[0][0])) * Math.cos(toRadians(points[1][0]))
          * Math.cos(toRadians(points[1][1] - points[0][1])));

  let bearing = toDegrees(Math.atan2(X,Y));
  return bearing;
}

function toRadians(angle) {
  return angle / 180 * Math.PI;
}

function toDegrees(angle) {
  return angle / Math.PI * 180;
}
