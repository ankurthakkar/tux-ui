import GenericMapWidget from '../../generic-widgets/map-widget/component';
import Evented from '@ember/object/evented';
import { inject as service } from '@ember/service';
import { computed, observer } from '@ember/object';
import { run } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import MarkerDataJoin from './data-joins/marker';
import PolylineDataJoin from './data-joins/polyline';
import tomtom from 'tomtom';

const config = {
  title: 'Map'
};

const recenterControlOptions = {
  position: 'topleft',
  recenterAction: null
};

export var RecenterControl = tomtom.L.Control.extend({
  options: recenterControlOptions,

  onAdd: function(/* map */) {
    let name = 'leaflet-control-recenter';
    let container = tomtom.L.DomUtil.create('div', 'leaflet-control-recenter leaflet-bar');

    this._recenterButton = this._createButton('<i class="fa fa-crosshairs fa-lg crosshairs-big" aria-hidden="true"></i>', "Recenter", name,  container, this._recenterMap);
    return container;
  },

  _recenterMap: function() {
    this.options.mapWidget.onRecenterClick()
  },

  _createButton: function (html, title, className, container, fn) {
		var link = tomtom.L.DomUtil.create('a', className, container);
		link.innerHTML = html;
		link.href = '#';
		link.title = title;
		/*
		 * Will force screen readers like VoiceOver to read this as "Zoom in - button"
		 */
		link.setAttribute('role', 'button');
		link.setAttribute('aria-label', title);

		tomtom.L.DomEvent.disableClickPropagation(link);
		tomtom.L.DomEvent.on(link, 'click', tomtom.L.DomEvent.stop);
		tomtom.L.DomEvent.on(link, 'click', fn, this);
		tomtom.L.DomEvent.on(link, 'click', this._refocusOnMap, this);

		return link;
	},
});


export default GenericMapWidget.extend(Evented, {
  mapContext: service(),
  geocode: service(),
  activeContext: service(),
  maximizer: service(),

  markerDataJoin: null,
  polylineDataJoin: null,

  config,

  isSearchEnabled: computed.alias('mapContext.isSearchEnabled'),
  searchText: computed.alias('mapContext.searchText'),
  lat: computed.alias('mapContext.lat'),
  lng: computed.alias('mapContext.lng'),
  isVisible: computed.alias('mapContext.isVisible'),
  polylines: computed.alias('mapContext.polylines'),
  markers: computed.alias('mapContext.markers'),
  agencyMarkers: computed.alias('mapContext.agencyMarkers'),
  isFiltered: computed.alias('mapContext.isFiltered'),
  contextMenu: computed.alias('mapContext.contextMenu'),
  contextMenuOptions: computed.alias('mapContext.contextMenuOptions'),
  contextMenuPosition: computed.alias('mapContext.contextMenuPosition'),
  clearButtonEnabled: computed.gt('activeContext.checkedItems.length', 0),

  init() {
    this._super(...arguments);
    this.set('markerDataJoin', MarkerDataJoin.create());
    this.set('polylineDataJoin', PolylineDataJoin.create());
  },

  didInitializeMap(map) {
    let locationControl = new RecenterControl({ mapWidget: this });
    locationControl.addTo(map);

    //TODO: bind the click event
    this.set('markerDataJoin.map', map);
    this.set('polylineDataJoin.map', map);

    this.get('markerDataJoin').on('click', this, 'onLayerClick');
    this.get('markerDataJoin').on('dblClick', this, 'onLayerDblClick');
    this.get('markerDataJoin').on('contextMenu', this, 'onLayerContextMenu');
    this.get('polylineDataJoin').on('click', this, 'onLayerClick');
    this.get('polylineDataJoin').on('dblClick', this, 'onLayerDblClick');
    this.get('polylineDataJoin').on('contextMenu', this, 'onLayerContextMenu');

    this.onMarkersChange();
    this.onPolylinesChange();
    this.get('geocode.activeGeocode');
    this.get('geocode.activePickGeocode');
    this.get('geocode.activeDropGeocode');
  },

  willDestroyElement() {
    this.get('markerDataJoin').off('click', this, 'onLayerClick');
    this.get('markerDataJoin').off('dblClick', this, 'onLayerDblClick');
    this.get('markerDataJoin').off('contextMenu', this, 'onLayerContextMenu');
    this.get('polylineDataJoin').off('click', this, 'onLayerClick');
    this.get('polylineDataJoin').off('dblClick', this, 'onLayerDblClick');
    this.get('polylineDataJoin').off('contextMenu', this, 'onLayerContextMenu');

    this.get('markerDataJoin').clear();
    this.get('polylineDataJoin').clear();

    this._super(...arguments);
  },

  activeGeocode: observer('geocode.activeGeocode', function() {
    const map = this.get('map');
    const geocodeService = this.get('geocode');

    if (geocodeService.get('activeGeocode')) {
      map.on('click', function(event) {
        geocodeService.saveNewGeocode(event.latlng);
      });
    }
    else {
      map.off('click');
    }
  }),

  activeMarker: observer('geocode.activeMarker', function() {
    const map = this.get('map');
    const geocodeService = this.get('geocode');

    if (geocodeService.get('oldMarker')) {
      geocodeService.get('oldMarker').remove();
    }

    if (geocodeService.get('activeMarker')) {
      const marker = geocodeService.get('activeMarker');
      marker.addTo(map);
      map.setView(marker._latlng, geocodeService.get('zoom'));
    }
  }),

  activePickMarker: observer('geocode.activePickMarker', function() {
    const map = this.get('map');
    const geocodeService = this.get('geocode');

    if (geocodeService.get('oldPickMarker')) {
      geocodeService.get('oldPickMarker').remove();
    }

    if (geocodeService.get('activePickMarker')) {
      const marker = geocodeService.get('activePickMarker');
      marker.addTo(map);
      map.setView(marker._latlng, geocodeService.get('zoom'));
    }
  }),

  activeDropMarker: observer('geocode.activeDropMarker', function() {
    const map = this.get('map');
    const geocodeService = this.get('geocode');

    if (geocodeService.get('oldDropMarker')) {
      geocodeService.get('oldDropMarker').remove();
    }

    if (geocodeService.get('activeDropMarker')) {
      const marker = geocodeService.get('activeDropMarker');
      marker.addTo(map);
      map.setView(marker._latlng, geocodeService.get('zoom'));
    }
  }),

  updateMarkerData() {
    let markerDataJoin = this.get('markerDataJoin');
    let markers = this.get('markers');
    markerDataJoin.join(markers);
  },

  updatePolylineData() {
    let polylineDataJoin = this.get('polylineDataJoin');
    let polylines = this.get('polylines');
    polylineDataJoin.join(polylines);
  },

  onMarkersChange: observer(
    'markers.@each.{lat,lng,style,opacity,label,options}',
    function() {
    run.scheduleOnce('actions', this, 'updateMarkerData');
  }),

  onPolylinesChange: observer(
    'polylines.@each.{points,style,opacity,label,options,otpStatus}',
    function() {
    run.scheduleOnce('actions', this, 'updatePolylineData');
  }),

  onLayerClick(modelName, id, e) {
    this.get('mapContext').onLayerClick(modelName, id, e);
  },

  onLayerDblClick(modelName, id, e) {
    this.get('mapContext').onLayerDblClick(modelName, id, e);
  },

  onLayerContextMenu(modelName, id, e) {
    this.get('mapContext').onLayerContextMenu(modelName, id, e);
  },

  onRecenterClick() {
    this.notifyPropertyChange('lat');
    this.notifyPropertyChange('lng');
  },

  actions: {
    onSearchButtonClick() {
      this.toggleProperty('isSearchEnabled');

      run.scheduleOnce('afterRender', () => {
        if (this.get('isSearchEnabled')) {
          this.$('.map-widget-search-box').focus();
        }
      })
    },

    onRemoveSearchClick() {
      this.set('searchText', '');
      this.toggleProperty('isSearchEnabled');
    },

    onSearchTextChange(searchText) {
      this.set('searchText', searchText);
    },

    onClearButtonClick() {
      this.get('activeContext').clearCheckedItems();
    },

    onFilterButtonClick() {
      let workspace = this.get('workspace');

      let topState = workspace.get('topState');
      while (topState === 'filterMapWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Map Layers";

      workspace.pushState('filterMapWidget', {
        displayName
      });
    },

    onLayerToggle(layer) {
      this.toggleProperty(`isVisible.${layer}`);
    },

    setContextMenu(dropdown) {
      this.set('contextMenu', dropdown);
    },

    onContextMenuOptionClick(option) {
      this.get('mapContext').onContextMenuOptionClick(option);
    },

    onHeaderDoubleClick() {
      let maximizer = this.get('maximizer');
      if (maximizer.maximizedWidget === this.get('widget')) {
        maximizer.minimize();
        return;
      }
      maximizer.maximize(this.get('widget'));
    },

    onExitMaximizedClick() {
      let maximizer = this.get('maximizer');
      maximizer.minimize();
    }
  }
});
