import OTPFormattedRow from '../../../table/rows/otp-formatted-row/component';
import { computed } from '@ember/object';

export default OTPFormattedRow.extend({
  attributeBindings: ['draggable'],
  draggable: 'true',

  // TODO: delete or revise when API and/or model is updated.
  otp: computed('row.otp', function() {
    return Number(this.get('row.otp'));
  })
});
