export default {
  component: 'iq-widgets/stops-widget',
  rowComponent: 'iq-widgets/stops-widget/stops-row',
  modelName: 'iq-stop',
  defaultSortId: 'id',
  defaultSortAsc: false,

  title: 'Stops',

  columns: [
    {
      id: 'id',
      type: 'uuid',
      label: 'ID',
      valuePath: 'bsStop.id',
      valuePreview: '1',
      searchable: true,
      isMapLabelVisible: true,
      defaultWidth: 60
    },
    // {
    //   id: 'clusterOrd',
    //   type: 'uuid',
    //   label: 'Cluster Ord',
    //   valuePath: 'dsStop.trip.cluster.ordinal',
    //   searchable: true,
    //   defaultWidth: 50
    // },
    // {
    //   id: 'ordinal',
    //   type: 'integer',
    //   label: 'Ordinal',
    //   valuePath: 'dsStop.ordinal',
    //   searchable: true,
    //   defaultWidth: 50
    // },
    {
      id: 'type',
      type: 'text',
      label: 'Type',
      valuePath: 'bsStop.type',
      valuePreview: 'Pick',
      searchable: true,
      isMapLabelVisible: true,
      defaultWidth: 50
    },
    {
      id: 'tripId',
      type: 'uuid',
      label: 'Trip ID',
      valuePath: 'bsStop.booking.id',
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'riderLastName',
      type: 'text',
      label: 'Passenger Last Name',
      valuePath: 'bsStop.booking.rider.lastName',
      searchable: true,
      defaultWidth: 75
    },
    {
      id: 'riderFirstName',
      type: 'text',
      label: 'Passenger First Name',
      valuePath: 'bsStop.booking.rider.firstName',
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'riderPhone',
      type: 'text',
      label: 'Passenger Phone Number',
      valuePath: 'bsStop.booking.rider.phone',
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'riderId',
      type: 'text',
      label: 'Passenger ID',
      valuePath: 'bsStop.booking.rider.id',
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'status',
      type: 'text',
      label: 'Status',
      valuePath: 'dsStop.status',
      valuePreview: 'Planned',
      editable: false,
      defaultWidth: 60
    },
    {
      id: 'OTP',
      type: 'text',
      label: 'OTP Status',
      valuePath: 'otpLabel',
      defaultWidth: 40,
      highlightable: true
    },
    {
      id: 'ETA',
      type: 'date',
      label: 'ETA',
      valuePath: 'dsStop.eta',
      editable: true,
      format: 'HH:mm',
      defaultWidth: 40,
      highlightable: true
    },
    {
      id: 'promisedTime',
      type: 'datetime',
      label: 'Promise Time',
      valuePath: 'dsStop.trip.promisedTime',
      editable: true,
      format: 'YYYY-MM-DD hh:mm A',
      defaultWidth: 50
    },
    {
      id: 'requestedTime',
      type: 'time',
      label: 'Request Time',
      valuePath: 'dsStop.trip.requestedTime',
      editable: true,
      format: 'HH:mm A',
      defaultWidth: 70
    },
    {
      id: 'actualArrivalTime',
      type: 'time',
      label: 'Actual Arrive Time',
      valuePath: 'dsStop.actualArrivalTime',
      editable: true,
      format: 'HH:mm A',
      defaultWidth: 50
    },
    {
      id: 'actualDepartTime',
      type: 'time',
      label: 'Actual Depart Time',
      valuePath: 'dsStop.actualDepartTime',
      editable: true,
      format: 'HH:mm A',
      defaultWidth: 50
    },
    {
      id: 'plannedDepartTime',
      type: 'datetime',
      label: 'Planned Depart Time',
      valuePath: 'dsStop.plannedDepartTime',
      editable: true,
      format: 'YYYY-MM-DD hh:mm A',
      defaultWidth: 50
    },
    {
      id: 'routeName',
      type: 'text',
      label: 'Route Name',
      valuePath: 'dsStop.trip.cluster.route.name',
      valuePreview: '40',
      defaultWidth: 70
    },
    {
      id: 'notes',
      type: 'text',
      label: 'Notes',
      valuePath: 'bsStop.notes',
      editable: true,
      searchable: true,
      defaultWidth: 100
    },
    {
      id: 'address',
      type: 'text',
      label: 'Address ',

      valuePath: 'bsStop.location.streetAddress',
      editable: true,
      hidden: true,
      defaultWidth: 80
    },
    {
      id: 'city',
      type: 'text',
      label: 'City',
      valuePath: 'dsStop.place.city',
      editable: true,
      hidden: true,
      defaultWidth: 80
    },
    {
      id: 'odometer',
      type: 'number',
      label: 'Odometer on Arrival',
      valuePath: 'dsStop.odometer',
      editable: true,
      searchable: true
    },
    {
      id: 'serviceWindow',
      type: 'text',
      label: 'Service Window',
      valuePath: 'dsStop.serviceWindow',
      editable: true,
      hidden: true,
      defaultWidth: 80
    },
    {
      id: 'BreakStartTime',
      type: 'time',
      label: 'Break Start Time',
      valuePath: 'dsStop.estimatedStart',
      editable: true,
      format: 'HH:mm A',
      defaultWidth: 50
    },
    {
      id: 'BreakEndTime',
      type: 'time',
      label: 'Break End Time',
      valuePath: 'dsStop.estimatedEnd',
      editable: true,
      format: 'HH:mm A',
      defaultWidth: 50
    },
    {
      id: 'BreakType',
      type: 'text',
      label: 'Break Type',
      valuePath: 'dsStop.BreakType',
      valuePreview: 'Floating',
      defaultWidth: 70
    },
    {
      id: 'BreakCategory',
      type: 'text',
      label: 'Break Category',
      valuePath: 'dsStop.BreakCategory',
      defaultWidth: 70
    },
    {
      id: 'slackMinutes',
      type: 'text',
      label: 'Slack Minutes',
      valuePath: 'dsStop.slackMinutes',
      defaultWidth: 70
    },
    {
      id: 'onboardCount',
      type: 'number',
      label: 'Onboard Count',
      valuePath: 'bsStop.onboardCount',
      defaultWidth: 70
    }
  ]
};
