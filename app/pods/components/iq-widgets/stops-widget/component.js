import IQUXColumnWidget from '../column-widget/component';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { findRowRecordByElement, findRowElement } from 'adept-iq/utils/find-row-record';
import { isBlank } from '@ember/utils';
import { guidFor } from '@ember/object/internals';
import ENV from 'adept-iq/config/environment';
import moment from 'moment';

const INCLUDES = [
  'clusters',
  'vehicleRoutes',
  'vehicleRoutes.driverShifts',
  'vehicleRoutes.vehicle',
  'vehicleRoutes.driverShifts.driver',
  'vehicleRoutes.vehicle.vehicleType',
  'driverBreaks',
  'driverBreaks.driverBreakEvents',
  'vehicleRoutes.vehicle.vehicleEvents',
  'clusters.trips',
  'clusters.trips.stops'
];

export default IQUXColumnWidget.extend({
  classNames: ['stops-widget'],
  editModal: service(),
  dragCoordinator: service(),
  ajax: service(),
  store: service(),
  session: service(),
  editComponent: 'iq-widgets/stops-form-widget',
  driverBreakEditComponent: 'iq-widgets/driver-breaks-form-widget',
  vehicleEventComponent: 'iq-widgets/vehicle-event-form-widget',
  noShowComponent: 'iq-widgets/noshow-event-stop-form-widget',

  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,
  routeexecevent: service(),

  saveDriverBreakFunction(records) {
    this.set('editModal.errors', []);


    return new Promise((resolve, reject) => {
      records.forEach(record => {
        let promisedStartMoment = moment(record.get('promisedStart'));
        let promisedEndMoment = moment(record.get('promisedEnd'));

        record.set('estimatedStart', record.get('promisedStart'));
        record.set('estimatedEnd', record.get('promisedEnd'));

        record.set('plannedDuration', promisedEndMoment.diff(promisedStartMoment, 'seconds'));
        record.set('externalId', `${record.get('route.id')}${moment().unix()}`);
        record.set('notes', `${promisedEndMoment.diff(promisedStartMoment, 'minutes')} minutes break`);

        record.set('ordinal', 1);
        record.set('state', 'planned');
        record.save()
          .then(() => {
            record.set('isForceDirty', false);
            this.get('activeContext').refreshTableContent(this.get('modelName'));
          })
          .catch(e => {
            reject(e);
          });
      });
    });
  },


  sendMessagesToVehicle(record, status) {

    let assignedDriver = record.get('trip.cluster.route.assignedDriver');
    let assignedVehicle = record.get('trip.cluster.route.assignedVehicle');
    let rider = record.get("trip.rider");
    let message = ` ${status} action performed by dispatch for ${rider.get('firstName')} ${rider.get('lastName')},
               ${record.get('type')} : ${record.get('place.address')} , ${record.get('trip.requestedTime')} `;

    let draftMessage = this.get('store').createRecord('tm-canned-message', {
      // TODO: should this be `driver.externalId`?
      driverId: assignedDriver.get('externalId'),
      vehicleId: assignedVehicle.get('name'),
      routeId: `${record.get('trip.cluster.route.id')}`,
      replyToMsgId:`${record.get('trip.id')}`,
      body:message
      // TODO: how do we get the current route ID for a vehicle?
      // routeId: driver.get('routeId')
    });

    return draftMessage.save();
  },


  sourceRowId: null,
  dragTarget: null,

  saveStopArriveRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        let routeStartData =  this.get('routeexecevent').stopArriveData();
        let preparedRouteStartData =  this.get('routeexecevent').prepareStopArriveData(routeStartData,record);

        var json = JSON.stringify(preparedRouteStartData);
        let session = this.get('session');

        return this.get('ajax').post(ENV.API.avlmService.host+'/production/route-exec-event', {
          method: 'POST',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('route');
          me.sendMessagesToVehicle(record,'Arrive');
        }).catch(function(error) {
          reject(error);
        });
      })
    });
  },

  saveStopDepartRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {

        let routeEndData =  this.get('routeexecevent').stopDepartData();
        let preparedRouteEndData =  this.get('routeexecevent').prepareStopDepartData(routeEndData,record);

        var json = JSON.stringify(preparedRouteEndData);
        let session = this.get('session');

        return this.get('ajax').post(ENV.API.avlmService.host+'/production/route-exec-event', {
          method: 'POST',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('route');
          me.sendMessagesToVehicle(record,'Depart');
        }).catch(function(error) {
         reject(error);
        });
      })
    });
  },
  saveNoShowRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        let noShowData =  this.get('routeexecevent').noShowData();
        let prepareNoShowData =  this.get('routeexecevent').prepareStopNoShowData(noShowData,record);

        var json = JSON.stringify(prepareNoShowData);
        let session = this.get('session');

        return this.get('ajax').post(ENV.API.avlmService.host+'/production/route-exec-event', {
          method: 'POST',
          contentType: "application/json",
          headers: {
            "Authorization": `Bearer ${session.data.authenticated.token}`
          },
          data: json
        }).then((response) => {
          record.set('isForceDirty', false);
          record.set('id',response.data.id);
          me.get('activeContext').refreshTableContent('route');
        }).catch(function(error) {
         reject(error);
        });
      })
    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'Table Action',
      action: function() {
        alert('I still work');
      }
    }]);
    this.set('singleActions', [{
      name: 'Arrive',
        action: (model) => {
          let records = [];
          records.push(model);
          model.set('odometer','');
          model.set('timestamp',new Date());
          this.saveStopArriveRecordFunction(records);
        }
      },
      {
        name: 'Depart',
        action: (model) => {
          let records = [];
          records.push(model);
          model.set('odometer','');
          model.set('timestamp',new Date());
          this.saveStopDepartRecordFunction(records);
        }
      },
      {
        name: 'No Show',
        action: (model) => {
          let noShowReasonCodes = this.get('store').peekAll('no-show-reason-code');
          model.set('noShowReason',noShowReasonCodes.firstObject);
          model.set('timestamp',new Date());
          let noShowComponent = this.get('noShowComponent');
          this.get('editModal').open(noShowComponent, [model], this.get('saveNoShowRecordFunction').bind(this));

        }
      },
      {
        name: 'Remove',
        action: (/* model */) => {

        }
      } ,
      {
        name: 'Edit',
          action: (/* model */) => {

          }
      },
      {
        name: 'Add Driver Break',
          action: (model) => {
            let placeModel = this.get('store').createRecord('place');
            let newModel = this.get('store').createRecord('driver-break', {
              route: model.get('trip.cluster.route'),
              place: placeModel,
              promisedStart: new Date(),
              promisedEnd: new Date()
            });

            this.get('records').pushObject(newModel);
            let editComponent = this.get('driverBreakEditComponent');
            this.get('editModal').open(editComponent, [newModel], this.get('saveRecordFunction').bind(this));
          }
      }
    ]);

    this.set('bulkActions', [{
      name: 'Bulk Edit',
      action: (models) => {
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, models);
      }
    }]);
  },

 // singleActions: computed('table.rows.@each.{selected,status}', function() {
 //   let stopArriveAction = this.get('stopArriveAction');
 //   let stopDepartAction = this.get('stopDepartAction');
 //   let stopNoshowAction = this.get('stopNoshowAction');
 //   let breakStartAction = this.get('breakStartAction');
 //   let breakEndAction = this.get('breakEndAction');
 //   let breakRemoveAction = this.get('breakRemoveAction');
 //   let breakAddAction = this.get('breakAddAction');
 //   let breakEditAction = this.get('breakEditAction');

 //   let row = this.get('table.rows').findBy('selected', true);
 //   if (!row) return [];

 //   switch(row.get('type')) {
 //     case 'pick': {
 //       switch(row.get('status')) {
 //         case 'Arrived': {
 //           return [stopDepartAction,stopNoshowAction,breakAddAction];
 //         }
 //         case 'Scheduled'  :  {
 //           if(row.get('actualArrivalTime') === null ||
 //             row.get('actualArrivalTime') === undefined) {
 //               return [stopArriveAction,stopNoshowAction,breakAddAction];
 //           } else {
 //             return [stopNoshowAction,breakAddAction];
 //           }
 //         }
 //         case 'Cancelled': {
 //           return [breakAddAction];
 //         }
 //         case 'No Showed': {
 //           return [breakAddAction];
 //         }
 //         default: {
 //           return [breakAddAction];
 //         }
 //       }
 //       break;
 //     }
 //     case 'drop': {
 //       switch(row.get('status')) {
 //         case 'Arrived'  :  {
 //           return [stopDepartAction,breakAddAction];
 //         }
 //         case 'Scheduled': {
 //           if(row.get('actualArrivalTime') === null ||
 //             row.get('actualArrivalTime') === undefined) {
 //               return [stopArriveAction,breakAddAction];
 //           }
 //         }
 //         case 'Cancelled': {
 //           return [breakAddAction];
 //         }
 //         default: {
 //           return [breakAddAction];
 //         }
 //       }
 //       break;
 //     }
 //    case 'break': {
 //     switch(row.get('status')) {
 //       case 'Arrived': {
 //         return [breakEndAction,breakAddAction];
 //       }
 //       case 'Scheduled'  :  {
 //         if(row.get('breakStartTime') === null ||
 //           row.get('breakStartTime') === undefined) {
 //             return [breakStartAction,breakEditAction,breakRemoveAction,breakAddAction];
 //         } else {
 //           return [breakEndAction,breakAddAction];
 //         }
 //       }
 //       case 'Departed': {
 //         return [breakAddAction];
 //       }
 //       default: {
 //         return [breakAddAction];
 //       }
 //     }
 //     }
 //   }
 // }),
  dragRowGroup: computed('row', function() {
    return this.get('table.rows');
  }).readOnly(),

  dragDirection: computed('dragTarget', 'sourceRowId', function() {
    let targetRow = this.get('dragTarget');
    if (targetRow) {
      let rows = this.get('dragRowGroup');
      let targetIdx = rows.indexOf(findRowRecordByElement(targetRow, rows));
      let sourceIdx = rows.indexOf(rows.findBy('rowId', this.get('sourceRowId')));
      return (sourceIdx - targetIdx);
    }
  }).readOnly(),

  dragOverClass: computed('dragDirection', function() {
    let direction = this.get('dragDirection') < 0 ? 'below' : 'above';
    return ` drag-target-${direction} drag-target`;
  }),

  dragStart(event) {
    let row = findRowRecordByElement(event.target, this.get('dragRowGroup'));
    if (isBlank(row)) return;
    this.set('sourceRowId', row.get('rowId'));

    let dragId = guidFor(row);
    this.get('dragCoordinator').setSourceRow(dragId, row, 'stops');
    event.dataTransfer.setData('text', dragId);
  },

  dragOver(event) {
    event.preventDefault();
    if (this.get('dragCoordinator.widgetType') !== 'stops') return;

    let rowElement = findRowElement(event.target);
    // User might drag over other widget elements.
    if (isBlank(rowElement)) {
      return;
    }
    this.set('dragTarget', rowElement);

    this.$(`#${rowElement.id}`).addClass(this.get('dragOverClass'));
  },

  dragLeave(event) {
    event.preventDefault();
    let rowElement = findRowElement(event.target);
    // User might drag over other widget elements.
    if (isBlank(rowElement)) {
      return;
    }

    this.$(`#${rowElement.id}`).removeClass(['drag-target-above','drag-target-below','drag-target']);
  },

  drop(event) {
    if (this.get('dragCoordinator.widgetType') !== 'stops') return;

    let rowElement = findRowElement(event.target);
    // User might drop onto other widget elements.
    if (isBlank(rowElement)) {
      return;
    }
    this.$(`#${rowElement.id}`).removeClass(['drag-target-above','drag-target-below','drag-target']);

    let table = this.get('table');
    let rows = this.get('dragRowGroup');

    // let sourceRowRecord = rows.findBy('rowId', this.get('sourceRowId'));
    // // User might drop elements that are not Stops widget rows.
    // if (isBlank(sourceRowRecord)) {
    //   return;
    // }
    let sourceRowRecord = this.get('dragCoordinator').getSourceRow(event.dataTransfer.getData('text'));
    if (isBlank(sourceRowRecord)) return;


    let targetRowRecord = findRowRecordByElement(event.target, rows);
    let _rows = rows.toArray();
    let targetRowIdx = _rows.indexOf(targetRowRecord) + (this.get('dragDirection') < 0 ? 0 : 0);

    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
    event.stopPropagation();

    table.propertyWillChange('rows');

    _rows.removeObject(sourceRowRecord);
    _rows.insertAt(targetRowIdx, sourceRowRecord);
    rows.setObjects(_rows);

    table.propertyDidChange('rows');
  },

  dragEnd() {
    this.set('sourceRowId', null);
    this.set('dragTarget', null);
    this.get('dragCoordinator').clearSourceRows();
  }
});

