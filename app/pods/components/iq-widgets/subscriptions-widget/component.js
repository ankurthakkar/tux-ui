import IQUXColumnWidget from '../column-widget/component';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { isNone } from '@ember/utils';
import _ from 'lodash';

const INCLUDES = [];

export default IQUXColumnWidget.extend({
  dragCoordinator: service(),
  store: service(),
  classNames: ['subscriptions-widget'],
  editModal: service(),
  ajax: service(),
  editComponent: 'iq-widgets/subscriptions-form-widget',
  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,

  saveRecordFunction(records) {
    this.set('editModal.errors', []);

    function saveDeleteAllTravelNeeds(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {
        deleteObject._record.deleteRecord();
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => {
            object.save();
          }));
        });
    }

    function saveNewTravelNeedObjects(subscription, oldObjects, newObjects) {
      return Promise.all(newObjects.map(newObject => {
        if (newObject.get('count') !== null && newObject.get('count') > 0) {
          newObject.set('subscription', subscription);

          newObject.save();
          return newObject;
        }
      }));
    }

    function saveRecurrencePatterns(objects) {
      return Promise.all(objects.map(object => {
        object.save();
      }));
    }

    function saveExlusions(objects) {
      return Promise.all(objects.map(object => {
        if (!isNone(object.get('startDate')) && !isNone(object.get('endDate')))
        {
          object.save();
        }
      }));
    }

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        record.save()
          .then(() => {
            saveRecurrencePatterns(record.get('recurrencePatterns'))
              .then(() => {
                saveExlusions(record.get('exclusions'))
                  .then(() => {
                    saveDeleteAllTravelNeeds(record.get('subscriptionTravelNeeds'))
                      .then(() => {
                        saveNewTravelNeedObjects(record, record.get('subscriptionTravelNeeds'), record.get('newTravelNeeds'))
                          .then(() => {
                            record.set('isForceDirty', false);
                            this.get('activeContext').refreshTableContent(this.get('modelName'));
                          });
                      });
                    });
              });
          })
          .catch(e => {
              reject(e);
          });
      });
    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'New Subscription',
      action: function() {
        let subscriptionModel = this.get('store').createRecord(this.get('config.modelName'));
        let subscriptionTravelNeedModel = this.get('store').createRecord('bs-subscription-travel-need');
        let exclusionModel = this.get('store').createRecord('bs-subscription-exclusion');
        let recurrencePatternModel = this.get('store').createRecord('bs-subscription-recurrence-pattern');

        subscriptionModel.set('subscriptionTravelNeeds', [subscriptionTravelNeedModel]);
        subscriptionModel.set('exclusions', [exclusionModel]);
        subscriptionModel.set('recurrencePatterns', [recurrencePatternModel]);

        subscriptionModel.set('startDate', new Date());
        subscriptionModel.set('endDate', new Date());

        subscriptionModel.set('requestTime', new Date());

        this.get('records').pushObject(subscriptionModel);
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, [subscriptionModel], this.get('saveRecordFunction').bind(this));
      }.bind(this)
    }]);

    this.set('singleActions', [
    {
      name: 'Edit',
      action: (model) => {
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, [model], this.get('saveRecordFunction').bind(this));
      }
    },
    {
      name: 'Activity Log',
      action: (/* model */) => {

      }
    }]);

    this.set('bulkActions', [{
      name: 'Bulk Edit',
      action: (/* models */) => {

      }
    },
    {
      name: 'Activity Log',
      action: (/* models */) => {

      }
    }]);
  },

  rowGroup: computed('row', function() {
    return this.get('table.rows');
  }).readOnly(),

});
