export default {
  component: 'iq-widgets/vehicles-widget',
  rowComponent: 'table/rows/otp-formatted-row',
  modelName: 'iq-vehicle',

  title: 'Vehicles',

  defaultSortId: 'otp',
  defaultSortAsc: false,

  columns: [{
    id: 'name',
    type: 'text',
    label: 'ID',
    valuePath: 'id',
    valuePreview: '1',
    isMapLabelVisible: true,
    searchable: true,
    defaultWidth: 45
  },
  {
    id: 'typeName',
    type: 'text',
    label: 'Type',
    valuePath: 'ssVehicle.vehicleType.name',
    searchable: true,
    isMapLabelVisible: true,
    defaultWidth: 60
  },
  {
    id: 'availability',
    type: 'enum',
    label: 'Availabilities',
    valuePath: 'ssVehicle.formattedAvailabilities',
    cellDesc: 'Availabilities',
    isMapLabelVisible: true,
    defaultWidth: 100
  },
  {
    id: 'noOfAmbulatorySeats',
    type: 'text',
    label: 'Ambulatory seats',
    valuePath: 'ssVehicle.noOfAmbulatorySeats',
    editable: true,
    defaultWidth: 60,
    highlightable: true
  },
  {
    id: 'noOfWheelChairSeats',
    type: 'text',
    label: 'Wheelchair spaces',
    valuePath: 'ssVehicle.noOfWheelChairSeats',
    editable: true,
    defaultWidth: 60,
    highlightable: true
  },
  {
    id: 'active',
    type: 'boolean',
    label: 'Active',
    valuePath: 'ssVehicle.active',
    editable: true,
    defaultWidth: 45
  },
  {
    id: 'routeName',
    type: 'text',
    label: 'Route name',
    valuePath: 'dsVehicle.currentRoute.name',
    editable: true
  },
  {
    id: 'driverId',
    type: 'text',
    label: 'Driver Id',
    valuePath: 'dsVehicle.latestDriver.id',
    editable: true,
    isMapLabelVisible: true,
    defaultWidth: 45
  },

  {
    id: 'driverName',
    type: 'text',
    label: 'Driver Name',
    valuePath: 'dsVehicle.latestDriver.fullName',
    editable: true
  },
  {
    id: 'garageStartAddress',
    type: 'text',
    label: 'Starting Garage',
    valuePath: 'ssVehicle.startGarage.address',
    editable: true
  },
  {
    id: 'garageEndAddress',
    type: 'text',
    label: 'Ending Garage',
    valuePath: 'ssVehicle.endGarage.address',
    editable: true
  },
  {
    id: 'driverAPPVersion',
    type: 'text',
    label: 'Driver App Version',
    valuePath: 'driverAPPS.Version',
    editable: true
  },
  {
    id: 'cellularCarrier',
    type: 'text',
    label: 'Cellular Carrier',
    valuePath: 'driverAPPS.cellularCarrier',
    editable: true
  },
  {
    id: 'otp',
    type: 'number',
    label: 'OTP',
    valuePath: 'otpLabel',
    highlightable: true
  },
  // FIXME: none of these work
  {
    id: 'driverAPPId',
    type: 'text',
    label: 'Driver  App Id',
    valuePath: 'driverAPPS.id',
    editable: true
  },
  {
    id: 'hardewareId',
    type: 'text',
    label: 'Hardware Id',
    valuePath: 'driverAPPS.hardwareId',
    editable: true
  },
  {
    id: 'cellularType',
    type: 'text',
    label: 'Cellular Type',
    valuePath: 'driverAPPS.cellularType',
    editable: true
  }]
}
