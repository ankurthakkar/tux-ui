import IQUXColumnWidget from '../column-widget/component';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import _ from 'lodash';
import ENV from 'adept-iq/config/environment';

const INCLUDES = [];

export default IQUXColumnWidget.extend({
  classNames: ['vehicles-widget'],
  editModal: service(),
  //workspace: service(),
  editComponent: 'iq-widgets/vehicles-form-widget',
  ajax: service(),
  store: service(),
  vehicleinfo: service(),
  session: service(),

  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,

  saveRecordFunction(records) {

    function saveDeleteAllObjects(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {

        deleteObject._record.deleteRecord();
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => object.save()));
        });
    }

    function saveLocationObject(object) {
      if (object !== undefined) {
        return Promise.all([object.save()]);
      }
      return Promise.all([]);
    }


    this.set('editModal.errors', []);

    return new Promise((resolve, reject) => {
      records.forEach(record => {

        saveLocationObject(record.startGarages.firstObject)
              .then(() => {
                saveLocationObject(record.endGarages.firstObject)
                .then(() => {
                   record.save();
                })
            })
          .then(() => {
            saveDeleteAllObjects(record.get('availability'))
              .then(() => {

                saveDeleteAllObjects(record.get('shiftBreaks'))
                .then(() => {
                  record.set('isForceDirty', false);
                })
              })
              .catch(e => {
                if (e.errors[0].status === '400' || e.errors[0].status === '409') {
                  this.get('editModal.errors').pushObject('Invalid Vehicle Id');
                }
                else {
                  reject(e);
                }

              });
          })
      });
    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'New Vehicle',
      action: function() {

        let startGarages = [];
        let origin = this.get('store').createRecord('ss-location');
        origin.set('type','pick');
        startGarages.pushObject(origin);

        let endGarages = [];
        let destination = this.get('store').createRecord('ss-location');
        destination.set('type','drop');
        endGarages.pushObject(destination);

        let vehicleTypes = this.get('store').peekAll('ss-vehicle-type');

        let newModel = this.get('store').createRecord('ss-vehicle');
        newModel.set('startGarages',startGarages);
        newModel.set('endGarages',endGarages);
        newModel.set('startGarage',this.get('store').createRecord('ss-location'));
        newModel.set('endGarage',this.get('store').createRecord('ss-location'));
        newModel.set('shiftBreaks',[]);
        newModel.set('vehicleType',vehicleTypes.firstObject);
        newModel.set('active',true);

        this.get('records').pushObject(newModel);
        this.set('editModal.editableRecords', []);
        let editComponent = this.get('editComponent');

        this.get('editModal').open(editComponent, [newModel], this.get('saveRecordFunction').bind(this));
      }.bind(this)
    }]);

    this.set('singleActions', [{
      name: 'Send Message',
      action: (vehicle) => {
        this.sendMessagesToVehicles([vehicle]);
      }
    },
    {
      name: 'Edit',
      action: (model) => {
        let editComponent = this.get('editComponent');

        let startGarages = [];
        if(model.get('startGarage.id') !== undefined) {
          let origin = model.get('startGarage');

          origin.set('type','pick');
          startGarages.pushObject(origin);

        } else {
          let origin = this.get('store').createRecord('ss-location');
          origin.set('type','pick');
          startGarages.pushObject(origin);
        }
        let endGarages = [];
        if(model.get('startGarage.id') !== undefined) {
          let destination = model.get('endGarage');
          destination.set('type','pick');
          endGarages.pushObject(destination);
        } else {
          let destination = this.get('store').createRecord('ss-location');
          destination.set('type','drop');
          endGarages.pushObject(destination);
        }

        model.set('startGarages',startGarages);
        model.set('endGarages',endGarages);



        this.get('editModal').open(editComponent, [model], this.get('saveRecordFunction').bind(this));
      }
    },
    {
      name: 'Reset device',
      action: (model) => {
        let me = this;

        return new Promise((resolve, reject) => {

          let vehicleInfoData =  this.get('vehicleinfo').vehicleInfoData();
          let prepareVehicleInfoData =  this.get('vehicleinfo').prepareVehicleInfoData(vehicleInfoData,model);

          var json = JSON.stringify(prepareVehicleInfoData);
          let session = this.get('session');

            return this.get('ajax').patch(ENV.API.avlmService.host+'/production/vehicle-info', {
              method: 'PATCH',
              contentType: "application/json",
                headers: {
                  "Authorization": `Bearer ${session.data.authenticated.token}`
              },
              data: json
            }).then((response) => {

            }).catch(function(error) {
              reject(error);
            });
        });
      }
    }]);

    this.set('bulkActions', [{
      name: 'Reset device',
      action: (models) => {
        return new Promise((resolve, reject) => {
          models.forEach(record => {

          let vehicleInfoData =  this.get('vehicleinfo').vehicleInfoData();
          let prepareVehicleInfoData =  this.get('vehicleinfo').prepareVehicleInfoData(vehicleInfoData,record);

          var json = JSON.stringify(prepareVehicleInfoData);
          let session = this.get('session');

            return this.get('ajax').patch(ENV.API.avlmService.host+'/production/vehicle-info/'+record.get('name'), {
              method: 'PATCH',
              contentType: "application/json",
                headers: {
                  "Authorization": `Bearer ${session.data.authenticated.token}`
              },
              data: json
            }).then((/* response */) => {

            }).catch(function(error) {
              reject(error);
            });
          })
        });
      }
    },
    {
      name: 'Bulk Message',
      action: (vehicles) => {
        this.sendMessagesToVehicles(vehicles);
      }
    }]);
  },

  sendMessagesToVehicles(vehicles) {
    let draftMessages = vehicles.map((vehicle) => {
      return this.get('store').createRecord('tm-canned-message', {
        // TODO: should this be `driver.externalId`?
        driverId: vehicle.get('driver.id'),
        vehicleId: vehicle.get('id')

        // TODO: how do we get the current route ID for a vehicle?
        // routeId: driver.get('routeId')
      });
    });

    this.get('workspace').pushState('addNewMessage', { draftMessages });
  }
});
