import ChartjsWidget from '../../generic-widgets/chartjs-widget/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isBlank } from '@ember/utils';
import {
  buildFilterFunction,
  buildCompoundFilterNode
} from 'adept-iq/utils/filters';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';


export default ChartjsWidget.extend({
  workspace: service(),
  workspaceContext: service(),

  classNames: ['gauge-widget'],

  widget: null,

  options: computed.alias('widget.state.dataType'),

  /* Applies the choice of data source. */
  chartRecords: computed('options.modelName', 'workspaceContext.workspaceData.[]',function() {
    let modelName = this.get('options.modelName');
    if (isBlank(modelName)) return [];
    let wrappedRecords = this.get('workspaceContext.workspaceData').filterBy('modelName', modelName)
    return wrappedRecords.map((a) => a.record);
  }),

  /* Builds the chosen filters. */
  filterFunction: computed('options.columns', function() {
    console.log('filtering');
    let filterColumns = Object.values(this.get('options.columns')).map((col) => new FilterColumn(col));
    console.log(filterColumns);
    let args = filterColumns.map((column) => {
        return column.get('filterNode');
      });
    return buildFilterFunction(buildCompoundFilterNode('and', args));
  }),

  /* Applies the choice of filters. */
  filteredRecords: computed('chartRecords', 'filterFunction', function() {
    return this.get('chartRecords').filter(this.get('filterFunction'))
  }),

  /* Applies the choice of data to display. */
  displaySlice: computed('options.displayOptions.selected',
                          'filteredRecords',
                          function() {
    let displaySelection = this.get('options.displayOptions.selected');
    if (isBlank(displaySelection) ||
        isBlank(displaySelection.valueKey) ||
        isBlank(displaySelection.valueCategory)) {
      return this.get('filteredRecords');
    }

    return this.get('filteredRecords').filterBy(
                          displaySelection.valueKey,
                          displaySelection.valueCategory.value);
  }),

  /* Applies the choice of operation (metric option) to apply to the selected records. */
  dialValue: computed('displaySlice',
                      'options.displayOptions.selected.metricOption',
                      function() {
    let selectedMetric = this.get('options.displayOptions.selected.metricOption');
    if (isBlank(selectedMetric)) return this.get('displaySlice.length');

    switch (selectedMetric.id) {
      case 'count':
        return this.get('displaySlice.length');
      default:
        return 0;
    }
  }),

  minGaugeValue: computed('options.displayOptions.selected.metricOption', function() {
    let selectedMetric = this.get('options.displayOptions.selected.metricOption');
    if (isBlank(selectedMetric)) return 0;

    switch (selectedMetric.id) {
      case 'count':
        return 0;
      default:
        return 0;
      }
  }),

  /* Applies the box color visual options. */
  boxValues: computed('options.boxColor.{orange,red}', 'minGaugeValue', 'dialValue', function() {
    let min = this.get('minGaugeValue');
    let dialValue = this.get('dialValue');

    let boxColors = this.get('options.boxColor');
    let boxValues = [];
    boxValues.push(boxColors.orange - min);
    boxValues.push(boxColors.red - boxColors.orange);

    let maxValue = (boxColors.orange - min) + (2 * (boxColors.red - boxColors.orange));
    if (maxValue > dialValue) {
      boxValues.push(boxColors.red - boxColors.orange);
    } else {
      boxValues.push((boxColors.red - boxColors.orange) + (dialValue - maxValue));
    }
    return boxValues;
  }),

  dialRange: computed('boxValues', 'minGaugeValue', function() {
    let boxValues = this.get('boxValues');
    let min = this.get('minGaugeValue');
    let max = boxValues.reduce((a,b) => a + b, min);
    return { min: min, max: max };
  }),

  chartData: computed('boxValues', function() {
    let cData = {
      "datasets": [
        {
          "data": this.get('boxValues'),
          "backgroundColor": [
            // this.get('colors')[0],
            this.get('colors')[2],
            this.get('warningColor'),
            this.get('urgentColor'),
          ],
          "borderWidth": 0,
          "hoverBackgroundColor": [
            // this.get('colors')[0],
            this.get('colors')[2],
            this.get('warningColor'),
            this.get('urgentColor'),
          ],
          "hoverBorderWidth": 0
        }
      ],
      labels: []
    };
    return cData;
  }),

  /* Applies the chart title input. */
  chartTitle: computed('options.name', function() {
    if (isBlank(this.get('options'))) {
      return 'Gauge Chart';
    } else if (isBlank(this.get('options.title'))) {
      return this.get('options.name') + ' Gauge Chart';
    }
    return this.get('options.title');
  }),

  /* Applies the visual options, value and/or percentage display. */
  dialDisplay: computed('dialValue', 'options.visualOptions', function() {
    let text, visualOptions = this.get('options.visualOptions');
    let dialValue = this.get('dialValue');
    if (visualOptions.value.isChecked) {
      text = dialValue.toString();
    }
    if (visualOptions.percentage.isChecked && this.get('filteredRecords.length') !== 0) {
      text = text ? text + ', ' : '';
      text += (Math.floor(100 * dialValue / this.get('filteredRecords.length')).toString() + '%')
    }
    return text || '';
  }),

  chartOptions: computed('dialRange', 'dialValue', 'dialDisplay',function() {
    let options = {
      gauge: true,
      labels: {
        dial: this.get('dialValue'),
        min: this.get('dialRange').min,
        max: this.get('dialRange').max,
      },
      rotation: -1.0 * Math.PI, // start angle in radians
      circumference: Math.PI, // sweep angle in radians
      title: {
        "display": true,
        "text": this.get('dialDisplay'),
        "position": "bottom"
      },
      tooltips: {
        "enabled": false,
      },
      onResize: function(/* chart, size */) {
      }
    };
    return options;
  }),

  actions: {
    onFilterButtonClick() {
      let workspace = this.get('workspace');
      let topState = workspace.get('topState');
      while (topState === 'gaugeWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Gauge Chart";
      let widget = this.get('widget');

      workspace.pushState('gaugeWidget', {
        displayName, widget
      });
    },
  }
});
