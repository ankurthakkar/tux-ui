/* global Chart */
import EmberChart from "../../../ember-chart-fix/component"
import { computed } from '@ember/object';

const NEEDLE_RADIUS = 80;

export default EmberChart.extend({
  chart: null,

  needleAngle: computed('options.labels.{dial,min,max}', function() {
    let options = this.get('options');
    let needleAngle = (options.labels.dial - options.labels.min) / (options.labels.max - options.labels.min);
    return options.rotation + (needleAngle * Math.PI);
  }),

  drawGaugeComponents() {
    let chart = this.get('chart');
    let options = chart.options;
    let ctx = chart.chart.ctx;
    let cw = chart.width;
    let ch = chart.height;

    // Get positions for dial
    let cx = cw / 2;
    let cy = ch - (ch / 4);
    let radianAngle = this.get('needleAngle');
    let radius = NEEDLE_RADIUS;

    ctx.translate(cx, cy);
    ctx.rotate(radianAngle);
    ctx.beginPath();
    ctx.moveTo(0, -5);
    ctx.lineTo(radius, 0);
    ctx.lineTo(0, 5);
    ctx.fillStyle = "#007bff";
    ctx.fill();
    ctx.rotate(-radianAngle);
    ctx.translate(-cx, -cy);
    ctx.beginPath();
    ctx.arc(cx, cy, 7, 0, Math.PI * 2);
    ctx.fill();

    // configure label text
    let fontStyle = 'Avenir Next';
    let txt1 = options.labels.min;
    let txt2 = options.labels.max;
    // let txt3 = options.labels.dial;
    let color = '#666666';
    ctx.font = "12px " + fontStyle;
    ctx.fillStyle = color;

    ctx.textAlign = 'center';
    ctx.testBaseline = 'bottom';
    let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
    let yPadding = 7;
    let yPosition = ((chart.chartArea.top + chart.chartArea.bottom) + yPadding);
    ctx.fillText(txt1, centerX - NEEDLE_RADIUS, yPosition);
    ctx.fillText(txt2, centerX + NEEDLE_RADIUS, yPosition);
    // ctx.fillText(txt3, centerX, yPosition);

  },
  didReceiveAttrs() {
    this._super(...arguments);

    let _this = this;
    Chart.plugins.register({
     afterDraw: function (chart) {
       if (chart.config.options.gauge) {
         _this.drawGaugeComponents();
       }
     }
    });
    Chart.plugins.register({
      resize: function() {}
    })
  },

  didInsertElement() {
    let context = this.get('element');
    let data    = this.get('data');
    let type    = this.get('type');
    let options = this.get('options');

    let chart = new Chart(context, {
      type: type,
      data: data,
      options: options
    });
    this.set('chart', chart);
  },
});
