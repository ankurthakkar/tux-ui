/* eslint-disable ember/avoid-leaking-state-in-ember-objects */
import IQUXColumnWidget from '../column-widget/component';
import { Promise } from 'rsvp';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import {  get } from '@ember/object';
import ENV from 'adept-iq/config/environment';
import _ from 'lodash';

const INCLUDES = [];

export default IQUXColumnWidget.extend({
  classNames: ['passengers-widget'],
  editModal: service(),
  store: service(),
  booking:service(),
  ajax: service(),
  errorMessage: service(),
  activeContext: service(),
  session: service(),
  editComponent: 'iq-widgets/passengers-form-widget',
  newTripComponent: 'iq-widgets/new-trip-form-widget',

  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,

  // TODO: saveRecordFunction shall be moved to a service later on
  // This save is specific to saving passengers and all related models.
  saveRecordFunction(records) {
    let addressAliasChangeArray = [];
    let travelNeedsChangeArray = [];
    let bookingLocations = [];
    let bookingTravelNeeds = [];

    function saveDeleteAllEligibilities(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {
        deleteObject._record.deleteRecord();
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => object.save()));
        });
    }

    function saveDeleteAllTravelNeeds(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {
        travelNeedsChangeArray.push(deleteObject._record.get('travelNeedType.name'));
        deleteObject._record.deleteRecord();
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => {
            if (object.dirtyType) {
              travelNeedsChangeArray.push(object.get('travelNeedType.name'));
            }
            object.save();
          }));
        });
    }

    function saveNewTravelNeedObjects(rider, oldObjects, newObjects) {
      return Promise.all(newObjects.map(newObject => {
        if (newObject.get('count') !== null && newObject.get('count') > 0) {
          newObject.set('rider', rider);
          newObject.save();
          travelNeedsChangeArray.push(newObject.get('travelNeedType.name'));
          return newObject;
        }
      }));
    }

    function saveLocationObject(object) {
      if (object !== undefined) {
        return Promise.all([object.save()]);
      }
      return Promise.all([]);
    }


    function saveNewAddressObjects(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {
        deleteObject._record.deleteRecord();
        addressAliasChangeArray.push(deleteObject._record.alias);
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => {
            return saveLocationObject(object.locations.firstObject)
              .then(() => {
                if (object.dirtyType) {
                  addressAliasChangeArray.push(object.alias);
                  addressAliasChangeArray = _.compact(addressAliasChangeArray);
                  addressAliasChangeArray = _.uniq(addressAliasChangeArray);
                }

                return object.save();
              });
            }));
        });
    }

    function collectBookingChange(riderId, aliasAffected, travelNeedsAffected) {
      const option = {
        filter: `and(eq(rider.riderId,'${riderId}'),eq(booking.exportStatus,'notExported'))`
      };

      return this.get('store').query('bs-leg', option).then((results) => {

        results.forEach((record) => {
          let travelNeeds = record.legTravelNeeds;

          if (_.includes(aliasAffected, record.get('origin.alias'))) {
            bookingLocations.push(record);
          }
          if (_.includes(aliasAffected, record.get('destination.alias'))) {
            bookingLocations.push(record);
          }

          if (travelNeeds.length > 0) {
            travelNeeds.forEach(travelNeed => {
              const travelNeedType = this.get('store').peekRecord('bs-travel-need-type', travelNeed.get('travelNeedType.id'));
              if (_.includes(travelNeedsAffected, travelNeedType.name)) {
                bookingTravelNeeds.push(record);
              }
            });

          }
        });
      });
    }

    function collectDispatchChange(riderId, aliasAffected, travelNeedsAffected) {
      const option = {
        filter: `and(eq(rider.externalId,'${riderId}'),eq(stops.state,'notDispatched'))&include=stops,stops.place,rider,rider.travelNeeds`
      };

      return this.get('store').query('trip', option).then((results) => {
        results.forEach((record) => {
          let travelNeeds = record.get('rider.travelNeeds');
          let stops = record.get('stops')
          stops.forEach((stop) => {
            if (_.includes(aliasAffected, stop.get('place.name'))) {
              bookingLocations.push(record);
            }
          });

          if (travelNeeds.length > 0) {
            travelNeeds.forEach(travelNeed => {
              const travelNeedType = this.get('store').peekRecord('travel-need-type', travelNeed.get('travelNeedType.id'));
              if (_.includes(travelNeedsAffected, travelNeedType.name)) {
                bookingTravelNeeds.push(record);
              }
            });

          }
        });
      });
    }

    function notifyUserOfAffectedBookingChange(riderId, aliasAffected, travelNeedsAffected) {
      collectBookingChange.bind(this)(riderId, aliasAffected, travelNeedsAffected)
        .then(() => {
          return collectDispatchChange.bind(this)(riderId, aliasAffected, travelNeedsAffected);
        })
        .then(() => {
          let message = '';

          if (bookingLocations.length) {
            message += `This address change will not apply to ${bookingLocations.length} current trips.`;
          }

          if (bookingTravelNeeds.length) {
            message += `\nThis travel need change will not apply to ${bookingTravelNeeds.length} current trips.`;
          }

          if (message.length > 0) {
            this.get('errorMessage').pushInfo({ detail: message});
          }
        });
    }

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        saveNewAddressObjects(record.get('primaryAddresses'))
          .then((objects) => {
            record.set('primaryAddresses', objects);

            saveNewAddressObjects(record.get('favoriteAddresses'))
              .then(() => {
                record.save()
                  .then(() => {
                    saveDeleteAllTravelNeeds(record.get('travelNeeds'))
                    .then(() => {
                      saveNewTravelNeedObjects(record, record.get('travelNeeds'), record.get('newTravelNeeds'))
                      .then((objects) => {
                        objects.forEach(object => {
                          if (!_.isUndefined(object)) {
                            record.get('newTravelNeeds').removeObject(object);
                            record.get('travelNeeds').addObject(object);

                        }});

                        saveDeleteAllEligibilities(record.get('eligibilities'))
                        .then((/* objects */) => {
                          record.set('isForceDirty', false);
                          this.get('activeContext').refreshTableContent(this.get('config.modelName'));

                        })
                        .then(() => {
                          if (addressAliasChangeArray.length > 0 || travelNeedsChangeArray.length > 0) {
                            notifyUserOfAffectedBookingChange.bind(this)(record.id, addressAliasChangeArray, travelNeedsChangeArray);
                          }
                        });
                      });
                    });
                });
              });
            })
          .catch(e => {
            reject(e);
          });
      });
    });
  },
  saveTripRecordFunction(records) {
    let me = this;

    return new Promise((resolve, reject) => {
      records.forEach(record => {

        if(record.get('id') !== null){
          let bookingData =  this.get('booking').bookingData();
          let preparedBookingData =  this.get('booking').prepareBookingData(bookingData,record);
          preparedBookingData.data.id = record.get('id');

          var json = JSON.stringify(preparedBookingData);
          let session = this.get('session');

          return this.get('ajax').put(ENV.API.avlmBookingService.host+'/production/booking/'+record.get('id'), {
            method: 'PUT',
            contentType: "application/json",
            crossDomain: true,
            headers: {
              "Authorization": `Bearer ${session.data.authenticated.token}`
            },
            data: json
          }).then((response) => {
            record.set('isForceDirty', false);
            record.set('id',response.data.id);
            me.get('activeContext').refreshTableContent('bs-booking');
          }).catch(function(error) {
            reject(error);
          });

        } else {
          let bookingData =  this.get('booking').bookingData();
          let preparedBookingData =  this.get('booking').prepareBookingData(bookingData,record);

          var json = JSON.stringify(preparedBookingData);
          let session = this.get('session');

          return this.get('ajax').post(ENV.API.avlmBookingService.host+'/production/booking', {
            method: 'POST',
            contentType: "application/json",
            crossDomain: true,
            headers: {
              'Authorization': `Bearer ${session.data.authenticated.token}`
            },
            data: json
          }).then((response) => {
            record.set('isForceDirty', false);
            record.set('id',response.data.id);
            me.get('activeContext').refreshTableContent('bs-booking');
          }).catch(function(/*error*/) {
            // HACK: work around for Access-Control-Allow-Origin issue.
            // trip actually gets created.
            // reject(error);
          });
        }
      })

    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'New Passenger',
      action: function() {
        let newModel = this.get('store').createRecord(this.get('config.modelName'));

        newModel.set('dateOfBirth', new Date());
        this.get('records').pushObject(newModel);
        let editComponent = this.get('editComponent');

        let rmsRider = this.get('store').createRecord('rmsRider');
        newModel.set('rmsRider', rmsRider);
        this.get('editModal').open(editComponent, [rmsRider], this.get('saveRecordFunction').bind(this));
      }.bind(this)
    }
    ]);

    this.set('singleActions', [{
      name: 'Edit',
      action: (model) => {
        let editComponent = this.get('editComponent');
        let rmsRider = model.get('rmsRider.content');

        this.get('editModal').open(editComponent, [rmsRider], this.get('saveRecordFunction').bind(this));
      }
    },
    {
      name: 'Add Trip',
      action: (model) => {
        let rmsRider = model.get('rmsRider.content');

        //HACK TO REMOVE
        let travelNeeds = rmsRider.get('travelNeeds');
        if (!travelNeeds) { travelNeeds = [] }
        //END

        const bsRider = this.get('store').createRecord('bs-rider', {
          riderId: get(rmsRider, 'riderId'),
          firstName: get(rmsRider, 'firstName'),
          middleName: get(rmsRider, 'middleName'),
          lastName: get(rmsRider, 'lastName'),
          notes: get(rmsRider, 'notes'),
          travelNeeds: travelNeeds
          //extraPassengers:get(model,'extraPassengers'),
          //equipments:get(model,'equipments')
        });


        let originlocations = [];
        let origin = this.get('store').createRecord('bs-location');
        origin.set('type','pick');
        originlocations.push(origin);

        let destinationlocations = [];
        let destination = this.get('store').createRecord('bs-location');
        destination.set('type','drop');
        destinationlocations.push(destination);

        let subscriptions = [];
        let subscription = this.get('store').createRecord('bs-leg-subscription');
        subscriptions.push(subscription);

        let fareTypes = this.get('store').peekAll('bs-fare-type');

        const booking = this.store.createRecord('bs-booking', {
          planState: 1,
          anchor: 'pick',
          fare: '0',
          rider: bsRider,
          origin: origin,
          destination: destination,
          originlocations:originlocations,
          destinationlocations:destinationlocations,
          subscriptions :subscriptions,
          travelNeeds: get(bsRider, 'travelNeeds'),
          bookingNotes: bsRider.get('notes'),
          driverNotes: '' ,
          fareType :fareTypes.firstObject,
          riderAddresses: get(rmsRider, 'riderAddresses'),
          eligibilities: get(rmsRider, 'eligibilities'),
          requestedTime: new Date()
        });

        this.get('records').pushObject(booking);
        let newTripComponent = this.get('newTripComponent');
        this.get('editModal').open(newTripComponent, [booking], this.get('saveTripRecordFunction').bind(this));
    }
  }]);

    this.set('bulkActions', [{
      name: 'Bulk Edit',
      action: (models) => {
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, models, this.get('saveRecordFunction').bind(this));
      }
    }]);
  }
});
