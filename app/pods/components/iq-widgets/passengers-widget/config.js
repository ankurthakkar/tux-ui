export default {
  component: 'iq-widgets/passengers-widget',
  rowComponent: 'table/rows/otp-formatted-row',
  modelName: 'iq-rider',

  defaultSortId: 'riderId',
  defaultSortAsc: false,

  title: 'Passengers',

  columns: [
    {
      id: 'riderId',
      index: 2,
      type: 'uuid',
      isKey: true,
      label: 'ID',
      valuePath: 'id',
      editable: false,
      hidden: false,
      defaultWidth: 30
    },
    {
      id: 'firstName',
      index: 0,
      type: 'text',
      label: 'First Name',
      valuePath: 'rmsRider.firstName',
      editable: true,
      hidden: false,
      searchable: true,
      defaultWidth: 60,
      validation: {
        type: 'text',
        minValue: 1,
        maxValue: 30
      }
    },
    {
      id: 'lastName',
      index: 1,
      type: 'text',
      label: 'Last Name',
      valuePath: 'rmsRider.lastName',
      editable: true,
      hidden: false,
      searchable: true,
      defaultWidth: 75,
      validation: {
        type: 'text',
        minValue: 1,
        maxValue: 30
      }
    },
    {
      id: 'fullPhoneNumber',
      index: 3,
      type: 'text',
      label: 'Phone Number',
      valuePath: 'rmsRider.fullPhoneNumber',
      editable: true,
      hidden: false,
      searchable: true,
      defaultWidth: 90,
      validation: {
        type: 'phoneNumber',
        minValue: 10,
        maxValue: 10
      }
    },
    {
      id: 'dateOfBirth',
      index: 4,
      type: 'date',
      label: 'Date Of Birth',
      valuePath: 'rmsRider.dateOfBirth',
      editable: true,
      hidden: false,
      format: 'MM/DD/YY',
      validation: {
        type: 'date',
        minValue: '1900-01-01',
        maxValue: 'now'
      }
    },
    {
      id: 'primaryAddress',
      index: 6,
      type: 'enum',
      label: 'Primary Addresses',
      valuePath: 'rmsRider.formattedPrimaryAddresses',
      cellDesc: 'Addresses',
      editable: false,
      hidden: false,
      defaultWidth: 120
    },
    {
      id: 'travelNeeds',
      index: 8,
      type: 'enum',
      label: 'Travel Needs',
      valuePath: 'rmsRider.formattedTravelNeeds',
      cellDesc: 'Travel Needs',
      editable: false,
      hidden: false
    },
    {
      id: 'favoriteAddresses',
      index: 7,
      type: 'enum',
      label: 'Favorite Addresses',
      valuePath: 'rmsRider.formattedFavoriteAddresses',
      cellDesc: 'Addresses',
      editable: false,
      hidden: false,
      defaultWidth: 120
    },
    {
      id: 'eligibilities',
      index: 9,
      type: 'enum',
      label: 'Booking eligible',
      valuePath: 'rmsRider.formattedEligibilities',
      cellDesc: 'Eligible Services',
      editable: false,
      hidden: false
    },
    {
      id: 'notes',
      index: 5,
      type: 'text',
      label: 'Notes',
      valuePath: 'rmsRider.notes',
      editable: true,
      hidden: false,
      searchable: true
    }
  ]
};
