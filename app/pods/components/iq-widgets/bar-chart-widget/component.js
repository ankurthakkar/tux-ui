import ChartjsWidget from '../../generic-widgets/chartjs-widget/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isBlank, isPresent } from '@ember/utils';
import {
  buildFilterFunction,
  buildCompoundFilterNode
} from 'adept-iq/utils/filters';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';

export default ChartjsWidget.extend({
  workspaceContext: service(),
  classNames: ['bar-chart-widget'],

  widget: null,

  options: computed.alias('widget.state.dataType'),

  /* Applies the choice of data source. */
  chartRecords: computed('options.modelName', 'workspaceContext.workspaceData.[]',function() {
    let modelName = this.get('options.modelName');
    if (isBlank(modelName)) return [];
    let wrappedRecords = this.get('workspaceContext.workspaceData').filterBy('modelName', modelName)
    return wrappedRecords.map((a) => a.record);
  }),

  /* Builds the chosen filters. */
  filterFunction: computed('options.columns', function() {
    let filterColumns = Object.values(this.get('options.columns')).map((col) => new FilterColumn(col));
    let args = filterColumns.map((column) => {
        return column.get('filterNode');
      });
    return buildFilterFunction(buildCompoundFilterNode('and', args));
  }),

  /* Applies the choice of filters. */
  filteredRecords: computed('chartRecords', 'filterFunction', function() {
    return this.get('chartRecords').filter(this.get('filterFunction'))
  }),

  chartSum: computed.alias('filteredRecords.length'),

  /* Gets the choice for slicing the data. */
  slicedBy: computed('options.displayOptions', function() {
    if (isBlank(this.get('options.displayOptions'))) return;
    return this.get('options.displayOptions').selected;
  }),

  /* Applies the choice for slicing the data. */
  recordSlices: computed('filteredRecords', 'slicedBy', function() {
    let categories = {},
      filteredRecords = this.get('filteredRecords'),
      slicedBy = this.get('slicedBy');

    if (isBlank(slicedBy)) return categories;

    let filterKey = this.get('slicedBy.id');
    let metricOption = this.get('slicedBy.metricOption.id');
    this.get('slicedBy.valueCategories').forEach((category) => {
      switch (metricOption) {
        case 'count':
          categories[category.label] = filteredRecords.filterBy(filterKey, category.value).length;
          break;
        case 'sumOccurrences': {
          categories[category.label] = filteredRecords.reduce((a,b) => {
            return a + b.get(filterKey).filter((item) => item === category.value).length;
          }, 0);
          break;
        }
      }

    });
    // { 'label1': <value>, 'label2': <value> }
    return categories;
  }),

  /* Applies the choices for displaying data counts and/or percentages. */
  aggregatedSliceData: computed('recordSlices', 'options.visualOptions.@each.isChecked', function() {
    let visualOptions = this.get('options.visualOptions');
    let showValues = visualOptions.findBy('id', 'value').isChecked;
    let showPercentage = visualOptions.findBy('id', 'percentage').isChecked;

    let totalCount =  this.get('chartSum');
    let data = [];
    Object.entries(this.get('recordSlices')).forEach((slice) => {
      let label = slice[0];
      let numRecords = slice[1];
      if (showValues) {
        label += (', ' + numRecords);
      }
      if (showPercentage && totalCount !== 0) {
        label += (', ' + Math.floor(100 * numRecords / totalCount) + '%');
      }
      data.push({ label: label, value: numRecords });
    });
    // [{...}, {...}, ...]
    return data;
  }),

  chartData: computed('aggregatedSliceData', function() {
    let aggregatedData = this.get('aggregatedSliceData');
    if (isBlank(aggregatedData)) return { data:[], labels:[] };

    let data = [], labels = [], backgroundColor = [];
    let colors = this.get('colors');
    aggregatedData.forEach((dataSlice, i) => {
      data.push(dataSlice.value);
      labels.push(dataSlice.label);
      backgroundColor.push(colors[i % colors.length]);
    });

    let datasets = [{
      data: data,
      backgroundColor: backgroundColor,
      borderWidth: 1
    }];

    return { datasets, labels };
  }),

  /* Applies the chart title input. */
  chartTitle: computed('widget.state.dataType.{name,title}', function() {
    let title = this.get('widget.state.dataType.title');
    if (isPresent(title)) return title;

    let name = this.get('widget.state.dataType.name');
    if (isPresent(name)) {
      return `${name} Bar Chart`;
    }

    return 'Bar Chart';
  }),

  chartOptions: computed('', function() {
    if (isBlank(this.get('widget.state'))) return {};

    let options = {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            min: 0
          }
        }]
      }
    }
    return options
  }),

  actions: {
    onFilterButtonClick() {
      let workspace = this.get('workspace');
      let topState = workspace.get('topState');
      while (topState === 'barChartWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Bar Chart";
      let widget = this.get('widget');

      workspace.pushState('barChartWidget', {
        displayName,
        widget
      });
    },
  }
});
