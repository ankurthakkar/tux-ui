import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';


export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),

  validateRecords() {
    let valid = true;
   // let records = this.get('editableRecords');

    this.set('editModal.errors', []);


    return valid;
  },

  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      onUndoClick() {
        /*let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);*/
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }

      // onCellValueChange(record, valuePath, value, options) {
      //   if (valuePath === 'vehicles.firstObject.name') {
      //     let vehicle = record.get('vehicles.firstObject');

      //     record.vehicles.removeObject(vehicle);
      //   }
      //   else {
      //     this.get('service').setRecordValue(record, valuePath, value, options);
      //   }

      // }
    }
});
