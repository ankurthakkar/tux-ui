export default {

  editableSections: [ {
    title: 'Read Only',
    fields: [
      {
        id: 'riderFirstName',
        label: 'First Name',
        valuePath: 'rider.firstName',
        editable: false
      },
      {
        id: 'riderLastName',
        label: 'Rider Last Name',
        valuePath: 'rider.lastName',
        editable: false
      },
      {
        id: 'externalId',
        label: 'External ID',
        valuePath: 'externalId',
        editable: false
      },
      {
        id: 'requestedTime',
        label: 'Request Time',
        valuePath: 'requestedTime',
        editable: false
      },
      {
        id: 'pickupaddress',
        label: 'Pickup Address ',
        valuePath: 'pick.place.address',
        editable: false
      },
      {
        id: 'dropaddress',
        label: 'Dropoff Address',
        valuePath: 'drop.place.address',
        editable: false
      }]
  },

  {
    title: 'Editable',
    fields: [
      {
      id: 'timestamp',
      type: 'datetimeflatpickr',
      label: 'Date Time',
      valuePath: 'timestamp',
      editable: true,
      hidden: false,
      format: 'YYYY-MM-DD hh:mm A',
      defaultWidth: 50
    },
    {
      id: 'noShowReason',
      type: 'enum',
      label: 'Type',
      valuePath: 'noShowReason',
      cellDesc: 'noShowReason',
      editable: true,
      hidden: false,
      extra: {
        optionModelName: 'no-show-reason-code',
        optionSearchPath: 'description'
      }
    },
    {
      id: 'notes',
      type: 'text',
      editable: true,
      label: 'Notes',
      valuePath: 'noShowNotes'
    }]
  }]
};
