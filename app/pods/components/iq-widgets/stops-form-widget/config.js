export default {
  editableSections: [{
    title: 'Section 1',
    fields: [{
      id: 'id',
      type: 'uuid',
      label: 'ID',
      valuePath: 'id',
      editable: false
    }]
  }, {
    title: 'Section 2',
    fields: [{
      id: 'state',
      type: 'enum',
      label: 'State',
      valuePath: 'state',
      extra: {
        options: [
          'Not Started',
          'Sign On',
          'Start',
          'In-Progress',
          'On-Break',
          'End',
          'Signoff'
        ],
        allowMultiple: true
      }
    }, {
      id: 'xmin',
      type: 'text',
      label: 'X Min',
      valuePath: 'xmin'
    }]
  }]
};
