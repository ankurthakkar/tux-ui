import ChartjsWidget from '../../generic-widgets/chartjs-widget/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isBlank, isPresent } from '@ember/utils';
import {
  buildFilterFunction,
  buildCompoundFilterNode
} from 'adept-iq/utils/filters';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';

export default ChartjsWidget.extend({
  classNames: ['analytics-widget','box-widget'],
  classNameBindings: ['stateClass','widthConstrained'],

  workspaceContext: service(),

  widget: null,

  options: computed.alias('widget.state.dataType'),

  /* Applies the choice of data source. */
  chartRecords: computed('options.modelName', 'workspaceContext.workspaceData.[]',function() {
    let modelName = this.get('options.modelName');
    if (isBlank(modelName)) return [];
    let wrappedRecords = this.get('workspaceContext.workspaceData').filterBy('modelName', modelName)
    return wrappedRecords.map((a) => a.record);
  }),

  /* Builds the chosen filters. */
  filterFunction: computed('options.columns', function() {
    let filterColumns = Object.values(this.get('options.columns')).map((col) => new FilterColumn(col));
    let args = filterColumns.map((column) => {
        return column.get('filterNode');
      });
    return buildFilterFunction(buildCompoundFilterNode('and', args));
  }),

  /* Applies the choice of filters. */
  filteredRecords: computed('chartRecords', 'filterFunction', function() {
    return this.get('chartRecords').filter(this.get('filterFunction'))
  }),

  /* Applies the choices of fields and metrics. */
  boxMetric: computed('options.displayOptions.selected',
                           'filteredRecords',
                           function() {
    let displaySelection = this.get('options.displayOptions.selected');
    if (isBlank(displaySelection) ||
        isBlank(displaySelection.valueKey) ||
        isBlank(displaySelection.valueCategory) ||
        isBlank(displaySelection.metricOption)) return;

    let slicedRecords = this.get('filteredRecords').filterBy(
                          displaySelection.valueKey,
                          displaySelection.valueCategory.value);

    let value = this.processRecords(slicedRecords, displaySelection.metricOption.id);
    let name = displaySelection.label + ' - ' + displaySelection.valueCategory.label;

    return { name, value };
  }),

  stateClass: computed('boxMetrics', 'options.boxColor.{orange,red}', function() {
    if (isBlank(this.get('options'))) return;
    let value = this.get('boxMetric.value');

    if (value >= this.get('options.boxColor.red')) {
      return 'red-state'
    } else if (value >= this.get('options.boxColor.orange')) {
      return 'orange-state';
    } else {
      return;
    }
  }),

  widthConstrained: computed('widget.width', function() {
    if (this.get('widget.width') === 1) {
      return 'width-constrained';
    }
    return;
  }),

  /* Applies the chart title input. */
  chartTitle: computed('widget.state.dataType.{name,title}', function() {
    let title = this.get('widget.state.dataType.title');
    if (isPresent(title)) return title;

    let name = this.get('widget.state.dataType.name');
    if (isPresent(name)) {
      return `${name} Box Widget`;
    }

    return 'Box Widget';
  }),

  buildColumnFilter() {
    let args = this.get('options.dataColumns').map((column) => {
      return column.get('filterNode');
    });

    return buildCompoundFilterNode('and', args);
  },

  processRecords(records, metricOption /*, valueKey */) {
    switch (metricOption) {
      case 'count':
        return records.length
    }
  },

  actions: {
    onFilterButtonClick() {
      let workspace = this.get('workspace');
      let topState = workspace.get('topState');
      while (topState === 'boxWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Box Analytics";
      let widget = this.get('widget');

      workspace.pushState('boxWidget', {
        displayName, widget
      });
    },
  }
});
