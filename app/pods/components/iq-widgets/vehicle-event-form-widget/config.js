export default {

  editableSections: [ {
    title: 'Read Only',
    fields: [{
      id: 'routeId',
      type: 'text',
      label: 'Route Id',
      valuePath: 'name',
      editable: false
    },{
      id: 'vehicle',
      type: 'text',
      label: 'Vehicle',
      valuePath: 'assignedVehicle.id',
      editable: false
    }]
  },{
    title: 'Editable',
    fields: [
      {
      id: 'timestamp',
      type: 'datetimeflatpickr',
      label: 'Date Time',
      valuePath: 'timestamp',
      editable: true,
      hidden: false,
      format: 'YYYY-MM-DD hh:mm A',
      defaultWidth: 50
    },
    {
      id: 'odometer',
      type: 'text',
      editable: true,
      label: 'Odometer',
      valuePath: 'odometer'
    }]
  }]
};
