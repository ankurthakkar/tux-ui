import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { copy } from '@ember/object/internals';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),
  errorMessage: service(),
  subsections: false,

  validateRecords() {
    let valid = true;
    this.set('editModal.errors', []);
    return valid;
  },

  sideDrawerTitle: computed(
    'editableRecords',
    'editableSections',
    function(){
      const sections = copy(this.get('editableSections'), true);
      const target = this.get('editableRecords')[0];

      const displaySection = sections.find((f) => {
        const title = f.title;
        const type  = target.get('type');
        return title === type
      });
      
      return displaySection.title
  }),

  subheading: computed(
    'editableRecords',
    'editableSections',
    function(){
      const sections = copy(this.get('editableSections'), true);
      const target = this.get('editableRecords')[0];

      const displaySection = sections.find((f) => {
        const title = f.title;
        const type  = target.get('type');
        return title === type
      });

      return displaySection.description
  }),

  filteredSections: computed(
    'editableRecords',
    'editableSections',
    function() {
      const sections = copy(this.get('editableSections'), true);
      const target = this.get('editableRecords')[0];

      const displaySection = sections.find((f) => {
        const title = f.title;
        const type  = target.get('type');
        return title === type
      });

      if(displaySection){ 
        if(displaySection.subsections){
          this.set('subsections', true);
        }
        if(displaySection.description){
          this.set('subheading', displaySection.description);
        }
        if(displaySection.title){
          this.set('title', displaySection.title);
        }
      }

      return displaySection.subsections || [displaySection];
    }
  ),

  // TODO: WHEN YOU CLICK ADD, ADD ANOTHER THING TO AN ARRAY ENTRY ON THE FORM
  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      addItem() {},

      onCellValueAppend(record, valuePath, modelName) {
        // todo: this should add a new eligibility, but where?
        return this.get('service').pushRecordValue(record, valuePath, modelName);
      },

      onUndoClick() {
        let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }
    }
});
