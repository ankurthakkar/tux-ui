import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),
  errorMessage: service(),

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);
    records.forEach(record => {
      const availabilities = record.availability;

      if (record.driverId === undefined || record.driverId.trim().length < 1) {
        this.get('editModal.errors').pushObject('Invalid Driver Id');
        this.get('errorMessage').pushWarning({ detail:'Invalid Driver Id' });
      }

      availabilities.forEach(availability => {
        const startTime = moment(availability.startTime);
        const endTime = moment(availability.endTime);
        const shiftStart = moment(availability.shiftStart, 'HH:mm:ss');
        const shiftEnd = moment(availability.shiftEnd, 'HH:mm:ss');

        if (endTime.isBefore(startTime)) {
          valid = false;
          this.get('editModal.errors').pushObject('Effective end date cannot be before effective start date');
        }

        if (shiftEnd.isBefore(shiftStart)) {
          valid = false;
          this.get('editModal.errors').pushObject('Shift end cannot be before shift start');
        }
      });

    });

    return valid;
  },

  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      onUndoClick() {
        let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }

      // onCellValueChange(record, valuePath, value, options) {
      //   if (valuePath === 'vehicles.firstObject.name') {
      //     let vehicle = record.get('vehicles.firstObject');

      //     record.vehicles.removeObject(vehicle);
      //   }
      //   else {
      //     this.get('service').setRecordValue(record, valuePath, value, options);
      //   }

      // }
    }
});
