import FormWidget from '../form-widget/component';
import config from './config';

export default FormWidget.extend({
  editableSections: config.editableSections,

  classNames: ['trip-form-widget'],
});
