import OTPFormattedRow from '../../../table/rows/otp-formatted-row/component';

export default OTPFormattedRow.extend({
  attributeBindings: ['draggable'],
  draggable: 'true',
});
