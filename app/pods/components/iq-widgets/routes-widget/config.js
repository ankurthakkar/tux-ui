export default {
  component: 'iq-widgets/routes-widget',
  rowComponent: 'iq-widgets/routes-widget/routes-row',
  modelName: 'route',
  title: 'Routes',

  defaultSortId: 'name',
  defaultSortAsc: true,

  columns: [
    {
      id: 'name',
      type: 'text',
      label: 'Name',
      valuePath: 'name',
      valuePreview: '22',
      editable: true,
      isMapLabelVisible: true,
      searchable: true,
      defaultWidth: 50
    },
    {
      id: 'vehicle',
      type: 'uuid',
      label: 'Vehicle ID',
      valuePath: 'assignedVehicle.name',
      searchable: true,
      defaultWidth: 50
    },
    {
      id: 'driverId',
      type: 'uuid',
      label: 'Driver ID',
      valuePath: 'assignedDriver.externalId',
      searchable: true,
      defaultWidth: 50
    },
    {
      id: 'driverLastName',
      type: 'text',
      label: 'Driver Last Name',
      valuePath: 'assignedDriver.lastName',
      searchable: true,
      defaultWidth: 75
    },
    {
      id: 'driverFirstName',
      type: 'text',
      label: 'Driver First Name',
      valuePath: 'assignedDriver.firstName',
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'status',
      type: 'text',
      label: 'Status',
      valuePath: 'status',
      valuePreview: 'Planned',
      editable: true,
      isMapLabelVisible: true,
      format: 'HH:mm A',
      defaultWidth: 75
    },
    {
      id: 'plannedStartTime',
      type: 'date',
      label: 'Planned Start Time',
      valuePath: 'plannedStartTime',
      valuePreview: '09:00 AM',
      editable: true,
      hidden: true,
      format: 'HH:mm A'
    },
    {
      id: 'actualStartTime',
      type: 'date',
      label: 'Actual Start Time',
      valuePath: 'routeStartTime',
      valuePreview: '09:05 AM',
      hidden: true,
      format: 'HH:mm A'
    },
    {
      id: 'plannedBreaks',
      type: 'enum',
      label: 'Planned Breaks',
      valuePath: 'plannedDriverBreaks',
      cellDesc: 'Driver Breaks',
      hidden: true
    },
    {
      id: 'actualBreaks',
      type: 'enum',
      label: 'Actual Breaks',
      valuePath: 'actualDriverBreaks',
      cellDesc: 'Driver Breaks',
      hidden: true
    },
    {
      id: 'vehicleType',
      type: 'text',
      label: 'Vehicle Type',
      valuePath: 'assignedVehicle.vehicleType.name',
      hidden: true
    },
    {
      id: 'plannedEndTime',
      type: 'date',
      label: 'Planned End Time',
      valuePath: 'plannedEndTime',
      valuePreview: '04:00 PM',
      editable: true,
      format: 'HH:mm A',
      hidden: true
    },
    {
      id: 'actualEndTime',
      type: 'date',
      label: 'Actual End Time',
      valuePath: 'actualStartEvent.timestamp',
      valuePreview: '04:35 PM',
      format: 'HH:mm A',
      hidden: true
    },
    {
      id: 'plannedStartDate',
      type: 'date',
      label: 'Planned Start Date',
      valuePath: 'plannedStartTime',
      valuePreview: '2018-05-15',
      editable: true,
      format: 'DD/MM/YYYY',
      hidden: true
    },
    {
      id: 'actualStartDate',
      type: 'date',
      label: 'Actual Start Date',
      valuePath: 'actualStartEvent.timestamp',
      valuePreview: '2018-05-15',
      format: 'DD/MM/YYYY',
      hidden: true
    },
    {
      id: 'plannedEndDate',
      type: 'date',
      label: 'Planned End Date',
      valuePath: 'plannedEndTime',
      valuePreview: '2018-05-15',
      editable: true,
      format: 'DD/MM/YYYY',
      hidden: true
    },
    {
      id: 'actualEndDate',
      type: 'date',
      label: 'Actual End Date',
      valuePath: 'actualEndEvent.timestamp',
      valuePreview: '2018-05-15',
      format: 'DD/MM/YYYY',
      hidden: true
    },
    {
      id: 'lifo',
      type: 'number',
      label: 'LIFO',
      valuePath: 'lifoDepth',
      valuePreview: '0',
      editable: true,
      hidden: true
    },
    {
      id: 'factor',
      type: 'number',
      label: 'Factor',
      valuePath: 'routeFactor',
      valuePreview: '1',
      editable: true,
      hidden: true
    },
    {
      id: 'otp',
      type: 'string',
      label: 'OTP Status',
      valuePath: 'otpLabel',
      valuePreview: 'On Time',
      highlightable: true
    },
  ]
};
