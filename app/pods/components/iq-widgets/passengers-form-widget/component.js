import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { isEmpty } from '@ember/utils';

export default FormWidget.extend({
  editModal: service(),
  errorMessage: service(),
  editableSections: config.editableSections,

  classNames: ['passenger-form-widget', 'data-test-passenger-form-widget'],

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);
    records.forEach(record => {
      const eligibilities = record.eligibilities;

      if (isEmpty(record.dateOfBirth)) {
        valid = false;
        this.get('editModal.errors').pushObject('Date of birth cannot be blank.');
      }

      if (isEmpty (record.firstName) || isEmpty (record.lastName) || record.firstName.length === 0 || record.lastName.length === 0) {
        valid = false;
        this.get('editModal.errors').pushObject('First name and last name cannot be blank.');
      }

      eligibilities.forEach(eligibility => {
        const from = moment(eligibility.from);
        const to = moment(eligibility.to);

        if (to.isBefore(from)) {
          valid = false;
          this.get('editModal.errors').pushObject('Eligibility to date cannot be before from date');
        }
      });

    });

    return valid;
  },

  actions: {
    // override undo because we will have to deal with undoing created
    // models for addresses, travel needs, and eligibility later.
    // IQUX-510
    onUndoClick() {
      let lastUndoState = this.get('editModal.undoHistory').popObject();

      if (lastUndoState === null) {
        let records = this.get('editableRecords');

        records.forEach(record => {
          record.set('isForceDirty', false);
        });

      }
      else {
        lastUndoState.forEach(({ record, properties }) => {
          record.setProperties(properties);
        });
      }

      this.set('isLastUndoStateCommitted', true);
    },

    onApplyClick() {
      if (this.validateRecords()) {
        this.get('service').apply();
      }
    }
  }
});
