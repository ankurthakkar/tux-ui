import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { isNone , isEmpty } from '@ember/utils';
import { get } from '@ember/object';
import $ from 'jquery';


export default FormWidget.extend({
  editModal: service(),
  geocode: service(),
  editableSections: config.editableSections,

  classNames: ['new-trip-form-widget'],

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);

    records.forEach(record => {
      const isEligible = record.get('eligibilities');
        const requestedTime = moment(record.get('requestedTime'));

        if (isNone(requestedTime)) {
          valid = false;
          this.get('editModal.errors').pushObject('Date should be there');
        }

        const eligibleRecord = isEligible.find((value) => {

            const from = moment(get(value, 'from'), "DD/MM/YYYY");
            const to = moment(get(value, 'to'), "DD/MM/YYYY");
            const tripDate = moment(requestedTime, "DD/MM/YYYY");

            return tripDate.isAfter(from) && tripDate.isBefore(to);
        });

        if (isNone(eligibleRecord)) {
          valid = false;
          this.get('editModal.errors').pushObject('Eligibility to date cannot be before from date');
        }

        // Hack: Missing state from base data. Easier to fix it here for
        // convention.
        if(isEmpty(record.get('originlocations.firstObject.state'))) {
          record.set('originlocations.firstObject.state', 'Wa');
        }

        if(isEmpty(record.get('destinationlocations.firstObject.state'))) {
          record.set('destinationlocations.firstObject.state', 'Wa');
        }

        if(isEmpty(record.get('originlocations.firstObject.latlng'))) {
          valid = false;
          this.get('editModal.errors').pushObject('Pickup address should be there');
        } else if(isEmpty(record.get('originlocations.firstObject.geoNode'))) {
          valid = false;
          this.get('editModal.errors').pushObject('Pickup address should be valid');
        }

        if(isEmpty(record.get('destinationlocations.firstObject.latlng'))) {
          valid = false;
          this.get('editModal.errors').pushObject('Drop address should be there');
        } else if(isEmpty(record.get('destinationlocations.firstObject.geoNode'))) {
          valid = false;
          this.get('editModal.errors').pushObject('Drop address should be valid');
        }

      });

    return valid;
  },


  actions: {
    // override undo because we will have to deal with undoing created
    // models for addresses, travel needs, and eligibility later.
    // IQUX-510
    onUndoClick() {
      let lastUndoState = this.get('editModal.undoHistory').popObject();

      if (lastUndoState === null) {
        let records = this.get('editableRecords');

        records.forEach(record => {
          record.set('isForceDirty', false);
        });

      }
      else {
        lastUndoState.forEach(({ record, properties }) => {
          record.setProperties(properties);
        });
      }

      this.set('isLastUndoStateCommitted', true);
    },

    onApplyClick() {

      if (this.validateRecords()) {
        $('html,body,.tomtom-map').removeClass('custom-cursor');
        this.get('geocode').activateGeocode(false);
        this.set('addressRecord', null);

        this.get('service').apply();
      }
    }
  }
});
