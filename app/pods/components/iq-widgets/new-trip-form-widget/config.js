export default {
  editableSections: [{
    title: 'Read Only',
    fields: [
    {
        id: 'id',
        type: 'text',
        label: 'ID',
        valuePath: 'id',
        editable: false
    },
    {
      id: 'firstName',
      type: 'text',
      label: 'Passenger First Name',
      valuePath: 'rider.firstName',
      editable: false
    }, {
      id: 'lastName',
      type: 'text',
      label: 'Passenger Last Name',
      valuePath: 'rider.lastName',
      editable: false
    },
    {
      id: 'riderId',
      type: 'text',
      label: 'Passenger ID',
      valuePath: 'rider.riderId',
      editable: false
    },
    ]
  },
  {
    title: 'Trip Details',
    fields: [{
      id: 'requestedTime',
      type: 'datetimeflatpickr',
      label: 'Date Time',
      valuePath: 'requestedTime',
      editable: true,
      hidden: false,
      format: 'YYYY-MM-DD hh:mm A',
      defaultWidth: 50
    },
    {
      id: 'anchor',
      type: 'enum',
      label: 'Anchor',
      valuePath: 'anchor',
      cellDesc: 'anchor',
      editable: true,
      hidden: false,
      extra: {
        options: [
          'pick',
          'drop'
        ],
        allowMultiple: false
     }
    },
    {
      id: 'originaddresses',
      type: 'locations',
      label: '',
      valuePath: 'originlocations',
      modelName: 'bs-location',
      isMultiType: true,
      extra: {
        aliasPath: 'alias',
        notesPath: 'notes',
        streetNumberPath: 'streetNumber',
        streetNamePath: 'streetName',
        localityPath: 'city',
        regionPath: 'county',
        subRegionPath: 'state',
        postalCodePath: 'postalCode',
        countryPath: 'country',
        defaultOptionsPath: 'riderAddresses'
      }
    },
    {
      id: 'destinationaddresses',
      type: 'locations',
      label: 'Drop-off Address',
      valuePath: 'destinationlocations',
      modelName: 'bs-location',
      isMultiType: true,
      extra: {
        aliasPath: 'alias',
        notesPath: 'notes',
        streetNumberPath: 'streetNumber',
        streetNamePath: 'streetName',
        localityPath: 'city',
        regionPath: 'county',
        subRegionPath: 'state',
        postalCodePath: 'postalCode',
        countryPath: 'country',
        defaultOptionsPath: 'riderAddresses'
      }
    }]
  }, {
  title: 'Travel Needs',
  fields: [{
    id: 'travelNeeds',
    type: 'travel-needs',
    label: '',
    valuePath: 'travelNeeds',
    modelName: 'rms-rider-travel-need',
    isMultiType: true,
    extra: {
      travelNeedTypeModelName: 'rms-travel-need-type',
      passengerTypeModelName: 'rms-passenger-type',
      countPath: 'count',
      travelNeedTypePath: 'travelNeedType',
      passengerTypePath: 'passengerType'
    }
  }]
},{
    title: 'Notes',
    fields: [{
      id: 'passengerNotes',
      type: 'text',
      label: 'Passenger Notes',
      valuePath: 'bookingNotes',
      editable: true
    },
    {
      id: 'driverNotes',
      type: 'text',
      label: 'Driver Notes',
      valuePath: 'driverNotes',
      editable: true
    }
    ]
  },{
    title: 'Fare',
    fields: [ {
      id: 'type',
      type: 'enum',
      label: 'Payment Type',
      valuePath: 'fareType',
      cellDesc: 'fareType',
      editable: true,
      hidden: false,
      extra: {
        optionModelName: 'bs-fare-type',
        optionSearchPath: 'name'
      }
    },{
      id: 'fare',
      type: 'number',
      label: 'Amount',
      valuePath: 'fare',
      editable: true
    }
    ]
  },
  {
    title: 'Subscription',
    fields: [{
      id: 'subscriptions',
      type: 'subscriptions',
      label: '',
      valuePath: 'subscriptions',
      modelName: 'bs-leg-subscription',
      isMultiType: true,
      extra: {
        recurringPath: 'recurring',
        dailyConfigPath: 'dailyConfig',
        startDateTimePath: 'startDateTime',
        endDateTimePath: 'endDateTime',
        sundayPath: 'sunday',
        mondayPath: 'monday',
        tuesdayPath: 'tuesday',
        wednesdayPath: 'wednesday',
        thursdayPath: 'thursday',
        fridayPath: 'friday',
        saturdayPath: 'saturday',
        excludeStartDateTimePath: 'excludeStartDateTime',
        excludeEndDateTimePath: 'excludeEndDateTime',
      }
    }]
  }
]
};
