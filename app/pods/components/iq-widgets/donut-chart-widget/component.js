/* global Chart */
import ChartjsWidget from '../../generic-widgets/chartjs-widget/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isBlank, isPresent } from '@ember/utils';
import {
  buildFilterFunction,
  buildCompoundFilterNode
} from 'adept-iq/utils/filters';
import FilterColumn from '../../generic-widgets/column-widget/classes/filter-column';

export default ChartjsWidget.extend({
  workspaceContext: service(),
  classNames: ['donut-chart-widget'],

  widget: null,

  options: computed.alias('widget.state.dataType'),

  /* Applies the choice of data source. */
  chartRecords: computed('options.modelName', 'workspaceContext.workspaceData.[]',function() {
    let modelName = this.get('options.modelName');
    if (isBlank(modelName)) return [];
    let wrappedRecords = this.get('workspaceContext.workspaceData').filterBy('modelName', modelName)
    return wrappedRecords.map((a) => a.record);
  }),

  /* Builds the chosen filters. */
  filterFunction: computed('options.columns', function() {
    let filterColumns = Object.values(this.get('options.columns')).map((col) => new FilterColumn(col));
    let args = filterColumns.map((column) => {
        return column.get('filterNode');
      });
    return buildFilterFunction(buildCompoundFilterNode('and', args));
  }),

  /* Applies the choice of filters. */
  filteredRecords: computed('chartRecords', 'filterFunction', function() {
    return this.get('chartRecords').filter(this.get('filterFunction'))
  }),

  /* Gets the choice for slicing the data. */
  slicedBy: computed('options.displayOptions.[]', function() {
    if (isBlank(this.get('options.displayOptions'))) return;
    return this.get('options.displayOptions').findBy('id','slicedBy').selected;
  }),

  chartSum: computed.alias('filteredRecords.length'),

  /* Applies the choice for slicing the data. */
  recordSlices: computed('filteredRecords', 'slicedBy', function() {
    let categories = {},
      filteredRecords = this.get('filteredRecords'),
      slicedBy = this.get('slicedBy');

    if (isBlank(slicedBy)) return categories;

    let filterKey = this.get('slicedBy.valueKey');
    this.get('slicedBy.valueCategories').forEach((category) => {
      categories[category.label] = filteredRecords.filterBy(filterKey, category.value);
    });
    // { 'label1': [], 'label2': [] }
    return categories;
  }),

  /* Applies the choices for displaying data counts and/or percentages. */
  aggregatedSliceData: computed('recordSlices', 'options.visualOptions.@each.isChecked', function() {
    let visualOptions = this.get('options.visualOptions');
    let showValues = visualOptions.findBy('id', 'value').isChecked;
    let showPercentage = visualOptions.findBy('id', 'percentage').isChecked;

    let totalCount =  this.get('chartSum');
    let data = [];
    Object.entries(this.get('recordSlices')).forEach((slice) => {
      let label = slice[0];
      let numRecords = slice[1].length;
      if (showValues) {
        label += (', ' + numRecords);
      }
      if (showPercentage && totalCount !== 0) {
        label += (', ' + Math.floor(100 * numRecords / totalCount) + '%');
      }
      data.push({ label: label, value: numRecords });
    });
    // [{...}, {...}, ...]
    return data;
  }),

  /* Applies the choice for sorting the data. */
  sortedData: computed('aggregatedSliceData', 'options.sortOrder.selected.sortAsc', function() {
    if (isBlank(this.get('aggregatedSliceData'))) return [];

    let data = this.get('aggregatedSliceData');

    let sortAsc = this.get('options.sortOrder.selected.sortAsc');
    if (sortAsc) {
      return data.sortBy('value');
    }
    // [{...}, {...}, ...]
    return data.sortBy('value').reverse();
  }),

  chartData: computed('sortedData', function() {
    let sortedData = this.get('sortedData');
    if (isBlank(sortedData)) return { data:[], labels:[] };

    let data = [], labels = [], backgroundColor = [];
    let colors = this.get('colors');
    sortedData.forEach((dataSlice, i) => {
      data.push(dataSlice.value);
      labels.push(dataSlice.label);
      backgroundColor.push(colors[i % colors.length]);
    });

    let datasets = [{
      data,
      backgroundColor
    }];

    return { datasets, labels };
  }),

  /* Applies the chart title input. */
  chartTitle: computed('widget.state.dataType.{name,title}', function() {
    let title = this.get('widget.state.dataType.title');
    if (isPresent(title)) return title;

    let name = this.get('widget.state.dataType.name');
    if (isPresent(name)) {
      return `${name} Donut Chart`;
    }

    return 'Donut Chart';
  }),

  /* Applies the choices for display of legend and record count total. */
  chartOptions: computed('options.{legendPlacement,visualOptions.@each.isChecked}', function() {
    if (isBlank(this.get('widget.state'))) return {};

    let options = {
      legend: {
        position: this.get('options.legendPlacement.selected.position'),
      },
      elements: {
        center: {
          text: this.get('chartSum'),
          fontStyle: 'AvenirNext', //Default Arial
          sidePadding: 15 //Default 20 (as a percentage)
        }
      },
      tooltips: {
        enabled: false
      }
    };

    let showTotal = this.get('options.visualOptions').findBy('id', 'total').isChecked;
    if (!showTotal) {
      delete options.elements.center;
    }

    return options
  }),

  didReceiveAttrs() {
    this._super(...arguments);

    Chart.pluginService.register({
     beforeDraw: function (chart) {
       if (chart.config.options.elements.center) {
         //Get ctx from string
         let ctx = chart.chart.ctx;

         //Get options from the center object in options
         let centerConfig = chart.config.options.elements.center;
         let fontStyle = centerConfig.fontStyle || 'Arial';
         let txt = centerConfig.text;
         let color = centerConfig.color || '#000';
         let sidePadding = centerConfig.sidePadding || 20;
         let sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
         //Start with a base font of 30px
         ctx.font = "45px " + fontStyle;

         //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
         let stringWidth = ctx.measureText(txt).width;
         let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

         // Find out how much the font can grow in width.
         let widthRatio = elementWidth / stringWidth;
         let newFontSize = Math.floor(30 * widthRatio);
         let elementHeight = (chart.innerRadius * 2);

         // Pick a new font size so it will not be larger than the height of label.
         let fontSizeToUse = Math.min(newFontSize, elementHeight);

         //Set font settings to draw it correctly.
         ctx.textAlign = 'center';
         ctx.textBaseline = 'middle';
         let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
         let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
         ctx.font = fontSizeToUse+"px " + fontStyle;
         ctx.fillStyle = color;

         //Draw text in center
         ctx.fillText(txt, centerX, centerY);
       }
     }
    });
  },

  actions: {
    onFilterButtonClick() {
      let workspace = this.get('workspace');
      let topState = workspace.get('topState');
      while (topState === 'donutChartWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Donut Chart";
      let widget = this.get('widget');

      workspace.pushState('donutChartWidget', {
        displayName,
        widget
      });
    },
  }
});
