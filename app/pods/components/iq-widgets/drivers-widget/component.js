import IQUXColumnWidget from '../column-widget/component';
import StaticWidgetConfig from './config';
import { inject as service } from '@ember/service';
import _ from 'lodash';

const INCLUDES = [];

export default IQUXColumnWidget.extend({
  classNames: ['drivers-widget'],
  editModal: service(),
  ajax: service(),
  store: service(),
  activeContext: service(),
  editComponent: 'iq-widgets/drivers-form-widget',

  // need to sub-class this in specific widget
  config: StaticWidgetConfig,
  defaultIncludes: INCLUDES,

  // TODO: saveRecordFunction shall be moved to a service later on
  // This save is specific to saving drivers and all related models.
  saveRecordFunction(records) {
    function saveDeleteAllObjects(objects) {
      const deleteObjects = _.difference(objects.content.canonicalState,
        objects.content.currentState);

      // delete objects that have been removed from the array
      return Promise.all(deleteObjects.map(deleteObject => {
        deleteObject._record.deleteRecord();
        return deleteObject.save();
      }))
        .then(() => {
          return Promise.all(objects.map(object => object.save()));
        });
    }

    this.set('editModal.errors', []);

    return new Promise((resolve, reject) => {
      records.forEach(record => {
        record.save()
          .then(() => {
            saveDeleteAllObjects(record.get('availability'))
              .then(() => {
                record.set('isForceDirty', false);
                this.get('activeContext').refreshTableContent(this.get('modelName'));
              });
          })
          .catch(e => {
            if (e.errors[0].status === '400' || e.errors[0].status === '409') {
              this.get('editModal.errors').pushObject('Invalid Driver Id');
            }
            else {
              reject(e);
            }

          });
      });
    });
  },

  didInsertElement() {
    this._super(...arguments);

    this.set('tableActions', [{
      name: 'New Driver',
      action: function() {
        let modelName = this.get('config.modelName');
        let newModel = this.get('store').createRecord(modelName);
        this.get('records').pushObject(newModel);
        this.set('editModal.editableRecords', []);
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, [newModel], this.get('saveRecordFunction').bind(this));
      }.bind(this)
    }]);

    this.set('singleActions', [{
      name: 'Edit',
      action: (model) => {
        let editComponent = this.get('editComponent');
        let saveRecordFn = this.get('saveRecordFunction').bind(this);
        this.get('editModal').open(editComponent, [model], saveRecordFn);
      }
    },
    {
      name: 'Send Message',
      action: (driver) => {
        this.sendMessageToDrivers([driver]);
      }
    }]);

    this.set('bulkActions', [{
      name: 'Bulk Edit',
      action: (models) => {
        let editComponent = this.get('editComponent');
        this.get('editModal').open(editComponent, models);
      }
    },
    {
      name: 'Bulk Message',
      action: (drivers) => {
        this.sendMessageToDrivers(drivers);
      }
    }]);
  },

  sendMessageToDrivers(drivers) {
    let draftMessages = drivers.map((driver) => {
      return this.get('store').createRecord('tm-canned-message', {
        driverId: driver.get('id'),
        vehicleId: driver.get('vehicleId'),
        routeId: driver.get('routeId')
      });
    });

    this.get('workspace').pushState('addNewMessage', { draftMessages });
  }
});

