export default {
  component: 'iq-widgets/drivers-widget',
  rowComponent: 'table/rows/otp-formatted-row',

  modelName: 'ss-driver',
  defaultSortId: 'driverId',
  defaultSortAsc: false,

  title: 'Drivers',

  columns: [
    {
      id: 'driverId',
      index: 0,
      type: 'text',
      label: 'ID',
      valuePath: 'driverId',
      editable: true,
      hidden: false,
      defaultWidth: 30
    },
    {
      id: 'lastName',
      index: 2,
      type: 'text',
      label: 'Last Name',
      valuePath: 'lastName',
      editable: true,
      hidden: false,
      searchable: true,
      defaultWidth: 75
    },
    {
      id: 'firstName',
      index: 1,
      type: 'text',
      label: 'First Name',
      valuePath: 'firstName',
      editable: true,
      hidden: false,
      searchable: true,
      defaultWidth: 60
    },
    {
      id: 'status',
      index: 3,
      type: 'text',
      label: 'Status',
      valuePath: 'status',
      editable: false,
      hidden: false,
      searchable: true,
      highlightable: true
    },
    {
      id: 'availability',
      index: 4,
      type: 'enum',
      label: 'Availability',
      valuePath: 'formattedAvailabilities',
      cellDesc: 'Availabilities',
      editable: false,
      hidden: false
    },
    {
      id: 'active',
      index: 5,
      type: 'boolean',
      label: 'Active',
      valuePath: 'active',
      editable: true,
      hidden: false,
      defaultWidth: 50
    },
    {
      id: 'preferredVehicle',
      index: 6,
      type: 'text',
      label: 'Prefered Vehicle ID',
      valuePath: 'preferredVehicleName',
      editable: false,
      hidden: false
    }
  ]
};
