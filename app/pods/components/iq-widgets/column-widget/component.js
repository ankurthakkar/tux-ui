import GenericColumnWidget from '../../generic-widgets/column-widget/component';
import { inject as service } from '@ember/service';
import { isEmpty, isPresent } from '@ember/utils';
import columnTypes from 'adept-iq/config/column-types';
import { makeArray } from '@ember/array';

import {
  buildCompoundFilterNode,
  buildValueFilterNode
} from 'adept-iq/utils/filters';

export default GenericColumnWidget.extend({
  activeContext: service(),

  classNames: ['iqux-column-widget'],
  layoutName: 'components/generic-widgets/column-widget',

  defaultIncludes: null,

  didInsertElement() {
    this._super(...arguments);

    this._onActiveContextRefresh = (modelNames) => {
      modelNames = makeArray(modelNames);
      if (!modelNames.includes(this.get('config.modelName'))) return;
      this.refreshData();
    };

    this._onActiveContextClear = (/* modelNames */) => {
      this.clearData();
    };

    this.get('activeContext').on('refresh', this._onActiveContextRefresh);
    this.get('activeContext').on('clear', this._onActiveContextClear);
  },

  willDestroyElement() {
    this._super(...arguments);

    this.get('activeContext').off('refresh', this._onActiveContextRefresh);
    this.get('activeContext').off('clear', this._onActiveContextClear);

    this._onActiveContextRefresh = null;
    this._onActiveContextClear = null;
  },

  fetchDataQuery() {
    let config = this.get('config') || {};
    let state = this.get('state') || {};

    let params = {
      sorts: []
    };

    if (this.get('paginationEnabled')) {
      params.page = {
        offset: this.get('records.length'),
        limit: this.get('limit')
      }
    }

    let sortId = state.sortId || config.defaultSortId;
    if (isPresent(sortId)) {
      let sortAsc;
      if (isPresent(state.sortAsc)) {
        sortAsc = state.sortAsc;
      } else if (isPresent(config.defaultSortAsc)) {
        sortAsc = config.defaultSortAsc;
      } else {
        sortAsc = true;
      }

      let columnConfig = this.get('config.columns').findBy('id', sortId);

      if (!columnConfig) {
        throw `tried to sort by column id '${sortId}', but no such column in widget config.js`;
      }

      let columnType = columnTypes.findBy('id', columnConfig.type);

      if (!columnType) {
        throw `tried to sort by column id '${sortId}', but column has no type in widget config.js`;
      }

      params.sorts.push({
        path: columnConfig.valuePath,
        asc: sortAsc,
        compare: columnType.compare
      });
    }

    let includes = this.get('defaultIncludes');
    if (!isEmpty(includes)) {
      params.includes = includes;
    }

    let filters = [this.buildSearchFilter(), this.buildColumnFilter()];
    params.filter = buildCompoundFilterNode('and', filters);

    let modelName = this.get('config.modelName');
    return this.get('activeContext').query(modelName, params);
  },

  buildSearchFilter() {
    let searchText = this.get('searchText');
    if (isEmpty(searchText)) return null;

    let searchableConfigs = this.get('table.columns')
      .rejectBy('hidden')
      .mapBy('config')
      .compact()
      .filterBy('searchable');

    let args = searchableConfigs.map((config) => {
      let columnType = columnTypes.findBy('id', config.type);
      let filterType = columnType.searchFilterType;
      return buildValueFilterNode(filterType, config.valuePath, [searchText]);
    });

    return buildCompoundFilterNode('or', args);
  },

  buildColumnFilter() {
    let args = this.get('table.columns').map((column) => {
      return column.get('filterNode');
    });

    return buildCompoundFilterNode('and', args);
  },

  actions: {
    onRowCheckboxToggle() {
      this._super(...arguments);

      let modelName = this.get('config.modelName')
      let checkedRecords = this.get('table.rows')
        .filterBy('isChecked')
        .mapBy('record');

        this.get('activeContext').setCheckedRecords(modelName, checkedRecords);
    }
  }
});
