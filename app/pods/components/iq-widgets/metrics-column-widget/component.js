import BaseWidget from '../../generic-widgets/base-widget/component';
import Table from 'ember-light-table';
import { run } from '@ember/runloop';
import { computed } from '@ember/object';

const fromPixels = (x) => parseInt(x.split('px')[0]);

export default BaseWidget.extend({
  classNames: ['analytics-widget', 'metrics-column-widget'],

  table: null,

  dataType: null,

  dataTypes: computed('', function() {
    return [
      "Schedules",
      "Routes",
      "Trips",
      "Stops",
      "Passengers",
      "Vehicles",
      "Users",
      "Drivers",
    ];
  }),

  model: computed(function() {
    return [{
      'sample': "Captain",
      'firstName': "Banana",
      'lastName': "Sundae",
    }, {
      'sample': "Mr.",
      'firstName': "Icecream",
      'lastName': "Sandwich",
    }];
  }),

  columns: computed(function() {
    return [{
      label: 'Label name',
      valuePath: 'sample',
      width: '100px',
      sortable: false,
      cellComponent: 'table/cells/base-cell',
      resizable: true
    }, {
      label: 'First Name',
      valuePath: 'firstName',
      width: '150px',
      cellComponent: 'table/cells/base-cell',
      resizable: true
    }, {
      label: 'Last Name',
      valuePath: 'lastName',
      width: '150px',
      cellComponent: 'table/cells/base-cell',
      resizable: true
    }, {
      id: 'spacer',
      width: '100%',
      sortable: false,
      resizable: false
    }];
  }),
  init() {
    this._super(...arguments);
    let table = new Table(this.get('columns'), this.get('model'), {
      enableSync: false
    });

    this.set('table', table);
  },

  actions: {
    chooseDataType(chosenType) {
      this.set('dataType', chosenType);
    },
    onColumnResize(column, pixels) {
      let id = column.get('id');
      let columns = {};
      columns[id] = { width: fromPixels(pixels) };
      this.get('widget').mergeState({ columns });
      run.scheduleOnce('afterRender', this, 'resizeBody');
    },
    onFilterButtonClick() {
      let workspace = this.get('workspace');
      let topState = workspace.get('topState');
      while (topState === 'metricsColumnWidget') {
        workspace.popState();
        topState = workspace.get('topState');
      }

      let displayName = "Metrics Column Analytics";

      workspace.pushState('metricsColumnWidget', {
        displayName
      });
    }
  }
});
