import GenericFormWidget from '../../generic-widgets/form-widget/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default GenericFormWidget.extend({
  editModal: service(),

  isShrunken: false,

  service: computed.alias('editModal'),

  actions: {
    onShrinkClick() {
      this.toggleProperty('isShrunken');
    }
  }
});
