export default {
  component: 'iq-widgets/alert-widget',
  rowComponent: 'table/rows/otp-formatted-row',

  modelName: 'alert',

  title: 'Alerts',

  hideWidgetControls: true,

  columns: [
    {
      id: 'generatedAt',
      index: 0,
      type: 'text',
      label: 'Time',
      valuePath: 'generatedAt',
      editable: false,
      hidden: false,
      defaultWidth: 70
  },{
      id: 'alert',
      index: 1,
      type: 'text',
      label: 'Alert Description',
      valuePath: 'alert',
      editable: false,
      hidden: false,
      defaultWidth: 400
  }]
};
