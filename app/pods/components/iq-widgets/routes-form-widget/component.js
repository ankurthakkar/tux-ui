import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),
  errorMessage: service(),

  classNames: ['data-test-route-form-widget'],


  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);
    records.forEach(record => {
      // make sure we apply the date to the plannedStartTime and plannedEndTime
      let plannedStartTime = record.get('plannedStartTime');
      let plannedEndTime = record.get('plannedEndTime');
      let routeDate = record.get('routeDate');


      plannedStartTime.setDate(routeDate.getDate());
      plannedStartTime.setMonth(routeDate.getMonth());
      plannedStartTime.setFullYear(routeDate.getFullYear());
      plannedEndTime.setDate(routeDate.getDate());
      plannedEndTime.setMonth(routeDate.getMonth());
      plannedEndTime.setFullYear(routeDate.getFullYear());


      if (record.name === undefined || record.name.length === 0) {
        valid = false;
        this.get('editModal.errors').pushObject('Name cannot be blank.');
      }

      if (record.selectedVehicle === undefined && record.selectedDriver !== undefined) {
        valid = false;
        this.get('editModal.errors').pushObject('Must have a vehicle selected if a driver is selected.');
      }

    });

    return valid;
  },

  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      onUndoClick() {
        /*let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);*/
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }

      // onCellValueChange(record, valuePath, value, options) {
      //   if (valuePath === 'vehicles.firstObject.name') {
      //     let vehicle = record.get('vehicles.firstObject');

      //     record.vehicles.removeObject(vehicle);
      //   }
      //   else {
      //     this.get('service').setRecordValue(record, valuePath, value, options);
      //   }

      // }
    }
});
