import FormWidget from '../form-widget/component';
import config from './config';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { isNone , isEmpty } from '@ember/utils';

export default FormWidget.extend({
  editableSections: config.editableSections,
  editModal: service(),

  classNames: ['vehicle-form-widget', 'data-test-vehicle-form-widget'],

  validateRecords() {
    let valid = true;
    let records = this.get('editableRecords');

    this.set('editModal.errors', []);
    records.forEach(record => {
      const availabilities = record.availability;

      const shiftBreaks = record.shiftBreaks;

      if (record.name === undefined || record.name.trim().length < 1) {
        this.get('editModal.errors').pushObject('Vehicle Id');
        valid = false;
      }

      if (record.get('vehicleType.id') === undefined || record.get('vehicleType.id') === null) {
        this.get('editModal.errors').pushObject('Vehicle Type');
        valid = false;
      }

      if(isEmpty(record.get('startGarages.firstObject.latlng'))) {
        valid = false;
        this.get('editModal.errors').pushObject('Start Garage address should be there');
      } else if(isEmpty(record.get('startGarages.firstObject.geoNode'))) {
        valid = false;
        this.get('editModal.errors').pushObject('Start Garage address should be valid');
      }

      if(isEmpty(record.get('endGarages.firstObject.latlng'))) {
        valid = false;
        this.get('editModal.errors').pushObject('End Garage address should be there');
      } else if(isEmpty(record.get('endGarages.firstObject.geoNode'))) {
        valid = false;
        this.get('editModal.errors').pushObject('End Garage address should be valid');
      }

      availabilities.forEach(availability => {
        const startTime = moment(availability.startTime);
        const endTime = moment(availability.endTime);
        const shiftStart = moment(availability.shiftStart, 'HH:mm:ss');
        const shiftEnd = moment(availability.shiftEnd, 'HH:mm:ss');

        if (endTime.isBefore(startTime)) {
          valid = false;
          this.get('editModal.errors').pushObject('Effective end date cannot be before effective start date');
        }

        if (shiftEnd.isBefore(shiftStart)) {
          valid = false;
          this.get('editModal.errors').pushObject('Shift end cannot be before shift start');
        }
      });


       shiftBreaks.forEach(shiftBreak => {

        const startTime = moment(shiftBreak.startTime, 'HH:mm:ss');
        const endTime = moment(shiftBreak.endTime, 'HH:mm:ss');


        if (endTime.isBefore(startTime)) {
          valid = false;
          this.get('editModal.errors').pushObject('Effective end date cannot be before effective start date');
        }
        let startDate = new Date(startTime);
        let endDate = new Date(endTime);

        var difference = startDate - endDate;
        var diff_result = new Date(difference);

        var hourDiff = diff_result.getHours();
        if (hourDiff  <= 0) {
          valid = false;
          this.get('editModal.errors').pushObject('Break end date cannot same as start date');
        }
        });

      });
    return valid;
  },

  actions: {
      // override undo because we will have to deal with undoing created
      // models for addresses, travel needs, and eligibility later.
      // IQUX-510
      onUndoClick() {
        /*let lastUndoState = this.get('editModal.undoHistory').popObject();

        if (lastUndoState === null) {
          let records = this.get('editableRecords');

          records.forEach(record => {
            record.set('isForceDirty', false);
          });

        }
        else {
          lastUndoState.forEach(({ record, properties }) => {
            record.setProperties(properties);
          });
        }

        this.set('isLastUndoStateCommitted', true);*/
      },

      onApplyClick() {
        if (this.validateRecords()) {
          this.get('service').apply();
        }
      }

      // onCellValueChange(record, valuePath, value, options) {
      //   if (valuePath === 'vehicles.firstObject.name') {
      //     let vehicle = record.get('vehicles.firstObject');

      //     record.vehicles.removeObject(vehicle);
      //   }
      //   else {
      //     this.get('service').setRecordValue(record, valuePath, value, options);
      //   }

      // }
    }
});
