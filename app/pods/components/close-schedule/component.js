import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: ['dashboard-picker'],

  workspace: service(),

  actions: {
    onXButtonClick() {
      this.get('workspace').popState('closeSchedule');
    }
  }
});
