import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';

export default Route.extend({
  session: service(),

  setupController(controller/*, model*/) {
    // can we pass in the user's email ?
    controller.set('data', {});
  },

  actions: {
    reset(/* userName */) {
      // let session = this.get('session');
      // let authenticator = 'authenticator:sso';

      let message = 'Reset Password not implemented yet'

      if (isPresent(message)) {
        return this.set('controller.data.errors', [message]);
      }
    }
  }
});
