import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  maximizer: service(),

  actions: {
    didTransition(/* transition */) {
      let widget = this.get('maximizer.maximizedWidget');
      if (widget === null) {
        this.transitionTo('index');
      }
    }
  }
});
