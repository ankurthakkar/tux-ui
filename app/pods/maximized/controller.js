import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { widgetTypesHash } from 'adept-iq/config/widget-types';

export default Controller.extend({
  maximizer: service(),

  widgetTypesHash,

  widget: computed.alias('maximizer.maximizedWidget'),
});
