import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  workspace: service(),
  data: null
});
