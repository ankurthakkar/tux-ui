import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  session: service(),
  workspace: service(),
  tooltip: service(),

  model() {
    this.store.findAll('rms-travel-need-type');
    this.store.findAll('rms-passenger-type');
    this.store.findAll('rms-eligibility-type');
    this.store.findAll('ss-vehicle-type');
    this.store.findAll('ss-vehicle-capacity-config');
    this.store.findAll('ss-vehicle-capacity-type');
    this.store.findAll('ss-driver');
    this.store.findAll('break-type');
    this.store.findAll('ss-shift-break');
    this.store.findAll('no-show-reason-code');
    this.store.findAll('cancel-type');
    this.store.findAll('agency-marker');
    this.store.findAll('travel-need-type');
    this.store.findAll('bs-travel-need-type');
    this.store.findAll('bs-passenger-type');
    this.store.findAll('bs-fare-type');

    this.store.findAll('provider');
  },

  beforeModel() {
    // if (!this.get('session.isAuthenticated')) {
    //   this.transitionTo('login');
    // }
  },

  actions: {
    createDashboard() {
      this.transitionTo('index.modals.create-dashboard');
    },

    openDashboard(dashboard) {
      // TODO: prompt to save changes
      this.get('tooltip').reset();
      this.transitionTo('dashboard', dashboard);
    },

    transitionTo() {
      this.transitionTo(...arguments);
    },

    logout() {
      this.get('session').invalidate().then(() => {
        this.transitionTo('login');
      })
    }
  }
});
