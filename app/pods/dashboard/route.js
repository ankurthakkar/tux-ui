import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  workspace: service(),

  model(params) {
    return this.store.findRecord('dashboard', params.id)
      .catch(() => {
        this.transitionTo('index');
      });
  },

  afterModel(model) {
    let workspace = this.get('workspace');
    workspace.resetForDashboardChange();
    workspace.set('dashboard', model);

    localStorage.setItem('lastDashboardId', model.get('id'));
  },

  setupController(controller, model) {
    let data = {
      dashboard: model
    };

    controller.set('data', data);
  }
});
