import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { debounce, run } from '@ember/runloop';
import $ from 'jquery';

const RESIZE_CURSOR_OFFSET = -2;
const MAX_WIDTH = 800;
const RESIZE_DEBOUNCE_TIME = 250;

export default Controller.extend({
  editModal: service(),
  editableRecords: computed.alias('editModal.editableRecords'),

  init() {
    this._super(...arguments);

    this._onResize = () => {
      if (this.get('isDragging') || this.get('isResizing')) return;
      debounce(this, 'resizedWindow', RESIZE_DEBOUNCE_TIME);
    };

    this._resizeObserver = new ResizeObserver(this._onResize);
    this._resizeObserver.observe($('body')[0]);
  },

  resizedWindow() {
    let position = $('.edit-widget-modal').width();
    let coords = {
      x: position,
    };
    if (this.get('isDestroyed')){
      return;
    }
    this.send('resizeModal', coords);
  },

  willDestroyElement() {
    this._resizeObserver.disconnect();
    this._resizeObserver = null;
    this._onResize = null;
  },


  actions: {
    resizeModal(position) {

      let windowWidth = $(window).width();

      // Resize cursor offset ensures cursor is centered on handle.
      let modalWidth = windowWidth - position.x + RESIZE_CURSOR_OFFSET;
      run.scheduleOnce('afterRender', () => {
        if (windowWidth - modalWidth >= MAX_WIDTH) {
          $('.edit-widget-modal').css('right', `${windowWidth-MAX_WIDTH}px`);
        } else {
          $('.edit-widget-modal').css('right', `${modalWidth}px`);
        }
      });
    }
  }
});
