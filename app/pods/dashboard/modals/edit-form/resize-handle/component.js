import Component from '@ember/component';
import { run } from '@ember/runloop';
import $ from 'jquery';

export default Component.extend({
  didInsertElement() {
    let elem = this.$();
    elem.on('mousedown', () => {
      //resize may happen outside of current element
      $(document).on('mousemove', (event) => {
        run.scheduleOnce('afterRender', this, 'resizeModal', event);
      });

      //rm listeners to avoid bloat
      $(document).on('mouseup', () => {
        run.scheduleOnce('afterRender', () => {
          $(document).off('mouseup');
          $(document).off('mousemove');
        });
      });
    });
  },

  resizeModal(e) {
    let coords = {
      x: e.pageX
    };

    this.get('onResizeModal')(coords);
  }
});
