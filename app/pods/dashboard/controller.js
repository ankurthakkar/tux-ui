import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  workspace: service(),
  tooltip: service(),
  store: service(),

  data: null,

  isEditing: computed.alias('workspace.isEditing'),
  tileSize: computed.alias('workspace.tileSize'),
  tileSpacing: computed.alias('workspace.tileSpacing'),
  dashboard: computed.alias('data.dashboard')
});
