import Marker from './base';
import { computed } from '@ember/object';

export default Marker.extend({
  style: 'location',
  label: 'Agency',
  isActive: false,

  agencyMarker: computed.alias('record'),
  lat: computed.alias('agencyMarker.lat'),
  lng: computed.alias('agencyMarker.lng'),
  options: computed.alias('agencyMarker.options')
});
