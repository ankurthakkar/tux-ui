import EmberObject from '@ember/object';

export default EmberObject.extend({
  animationTimer: null,

  id: null,
  record: null,

  lat: null,
  lng: null,
  opacity: 1.0,

  // these will usually be over-ridden by layer
  // see `app/pods/components/iq-widgets/map-widget/config/marker.js` for styles
  type: null,
  style: null,
  label: null,
  isActive: false,

  // these will be applied _after_ style defaults
  options: null
});