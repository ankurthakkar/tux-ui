import Marker from './base';
import { computed } from '@ember/object';

export default Marker.extend({
  style: 'pick',
  label: 'Stop',
  isActive: true,

  iqStop: computed.alias('record'),
  lat: computed.alias('iqStop.lat'),
  lng: computed.alias('iqStop.lng')
});
