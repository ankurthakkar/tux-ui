import Marker from './base';
import { computed, observer } from '@ember/object';
import { run } from '@ember/runloop';
import { isNone } from '@ember/utils';
import MARKERS from 'adept-iq/pods/components/iq-widgets/map-widget/config/marker';

// average over (up to) the last N update timestamps
const MAX_SMOOTHING_N = 10;

// don't average until there are N timestamps
const MIN_SMOOTHING_N = 5;

// assume 1s vehicle updates off the bat
const DEFAULT_INTERVAL_MS = 1000;

const Z_INDEX_OFFSET = 1000;

export default Marker.extend({
  style: 'vehicle',
  label: 'Vehicle',
  isActive: true,

  iqVehicle: computed.alias('record'),
  lat: null,
  lng: null,

  // cached marker location
  _lat: null,
  _lng: null,

  init() {
    this._super(...arguments);

    // each vehicle marker subscribes to a central `animationTimer` service
    // that fires an event on animation ticks
    this._onAnimationTimerTick = () => {
      run.scheduleOnce('afterRender', this, 'onAnimationTimerTick');
    };

    this.get('animationTimer').on('tick', this._onAnimationTimerTick);
  },

  destroy() {
    this.get('animationTimer').off(this._onAnimationTimerTick);
    this._super(...arguments);
  },

  // when a new vehicle location is received, we cache the current location of // the marker on the map (possibly between the last two known locations!);
  // the marker will now "drift" from its present map location to the new coords
  onLatLngChange: observer('iqVehicle.{lat,lng}', function (params) {
    if (this.marker) {
      this.set('_lat', this.marker._latlng.lat);
      this.set('_lng', this.marker._latlng.lng);
    }
  }),

  onAnimationTimerTick() {
    let iqVehicle = this.get('iqVehicle');
    if (!iqVehicle) return;

    // if the vehicle has no actual lat/lng, there is nothing we can do
    let actualLat = iqVehicle.get('lat');
    let actualLng = iqVehicle.get('lng');
    if (isNone(actualLat) || isNone(actualLng)) {
      this.set('lat', null);
      this.set('lng', null);
      return;
    }

    // if we don't have a cached marker location, snap marker to current coords
    let lat = this.get('_lat');
    let lng = this.get('_lng');
    if (isNone(lat) || isNone(lng)) {
      this.set('lat', actualLat);
      this.set('lng', actualLng);
      return;
    }

    // calculate average time between last n location updates
    let interval = this.computeAverageTimeWindow(iqVehicle);

    // calculate % of time that has elapsed before projected update
    let lastTime = iqVehicle.get('currentAVLLocation.localTimestamp').getTime();
    let now = (new Date()).getTime();
    let t = Math.min(1.0, (now - lastTime) / interval);

    // interpolate along vector from last cached marker location to known coords
    let diffLat = actualLat - lat;
    let diffLng = actualLng - lng;
    let newLat = lat + t*diffLat;
    let newLng = lng + t*diffLng;

    let y = Math.sin(diffLng) * Math.cos(actualLat);
    let x = (Math.cos(lat) * Math.sin(actualLat)) -
            (Math.sin(lat) * Math.cos(actualLat) * Math.cos(diffLng));

    let angle = 180 - Math.atan2(y, x) * 180 / Math.PI;

    if (this.marker) {
      // NOTE: we intentionally do not update the `lat` and `lng` properties,
      // becuase this would trigger an expensive update at the map widget level

      // we know vehicle heading now, so switch to arrow icon
      this.marker.setIcon(MARKERS.vehicleDriving.icon);

      // make this vehicle sit on top of other markers (e.g. stops)
      this.marker.setZIndexOffset(Z_INDEX_OFFSET);

      // update marker position and angle directly
      this.marker.setLatLng([newLat, newLng]);
      this.marker.setRotationAngle(90 + angle);
    }
  },

  // calculates the (rolling) average time between location updates
  computeAverageTimeWindow(iqVehicle) {
    let locations = iqVehicle.get('sortedAVLLocations');

    // if we have insufficient data points, return a default static guess
    if (locations.length < MIN_SMOOTHING_N) return DEFAULT_INTERVAL_MS;

    // use up to N points
    let n = Math.min(locations.length, MAX_SMOOTHING_N);

    // `localTimestamp` is a front-end only property set when we push the record
    let timestamps = locations.slice(locations.length - n, locations.length)
      .map((avlLocation) => avlLocation.get('localTimestamp').getTime());

    // compute the average
    let sum = 0;
    for (let i = 1; i < timestamps.length; i++) {
      sum += timestamps[i] - timestamps[i - 1];
    }

    // n points yields (n-1) intervals
    return sum / (n - 1);
  }
});

