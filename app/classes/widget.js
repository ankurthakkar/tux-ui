import EmberObject from '@ember/object';
import lodash from 'lodash';

let SERIALIZABLE_PROPERTIES = [
  'id',
  'typeId',
  'width',
  'height',
  'x',
  'y',
  'state'
];

export default EmberObject.extend({
  id: null,
  typeId: null,
  width: null,
  height: null,
  x: null,
  y: null,
  state: null,

  // parent dashboard
  dashboard: null,

  init() {
    this._super(...arguments);

    let state = this.get('state') || {};
    this.set('state', state);
  },

  // use this method to mutate state without replacing
  mergeState(changes={}) {
    let state = this.get('state');
    let mergedState = lodash.merge({}, state, changes);
    this.set('state', mergedState);
    this.notifyPropertyChange('state');
    this.set('dashboard.hasDirtyWidgets', true);
  },

  serialize() {
    let serialized = SERIALIZABLE_PROPERTIES.reduce((obj, propertyName) => {
      obj[propertyName] = this.get(propertyName);
      return obj;
    }, {});

    return lodash.cloneDeep(serialized);
  }
});
