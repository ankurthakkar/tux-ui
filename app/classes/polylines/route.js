import Polyline from './base';
import { computed } from '@ember/object';

export default Polyline.extend({
  dsRoute: computed.alias('record'),
  points: computed(
    'type',
    'dsRoute.{fullPolyline,navigationPolyline}',
    function() {

    switch (this.get('type')) {
      case 'planned':
        return this.get('dsRoute.fullPolyline');
      case 'performed':
        return this.get('dsRoute.fullPolyline');
      case 'navigation':
        return this.get('dsRoute.navigationPolyline');
      default:
        throw 'unknown route polyline type';
    }
  })
});
