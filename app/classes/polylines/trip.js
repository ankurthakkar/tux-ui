import Polyline from './base';
import { computed } from '@ember/object';

export default Polyline.extend({
  iqTrip: computed.alias('record'),
  points: computed.alias('iqTrip.bsBooking.polyline'),
  stopsIsVisible: null,
  arrowStyle: computed('stopsIsVisible', 'otpStatus', function() {
    let stopsIsVisible = this.get('stopsIsVisible');
    let otpStatus = this.get('otpStatus');

    return stopsIsVisible ?
      `tripArrowPlannedOffset${otpStatus}` :
      `tripArrowPlanned${otpStatus}`;
  })
});
