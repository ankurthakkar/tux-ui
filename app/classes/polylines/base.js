import EmberObject from '@ember/object';

export default EmberObject.extend({
  animationTimer: null,

  id: null,
  record: null,

  points: null,
  opacity: 1.0,

  // these will usually be over-ridden by layer
  // see `app/pods/components/iq-widgets/map-widget/config/polyline.js`
  type: null,
  style: null,
  label: null,
  isActive: false,
  otpStatus: null,

  // these will be applied _after_ style defaults
  options: null
});