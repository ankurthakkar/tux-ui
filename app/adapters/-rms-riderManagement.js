import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-rms-riderManagement',
  host: ENV.API.riderManagementService.host,

  pathForType: function(type) {
    // trim 'rms' prefix from model type
    let regEx = /^(?:rms-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
