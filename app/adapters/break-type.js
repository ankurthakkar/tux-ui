import SchedulingServiceAdapter from './-ss-schedulingService';

export default SchedulingServiceAdapter.extend({
  pathForType: function(/* type*/) {
      return 'break-type';
  }
});

