import ApplicationAdapter from './application';
import lodash from 'lodash';
import { underscore } from '@ember/string';
import RSVP from 'rsvp';
import { makeArray } from '@ember/array';

let WORKSPACE_CATEGORIES = ['workspaces-default', 'workspaces-users'];

export default ApplicationAdapter.extend({
  findRecord(store, type, id /*, snapshot*/) {
    return store.findRecord('cs-config-item', id)
      .then((configItem) => {
        return lodash.cloneDeep(configItem.get('value'));
      });
  },

  findAll(store, type /*, sinceToken, snapshotRecordArray*/) {
    let promiseHash = WORKSPACE_CATEGORIES.reduce((hash, category) => {
      hash[category] = this.query(store, type, category);
      return hash;
    }, {});

    return RSVP.hash(promiseHash).then((resultHash) => {
      return WORKSPACE_CATEGORIES.reduce((obj, category) => {
        resultHash[category].data.forEach((data) => {
          obj.data.push(data);
        });

        makeArray(resultHash[category].included).forEach((data) => {
          obj.included.push(data);
        });

        return obj;
      }, { data: [], included: [] });
    });
  },

  query(store, type, query /*, recordArray*/) {
    return store.query('cs-config-item', query)
      .then((configItems) => {
        return {
          data: configItems.map((configItem) => {
            let { data } = lodash.cloneDeep(configItem.get('value'));
            return data;
          })
        };
      });
  },

  createRecord(store, type, snapshot) {
    let { name, category } = snapshot.attributes();
    snapshot.id = `${category}/${underscore(name)}`;

    let data = {};
    let serializer = store.serializerFor(type.modelName);
    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    let record = store.createRecord('cs-config-item', {
      name: underscore(name),
      category,
      type: 'json',
      value: data
    });

    return record.save().then((configItem) => {
      return lodash.cloneDeep(configItem.get('value'));
    });
  },

  deleteRecord(store, type, snapshot) {
    let configItem = store.peekRecord('cs-config-item', snapshot.id);
    return configItem.destroyRecord().then(() => {
      return {
        data: {
          id: snapshot.id,
          type: 'dashboard',
          attributes: {}
        }
      };
    });
  },

  updateRecord(store, type, snapshot) {
    let serialized = {};
    let serializer = store.serializerFor(type.modelName);
    serializer.serializeIntoHash(serialized, type, snapshot, { includeId: true });

    let configItem = store.peekRecord('cs-config-item', snapshot.id);

    configItem.set('value', serialized);

    return configItem.save().then(() => {
      return serialized;
    });
  }
});
