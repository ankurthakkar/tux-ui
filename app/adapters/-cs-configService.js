import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-cs-configService',
  host: ENV.API.configService.host,
  namespace: 'config',

  pathForType(type) {
    // trim 'cs' prefix from model type
    let regEx = /^(?:cs-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
