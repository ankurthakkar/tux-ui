import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-bs-bookingService',
  host: ENV.API.bookingService.host,

  pathForType: function(type) {
    // trim 'bs' prefix from model type
    let regEx = /^(?:bs-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
