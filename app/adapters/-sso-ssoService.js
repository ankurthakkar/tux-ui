import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-sso-ssoService',
  host: ENV.API.ssoService.host,

  pathForType: function(type) {
    // trim 'sso' prefix from model type
    let regEx = /^(?:sso-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
