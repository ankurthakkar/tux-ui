import DispatchAdapter from './-dispatch';
import { get } from '@ember/object';
import { isPresent } from '@ember/utils';

export default DispatchAdapter.extend({
  // Adapted from https://github.com/emberjs/data/blob/v3.1.1/addon/-private/adapters/build-url-mixin.js#L89-L109
  urlForUpdateRecord (id, modelName) {
    let path;
    let url = [];
    let host = get(this, 'host');
    let prefix = this.urlPrefix();

    if (modelName) {
      path = this.pathForType(modelName);
      if (path) { url.push(path); }
    }

    if (prefix) { url.unshift(prefix); }

    url = url.join('/');
    if (!host && url && url.charAt(0) !== '/') {
      url = '/' + url;
    }

    return url;
  },

  handleResponse(status, headers, payload, requestData) {
    if (status === 400 && isPresent(payload.validation)) {
      status = 422;
      payload = { errors: [{ detail: payload.message }] };
    }

    return this._super(status, headers, payload, requestData);
  }
});
