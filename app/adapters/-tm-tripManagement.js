import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-tm-tripManagement',
  host: ENV.API.tripManagementService.host,

  pathForType: function(type) {
    // trim 'tm' prefix from model type
    let regEx = /^(?:tm-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
