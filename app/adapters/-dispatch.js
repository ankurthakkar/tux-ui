import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-dispatch',
  host: ENV.API.dispatchService.host
});
