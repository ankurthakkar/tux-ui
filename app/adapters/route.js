import DispatchAdapter from './-dispatch';

// export default DispatchAdapter;

export default DispatchAdapter.extend({
  // TODO: This is a temporary solution for creating a route
  // dispatch backend will have a proper endpoint for POST route
  buildURL: function (modelType, id, snapshot, requestType) {
    if (requestType === 'createRecord') {
      return `${this.host}/schedule/${snapshot.record.get('schedule.id')}/route/clusterization`;
    }
    return this._super(...arguments);
  }
});
