import ApplicationAdapter from './application';
import ENV from '../config/environment';

export default ApplicationAdapter.extend({
  defaultSerializer: '-ss-schedulingService',
  host: ENV.API.schedulingService.host,

  pathForType: function(type) {
    // trim 'ss' prefix from model type
    let regEx = /^(?:ss-)?(.*)$/
    let match = regEx.exec(type)[1];
    return this._super(match);
  }
});
