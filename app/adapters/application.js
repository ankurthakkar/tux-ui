import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import { inject as service } from '@ember/service';
import { dasherize } from '@ember/string';
import { isPresent } from '@ember/utils';

const { JSONAPIAdapter } = DS;

export default JSONAPIAdapter.extend(DataAdapterMixin, {
  errorMessage: service(),

  authorizer: 'authorizer:sso',

  normalizeErrorResponse(status, headers, payload) {
    if (payload && typeof payload === 'object' && payload.errors) {
      return payload.errors;
      // To deal with the API's error messages that do not comply to JSONAPI Standard.
    } else if (isPresent(payload.message)) {
      let error = {
          status: `${status}`,
          title: "The backend responded with an error",
          detail: payload.message
        };
      this.get('errorMessage').pushError(error);
      return [error];
    } else {
      return [
        {
          status: `${status}`,
          title: "The backend responded with an error",
          detail: `${payload}`
        }
      ];
    }
  },

  pathForType(modelName) {
    return dasherize(modelName);
  },

  ajaxOptions(url, type) {
    let hash = this._super(...arguments);

    // this._super() handles all but the GET case; in this case, the data
    // object is normally passed straight to $.ajax, but this encodes URI
    // characters like + and -. Instead, we encode params to a string ourself.
    if (hash.data && type === 'GET') {
      hash.data = Object.entries(hash.data)
        .filter(([...value]) => isPresent(value))
        .map(([key, value]) => `${key}=${value}`)
        .join('&');

      hash.data = encodeURI(hash.data);
    }

    return hash;
  }
});
