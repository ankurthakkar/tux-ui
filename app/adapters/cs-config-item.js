import ConfigServiceAdapter from './-cs-configService';

const DEFAULT_CATEGORY = 'config';

export default ConfigServiceAdapter.extend({
  updateRecord(store, type, snapshot) {
    let data = {};
    let serializer = store.serializerFor(type.modelName);

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    // config service does not allow `id` or `name` on update
    delete data.data.id;
    delete data.data.attributes.name;

    let url = this.buildURL(type.modelName, snapshot.id, snapshot, 'updateRecord');

    return this.ajax(url, 'PATCH', { data: data });
  },

  ajaxOptions(url, type) {
    let options = this._super(...arguments);

    if (type === 'GET') {
      // clear this stuff out; config API doesn't use it
      options.data = null;
    }

    return options;
  },

  urlForCreateRecord(modelName, snapshot) {
    let { category } = snapshot.attributes();
    return this._buildUrlForPath(category)
  },

  urlForDeleteRecord(id /*, modelName, snapshot*/) {
    return this._buildUrlForPath(id);
  },

  urlForFindAll(/*modelName, snapshot*/) {
    // this isn't _really_ a findAll
    return this._buildUrlForPath(DEFAULT_CATEGORY);
  },

  urlForFindRecord(id /*, modelName, snapshot*/) {
    return this._buildUrlForPath(id);
  },

  urlForQuery(query /*, modelName*/) {
    return this._buildUrlForPath(query);
  },

  urlForQueryRecord(query /*, modelName*/) {
    return this._buildUrlForPath(query);
  },

  urlForUpdateRecord(id /*, modelName, snapshot*/) {
    return this._buildUrlForPath(id);
  },

  _buildUrlForPath(path) {
    let host = this.get('host');
    let namespace = this.get('namespace');
    return `${host}/${namespace}/${path}`;
  }
});
