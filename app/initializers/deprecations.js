import { registerDeprecationHandler } from '@ember/debug';

const DEPRECATION_IDS_BLACKLIST = [
  'ember-simple-auth.session.authorize',
  'ember-simple-auth.baseAuthorizer'
];

export function initialize() {
  registerDeprecationHandler((message, options, next) => {
    if (DEPRECATION_IDS_BLACKLIST.includes(options.id)) return;
    return next(message, options);
  });
}

export default {
  initialize
};
