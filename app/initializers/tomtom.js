import tomtom from 'tomtom';

export function initialize() {
  tomtom.setProductInfo('Adept IQ', '0.0.0');
}

export default {
  initialize
};
