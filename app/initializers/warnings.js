import { registerWarnHandler } from '@ember/debug';

const WARNING_IDS_BLACKLIST = [
  'ember-light-table.height-attribute',
  'ember-htmlbars.style-xss-warning'
];

export function initialize() {
  registerWarnHandler((message, options, next) => {
    if (WARNING_IDS_BLACKLIST.includes(options.id)) {
      console.trace = null; // eslint-disable-line no-console
      return;
    }
    return next(message, options);
  });
}

export default {
  initialize
};
