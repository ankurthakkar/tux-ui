import Authenticator from 'ember-simple-auth/authenticators/base';
import { Promise } from 'rsvp';
import { isPresent } from '@ember/utils';
import fetch from 'fetch';
import ENV from '../config/environment';

export default Authenticator.extend({
  authenticate(userName, password, tokenPayload) {
    // Bypass login when token payload from /signup during registration passed.
    if (isPresent(tokenPayload)) return Promise.resolve(tokenPayload);

    let { host } = ENV.API.ssoService;

    let url = host + '/login';
    let base64Token = btoa(`${userName}:${password}`);

    let options = {
      headers: {
        Authorization: 'Basic ' + base64Token
      },
      method: 'GET'
    };

    return new Promise((resolve, reject) => {
      fetch(url, options).then(response => {
        response.json().then(body => {
          if (!response.ok) {
            reject(body);
          } else {
            resolve(body);
          }
        });
      }).catch(reject);
    });
  },

  restore(data) {
    return Promise.resolve(data);
  }
})
