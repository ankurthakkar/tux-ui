import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
  belongsTo,
} = DS;

export default Model.extend({
  count: attr('number'),

  rider: belongsTo('rms-rider'),
  travelNeedType: belongsTo('rms-travel-need-type'),
  passengerType: belongsTo('rms-passenger-type'),

  isEquipment: computed('travelNeedType', function() {
    return !this.get('travelNeedType.isServiceAnimal') &&
      this.get('passengerType.isClient');
  }),

  isExtraPassenger: computed('travelNeedType', 'passengerType', function() {
    return this.get('travelNeedType.isServiceAnimal') ||
      this.get('passengerType.isPca') ||
      this.get('passengerType.isCompanion');
  })
});
