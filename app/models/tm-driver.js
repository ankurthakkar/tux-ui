import DS from 'ember-data';
import { belongsTo, hasMany } from 'ember-data/relationships';
import attr from 'ember-data/attr';

const {
  Model
} = DS;

export default Model.extend({
  status: attr('string'),
  name: attr('string'),
  lastName: attr('string'),
  firstName: attr('string'),
  middleName: attr('string'),
  badgeNr: attr('string'),
  email: attr('string'),

  // these are non-JSON-API compliant object attrs that will need to be
  // normalized before consumption
  provider: attr('object'),
  phone1: attr('object'),
  phone2: attr('object'),
  address: attr('object'),
  attributes: attr('object'),
  restrictions: attr('object'),
  licenses: attr('object'),

  iqDriver: belongsTo('iq-driver', { inverse: 'tmDrivers' }),

  cannedMessages: hasMany('tm-canned-message')
});
