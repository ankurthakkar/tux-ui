import DS from 'ember-data';
const { Model, belongsTo, attr } = DS;

export default Model.extend({
    type: attr('string'),
    timestamp: attr('date'),
    description: attr('string'),
    route: belongsTo()
});
