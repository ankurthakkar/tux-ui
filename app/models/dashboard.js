import DS from 'ember-data';
import { computed, observer } from '@ember/object';
import Widget from 'adept-iq/classes/widget';
import { isPresent } from '@ember/utils';
import lodash from 'lodash';

const {
  Model,
  attr
} = DS;

const CLONEABLE_PROPERTIES = ['name', 'role', '_widgets'];

export default Model.extend({
  name: attr('string'),
  role: attr('string'),

  // 'workspaces-{users,default,admin}'; can't be changed once saved
  category: attr('string', { defaultValue: 'workspaces-users' }),

  // absolute timeframe as selected by user
  startDate: attr('date', {
    defaultValue: () => moment().startOf('day').toDate()
  }),

  endDate: attr('date', {
    defaultValue: () => moment().startOf('day').add(1, 'day').toDate()
  }),

  // when `startDate` or `endDate` is modified, store the user's current date
  // so that the relative timeframe can be calculated from a later date
  referenceDate: attr('date', {
    defaultValue: () => moment().startOf('day').toDate()
  }),

  // private; do not use directly
  _widgets: attr('object'),

  // hydrated "virtual models"; these are what the app interacts with
  widgets: null,

  // TODO: move this into (map) widget state
  mapState: attr('object', { defaultValue: () => {} }),

  // widgets update this property when they change
  hasDirtyWidgets: false,

  // lookup to re-use classes
  widgetCache: null,

  // caches widget state for rollback
  widgetsSavepoint: null,

  isDirty: computed.or('hasDirtyAttributes', 'hasDirtyWidgets'),

  init() {
    this.set('widgetCache', {});
    this.set('widgets', []);
    this._onWidgetsChange();
  },

  // when the underlying `_widgets` POJO changes, we completely reset the
  // `widgets` array; this allows incoming payloads to overwrite local state
  _onWidgetsChange: observer('_widgets', function() {
    let widgets = this.get('widgets');
    widgets.clear();

    let widgetOptions = this.get('_widgets') || [];

    widgetOptions.forEach((options) => {
      if (!options.state) {
        options.state = {};
      }

      let widget = this.makeWidget(options);

      widget.notifyPropertyChange('state');

      widgets.pushObject(widget);
    });

    this.makeWidgetsSavepoint();
  }),

  height: computed('widgets.@each.{y,height}', function() {
    return this.get('widgets').reduce((max, widget) => {
      let y = widget.get('y');
      let height = widget.get('height');
      return Math.max(max, y + height);
    }, 0);
  }),

  width: computed('widgets.@each.{x,width}', function() {
    return this.get('widgets').reduce((max, widget) => {
      let x = widget.get('x');
      let width = widget.get('width');
      return Math.max(max, x + width);
    }, 0);
  }),

  save() {
    let promise = this._super(...arguments);
    promise.then(() => this.set('hasDirtyWidgets', false));
    return promise;
  },

  reload() {
    let promise = this._super(...arguments);
    promise.then(() => this._onWidgetsChange());
    return promise;
  },

  getOptionsForClone() {
    return lodash.cloneDeep(this.getProperties(CLONEABLE_PROPERTIES));
  },

  makeWidgetsSavepoint() {
    let widgetsSavepoint =
      this.get('widgets').map((widget) => widget.serialize());

    this.set('widgetsSavepoint', widgetsSavepoint);
  },

  rollbackWidgets() {
    let widgetsSavepoint = this.get('widgetsSavepoint');
    if (!widgetsSavepoint) {
      throw 'no dashboard savepoint; must call makeWidgetsSavepoint() first';
    }

    this.get('widgets').clear();

    widgetsSavepoint.forEach((options) => {
      this.addWidget(options);
    });
  },

  addWidget(options) {
    let widget = this.makeWidget(options);
    this.get('widgets').pushObject(widget);
  },

  removeWidget(widget) {
    this.get('widgets').removeObject(widget);
  },

  makeWidget(options) {
    let { id } = options;
    let widgetCache = this.get('widgetCache');

    if (isPresent(id)) {
      let widget = widgetCache[id];
      if (widget) {
        widget.setProperties(options);
        return widget;
      }
    } else {
      id = 1;
      let usedIds = Object.keys(widgetCache);
      while (usedIds.includes(id.toString())) {
        id += 1;
      }
      id = id.toString();
    }

    let extendedOptions = Object.assign({}, options, {
      id,
      dashboard: this
    });

    let widget = Widget.create(extendedOptions);
    widgetCache[id] = widget;

    return widget;
  },

  // TODO: this should live in map widget state
  mergeMapState(changes = {}) {
    let mapState = this.get('mapState');
    let mergedState = lodash.merge({}, mapState, changes);
    this.set('mapState', mergedState);
    this.notifyPropertyChange('mapState');
    this.set('hasDirtyWidgets', true);
  }
});
