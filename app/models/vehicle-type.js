import DS from 'ember-data';

const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
    name: attr('string'),
    schedule: belongsTo(),
    vehicle: hasMany(),
    vehicleCapacityConfigs: hasMany()
});
