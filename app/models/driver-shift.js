import DS from 'ember-data';
const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
    plannedStartTime: attr('date'),
    plannedEndTime: attr('date'),
    driver: belongsTo(),
    vehicleRoute: belongsTo('vehicleRoute', { inverse: 'driverShifts'}),
    driverEvents: hasMany()
});


