import Model from 'ember-data/model';
import { belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';
import OTPLabelMixin from '../mixins/otp-label';

export default Model.extend(OTPLabelMixin, {
  bsBooking: belongsTo(),
  dsTrip: belongsTo('trip'),

  otp: computed.alias('dsTrip.otp'),
  otpValue: computed.alias('dsTrip.otpValue'),

  status: computed('bsBooking.status', 'dsTrip.status', function() {
    let trip = this.get('dsTrip.content');
    if (trip) {
      return trip.get('status');
    }

    return this.get('bsBooking.status');
  })
});
