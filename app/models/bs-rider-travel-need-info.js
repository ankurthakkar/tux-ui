import DS from 'ember-data';
import { computed, get } from '@ember/object';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    count: attr('number'),
    rider: belongsTo('rider-info'),
    travelNeedType: belongsTo('bs-travel-need-type'),
    passengerType: belongsTo('bs-passenger-type'),

    isClient: computed.equal('passengerType.name', 'client'),
    isPca: computed.equal('passengerType.name', 'pca'),
    isCompanion: computed.equal('passengerType.name', 'companion'),

    basicTravelNeedType: computed('travelNeedType', function() {
        const name = get(this, 'travelNeedType.name');

        return (
            name === 'ambulatory' ||
            name === 'wideAmbulatory' ||
            name === 'wheelchair' ||
            name === 'wideWheelchair'
        );
    })
});
