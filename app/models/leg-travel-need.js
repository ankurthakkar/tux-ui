import DS from 'ember-data';
import { get, computed } from '@ember/object';

const { Model, attr, belongsTo } = DS;

const LegTravelNeed = Model.extend({
    count: attr('number'),
    leg: belongsTo('bs-leg'),
    travelNeedType: belongsTo('bs-travel-need-type'),
    passengerType: belongsTo('bs-passenger-type'),

    isClient: computed.equal('passengerType.name', 'client'),
    isPca: computed.equal('passengerType.name', 'pca'),
    isCompanion: computed.equal('passengerType.name', 'companion'),

    basicTravelNeedType: computed('travelNeedType', function() {
        const name = get(this, 'travelNeedType.name');

        return (
            name === 'ambulatory' ||
            name === 'wideAmbulatory' ||
            name === 'wheelchair' ||
            name === 'wideWheelchair'
        );
    })
});

export default LegTravelNeed;
