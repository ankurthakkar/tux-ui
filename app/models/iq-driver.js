import DS from 'ember-data';
import { belongsTo, hasMany } from 'ember-data/relationships';

const { Model } = DS;

export default Model.extend({
  dsDriver: belongsTo('driver', { inverse: 'iqDriver' }),
  ssDriver: belongsTo('ss-driver', { inverse: 'iqDriver' }),
  // tmDriver: belongsTo('tm-driver', { inverse: 'iqDriver' })

  // TODO: this should be a belongsTo, but the sample data is bunk
  tmDrivers: hasMany('tm-driver', { inverse: 'iqDriver' })
});
