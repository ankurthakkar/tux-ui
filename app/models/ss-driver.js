import DS from 'ember-data';
import { computed } from '@ember/object';
import moment from 'moment';
import ENV from '../config/environment';
import { get } from '@ember/object';
import { belongsTo } from 'ember-data/relationships';

const {
  Model,
  attr,
  hasMany
} = DS;

function formatTextExtension(className, value) {
  return {
    className: className,
    value: value
  };
}

export default Model.extend({
    driverId: attr('string'),
    firstName: attr('string'),
    middleName: attr('string'),
    lastName: attr('string'),
    areaCode: attr('string'),
    phoneNumber: attr('string'),
    active: attr('boolean'),
    preferredVehicleName: attr('string'),

    iqDriver: belongsTo('iq-driver', { inverse: 'ssDriver' }),

    fullName: computed('firstName', 'middleName', 'lastName', function() {
      return `${get(this, 'firstName')} ${get(this, 'middleName') || ''} ${get(
          this,
          'lastName'
      )}`.replace(/\s+/, ' ');
    }),

    availability: hasMany('ss-driver-availability'),
    // TODO: waiting for proper backend preferred vehicle support
    // will change later when it is done
    vehicles: hasMany('ss-vehicle'),

    status: computed('status', function() {
        return 'Coming Soon';
    }),

    vehicleId: computed('vehicleId', function() {
        return 'Coming Soon';
    }),

    routeId: computed('vehicleId', function() {
      return 'Coming Soon';
    }),

    formattedAvailabilities: computed('availability', 'active', function() {
      const active = this.get('active');
      let formattedObject = {
        header: null,
        values: []
      };
      let availabilities = [];

      this.get('availability').forEach(function(item) {
        const startTime = moment(item.get('startTime')),
              endTime = moment(item.get('endTime')),
              now = moment();

        if (active && now.isBetween(startTime, endTime)) {
          formattedObject.header = null;

          availabilities.push(formatTextExtension('mainItem', `${startTime.format(ENV.dateTimeFormat.dateMoment)} - ${endTime.format(ENV.dateTimeFormat.dateMoment)}`));
          if (item.monday) {availabilities.push(formatTextExtension('valueItem', `Monday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.tuesday) {availabilities.push(formatTextExtension('valueItem', `Tuesday  ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.wednesday) {availabilities.push(formatTextExtension('valueItem', `Wednesday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.thursday) {availabilities.push(formatTextExtension('valueItem', `Thursday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.friday) {availabilities.push(formatTextExtension('valueItem', `Friday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.saturday) {availabilities.push(formatTextExtension('valueItem', `Saturday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
          if (item.sunday) {availabilities.push(formatTextExtension('valueItem', `Sunday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        }

        formattedObject.values = availabilities;
      });

      return formattedObject;
    })
})

