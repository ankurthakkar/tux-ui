import DS from 'ember-data';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

const { Model } = DS;

export default Model.extend({
  lat: attr('number'),
  lng: attr('number'),
  heading: attr('number'),
  speed: attr('number'),
  odo: attr('number'),
  timestamp: attr('date'),

  // we add this in because `id` gets replaced with a composite key
  vehicleId: attr('string'),

  iqVehicle: belongsTo(),

  localTimestamp: null,

  init() {
    this._super(...arguments);
    this.set('localTimestamp', (new Date()));
  }
});
