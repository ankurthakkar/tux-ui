import { get, getProperties, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import DS from 'ember-data';
import moment from 'moment';
const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
    name: attr('string'),
    mobileId: attr('number'),
    xmin: attr('string'),
    serviceStatus: attr('string'),
    lastGpsPing: attr('date'),
    mileage: attr('string'),
    schedule: belongsTo(),
    provider: belongsTo(),
    vehicleType: belongsTo('vehicle-type'),
    vehicleRoutes: hasMany(),
    vehicleEvents: hasMany(),

    iqVehicle: belongsTo('iq-vehicle', { inverse: 'dsVehicle' }),

    latestDriver: computed.alias('latestVehicleRoute.latestDriver'),
    latestVehicleRoute: computed('vehicleRoutes.@each.plannedStartTime', function() {
        return this.get('vehicleRoutes').sortBy('plannedStartTime').get('lastObject');
    }),

    latestVehicleEvent: computed('vehicleEvents', function() {
        const vehicleEvents = get(this, 'vehicleEvents');

        if (vehicleEvents) {
            return vehicleEvents.sortBy('timestamp').objectAt(get(vehicleEvents, 'length') - 1);
        }

        return null;
    }),
    currentRoute: computed('vehicleRoutes', 'vehicleRoutes.@each.route', function() {
        const vehicleRoutes = get(this, 'vehicleRoutes');
        const now = moment();

        if (!isEmpty(vehicleRoutes)) {
            const currentVr = vehicleRoutes.find((vr) => {
                const { plannedStartTime, plannedEndTime } = getProperties(
                    vr,
                    'plannedStartTime',
                    'plannedEndTime'
                );

                return (
                    moment(plannedEndTime).isAfter(now) && moment(plannedStartTime).isBefore(now)
                );
            });

            if (!isEmpty(currentVr)) {
                return get(currentVr, 'route');
            }
        }
        return null;
    }),
    currentAssignment: computed('vehicleRoutes', function() {
        const vehicleRoutes = get(this, 'vehicleRoutes');
        const now = moment();

        if (!isEmpty(vehicleRoutes)) {
            const currentVr = vehicleRoutes.find((vr) => {
                const { plannedStartTime, plannedEndTime } = getProperties(
                    vr,
                    'plannedStartTime',
                    'plannedEndTime'
                );

                return (
                    moment(plannedEndTime).isAfter(now) && moment(plannedStartTime).isBefore(now)
                );
            });

            if (!isEmpty(currentVr)) {
                return currentVr;
            }
        }
        return null;
    }),
    isAssignable: computed('vehicleRoutes.[]', function() {
        return get(this, 'vehicleRoutes.length') === 0;
    }),

    isVehicleAvailable(startTime, endTime) {
      let retVal = true;
      const vehicleRoutes = get(this, 'vehicleRoutes');

      if (!isEmpty(vehicleRoutes)) {
        vehicleRoutes.forEach((vehicleRoute) => {

          if (((startTime.isSameOrAfter(vehicleRoute.get('plannedStartTime')) &&
            startTime.isSameOrBefore(vehicleRoute.get('plannedEndTime'))) ||
            (endTime.isSameOrAfter(vehicleRoute.get('plannedStartTime')) &&
            endTime.isSameOrBefore(vehicleRoute.get('plannedEndTime'))))) {
            retVal = false;
          }

        });
      }

      return retVal;
    }
});
