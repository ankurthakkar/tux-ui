import DS from 'ember-data';
import fetch from 'fetch';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  start: attr('ss-date'),
  end: attr('ss-date'),
  name: attr('string'),
  status: attr('string', { defaultValue: 'unscheduled' }),

  importBookings() {
    let id = this.get('id');
    let modelName = this.constructor.modelName;
    let adapter = this.store.adapterFor(modelName);
    let baseUrl = adapter.buildURL(modelName, id, null, 'findRecord');

    return fetch(`${baseUrl}/import`, {
      method: 'PUT',
      headers: adapter.headersForRequest()
    }).then((response) => {
      if (!response.ok) {
        throw Error(`Failed to import bookings to schedule with id='${id}'.`);
      }
    });
  },

  regenerateSchedule() {
    let id = this.get('id');
    let modelName = this.constructor.modelName;
    let adapter = this.store.adapterFor(modelName);
    let baseUrl = adapter.buildURL(modelName, id, null, 'findRecord');

    return fetch(`${baseUrl}/generation`, {
      method: 'PUT',
      headers: adapter.headersForRequest()
    }).then((response) => {
      if (!response.ok) {
        throw Error(`Failed to generate schedule with id='${id}'.`);
      }
    });
  },

  exportSchedule() {
    let id = this.get('id');
    let modelName = this.constructor.modelName;
    let adapter = this.store.adapterFor(modelName);
    let baseUrl = adapter.buildURL(modelName, id, null, 'findRecord');

    return fetch(`${baseUrl}/export`, {
      method: 'PATCH',
      headers: adapter.headersForRequest()
    }).then((response) => {
      if (!response.ok) {
        throw Error(`Failed to export schedule with id='${id}'.`);
      }
    });
  }
});
