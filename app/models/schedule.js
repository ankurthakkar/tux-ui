import DS from 'ember-data';

const {
  Model,
  attr,
  hasMany
} = DS;

export default Model.extend({
  externalId: attr('string'),
  startTime: attr('date'),
  endTime: attr('date'),

  provider: hasMany(),
  providerTypes: hasMany(),
  route: hasMany()
});
