import DS from 'ember-data';
import { get, computed } from '@ember/object';

const { Model, attr, belongsTo } = DS;

const LegTravelNeed = Model.extend({
    count: attr('number'),
    leg: belongsTo('bs-leg'),
    travelNeedType: belongsTo('bs-travel-need-type'),
    passengerType: belongsTo('bs-passenger-type'),

    isClient: computed.equal('passengerType.name', 'client'),
    isPca: computed.equal('passengerType.name', 'pca'),
    isCompanion: computed.equal('passengerType.name', 'companion'),

    basicTravelNeedType: computed('travelNeedType', function() {
        const name = get(this, 'travelNeedType.name');

        return (
            name === 'ambulatory' ||
            name === 'wideAmbulatory' ||
            name === 'wheelchair' ||
            name === 'wideWheelchair'
        );
    }),

    isEquipment: computed('travelNeedType', function() {
      return !this.get('travelNeedType.isServiceAnimal') &&
        !this.get('passengerType.isClient');
    }),

    isExtraPassenger: computed('travelNeedType', 'passengerType', function() {
      return this.get('travelNeedType.isServiceAnimal') ||
        this.get('passengerType.isPca') ||
        this.get('passengerType.isCompanion');
    })
});

export default LegTravelNeed;
