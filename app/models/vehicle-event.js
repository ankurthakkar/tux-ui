import { collect } from '@ember/object/computed';
import DS from 'ember-data';
const { Model, attr} = DS;

export default Model.extend({
    type: attr('string'),
    timestamp: attr('date'),
    lat: attr('number'),
    lng: attr('number'),
    heading: attr('number'),
    speed: attr('number'),
    odometer: attr('number'),
    location: collect('lat', 'lng')
});
