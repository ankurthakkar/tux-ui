import DS from 'ember-data';
import { computed } from '@ember/object';

const { Model, attr } = DS;

export default Model.extend({
  code: attr('string'),
  externalCode: attr('string'),
  description: attr('string'),
  name: computed.alias('description')
});
