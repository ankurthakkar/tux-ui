import { computed, get } from '@ember/object';
import DS from 'ember-data';

const { Model, attr, belongsTo, hasMany } = DS;

export default Model.extend({
  alias: attr('string'),
  streetNumber: attr('string'),
  streetName: attr('string'),
  streetAddress: attr('string'),
  room: attr('string'),
  city: attr('string'),
  county: attr('string'),
  state: attr('string'),
  postalCode: attr('string'),
  country: attr('string'),
  countryAbbreviation:attr('string'),
  notes: attr('string'),
  address: attr('nested'),
  coordinates: attr('nested'),
  pointOfInterest: attr('nested'),
  geoNode: attr('number'),
  locality: attr('string'),
  region: attr('string'),
  subRegion: attr('string'),
  postOfficeBoxNumber: attr('string'),
  stop: belongsTo('bs-stop'),
  legOrigin: belongsTo('bs-leg', { inverse: 'origin' }),
  legDestination: belongsTo('bs-leg', { inverse: 'destination' }),

  addresses: hasMany('bs-address'),
  tomtomFormattedAddress: computed('streetNumber', 'streetName',
    'locality', function() {
    let address = null;

    if (this.get('streetName') !== undefined) {
      address = {
        address: {
          freeformAddress: `${this.get('streetNumber')} ${this.get('streetName')} ${this.get('locality') }`
        }
      };
    }

    return address;
  }),

  fullAddress: computed('address', 'pointOfInterest', function() {
    const alias = get(this, 'pointOfInterest.alias');

    const address = get(this, 'address');

    const streetAddress = [];

    if (address) {
      if (alias) {
          streetAddress.push(alias + ',');
      }
      streetAddress.push(address.streetNumber);
      streetAddress.push(address.streetName);

      const result = [streetAddress.join(' ')];

      result.push(address.city);
      // result.push(address.county);
      result.push(address.stateAbbreviation);
      result.push(address.postalCode);

      return result.join(', ');
    }
    return '';
}).readOnly()
});

