import DS from 'ember-data';
import { get, computed } from '@ember/object';
import { belongsTo } from 'ember-data/relationships';

const { Model, attr, hasMany } = DS;

export default Model.extend({
    externalId: attr('string'),
    firstName: attr('string'),
    lastName: attr('string'),
    middleName: attr('string'),
    travelNeeds: hasMany(),
    trips: hasMany(),

    iqRider: belongsTo('iq-rider', { inverse: 'dsRider' }),

    fullName: computed('firstName', 'middleName', 'lastName', function() {
        return `${get(this, 'firstName')} ${get(this, 'middleName') || ''} ${get(
            this,
            'lastName'
        )}`.replace(/\s+/, ' ');
    })
});
