import DS from 'ember-data';
const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
    name: attr('string'),
    engineId: attr('string'),
    engineName: attr('string'),
    constrainedByLIFO: attr('boolean'),
    dwellFactor: attr('number'),
    description: attr('string'),
    schedule: belongsTo(),
    vehicleCapacityConfigs: hasMany()
});
