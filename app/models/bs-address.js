import DS from 'ember-data';
import { computed, get } from '@ember/object';

const { Model, attr, hasMany } = DS;

export default Model.extend({
    alias: attr('string'),
    streetNumber: attr('string'),
    streetName: attr('string'),
    locality: attr('string'),
    region: attr('string'),
    subRegion: attr('string'),
    postOfficeBoxNumber: attr('string'),
    postalCode: attr('string'),
    country: attr('string'),
    notes: attr('string'),
    locations: hasMany('bs-location'),
    ridersPrimary: hasMany('bs-rider'),
    ridersFavorite: hasMany('bs-rider'),

    location: computed('locations', function() {
        return get(this, 'locations').objectAt(0);
    }),

    fullAddress: computed(
        'streetNumber',
        'streetAddress',
        'locality',
        'region',
        'postalCode',
        'alias',
        function() {
            const alias = get(this, 'alias');
            const streetNumber = get(this, 'streetNumber');
            const streetAddress = get(this, 'streetAddress');
            const locality = get(this, 'locality');
            const region = get(this, 'region');
            const postalCode = get(this, 'postalCode');

            const address = [];

            if (alias) {
                address.push(alias + ',');
            }

            address.push(streetNumber);
            address.push(streetAddress);

            const result = [address.join(' ')];

            result.push(locality);
            result.push(region);
            result.push(postalCode);

            return result.join(', ');
        }
    ).readOnly()
});
