import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
  belongsTo,
  hasMany
} = DS;

export default Model.extend({
    plannedStartTime: attr('date'),
    plannedEndTime: attr('date'),

    route: belongsTo(),
    vehicle: belongsTo(),

    driverShifts: hasMany(),

    latestDriver: computed.alias('latestDriverShift.driver'),
    latestDriverShift: computed('driverShifts.@each.plannedStartTime', function() {
      return this.get('driverShifts').sortBy('plannedStartTime').get('lastObject');
    })
});
