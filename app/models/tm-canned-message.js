import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

const { Model, attr } = DS;

export const MESSAGE_STATUS_SENDING = 'Sending';
export const MESSAGE_STATUS_SENT = 'Sent';
export const MESSAGE_STATUS_RECEIVED = 'Received';
export const MESSAGE_STATUS_FAILED = 'Failed';
export const MESSAGE_STATUS_DELETED = 'Deleted';

export const MESSAGE_STATUSES = [
  MESSAGE_STATUS_SENDING,
  MESSAGE_STATUS_SENT,
  MESSAGE_STATUS_RECEIVED,
  MESSAGE_STATUS_FAILED,
  MESSAGE_STATUS_DELETED
];

export default Model.extend({
  body: attr('string'),
  messageNr: attr('string'),
  replyToMsgId: attr('string'),
  priority: attr('string', { defaultValue: 'normal' }),

  createdTime: attr('date', { defaultValue: () => new Date() }),
  deliveredTime: attr('date'),
  readTime: attr('date'),
  deletedTime: attr('date'),

  // these are used to normalize non-JSON-API payload
  driverId: attr('string'),
  driverEndPointType: attr('string', { defaultValue: 'recipient' }),

  // TODO: make sure API is supplying these
  vehicleId: attr('string'),
  routeId: attr('string'),

  // this is "proxied" to `driverId` and `driverEndPointType` above
  driver: belongsTo('tm-driver'),

  // TODO: implement when supported by API
  // vehicle: belongsTo('tm-vehicle'),
  // route: belongsTo('tm-route'),

  isDelivered: computed('deliveredTime', function() {
    return isPresent(this.get('deliveredTime'));
  }),

  isRead: computed('readTime', function() {
    return isPresent(this.get('readTime'));
  }),

  isDeleted: computed('deletedTime', function() {
    return isPresent(this.get('deletedTime'));
  }),

  status: computed('isDelivered', 'isRead', 'isDeleted', function() {
    if (this.get('isDeleted')) return MESSAGE_STATUS_DELETED;
    if (this.get('isRead')) return MESSAGE_STATUS_RECEIVED;
    if (this.get('isDelivered')) return MESSAGE_STATUS_SENT;

    // TODO: how do we know when a message is "failed"?
    return MESSAGE_STATUS_SENDING;
  }),

  readString: computed('isRead', function() {
    return this.get('isRead') ? 'Yes' : 'No';
  })
});
