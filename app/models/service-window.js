import DS from 'ember-data';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  name: attr('string'),
searchWindowStart: attr(),
searchWindowEnd: attr(),
promiseWindowStart: attr(),
promiseWindowEnd: attr(),
dropWindowStart: attr(),
dropWindowEnd: attr(),
});
