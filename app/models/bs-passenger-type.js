import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
} = DS;

export default Model.extend({
  name: attr('string'),

  isPca: computed('name', function() {
    return this.get('name') === 'pca';
  }),

  isCompanion: computed('name', function() {
    return this.get('name') === 'companion';
  }),

  isClient: computed('name', function() {
    return this.get('name') === 'client';
  })
});
