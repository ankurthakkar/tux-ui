import DS from 'ember-data';
import { computed } from '@ember/object';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    name: attr('string'),
    vehicleCapacityCount: attr('number'),
    loadTime: attr('number'),
    unloadTime: attr('number'),
    displayName: attr('string'),
    displayOrder: attr('number'),
    schedule: belongsTo(),
    vehicleCapacityType: belongsTo(),
    isServiceAnimal: computed('name', function() {
      return this.get('name') === 'serviceAnimal';
    }),

    isAmbulatory: computed('name', function() {
      return this.get('name') === 'ambulatory';
    })
});
