import DS from 'ember-data';
import { computed } from '@ember/object';

const { Model, attr } = DS;

export default Model.extend({
    name: attr('string'),
    loadTime: attr('number'),
    unloadTime: attr('number'),
    displayName: attr('string'),
    displayOrder: attr('number'),
    description: attr('string'),
    isGeneral: computed.equal('name', 'serviceAnimal'),
    isBasic: computed.not('isGeneral')
});
