import DS from 'ember-data';
import { computed, get } from '@ember/object';

const { Model, attr } = DS;

export default Model.extend({
    name: attr('string'),
    loadTime: attr('number'),
    unloadTime: attr('number'),
    displayName: attr('string'),
    displayOrder: attr('number'),
    description: attr('string'),

    isBasic: computed('name', function() {
        const name = get(this, 'name');

        if (name === 'serviceAnimal') {
            return false;
        }
        return true;
    })
});
