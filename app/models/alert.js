import DS from 'ember-data';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  generatedAt: attr('date'),
  alert: attr('string'),
  category: attr('string'),
  vehicles: attr('vehicle', { inverse: null }),
  drivers: attr('driver', { inverse: null }),
  routes: attr('route', { inverse: null })
});
