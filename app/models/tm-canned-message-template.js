import DS from 'ember-data';

const { Model, attr } = DS;

export default Model.extend({
  body: attr('string'),
  messageNr: attr('string'),

  // TODO: normalize in serializer
  provider: attr('object')
});
