import { computed, get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import DS from 'ember-data';
import NameParser from '../utils/name-parser';

const { Model, attr, hasMany } = DS;

/*
        This is the rider model for data coming from rider-management and later updated with
        travel-needs. The rider-info model holds data to be sent to the booking service.
        This is necessary because a complex nesting of DS models and ember-data-fragments
        has problems serializing. See app/confirm/route.js.
 */
export default Model.extend({
    riderId: attr('string'),
    firstName: attr('string'),
    middleName: attr('string'),
    lastName: attr('string'),
    notes: attr('string'),
    travelNeeds: hasMany('bs-rider-travel-need-info'),

    fullName: computed('firstName', 'middleName', 'lastName', {
        get(/* key*/) {
            let middleName = get(this, 'middleName');

            if (isEmpty(middleName)) {
                middleName = ' ';
            } else {
                middleName = ` ${middleName} `;
            }
            return `${get(this, 'firstName')}${middleName}${get(this, 'lastName') || ''}`.trim();
        },
        set(key, value) {
            const nameParts = NameParser.parse(value);

            set(this, 'firstName', nameParts.firstName);
            set(this, 'middleName', nameParts.middleName);
            set(this, 'lastName', nameParts.lastName);

            return value;
        }
    })
});
