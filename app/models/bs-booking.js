import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany, belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';
import { get } from '@ember/object';
import { isPresent, isNone } from '@ember/utils';

export default Model.extend({
  status: attr('string'),
  planState: attr('number'),
  requestedTime: attr('date'),
  anchor: attr('string'),

  iqTrip: belongsTo(),
  fareType: belongsTo('bs-fare-type'),
  pick: belongsTo('bs-stop', { inverse: 'pickBooking' }),
  drop: belongsTo('bs-stop', { inverse: 'dropBooking' }),

  // this is created for AC; should go through legs
  rider: belongsTo('bs-rider'),

  legs: hasMany('bs-leg'),
  originlocations: hasMany('bs-location'),
  destinationlocations: hasMany('bs-location'),
  travelNeeds: hasMany('rms-rider-travel-need'),

  stops: computed.collect('pick', 'drop'),
  leg: computed.alias('legs.firstObject'),
  legTravelNeeds: computed.alias('leg.legTravelNeeds'),
  extraPassengers: computed.alias('rider.extraPassengers'),
  equipments: computed.alias('rider.equipments'),

  // FIXME: do we need to map before getting `equipments` here?
  legEquipments: computed('leg', function() {
    const legTravelNeeds = get(this, 'leg.legTravelNeeds');

    return legTravelNeeds.get('equipments');
  }),

  // FIXME: do we need to map before getting `extraPassengers` here?
  legExtraPassengers: computed('leg', function() {
    const legTravelNeeds = get(this, 'leg.legTravelNeeds');

    return legTravelNeeds.get('extraPassengers');
  }),

  location: computed('pick.location', 'drop.location', function() {
    let location = [];
    let start = [this.get('pick.location.lat'), this.get('pick.location.lng')];
    if (isPresent(start[0]) && isPresent(start[1])) location.push(start);
    let end = [this.get('drop.location.lat'), this.get('drop.location.lng')];
    if (isPresent(end[0]) && isPresent(end[1])) location.push(end);
    return location;
  }),
  
  polyline: computed('pick.location.{lat,lng}', 'drop.location.{lat,lng}', function() {
    let pickLat = this.get('pick.location.lat');
    let pickLng = this.get('pick.location.lng');
    let dropLat = this.get('drop.location.lat');
    let dropLng = this.get('drop.location.lng');

    if ([pickLat, pickLng, dropLat, dropLng].any(isNone)) {
      return null;
    }

    return [[pickLat, pickLng], [dropLat, dropLng]];
  })
});
