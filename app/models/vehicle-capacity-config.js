import DS from 'ember-data';
const { Model, belongsTo, attr } = DS;

export default Model.extend({
    count: attr('number'),
    vehicleType: belongsTo(),
    vehicleCapacityType: belongsTo()
});
