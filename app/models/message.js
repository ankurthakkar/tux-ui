import { computed } from '@ember/object';
import DS from 'ember-data';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  date: attr('date'),
  time: attr('time'),
  message: attr('string'),
  driverId: attr('uuid'),
  driverLastName: attr('string'),
  driverFirstName: attr('string'),
  vehicleId: attr('uuid'),
  routeId: attr('uuid'),
  hidden: attr('boolean', {defaultValue: false}),
  status: computed('', function() {
    let a = ['Received', 'Sent', 'Sending', 'Failed'];
    return a[Math.floor(Math.random()*a.length)];
  }),
});
