import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

const { Model } = DS;

export default Model.extend({
  dsRider: belongsTo('rider', { inverse: 'iqRider' }),
  rmsRider: belongsTo('rms-rider', { inverse: 'iqRider' }),
  bsRider: belongsTo('bs-rider', { inverse: 'iqRider' })
});
