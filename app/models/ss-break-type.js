import DS from 'ember-data';
import { hasMany } from 'ember-data/relationships';

const { Model, attr } = DS;

export default Model.extend({
  name: attr('string'),
  defaultDuration: attr('number'),
  isPaid: attr('boolean'),
  isRecognized: attr('boolean'),

  shiftBreaks: hasMany('ss-shift-break')
});
