import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  // these determine the `id` when persisted
  category: attr('string'),
  name: attr('string'),

  description: attr('string'),
  unit: attr('string'),
  type: attr('string'),

  value: attr('object'),
  defaultValue: attr('object')
});
