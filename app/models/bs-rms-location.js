import DS from 'ember-data';

const { Model, attr, hasMany } = DS;

export default Model.extend({
    lat: attr('number'),
    lng: attr('number'),
    geoNode: attr('number'),
    addresses: hasMany('bs-address')
});
