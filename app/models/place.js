import { collect } from '@ember/object/computed';
import { computed } from '@ember/object';
import DS from 'ember-data';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    type: attr('string'),
    lat: attr('number'),
    lng: attr('number'),
    geonode: attr('number'),
    streetNumber: attr('string'),
    streetAddress: attr('string'),
    locality: attr('string'),
    city: attr('string'),
    subRegion: attr('string'),
    region: attr('string'),
    postOfficeNumber: attr('string'),
    postalCode: attr('string'),
    country: attr('string'),
    name: attr('string'),
    notes: attr('string'),
    schedule: belongsTo(),
    location: collect('lat', 'lng'),

    // computed position is needed because of dealing with tomtom address format
    // and other format model from booking service, rider management,...
    position: computed('lat', 'lng', function() {
      return {
        lat: this.get('lat'),
        lng: this.get('lng'),
        lon: this.get('lng')
      };
    }),

    address: computed('streetNumber', 'streetAddress', 'locality', function() {
      let address = null;

      if (this.get('streetAddress') !== undefined) {
        address = {
          id: this.get('id'),
          type: this.get('type'),
          freeformAddress: `${this.get('streetNumber')} ${this.get('streetAddress')} ${this.get('locality') }`
        };
      }

      return address;
    }),

    tomtomFormattedAddress: computed('streetNumber', 'streetAddress', 'locality', function() {
      let address = null;

      if (this.get('streetAddress') !== undefined) {
        address = {
          address: {
             freeformAddress: `${this.get('streetNumber')} ${this.get('streetAddress')} ${this.get('locality') }`
          }
        };
      }

      return address;
    }),

    // FIXME: Remove. Here for demoing addresses field type.
    route: belongsTo()
});
