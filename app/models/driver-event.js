import { collect } from '@ember/object/computed';
import DS from 'ember-data';
const { Model, attr, belongsTo } = DS;

export default Model.extend({
    type: attr('string'),
    timestamp: attr('date'),
    lat: attr('number'),
    lng: attr('number'),
    heading: attr('number'),
    speed: attr('number'),
    odometer: attr('number'),
    driverShift: belongsTo(),
    location: collect('lat', 'lng')
});
