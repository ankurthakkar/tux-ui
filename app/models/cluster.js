import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
  belongsTo,
  hasMany
} = DS;

export default Model.extend({
  distance: attr('number'),
  endTime: attr('date'),
  headCount: attr('number'),
  ordinal: attr('number'),
  startTime: attr('date'),
  state: attr('string'),

  route: belongsTo(),
  schedule: belongsTo(),
  trips: hasMany(),

  orderedStops: computed('trips.@each.ordinalStops', function() {
    let ordinalStops = this.get('trips').reduce((arr, trip) => {
      let stops = trip.get('ordinalStops');
      arr.push(...stops);
      return arr;
    }, []);

    return ordinalStops.sortBy('ordinal');
  })
});
