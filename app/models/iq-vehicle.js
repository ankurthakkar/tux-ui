import Model from 'ember-data/model';
import { belongsTo, hasMany } from 'ember-data/relationships';
import attr from 'ember-data/attr';
import OTPLabelMixin from '../mixins/otp-label';
import { computed } from '@ember/object';

export default Model.extend(OTPLabelMixin, {
  otp: attr('string', { defaultValue: 'O' }),

  dsVehicle: belongsTo('vehicle', { inverse: 'iqVehicle' }),
  ssVehicle: belongsTo('ss-vehicle', { inverse: 'iqVehicle' }),

  avlLocations: hasMany(),

  lat: computed.alias('currentAVLLocation.lat'),
  lng: computed.alias('currentAVLLocation.lng'),

  currentAVLLocation: computed.alias('sortedAVLLocations.lastObject'),
  sortedAVLLocations: computed('avlLocations.[]', function() {
    return this.get('avlLocations').sortBy('timestamp');
  }),

  point: computed.collect('lat', 'lng')
});
