import DS from 'ember-data';

const { Model, attr } = DS;

export default Model.extend({
  code: attr('string'),
  externalCode: attr('string'),
  description: attr('string')
});
