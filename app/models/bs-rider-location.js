import DS from 'ember-data';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    locationType: attr('string'),
    location: belongsTo('bs-location')
});
