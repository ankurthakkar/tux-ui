import DS from 'ember-data';
const { Model, belongsTo,  attr } = DS;

export default Model.extend({

    state: attr('string'),
    vehicle: belongsTo('ss-vehicle'),
    address: belongsTo('ss-location'),
    promisedStart: attr('date'),
    plannedDuration: attr('number'),
    notes: attr('string'),
    breakType: belongsTo('ss-break-type'),
    startTime: attr('date'),
    endTime: attr('date'),
    addresses: DS.attr('array' , { defaultValue: function() { return []; } })


});
