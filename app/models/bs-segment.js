import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';

export default Model.extend({
    promiseTime: attr('date'),
    anchor: attr('string'),
    fare: attr('number'),
    travelMode: attr('string'),

    pick: belongsTo('bs-stop'),
    drop: belongsTo('bs-stop'),
    fareType: belongsTo('bs-fare-type'),
    leg: belongsTo('bs-leg'),

    stops: computed.collect('pick', 'drop')
});
