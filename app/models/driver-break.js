import { computed } from '@ember/object';
import DS from 'ember-data';
const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
   driverBreakId :attr('number'),
    externalId: attr('string'),
    state: attr('string'),
    ordinal: attr('number'),
    promisedStart: attr('date'),
    estimatedStart: attr('date'),
    estimatedEnd: attr('date'),
    plannedDuration: attr('number'),
    notes: attr('string', {defaultValue: ''}),
    driverBreakEvents: hasMany(),
    breakType: belongsTo(),
    place: belongsTo(),
    route: belongsTo(),
    schedule: belongsTo(),
    actualArrivalTime: attr('date'),
    actualDepartTime: attr('date'),
    odometer: attr('number'),
    plannedDepartTime: attr('date'),
    serviceWindow: attr('string'),
    breakStartTime: attr('date'),
    breakEndTime: attr('date'),
    BreakType: attr('string'),
    BreakCategory: attr('string'),
    eventtype: 'driver-break',
    promisedEnd: attr('date'),

    places: computed('place', function() {
      return [this.get('place')];
    })
});


