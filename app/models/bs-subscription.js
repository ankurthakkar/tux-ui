import DS from 'ember-data';
import { computed } from '@ember/object';
import { hasMany } from 'ember-data/relationships';
import lodash from 'lodash';

const { sample } = lodash;

const {
  Model,
  attr,
  belongsTo
} = DS;
function formatTextExtension(className, value) {
  return {
    className: className,
    value: value
  };
}

export default Model.extend({
  requestTime: attr('date'),
  anchor: attr('string'),
  purpose: attr('string'),
  origin: belongsTo('bs-location'),
  destination: belongsTo('bs-location'),
  fare: attr('number'),
  originNotes: attr('string'),
  destinationNotes: attr('string'),
  startDate:  attr('date'),
  endDate:  attr('date'),
  maximumOccurrences: attr('number'),
  remainingOccurrences: 0,
  rider: belongsTo('bs-rider'),

  subscriptionTravelNeeds: hasMany('bs-subscription-travel-need'),
  fareType: belongsTo('bs-fare-type'),
  exclusions: hasMany('bs-subscription-exclusion'),
  recurrencePatterns: hasMany('bs-subscription-recurrence-pattern'),
  status: computed('', function() {
    return sample(['Paused', 'Active']);
  }),
  origins: computed('origin', function() {
    return [this.get('origin.content')];
  }),

  destinations: computed('destination', function() {
    return [this.get('destination.content')];
  }),

  equipments: computed.filterBy('subscriptionTravelNeeds', 'isEquipment', true),
  extraPassengers: computed.filterBy('subscriptionTravelNeeds', 'isExtraPassenger', true),

  formattedRecurrence: computed('recurrencePatterns', function() {
    let formattedObject = {
      header: null,
      values: []
    };
    let recurrencePatterns = this.get('recurrencePatterns.firstObject');
    const selectedDOWArray = [];

    if (recurrencePatterns.get('sunday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Monday`))}
    if (recurrencePatterns.get('monday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Tuesday`))}
    if (recurrencePatterns.get('tuesday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Wednesday`))}
    if (recurrencePatterns.get('wednesday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Thursday`))}
    if (recurrencePatterns.get('thursday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Friday`))}
    if (recurrencePatterns.get('friday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Saturday`))}
    if (recurrencePatterns.get('saturday')) {selectedDOWArray.push(formatTextExtension('valueItem', `Sunday`))}

    formattedObject.values = selectedDOWArray;

    return formattedObject;
  })
});
