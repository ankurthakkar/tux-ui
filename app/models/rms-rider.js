import DS from 'ember-data';
import { get, computed } from '@ember/object';
import { belongsTo } from 'ember-data/relationships';

const {
  Model,
  attr,
  hasMany
} = DS;

function formatTextExtension(className, value) {
  return {
    className: className,
    value: value
  };
}

export default Model.extend({
  riderId: attr('string'),
  firstName: attr('string'),
  middleName: attr('string'),
  lastName: attr('string'),
  areaCode: attr('string', { defaultValue: '' }),
  phoneNumber: attr('string', { defaultValue: '' }),
  extension: attr('string', { defaultValue: '' }),
  dateOfBirth: attr('date'),
  loadTime: attr('number'),
  unloadTime: attr('number'),
  notes: attr('string'),

  primaryAddresses: hasMany('rms-address', { inverse: 'ridersPrimary' }),
  favoriteAddresses: hasMany('rms-address', { inverse: 'ridersFavorite' }),
  travelNeeds: hasMany('rms-rider-travel-need'),
  eligibilities: hasMany('rms-rider-eligibilities'),

  iqRider: belongsTo(),

  equipments: computed.filterBy('travelNeeds', 'isEquipment', true),
  extraPassengers: computed.filterBy('travelNeeds', 'isExtraPassenger', true),

  fullPhoneNumber: computed('areaCode', 'phoneNumber', 'extension', {

    get(/* key */) {
      const re = /\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})/g;
      const subst = '$1 $2-$3';
      const str = `${get(this, 'areaCode')} ${get(this, 'phoneNumber')}`;


      return str.replace(re, subst);
    },
    set(_, value) {
      value = value.trim();
      this.set('areaCode', value.substr(0, 3));
      this.set('phoneNumber',  value.substr(4, 10).replace(/-/g, ""));
      return value;
    }
  }),

  formattedPrimaryAddresses: computed('primaryAddresses', function() {
    let addresses = [];

    this.get('primaryAddresses').forEach(function(item) {
      addresses.push(formatTextExtension('valueItem', `${item.get('streetNumber')} ${item.get('streetAddress')} ${item.get('locality') }`));
    });

    return {values: addresses};
  }),

  formattedFavoriteAddresses: computed('favoriteAddresses', function() {
    let addresses = [];

    this.get('favoriteAddresses').forEach(function(item) {
      addresses.push(formatTextExtension('valueItem', `${item.get('streetNumber')} ${item.get('streetAddress')} ${item.get('locality') }`));
    });

    return {values: addresses};
  }),
  riderAddresses : computed('primaryAddresses','favoriteAddresses', function() {
    let addresses = [];

    this.get('primaryAddresses').forEach(function(item) {
      addresses.push(item);
    });
    this.get('favoriteAddresses').forEach(function(item) {
      addresses.push(item);
    });

    return addresses;
  }),

  formattedTravelNeeds: computed('travelNeeds', function() {
    let travelNeeds = [];

    this.get('equipments').forEach(function(item) {
      travelNeeds.push(formatTextExtension('valueItem', `${item.get('count')} ${item.get('travelNeedType.name')}`));
    });

    this.get('extraPassengers').forEach(function(item) {
      travelNeeds.push(formatTextExtension('valueItem', `${item.get('count')} ${item.get('passengerType.name')}`));
    });

    return {values: travelNeeds};
  }),

  formattedEligibilities: computed('eligibilities', function() {
    let eligibilities = [];

    this.get('eligibilities').forEach(function(item) {
      eligibilities.push(formatTextExtension('valueItem', `${item.get('eligibilityType.name')}`));
    });

    return {values: eligibilities};
  })
});

