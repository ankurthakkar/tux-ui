import Inflector from 'ember-inflector';

Inflector.inflector.uncountable('bs-booking');
Inflector.inflector.uncountable('bs-rider');
Inflector.inflector.uncountable('bs-travel-need-type');
Inflector.inflector.uncountable('bs-travelNeedType');
Inflector.inflector.uncountable('bs-stop');
Inflector.inflector.uncountable('bs-location');
Inflector.inflector.uncountable('bs-leg');
Inflector.inflector.uncountable('bs-segment');
Inflector.inflector.uncountable('bs-leg-travel-need');

export default {};
