import DS from 'ember-data';
import { computed } from '@ember/object';
import { get } from '@ember/object';
import { isEmpty } from '@ember/utils';
import _ from 'lodash';

const { Model, attr, hasMany } = DS;


export default Model.extend({
    name: attr('string'),
    vehicleCapacityConfigs: hasMany('ss-vehicle-capacity-config'),

    noOfAmbulatorySeats: computed('vehicleCapacityConfigs', function() {

      const vehicleCapacityConfigs = get(this, 'vehicleCapacityConfigs');

      if (!isEmpty(vehicleCapacityConfigs)) {


          var ambulatoryConfig = _.find(vehicleCapacityConfigs.toArray(), (capacityConfig) => {
            // TODO: Ankur -  Hardcoded will be remove to some configuration
            return get(capacityConfig, 'vehicleCapacityType.name') === 'Ambulatory';
          });
        return get(ambulatoryConfig,'count');
      }

      return null;
    }),
    noOfWheelChairSeats: computed('vehicleCapacityConfigs', function() {

      const vehicleCapacityConfigs = get(this, 'vehicleCapacityConfigs');

      if (!isEmpty(vehicleCapacityConfigs)) {


          var wheelChairConfig = _.find(vehicleCapacityConfigs.toArray(), (capacityConfig) => {
            // TODO: Ankur -  Hardcoded will be remove to some configuration
            return get(capacityConfig, 'vehicleCapacityType.name') === 'Wheelchair';
          });
        return get(wheelChairConfig,'count');
      }

      return null;
  }),
});
