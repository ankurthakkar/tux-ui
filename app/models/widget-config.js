import DS from 'ember-data';
import _ from 'lodash';

const {
  Model,
  attr
} = DS;

export default Model.extend({
  componentName: attr('string'),
  title: attr('string'),
  width: attr('number'),
  minWidth: attr('number'),
  height: attr('number'),
  minHeight: attr('number'),
  x: attr('number'),
  y: attr('number'),
  options: attr('object', { defaultValue: () => {} }),
  rowComponent: attr('string'),
  hideWidgetControls: attr('boolean'),

  mergeOptions(changes={}) {
    let options = this.get('options');
    let mergedOptions = _.merge({}, options, changes);
    this.set('options', mergedOptions);
    this.notifyPropertyChange('options');
  }
});
