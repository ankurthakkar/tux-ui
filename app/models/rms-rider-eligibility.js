import DS from 'ember-data';

const {
  Model,
  attr,
  belongsTo
} = DS;

export default Model.extend({
  from: attr('date', {
    defaultValue() { return new Date(); }
  }),
  to: attr('date', {
    defaultValue() { return new Date(); }
  }),

  rider: belongsTo('rms-rider'),
  eligibilityType: belongsTo('rms-eligibility-type')
});
