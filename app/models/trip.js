import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { computed, get } from '@ember/object';
import { isEmpty, isBlank, isPresent } from '@ember/utils';
import moment from 'moment';
import { sample } from 'lodash';

export default Model.extend({
  anchor: attr('string'),
  externalId: attr('string'),
  fare: attr('number'),
  notes: attr('string'),
  promisedTime: attr('date'),
  requestedTime: attr('date'),
  timestamp: attr('date', { defaultValue: () => new Date() }),
  status: attr('string'),
  statusNote: attr('string'),
  statusReason: attr('string'),
  cancelNotes: attr('string'),
  noShowNotes: attr('string'),
  otp: attr('string', { defaultValue: 'O' }),
  otpValue: attr('number'),

  rider: belongsTo(),
  serviceWindow: belongsTo(),
  cluster: belongsTo(),
  fareType: belongsTo(),
  cancelReason: belongsTo('cancel-type'),
  noShowReason: belongsTo('no-show-reason-code'),
  schedule: belongsTo(),

  iqTrip: belongsTo({ inverse: 'dsTrip' }),

  stops: hasMany('stop'),

  ordinalStops: computed('stops.@each.ordinal', function() {
    return this.get('stops').filter((stop) => isPresent(stop.get('ordinal')));
  }),

  // FIXME: add dependent keys
  plannedTravelNeeds: computed('', function() {
    const travelNeeds = get(this, 'rider.travelNeeds');
    let needTypes = travelNeeds.map((need) => get(need, 'travelNeedType.name'));

    let mockedNeedTypes = needTypes.map((need) => {
      if (isBlank(need)) {
        return sample(["ambulatory", "wideWheelchair", "wheelchair", "wideAmbulatory", "serviceAnimal"]);
      }
      return need;
    });

    return mockedNeedTypes;
  }),

  noOfTravelNeeds: computed.alias('plannedTravelNeeds.length'),

  pick: computed('stops.@each.type', function () {
    return this.get('stops').findBy('type', 'pick');
  }),

  drop: computed('stops.@each.type', function () {
    return this.get('stops').findBy('type', 'drop');
  }),

  anchorStop: computed('anchor', 'drop.content', function() {
    if (this.get('drop.content')) return this.get('drop');
    return this.get('pick');
  }),

  serviceWindowStartTime: computed('serviceWindow', function() {
    const serviceWindow = this.get('serviceWindow');
    const earliestPickTime = moment(this.get('promisedTime'));

    if (!isEmpty(serviceWindow)) {
      // if the service window information is available, the earliest pick time is the start of the promise window
      let promiseWindowStart = this.get('serviceWindow.promiseWindowStart');
      earliestPickTime.subtract(promiseWindowStart, 'm');
    }

    return earliestPickTime;
  }),

  serviceWindowEndTime: computed('serviceWindow', function() {
    const serviceWindow = this.get('serviceWindow');
    const earliestDropTime = moment(this.get('requestedTime'));

    if (serviceWindow && earliestDropTime !== null) {
      // if the service window information is available, the earliest drop time is the start of the promise window

      let dropWindowStart = this.get('serviceWindow.dropWindowStart');
      earliestDropTime.subtract(dropWindowStart, 'm');
    }

    return earliestDropTime;
  }),

  // TODO: update when the API provides data, or when a formula is provided.
  type: computed('', function() {
    return sample(['subscription', 'single']);
  })
});
