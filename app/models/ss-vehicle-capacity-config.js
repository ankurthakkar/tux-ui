import { computed, get } from '@ember/object';
import DS from 'ember-data';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    group: attr('number'),
    count: attr('number'),
    vehicleType: belongsTo('ss-vehicle-type'),
    vehicleCapacityType: belongsTo('ss-vehicle-capacity-type'),
    vehicleCapacityTypeName: computed('vehicleCapacityType', function() {
        return get(this, 'vehicleCapacityType.name');
    })
});
