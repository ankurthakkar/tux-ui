import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';

export default Model.extend({
  type: attr('string'),
  notes: attr('string'),

  location: belongsTo('bs-location'),

  // Ember-Data needs explicit inverses
  pickBooking: belongsTo('bs-booking', { inverse: 'pick' }),
  dropBooking: belongsTo('bs-booking', { inverse: 'drop' }),

  // use this to get the booking (e.g. from stops widget)
  booking: computed('pickBooking.content', 'dropBooking.content', function() {
    return this.get('pickBooking.content') || this.get('dropBooking.content');
  }),

  onboardCount: attr('number'),

  // old way, using bs API
  segment: belongsTo('bs-segment', { inverse: 'pick' }),
  segmentDrop: belongsTo('bs-segment', { inverse: 'drop' }),

  iqStop: belongsTo()
});
