import { computed } from '@ember/object';
import DS from 'ember-data';

const { Model, attr } = DS;

export default Model.extend({
  alias: attr('string'),
    streetNumber: attr('string'),
    streetName: attr('string'),
    room: attr('string'),
    city: attr('string'),
    county: attr('string'),
    state: attr('string'),
    postalCode: attr('string'),
    country: attr('string'),
    countryAbbreviation:attr('string'),
    geoNode: attr('number'),
    type: attr('string'),
    stateAbbreviation:  attr('string'),
    lat: attr('number'),
    lng: attr('number'),

    latlng: computed('lat', 'lng', function() {
      if (this.get('lat') !== undefined && this.get('lng') !== undefined) {
        return `${this.get('lat')}/${this.get('lng')}`;
      }
      return '';
    }),
    tomtomFormattedAddress: computed('streetNumber', 'streetName',
   'locality', function() {
    let address = null;

    if (this.get('streetName') !== undefined) {
      address = {
        address: {
          freeformAddress: `${this.get('streetNumber')} ${this.get('streetName')} ${this.get('city') }`
        }
      };
    }

    return address;
  }),
  address: computed('alias','streetNumber', 'streetName',
   'locality', function() {
    let address = null;

    if (this.get('streetName') !== undefined) {
      address = `${this.get('alias')} ${this.get('streetNumber')} ${this.get('streetName')} ${this.get('locality') }`
    }

    return address;
  })
});

