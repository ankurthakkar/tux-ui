import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
  hasMany
} = DS;

export default Model.extend({
  alias: attr('string', { defaultValue: '' }),
  streetNumber: attr('string'),
  streetAddress: attr('string'),
  locality: attr('string', { defaultValue: '' }),
  subRegion: attr('string', { defaultValue: '' }),
  region: attr('string', { defaultValue: '' }),
  postOfficeBoxNumber: attr('string', { defaultValue: '' }),
  postalCode: attr('string', { defaultValue: '' }),
  country: attr('string', { defaultValue: '' }),
  notes: attr('string', { defaultValue: '' }),
  type: attr('string', { defaultValue: 'passenger' }),

  ridersPrimary: hasMany('rms-rider'),
  ridersFavorite: hasMany('rms-rider'),
  locations: hasMany('rms-location'),

  // needed to syncronize attribute name with tomtom
  countrySecondarySubdivision: computed('region', function() {
    return this.get('region');
  }),

  // computed position is needed because of dealing with tomtom address format
  // and other format model from booking service, rider management,...
  position: computed('locations', function() {
    let firstLocation = this.get('locations.firstObject');

    return {
      lat: firstLocation.get('lat'),
      lng: firstLocation.get('lng'),
      lon: firstLocation.get('lng')
    };
  }),

  tomtomFormattedAddress: computed('streetNumber', 'streetAddress',
   'locality', function() {
    let address = null;

    if (this.get('streetAddress') !== undefined) {
      address = {
        address: {
          freeformAddress: `${this.get('streetNumber')} ${this.get('streetAddress')} ${this.get('locality') }`
        }
      };
    }
    return address;
  }),

  address: computed('alias','streetNumber', 'streetAddress',
   'locality', function() {
    let address = null;

    if (this.get('streetAddress') !== undefined) {
      address = {
          freeformAddress: `${this.get('alias')} ${this.get('streetNumber')} ${this.get('streetAddress')} ${this.get('locality') }`
      };
    }

    return address;
  })
});
