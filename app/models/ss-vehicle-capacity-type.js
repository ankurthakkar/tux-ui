import DS from 'ember-data';

const { Model, attr } = DS;

export default Model.extend({
    name: attr('string'),
    description: attr('string'),
    engineId: attr('string'),
    engineName: attr('string'),
    constrainedByLIFO: attr('boolean'),
    dwellFactor: attr('number')
});
