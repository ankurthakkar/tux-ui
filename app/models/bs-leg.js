import { computed, get } from '@ember/object';
import DS from 'ember-data';
import { isEmpty } from '@ember/utils';
const { Model, attr, belongsTo, hasMany } = DS;

const LegModel = Model.extend({
    requestTime: attr('date'),
    anchor: attr('string'),
    purpose: attr('string'),

    address: attr('nested'),
    coordinates: attr('nested'),
    pointOfInterest: attr('nested'),
    geoNode: attr('number'),

    stop: belongsTo('stop'),

    booking: belongsTo('bs-booking'),
    origin: belongsTo('bs-location',{async:false}),
    destination: belongsTo('bs-location',{async:false}),
    rider: belongsTo('bs-rider',{async:false}),
    locations :  hasMany('bs-location'),


    extraPassengers: computed('', function() {
      const rider = get(this, 'rider');

      return rider.get('extraPassengers');
    }),
    equipments: computed('', function() {
      const rider = get(this, 'rider');

      return rider.get('equipments');
    }),
   // originlocations :  hasMany('bs-location'),
   // destinationlocations :  hasMany('bs-location'),

    segments: hasMany('bs-segment'),
    segment: computed('segments', function() {
      if(get(this, 'segments') != null) {
        const segments = get(this, 'segments').toArray();
        return segments[0];
      } else {
        return null;
      }

    }),
    legTravelNeeds: hasMany('bs-leg-travel-need'),

    noOfTravelNeeds: computed('', function() {
      const noOfTravelNeeds = get(this, 'legTravelNeeds');

      if (!isEmpty(noOfTravelNeeds)) {
          return noOfTravelNeeds.length;
      }
      return 0;
    }),

    sortedLegTravelNeeds: computed('legTravelNeeds', function() {
        return get(this, 'legTravelNeeds').sortBy('travelNeedType.name');
    }),

   /* originlocations1: computed('origin', function() {
        let originlocations =[];
        originlocations.push(get(this, 'origin'));
      return originlocations;
    }),

    destinationlocations1: computed('destination', function() {
        let destinationlocations =[];
        destinationlocations.push(get(this, 'destination'));
      return destinationlocations;
    }),*/

  tomtomFormattedAddress: computed('streetNumber', 'streetAddress',
   'locality', function() {
    let address = null;

    if (this.get('streetAddress') !== undefined) {
      address = {
        address: {
          freeformAddress: `${this.get('streetNumber')} ${this.get('streetAddress')} ${this.get('city') }`
        }
      };
    }

    return address;
  })
});

export default LegModel;
