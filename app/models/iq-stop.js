import Model from 'ember-data/model';
import { belongsTo } from 'ember-data/relationships';
import { computed } from '@ember/object';
import OTPLabelMixin from '../mixins/otp-label';

export default Model.extend(OTPLabelMixin, {
  dsStop: belongsTo('stop'),
  bsStop: belongsTo(),

  lat: computed.alias('bsStop.location.lat'),
  lng: computed.alias('bsStop.location.lng'),

  otp: computed.alias('dsStop.otp'),
  otpValue: computed.alias('dsStop.otpValue'),
  status: computed.alias('dsStop.status')
});
