import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr,
  belongsTo
} = DS;

const WOM_OPTIONS = {
  first: 'first',
  second: 'second',
  third: 'Third',
  fourth: 'fourth',
  last: 'last'
};

export default Model.extend({

  type: attr('string'),
  separationCount: attr('number'),
  sunday: attr('boolean'),
  monday: attr('boolean'),
  tuesday: attr('boolean'),
  wednesday: attr('boolean'),
  thursday: attr('boolean'),
  friday: attr('boolean'),
  saturday: attr('boolean'),
  dayOfMonth: attr('number',  { defaultValue: null }),
  weekOfMonth: attr('number',  { defaultValue: null }),
  monthOfYear: attr('number',  { defaultValue: null }),
  subscription: belongsTo('bs-subscription'),

  everyweekday: attr('string'),
  pattern  : attr('string'),
  recurring: attr('boolean',  { defaultValue: true }),

  selectedDOWs: computed('sunday','monday','tuesday','wednesday','thursday','friday', 'saturday', function() {
    const selectedDOWArray = [];

    if (this.get('sunday')) selectedDOWArray.push('sunday');
    if (this.get('monday')) selectedDOWArray.push('monday');
    if (this.get('tuesday')) selectedDOWArray.push('tuesday');
    if (this.get('wednesday')) selectedDOWArray.push('wednesday');
    if (this.get('thursday')) selectedDOWArray.push('thursday');
    if (this.get('friday')) selectedDOWArray.push('friday');
    if (this.get('saturday')) selectedDOWArray.push('saturday');

    return selectedDOWArray;
  }),

  isEveryDay: computed('sunday','monday','tuesday','wednesday','thursday','friday', 'saturday', function() {
    return this.get('sunday') && this.get('monday') && this.get('tuesday') &&
      this.get('wednesday') && this.get('thursday') && this.get('friday') &&
      this.get('saturday');
  }),

  selectedWOM: computed('weekOfMonth', {

    get(/* key */) {
      const weekOfMonth = this.get('weekOfMonth');

      switch(weekOfMonth) {
        case -1:
          return WOM_OPTIONS.last;
        case 1:
          return WOM_OPTIONS.first;
        case 2:
          return WOM_OPTIONS.second;
        case 3:
          return WOM_OPTIONS.third;
        case 4:
          return WOM_OPTIONS.fourth;
        default:
          return  null;
      }
    },
    set(_, value) {
      let weekOfMonth = null;

      switch(value) {
        case WOM_OPTIONS.last:
          weekOfMonth = -1;
          break;
        case WOM_OPTIONS.first:
          weekOfMonth = 1;
          break;
        case WOM_OPTIONS.second:
          weekOfMonth = 2;
          break;
        case WOM_OPTIONS.third:
          weekOfMonth = 3;
          break;
        case WOM_OPTIONS.fourth:
          weekOfMonth = 4;
          break;
        default:
          weekOfMonth = null;
      }

      this.set('weekOfMonth', weekOfMonth);
      return value;
    }
  }),

  subscriptionStartDate: computed('subscription.startDate', {

    get(/* key */) {
      return this.get('subscription.startDate');
    },
    set(_, value) {
      this.set('subscription.startDate', value);
      return value;
    }
  }),

  subscriptionEndDate: computed('subscription.endDate', {

    get(/* key */) {
      return this.get('subscription.endDate');
    },
    set(_, value) {
      this.set('subscription.endDate', value);
      return value;
    }
  }),

  subscriptionMaxOccurences: computed('subscription.maximumOccurrences', {

    get(/* key */) {
      return this.get('subscription.maximumOccurrences');
    },
    set(_, value) {
      this.set('subscription.maximumOccurrences', value);
      return value;
    }
  }),

  exclusionStartDate: computed('subscription.exclusions.firstObject.startDate', {

    get(/* key */) {
      return this.get('subscription.exclusions.firstObject.startDate');
    },
    set(_, value) {
      this.set('subscription.exclusions.firstObject.startDate', value);
      return value;
    }
  }),

  exclusionEndDate: computed('subscription.exclusions.firstObject.endDate', {

    get(/* key */) {
      return this.get('subscription.exclusions.firstObject.endDate');
    },
    set(_, value) {
      this.set('subscription.exclusions.firstObject.endDate', value);
      return value;
    }
  })


});
