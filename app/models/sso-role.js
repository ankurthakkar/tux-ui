import DS from 'ember-data';

const {
  Model,
  attr
} = DS;

export default Model.extend({
    roleName: attr('string'),
    displayName: attr('string'),
    status: attr('string')
});
