import { computed, get } from '@ember/object';
import DS from 'ember-data';
import moment from 'moment';

const { Model, attr, belongsTo } = DS;

export default Model.extend({
    from: attr('string'),
    to: attr('string'),
    rider: belongsTo('bs-rider'),
    eligibilityType: belongsTo('rms-eligibility-type'),

    formattedFrom: computed('from', function() {
        const momentFrom = moment(get(this, 'from'));

        return momentFrom.format('LL');
    }),

    formattedTo: computed('to', function() {
        const momentFrom = moment(get(this, 'to'));

        return momentFrom.format('LL');
    })
});
