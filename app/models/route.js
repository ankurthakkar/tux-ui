import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { computed, get } from '@ember/object';
import { isEmpty, isPresent } from '@ember/utils';
import lodash from 'lodash';
import moment from 'moment';
import OTPLabelMixin from '../mixins/otp-label';

const { find } = lodash;

export default Model.extend(OTPLabelMixin, {
  name: attr('string'),
  plannedStartTime: attr('date'),
  plannedEndTime: attr('date'),
  lifoDepth: attr('number'),
  routeFactor: attr('number'),
  isBooleanExample: attr('boolean', { defaultValue: false }),
  enumExamples: attr('object', { defaultValue: () => ['One'] }),
  enumExample: attr('string', { defaultValue: 'One' }),
  routeDate: attr('date'),
  vehicleName: attr('string'),
  selectedVehicle: attr(),
  selectedDriver: attr(),
  driverExternalId: attr('string'),
  odometer: attr('string'),
  otp: attr('string', { defaultValue: 'O' }),
  otpValue: attr('number'),
  timestamp:  attr('date', { defaultValue: () => new Date() }),

  pulloutPolyline: attr('polyline'),
  stopPolylines: attr('object'),

  navigationPolyline: attr('polyline'),

  fullPolyline: computed('stopPolylines', 'orderedStops', 'counter', function() {
    let stopPolylines = this.get('stopPolylines');
    let stops = this.get('orderedStops');
    if (isEmpty(stops)) return [];

    return stops.reduce((arr, stop) => {
      let obj = stopPolylines.find((obj) => {
        return obj.stopId.toString() === stop.get('id').toString();
      });

      if (obj && obj.polyline) {
        arr.push(...obj.polyline);
      }

      return arr;
    }, []);
  }),

  orderedStops: computed('orderedClusters.@each.orderedStops', function() {
    return this.get('orderedClusters').reduce((arr, cluster) => {
      let stops = cluster.get('orderedStops');
      arr.push(...stops);
      return arr;
    }, []);
  }),

  orderedClusters: computed('clusters.@each.ordinal', function() {
    return this.get('clusters').sortBy('ordinal');
  }),

  status: attr('string'),

  provider: belongsTo(),
  schedule: belongsTo(),
  assignRoute: belongsTo('route'),

  clusters: hasMany(),
  vehicleRoutes: hasMany(),
  driverBreaks: hasMany(),
  places: hasMany(),
  routePlaces: hasMany(),

  isCompleted: computed.equal('status', 'completed'),
  isEmptyRoute: computed.empty('clusters.firstObject.trips'),

  isVehicleAvailable(vehicle) {
    let plannedStartTime = this.get('plannedStartTime');
    let plannedEndTime = this.get('plannedEndTime');
    let routeDate = this.get('routeDate');

    plannedStartTime.setDate(routeDate.getDate());
    plannedStartTime.setMonth(routeDate.getMonth());
    plannedStartTime.setFullYear(routeDate.getFullYear());
    plannedEndTime.setDate(routeDate.getDate());
    plannedEndTime.setMonth(routeDate.getMonth());
    plannedEndTime.setFullYear(routeDate.getFullYear());

    return vehicle.isVehicleAvailable(moment(plannedStartTime), moment(plannedEndTime));
  },

  isDriverAvailable(driver) {
    let plannedStartTime = this.get('plannedStartTime');
    let plannedEndTime = this.get('plannedEndTime');
    let routeDate = this.get('routeDate');

    plannedStartTime.setDate(routeDate.getDate());
    plannedStartTime.setMonth(routeDate.getMonth());
    plannedStartTime.setFullYear(routeDate.getFullYear());
    plannedEndTime.setDate(routeDate.getDate());
    plannedEndTime.setMonth(routeDate.getMonth());
    plannedEndTime.setFullYear(routeDate.getFullYear());

    return driver.isDriverAvailable(moment(plannedStartTime), moment(plannedEndTime));
  },

  garages: computed.mapBy('routePlaces', 'place'),

  assignedDriver: computed.alias('latestVehicleRoute.latestDriver'),
  assignedVehicle: computed.alias('latestVehicleRoute.vehicle'),
  latestVehicleRoute: computed('vehicleRoutes.@each.plannedStartTime', function() {
    return this.get('vehicleRoutes').sortBy('plannedStartTime').get('lastObject');
  }),

  // FIXME: set dependent keys so this updates in realtime
  actualDriverBreaks: computed('driverBreaks', function() {
    const driverBreaks = get(this, 'driverBreaks');
    let driverBreaksTaken = [];

    if (!isEmpty(driverBreaks)) {
      const driverBreakEvents = get(driverBreaks, 'driverBreakEvents');

      if(!isEmpty(driverBreakEvents)) {

        var completedDriverBreaks = find(driverBreakEvents.toArray(), (driverBreakEvent) => {
          // TODO: Ankur -  Hardcoded will be remove to some configuration
          return driverBreakEvent.type === 'end';
        });

        completedDriverBreaks.toArray().forEach(function(item) {
          driverBreaksTaken.push(`${item.get('estimatedStart')} ${item.get('estimatedEnd')} `);
        });

      }
    }
    return driverBreaksTaken;
  }),

  // FIXME: set dependent keys so this updates in realtime
  plannedDriverBreaks: computed('driverBreaks', function() {
    let driverBreaks = [];

    this.get('driverBreaks').forEach(function(item) {
      driverBreaks.push(`${item.get('estimatedStart')} ${item.get('estimatedEnd')} `);
    });
    return driverBreaks;
  }),

  // FIXME: set dependent keys so this updates in realtime
  actualStartEvent: computed('assignedVehicle', function() {
    const assignedVehicle = get(this, 'assignedVehicle');

    if (!isEmpty(assignedVehicle)) {
      const vehicleEvents = get(assignedVehicle, 'vehicleEvents');

      if (!isEmpty(vehicleEvents)) {

        const lastVehicleEvent =  vehicleEvents.sortBy('timestamp').objectAt(get(vehicleEvents, 'length') - 1);

        // TODO: Ankur -  Hardcoded will be remove to some configuration
        if(!isEmpty(lastVehicleEvent) && lastVehicleEvent.type === 'pullout') {
          return lastVehicleEvent;
        }
      }
    }
    return null;
  }),

  // FIXME: set dependent keys so this updates in realtime
  actualEndEvent: computed('assignedVehicle', function() {
    const assignedVehicle = get(this, 'assignedVehicle');

    if (!isEmpty(assignedVehicle)) {
      const vehicleEvents = get(assignedVehicle, 'vehicleEvents');

      if (!isEmpty(vehicleEvents)) {
        const lastVehicleEvent =  vehicleEvents.sortBy('timestamp').objectAt(get(vehicleEvents, 'length') - 1);

        //Ankur -  Hardcoded will be remove to some configuration
        if(!isEmpty(lastVehicleEvent) && lastVehicleEvent.type === 'pullin') {
          return lastVehicleEvent;
        }
      }
    }
    return null;
  })
});

