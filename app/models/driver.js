import DS from 'ember-data';
import { isEmpty } from '@ember/utils';
import { get, getProperties, computed } from '@ember/object';
import moment from 'moment';

const { Model, belongsTo, hasMany, attr } = DS;

export default Model.extend({
    externalId: attr('number'),
    firstName: attr('string'),
    middleName: attr('string'),
    lastName: attr('string'),
    travelTimeAdjustment: attr('number'),
    phoneNumber: attr('string'),
    schedule: belongsTo(),
    provider: belongsTo(),
    driverShifts: hasMany(),
    driverPreAssignments: hasMany(),

    iqDriver: belongsTo('iq-driver', { inverse: 'dsDriver' }),

    fullName: computed('firstName', 'middleName', 'lastName', function() {
        return `${get(this, 'firstName')} ${get(this, 'middleName') || ''} ${get(
            this,
            'lastName'
        )}`.replace(/\s+/, ' ');
    }),
    latestDriverEvent: computed('driverShifts', function() {
        const driverShifts = get(this, 'driverShifts');

        if (!isEmpty(driverShifts)) {
            const currentDS = get(this, 'currentShift');

            if (currentDS) {
                const driverEvents = get(currentDS, 'driverEvents');
                const lastDriverEvent = driverEvents
                    .sortBy('timestamp')
                    .objectAt(get(driverEvents, 'length') - 1);

                return lastDriverEvent;
            }
        }
        return null;
    }),
    currentVehicle: computed('driverShifts', function() {
        const driverShifts = get(this, 'driverShifts');

        if (!isEmpty(driverShifts)) {
            const currentDS = get(this, 'currentShift');

            if (currentDS) {
                const vehicleRoute = get(currentDS, 'vehicleRoute');

                return get(vehicleRoute, 'vehicle');
            }
        }
        return null;
    }),

    currentShift: computed('driverShifts', function() {
        const driverShifts = get(this, 'driverShifts');

        const now = moment();

        if (!isEmpty(driverShifts)) {
            const currentDS = driverShifts.find((ds) => {
                const { plannedStartTime, plannedEndTime } = getProperties(
                    ds,
                    'plannedStartTime',
                    'plannedEndTime'
                );

                return (
                    moment(plannedEndTime).isAfter(now) && moment(plannedStartTime).isBefore(now)
                );
            });

            if (!isEmpty(currentDS)) {
                return currentDS;
            }
        }
        return null;
    }),


    isDriverAvailable(startTime, endTime) {
      let retVal = true;
      const driverShifts = get(this, 'driverShifts');

      if (!isEmpty(driverShifts)) {
        driverShifts.forEach((driverShift) => {

          if (((startTime.isSameOrAfter(driverShift.get('plannedStartTime')) &&
            startTime.isSameOrBefore(driverShift.get('plannedEndTime'))) ||
            (endTime.isSameOrAfter(driverShift.get('plannedStartTime')) &&
            endTime.isSameOrBefore(driverShift.get('plannedEndTime'))))) {
            retVal = false;
          }

        });
      }

      return retVal;
    }
});
