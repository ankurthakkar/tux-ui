import DS from 'ember-data';

const { Model, attr } = DS;

export default Model.extend({
  name: attr('string'),
  defaultDuration: attr('number'),
  isPaid: attr('boolean'),
  isRecognized: attr('boolean')
});
