import DS from 'ember-data';

const {
  Model,
  attr,
  belongsTo
} = DS;

export default Model.extend({

  startDate: attr('date', {defaultValue: null}),
  endDate: attr('date', {defaultValue: null}),
  subscription: belongsTo('bs-subscription'),
});
