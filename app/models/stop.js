import DS from 'ember-data';
import { computed } from '@ember/object';
import lodash from 'lodash';

const { sample } = lodash;

const {
  Model,
  attr,
  belongsTo
} = DS;

export default Model.extend({
  distanceToNext: attr('number'),
  dwell: attr('number'),
  eta: attr('date'),
  noShowState: attr('string'),
  notes: attr('string'),
  onboardCount: attr('number'),
  ordinal: attr('number'),
  phone1: attr('string'),
  plannedTime: attr('date'),
  state: attr('string'),
  travelTimeToNext: attr('number'),//pickup ,drop, break ,pullinGarage, pulloutGarage
  type: attr('string'),
  odometer:attr('string'),
  schedule: belongsTo(),
  timestamp:  attr('date', {
    defaultValue() {return new Date()}
  }),
  status: attr('string'),
  otp: attr('string', { defaultValue: 'O' }),
  otpValue: attr('number'),

  // added for iq-identification
  externalId: attr('string'),

  iqStop: belongsTo(),


  trip: belongsTo({ inverse: 'stops' }),

  place: belongsTo(),
  actualArrivalTime: attr('date'),
  actualDepartTime: attr('date'),
  plannedDepartTime: attr('date'),
  serviceWindow: attr('string'),
  breakStartTime: attr('date'),
  breakEndTime: attr('date'),
  BreakType: attr('string'),
  BreakCategory: attr('string'),
  eventtype: 'stop',
  noShowReason : belongsTo('no-show-reason-code'),
  noShowNotes: attr('string')
});
