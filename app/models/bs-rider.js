import { computed, get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';
import DS from 'ember-data';
import NameParser from '../utils/name-parser';
import { belongsTo } from 'ember-data/relationships';

const { Model, attr, hasMany } = DS;

/*
        This is the rider model for data coming from rider-management and later updated with
        travel-needs. The rider-info model holds data to be sent to the booking service.
        This is necessary because a complex nesting of DS models and ember-data-fragments
        has problems serializing. See app/confirm/route.js.
 */
export default Model.extend({
  riderId: attr('string'),
  firstName: attr('string'),
  middleName: attr('string'),
  lastName: attr('string'),
  areaCode: attr('string'),
  phoneNumber: attr('string'),
  extension: attr('string'),
  notes: attr('string'),
  eligibilities: hasMany('rms-rider-eligibilities'),

  iqRider: belongsTo('iq-rider', { inverse: 'bsRider' }),
  bookings: hasMany('bs-booking'),

  equipments: computed.filterBy('travelNeeds', 'isEquipment', true),
  extraPassengers: computed.filterBy('travelNeeds', 'isExtraPassenger', true),
  travelNeeds: hasMany('rms-rider-travel-need'),

  primaryAddresses: hasMany('rms-address', {
      inverse: 'ridersPrimary'
  }),
  favoriteAddresses: hasMany('rms-address', {
      inverse: 'ridersFavorite'
  }),

  // FIXME: these don't seem to be used anywhere; if they are to be used,
  // they need to be refactored not to use a compound key. To do this,
  // alias `travelNeedType.name` to a flattened `travelNeedTypeName` property
  // on the `travelNeed model`, then use that as the `filterBy` key.

  // generalForClient: computed.filterBy(
  //     'travelNeedForClient',
  //     'travelNeedType.name',
  //     'serviceAnimal'
  // ),

  // generalForCompanion: computed.filterBy(
  //     'travelNeedForCompanion',
  //     'travelNeedType.name',
  //     'serviceAnimal'
  // ),

  fullName: computed('firstName', 'middleName', 'lastName', {
      get(/* key*/) {
          let middleName = get(this, 'middleName');

          if (isEmpty(middleName)) {
              middleName = ' ';
          } else {
              middleName = ` ${middleName} `;
          }
          return `${get(this, 'firstName')}${middleName}${get(this, 'lastName') || ''}`.trim();
      },
      set(key, value) {
          const nameParts = NameParser.parse(value);

          set(this, 'firstName', nameParts.firstName);
          set(this, 'middleName', nameParts.middleName);
          set(this, 'lastName', nameParts.lastName);

          return value;
      }
  }),


  phone: computed('', function() {
    let rider = this.get('iqRider.rmsRider');
    if (rider) {
      return rider.get('fullPhoneNumber');
    }
  })
});
