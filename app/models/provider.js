import DS from 'ember-data';

const {
  Model,
  attr,
  belongsTo,
  hasMany
} = DS;

export default Model.extend({
  name: attr('string'),

  providerType: belongsTo(),
  schedule: belongsTo(),
  routes: hasMany()
});
