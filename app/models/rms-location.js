import DS from 'ember-data';
import { computed } from '@ember/object';
const {
  Model,
  attr,
} = DS;

export default Model.extend({
  lat: attr('number'),
  lng: attr('number'),
  geoNode: attr('number',  { defaultValue: 0 }),

  latlng: computed('lat', 'lng', function() {
    return `${this.get('lat')}/${this.get('lng')}`;
  })
});
