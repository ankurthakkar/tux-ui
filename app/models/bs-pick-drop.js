import MF from 'ember-data-model-fragments';
import DS from 'ember-data';

const { attr } = DS;

export default MF.Fragment.extend({
    type: attr('string'),
    location: MF.fragment('bs-location')
});
