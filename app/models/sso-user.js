import DS from 'ember-data';
import { computed } from '@ember/object';

const {
  Model,
  attr
} = DS;

export default Model.extend({
    email: attr('string'),
    userId: attr('string'),
    displayName: attr('string'),
    status: attr('string', {defaultValue: 'active'}),
    lastLoginAt: attr('date'),

    UserRoleRolenames: attr({
      defaultValue() {
        return [];
      }
    }),

    calculatedStatus: computed('status', 'lastLoginAt', {
      get(/* key */) {
        if (this.get('status') === 'active' &&
          this.get('lastLoginAt') === null) {
          return 'pending';
        }

        return this.get('status');
      },

      set(key, value) {
        this.set('status', value);
        if (this.get('status') === 'active' &&
          this.get('lastLoginAt') === null) {
          return 'pending';
        }
        return value;
      }
    })
});

