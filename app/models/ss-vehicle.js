import { computed } from '@ember/object';
import DS from 'ember-data';
import { isEmpty } from '@ember/utils';
import { get } from '@ember/object';
import { find, sample } from 'lodash';
import moment from 'moment';
import ENV from '../config/environment';

const {
  Model,
  attr,
  hasMany,
  belongsTo
} = DS;

function formatTextExtension(className, value) {
  return {
    className: className,
    value: value
  };
}

export default Model.extend({
    name: attr('string'),
    active: attr('boolean'),
    vehicleType: belongsTo('ss-vehicle-type'),
    driver: belongsTo('ss-driver'),

    iqVehicle: belongsTo('iq-vehicle', { inverse: 'ssVehicle' }),

    //originlocations :  hasMany('bs-location'),
    //destinationlocations :  hasMany('bs-location'),

    location: function() {
      let a = [[46.097755,-118.2776357], [46.099091,-118.2841407], [46.093589, -118.2851997]];
      let loc = sample(a);
      return {lat: loc[0], lon: loc[1]};
    },

    startGarage: belongsTo('ss-location',{async:false}),
    endGarage: belongsTo('ss-location',{async:false}),

    startGarages :  hasMany('ss-location'),
    endGarages :  hasMany('ss-location'),
    availability: hasMany('ss-vehicle-availability'),
    shiftBreaks: hasMany('ss-shift-break'),


    noOfAmbulatorySeats: computed('vehicleType', function() {

      const vehicleCapacityConfigs = get(this, 'vehicleType.vehicleCapacityConfigs');

      if (!isEmpty(vehicleCapacityConfigs)) {


          var ambulatoryConfig = find(vehicleCapacityConfigs.toArray(), (capacityConfig) => {
            // TODO: Ankur -  Hardcoded will be remove to some configuration
            return get(capacityConfig, 'vehicleCapacityType.name') === 'Ambulatory';
          });
        return get(ambulatoryConfig,'count');
      }

      return null;
    }),
    noOfWheelChairSeats: computed('vehicleType', function() {

      const vehicleCapacityConfigs = get(this, 'vehicleType.vehicleCapacityConfigs');

      if (!isEmpty(vehicleCapacityConfigs)) {


          var wheelChairConfig = find(vehicleCapacityConfigs.toArray(), (capacityConfig) => {
            // TODO: Ankur -  Hardcoded will be remove to some configuration
            return get(capacityConfig, 'vehicleCapacityType.name') === 'Wheelchair';
          });
        return get(wheelChairConfig,'count');
      }

      return null;
  }),
  /*shiftBreaks: computed('availability', function() {



    let shiftBreaksavailabilities = [];

    this.get('availability').forEach(function(item) {
      shiftBreaksavailabilities.push(item.get('shiftBreaks'));
    });
    return shiftBreaksavailabilities;
}),*/
  formattedAvailabilities: computed('availability', 'active', function() {
    const active = this.get('active');
    let formattedObject = {
      header: null,
      values: []
    };
    let availabilities = [];

    this.get('availability').forEach(function(item) {
      const startTime = moment(item.get('startTime')),
            endTime = moment(item.get('endTime')),
            now = moment();

      if (active && now.isBetween(startTime, endTime)) {
        formattedObject.header = null;

        availabilities.push(formatTextExtension('mainItem', `${startTime.format(ENV.dateTimeFormat.dateMoment)} - ${endTime.format(ENV.dateTimeFormat.dateMoment)}`));
        if (item.monday) {availabilities.push(formatTextExtension('valueItem', `Monday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.tuesday) {availabilities.push(formatTextExtension('valueItem', `Tuesday  ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.wednesday) {availabilities.push(formatTextExtension('valueItem', `Wednesday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.thursday) {availabilities.push(formatTextExtension('valueItem', `Thursday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.friday) {availabilities.push(formatTextExtension('valueItem', `Friday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.saturday) {availabilities.push(formatTextExtension('valueItem', `Saturday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
        if (item.sunday) {availabilities.push(formatTextExtension('valueItem', `Sunday ${item.formattedShiftStart} -${item.formattedShiftEnd}`))}
      }

      formattedObject.values = availabilities;
    });

    return formattedObject;
  })
});
