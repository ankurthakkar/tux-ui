import Authorizer from 'ember-simple-auth/authorizers/base';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';

export default Authorizer.extend({
  session: service(),

  authorize(data, block) {
    if (isPresent(data.token)) {
      block('Authorization', `Bearer ${data.token}`);
    }
  }
});
