import ApplicationSerializer from './application'

export default ApplicationSerializer.extend({
  normalize(typeClass, hash) {
    let bsBookingId = hash.id;
    let dsTripId = hash.attributes.dispatchTripPKId;

    // fix topic payload discrepencies
    let { requestTime } = hash.attributes;
    delete hash.attributes.requestTime;
    hash.attributes.requestedTime = requestTime;

    if (hash.hasOwnProperty('promiseTime')) {
      let { promiseTime } = hash.attributes;
      delete hash.attributes.promiseTime;
      hash.attributes.promisedTime = promiseTime;
    }

    let normalized = this._super(...arguments);

    normalized.data.relationships.bsBooking = {
      data: {
        id: bsBookingId,
        type: 'bs-booking'
      }
    };

    if (dsTripId) {
      normalized.data.relationships.dsTrip = {
        data: {
          id: dsTripId.toString(),
          type: 'trip'
        }
      };
    }

    return normalized;
  }
});
