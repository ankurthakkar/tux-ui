import DispatchSerializer from './-dispatch';
import { typeOf } from '@ember/utils';

export default DispatchSerializer.extend({
  attrs: {
    driverbreakId: { serialize: false },
    actualArrivalTime: { serialize: false },
    actualDepartTime: { serialize: false },
    odometer: { serialize: false },
    plannedDepartTime: { serialize: false },
    serviceWindow: { serialize: false },
    breakStartTime: { serialize: false },
    breakEndTime: { serialize: false },
    BreakType: { serialize: false },
    BreakCategory: { serialize: false },
    driverBreakId: { serialize: false },
    promisedEnd: { serialize: false }
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    // this is because of inconsistensies in api
    // returns an array instead of an object after a POST
    if (requestType === 'createRecord' && typeOf(payload.data) === 'array') {
      if (payload.data.length === 1) {
        payload.data = payload.data[0];
      }
    }

    return this._super(...arguments);
  }
});
