import DispatchSerializer from './-dispatch';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default DispatchSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-trip',
  iqModelForeignKey: 'externalId',
  iqModelRelationship: 'iqTrip',

  attrs: {
    requestedTime: 'requestTime',
    promisedTime: 'promiseTime',
    status: 'tripStatus'
  }
});
