import SchedulingServiceSerializer from './-dispatch';

export default SchedulingServiceSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    return json;
  },

  normalize(modelClass, resourceHash) {
    let normalized = this._super(modelClass, resourceHash);


    return normalized;
  }


});
