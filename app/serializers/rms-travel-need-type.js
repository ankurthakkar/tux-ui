import RiderManagementSerializer from './-rms-riderManagement';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'travelNeedType';

    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
        data[i].type = 'rms-travel-need-type';
    }

    if (!isNone(payload.included) && !isNone(payload.included.length)) {
      for (let i = 0; i < payload.included.length; i++) {
        const include = payload.included[i];

        if (include.type === 'travel-need-type') {
          include.type = 'rms-travel-need-type';
        }
      }
    }

    return payload;
  }
});
