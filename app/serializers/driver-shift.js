import DisaptchSerializer from './-dispatch';
import { typeOf } from '@ember/utils';

export default DisaptchSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    // this is because of inconsistensies in api
    // returns an array instead of an object after a POST
    if (requestType === 'createRecord' && typeOf(payload.data) === 'array') {
      if (payload.data.length === 1) {
        payload.data = payload.data[0];
      }
    }

    return this._super(...arguments);
  }
});
