import RiderManagementSerializer from './-rms-riderManagement';
import RiderSerializer from './rms-rider';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'riderTravelNeed';

    if (!isNone(json.data.relationships)) {
      const riderRelationship = json.data.relationships.rider;

      if (!isNone(riderRelationship)) {
        riderRelationship.data.type = 'rider';
      }

      const travelNeedTypeRelationship = json.data.relationships.travelNeedType;

      if (!isNone(travelNeedTypeRelationship)) {
        travelNeedTypeRelationship.data.type = 'travelNeedType';
      }

      const passengerTypeRelationship = json.data.relationships.passengerType;

      if (!isNone(passengerTypeRelationship)) {
        passengerTypeRelationship.data.type = 'passengerType';
      }
    }

    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const riderSerializer = new RiderSerializer();
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
      data[i].type = 'rms-rider-travel-need';

      //a1
      if (!isNone(data[i].relationships.rider) &&
        !isNone(data[i].relationships.rider.data)) {
        data[i].relationships.rider.data.type = 'rms-rider';
      }

      if (!isNone(data[i].relationships.travelNeedType.data)) {
        data[i].relationships.travelNeedType.data.type = 'rms-travel-need-type';
      }

      if (!isNone(data[i].relationships.passengerType.data)) {
        data[i].relationships.passengerType.data.type = 'rms-passenger-type';
      }
    }

    if (!isNone(payload.included) && !isNone(payload.included.length)) {
      for (let i = 0; i < payload.included.length; i++) {
        const include = payload.included[i];

        if (include.type === 'rider') {
            riderSerializer.normalizeResponse(store, primaryModelClass, { data: include });
        }
        if (include.type === 'travelNeedType') {
            include.type = 'rms-travel-need-type';
        }
        if (include.type === 'passengerType') {
            include.type = 'rms-passenger-type';
        }
      }
    }

    return payload;
  }
});
