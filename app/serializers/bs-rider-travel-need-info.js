import DS from 'ember-data';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';


const { JSONAPISerializer } = DS;

export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'riderTravelNeed';

        if (!isNone(json.data.relationships)) {
            const riderRelationship = json.data.relationships.rider;

            if (!isNone(riderRelationship)) {
                riderRelationship.data.type = 'rider';
            }

            const travelNeedTypeRelationship = json.data.relationships.travelNeedType;

            if (!isNone(travelNeedTypeRelationship)) {
                travelNeedTypeRelationship.data.type = 'travelNeedType';
            }

            const passengerTypeRelationship = json.data.relationships.passengerType;

            if (!isNone(passengerTypeRelationship)) {
                passengerTypeRelationship.data.type = 'passengerType';
            }
        }

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
        const data = makeArray(payload.data);

        for (let i = 0; i < data.length; i++) {
            data[i].type = 'bs-rider-travel-need-info';
        }

        if (payload.included) {
            for (let i = 0; i < payload.included.length; i++) {
                const include = payload.included[i];

                try {
                    if (include.type === 'rider') {
                        include.type = 'bs-riderInfo';
                    } else if (include.type === 'travelNeedType') {
                        include.type = 'bs-travelNeedTypeInfo';
                    }

                    if (
                        include.relationships &&
                        include.relationships.rider &&
                        include.relationships.rider.data &&
                        include.relationships.rider.data.type === 'rider'
                    ) {
                        include.relationships.rider.data.type = 'bs-rider-info';
                    }

                    if (
                        include.relationships &&
                        include.relationships.travelNeedType &&
                        include.relationships.travelNeedType.data &&
                        include.relationships.travelNeedType.data.type === 'travelNeedType'
                    ) {
                        include.relationships.travelNeedType.data.type = 'bs-travel-need-type-info';
                    }
                } catch (e) {
                    console.warn(e); // eslint-disable-line no-console
                }
            }
        }

        return payload;
    }
});
