import DS from 'ember-data';
import { makeArray } from '@ember/array';
import tomtom from 'tomtom';
const { JSONAPISerializer } = DS;


export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'travelNeedType';

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
      const data = makeArray(payload.data);
      
      const new_data = data.map((m) => {
        const new_icon = tomtom.L.svgIcon({icon: m.attributes.options.icon})
        m.attributes.options.icon = new_icon;
        return m;
      });
      
      payload.data = new_data
      return payload;
    }

});
