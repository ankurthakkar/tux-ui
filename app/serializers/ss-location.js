import SchedulingServiceSerializer from './-ss-schedulingService';

export default SchedulingServiceSerializer.extend({
  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'location';

    json.data.attributes.country = 'US';
    json.data.attributes.countryAbbreviation = 'US';
    json.data.attributes.stateAbbreviation = 'WA';
    json.data.attributes.geoNode = '543';

    delete json.data.attributes.type;

    return json;
  }
});
