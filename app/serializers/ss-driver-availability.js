import SchedulingServiceSerializer from './-ss-schedulingService';
import { isNone } from '@ember/utils';

export default SchedulingServiceSerializer.extend({
  attrs: {
    // driverIdId: { serialize: false }
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    // startDate and endDate should be set to start and end of day.
    let startDate = new Date(json.data.attributes.startTime);
    let endDate = new Date(json.data.attributes.endTime);

    startDate.setHours(0,0,0,0);
    endDate.setHours(23,59,59,999);
    json.data.attributes.startTime = startDate.toISOString();
    json.data.attributes.endTime = endDate.toISOString();

    return json;
  }
});
