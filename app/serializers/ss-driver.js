import SchedulingServiceSerializer from './-ss-schedulingService';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';
import moment from 'moment';

export default SchedulingServiceSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-driver',
  iqModelForeignKey: 'driverId',
  iqModelRelationship: 'iqDriver',

  attrs: {
    driverId: { serialize: false }
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    if (json.data.relationships) {
      // removing relationship because api service will not accept
      // relationship. MUST fix in API.
      delete json.data.relationships;
    }

    return json;
  },

  normalize(modelClass, resourceHash) {
    let normalized = this._super(modelClass, resourceHash);

    let { attributes } = normalized.data;

    // TODO: verify that this does/will appear on payload
    if (attributes.dateOfBirth) {
      attributes.dateOfBirth = moment(attributes.dateOfBirth).toDate();
    }

    return normalized;
  }
});
