import moment from 'moment';
import RiderSerializer from './rms-rider';
import RiderManagementSerializer from './-rms-riderManagement';

import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    // startDate and endDate should be set to start and end of day.
    let startDate = new Date(json.data.attributes.from);
    let endDate = new Date(json.data.attributes.to);

    startDate.setHours(0,0,0,0);
    endDate.setHours(23,59,59,999);
    json.data.attributes.from = startDate.toISOString();
    json.data.attributes.to = endDate.toISOString();

    json.data.type = 'riderEligibility';

    if (!isNone(json.data.relationships)) {
      const riderEligibilityRelationship = json.data.relationships.eligibilityType;

      if (!isNone(riderEligibilityRelationship)) {
          riderEligibilityRelationship.data.type = 'eligibilityType';
      }

      const riderRelationship = json.data.relationships.rider;

      if (!isNone(riderRelationship)) {
          riderRelationship.data.type = 'rider';
      }
    }

    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const riderSerializer = new RiderSerializer();
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
      data[i].type = 'rms-rider-eligibility';

      data[i].attributes.from = moment(data[i].attributes.from).toDate();
      data[i].attributes.to = moment(data[i].attributes.to).toDate();

      if (!isNone(data[i].relationships.eligibilityType.data)) {
        data[i].relationships.eligibilityType.data.type = 'rms-eligibility-type';
      }

      if (!isNone(data[i].relationships.rider) &&
        !isNone(data[i].relationships.rider.data)) {
        data[i].relationships.rider.data.type = 'rms-rider';
      }
    }

    if (!isNone(payload.included) && !isNone(payload.included.length)) {
      for (let i = 0; i < payload.included.length; i++) {
        const include = payload.included[i];

        if (include.type === 'rider') {
            riderSerializer.normalizeResponse(store, primaryModelClass, { data: include });
        }
        if (include.type === 'eligibilityType') {
            include.type = 'rms-eligibility-type';
        }
      }
    }

    return payload;
  }
});
