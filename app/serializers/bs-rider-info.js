import DS from 'ember-data';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';


const { JSONAPISerializer } = DS;

export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'rider';

        if (!isNone(json.data.relationships)) {
            const eligibilityRelationships = json.data.relationships.eligibilities;

            if (!isNone(eligibilityRelationships)) {
                eligibilityRelationships.data.map((object) => {
                    object.type = 'riderEligibility';
                });
            }

            const travelNeedRelationships = json.data.relationships.travelNeeds;

            if (!isNone(travelNeedRelationships)) {
                travelNeedRelationships.data.map((object) => {
                    object.type = 'riderTravelNeed';
                });
            }
        }

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
        const data = makeArray(payload.data);

        for (let i = 0; i < data.length; i++) {
            data[i].type = 'rider-info';
        }

        if (payload.included) {
            for (let i = 0; i < payload.included.length; i++) {
                const include = payload.included[i];

                try {
                    if (include.type === 'riderTravelNeed') {
                        include.type = 'bs-riderTravelNeedInfo';
                    } else if (include.type === 'travelNeedType') {
                        include.type = 'bs-travelNeedTypeInfo';
                    }

                    if (
                        include.relationships &&
                        include.relationships.riderTravelNeed &&
                        include.relationships.riderTravelNeed.data &&
                        include.relationships.riderTravelNeed.data.type === 'riderTravelNeed'
                    ) {
                        include.relationships.riderTravelNeed.data.type = 'bs-rider-travel-need-info';
                    }

                    if (
                        include.relationships &&
                        include.relationships.travelNeedType &&
                        include.relationships.travelNeedType.data &&
                        include.relationships.travelNeedType.data.type === 'travelNeedType'
                    ) {
                        include.relationships.travelNeedType.data.type = 'bs-travel-need-type-info';
                    }
                } catch (e) {
                    console.warn(e); // eslint-disable-line no-console
                }
            }
        }

        return payload;
    }
});
