import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  serialize(snapshot, options) {
    let serialized = this._super(snapshot, options);

    // TODO: handle new widgets stuff

    // serialize "live" widgets down to POJO configs; overwrite old ones
    delete serialized.data.attributes._widgets;

    serialized.data.attributes.widgets =
      snapshot.record.widgets.map((widget) => widget.serialize());

    return serialized;
  },

  normalizeSingleResponse(store, modelClass, payload /*, id, requestType*/) {
    // rename `widgets` to `_widgets`
    let { widgets } = payload.data.attributes;
    delete payload.data.attributes.widgets;
    payload.data.attributes._widgets = widgets;

    return this._super(...arguments);
  },

  normalizeArrayResponse(store, _, payload /*, id, requestType*/) {
    payload.data.forEach((data) => {
      // rename `widgets` to `_widgets`
      let { widgets } = data.attributes;
      delete data.attributes.widgets;
      data.attributes._widgets = widgets;
    });

    return this._super(...arguments);
  }
});
