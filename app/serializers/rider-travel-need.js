import DS from 'ember-data';
import { isNone } from '@ember/utils';

const { JSONAPISerializer } = DS;

export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'riderTravelNeed';

        if (!isNone(json.data.relationships)) {
            const riderRelationship = json.data.relationships.rider;

            if (!isNone(riderRelationship)) {
                riderRelationship.data.type = 'rider';
            }

            const travelNeedTypeRelationship = json.data.relationships.travelNeedType;

            if (!isNone(travelNeedTypeRelationship)) {
                travelNeedTypeRelationship.data.type = 'travelNeedType';
            }

            const passengerTypeRelationship = json.data.relationships.passengerType;

            if (!isNone(passengerTypeRelationship)) {
                passengerTypeRelationship.data.type = 'passengerType';
            }
        }

        return json;
    }
});
