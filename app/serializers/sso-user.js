
import SSOServiceSerializer from './-sso-ssoService';
import _ from 'lodash';

export default SSOServiceSerializer.extend({

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        let roles = _.map(json.data.attributes.UserRoleRolenames, (role) => {
          return role.roleName;
        });

        return {
          displayName: json.data.attributes.displayName,
          email: json.data.attributes.email,
          status: json.data.attributes.status,
          roles: roles
        };
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
      let currentPayload = {
        data: []
      };

      let dataArray = [];

      let data;

      if (payload.length !== undefined) {
        if (payload.length > 0) {
          data = payload;
        }
        else {
          data = [];
        }

      }
      else {
        data = [payload];
      }

      for (let i = 0; i < data.length; i++) {
        dataArray.push({
          id: data[i].userId,
          type: 'sso-user',
          attributes: data[i]
        });
      }

      if (dataArray.length > 1) {
        currentPayload.data = dataArray;
      }
      else if (dataArray.length === 1) {
        if (payload.length !== undefined) {
          currentPayload.data = dataArray;
        }
        else {
          currentPayload.data = dataArray[0];
        }
      }
      else {
        currentPayload.data = [];
      }

      return currentPayload;
    }
});

