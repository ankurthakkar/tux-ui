import BookingServiceSerializer from './-bs-bookingService';
import { makeArray } from '@ember/array';
import { isNone } from '@ember/utils';

export default BookingServiceSerializer.extend({
  keyForAttribute: function(attr) {
      return attr;
  },
  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  serialize(snapshot, options = {}) {
      return this._super(...arguments);
  },

  normalize(modelClass, resourceHash) {
    let normalized = this._super(modelClass, resourceHash);

    let attributes = normalized.data.attributes;
    // flatten the address and coordinates so it matches
    // the model for dispatch, scheduling and rider management
    attributes.alias = '';
    if (!isNone(attributes.address)) {
      attributes.streetNumber = attributes.address.streetNumber;
      attributes.streetName = attributes.address.streetName;
      attributes.room = attributes.address.room;
      attributes.city = attributes.address.city;
      attributes.county = attributes.address.county;
      attributes.state = attributes.address.state;
      attributes.postalCode = attributes.address.postalCode;
      attributes.country = attributes.address.country;
      attributes.countryAbbreviation = attributes.address.countryAbbreviation;

      attributes.lat = attributes.coordinates.lat;
      attributes.lng = attributes.coordinates.lng;
    }

    return normalized;
  }
});
