import DispatchSerializer from './-dispatch';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default DispatchSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-stop',
  iqModelForeignKey: 'externalId',
  iqModelRelationship: 'iqStop',

  attrs: {
    distanceToNext: { serialize: false },
    dwell: { serialize: false },
    noShowState: { serialize: false },
    notes: { serialize: false },
    onboardCount: { serialize: false },
    ordinal: { serialize: false },
    phone1: { serialize: false },
    plannedTime: { serialize: false },
    state: { serialize: false },
    travelTimeToNext: { serialize: false },
    type: { serialize: false },
    trip: { serialize: false }
  }
});
