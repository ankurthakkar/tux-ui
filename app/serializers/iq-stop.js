import ApplicationSerializer from './application'

export default ApplicationSerializer.extend({
  normalize(typeClass, hash) {
    let bsStopId = hash.id;
    let dsStopId = hash.attributes.dispatchStopId;

    let normalized = this._super(...arguments);

    normalized.data.relationships.bsStop = {
      data: {
        id: bsStopId,
        type: 'bs-stop'
      }
    };

    if (dsStopId) {
      normalized.data.relationships.dsStop = {
        data: {
          id: dsStopId.toString(),
          type: 'stop'
        }
      };
    }

    // TODO: add avlmStop

    return normalized;
  }
});
