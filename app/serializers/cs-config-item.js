import ConfigServiceSerializer from './-cs-configService';
import { typeOf, isEmpty } from '@ember/utils';

const JSON_REGEX = /^[\{\[].*[\}\]]$/;
const OPTIONAL_ATTRS = ['description', 'unit', 'type'];

export default ConfigServiceSerializer.extend({
  attrs: {
    category: { serialize: false }
  },

  serialize() {
    let serialized = this._super(...arguments);

    OPTIONAL_ATTRS.forEach((attr) => {
      if (isEmpty(serialized.data.attributes[attr])) {
        delete serialized.data.attributes[attr];
      }
    });

    let { value } = serialized.data.attributes;
    if (typeOf(value) !== 'string') {
      serialized.data.attributes.value = JSON.stringify(value);
    }

    return serialized;
  },

  normalize() {
    let normalized = this._super(...arguments);

    // extract category and name from id
    let { id } = normalized.data;
    let segments = id.split('/');
    let name = segments[segments.length - 1];
    let category = segments.slice(0, segments.length - 1).join('/');

    normalized.data.attributes.name = name;
    normalized.data.attributes.category = category;

    let { value } = normalized.data.attributes;
    if (typeOf(value) === 'string' && JSON_REGEX.test(value)) {
      try {
        normalized.data.attributes.value = JSON.parse(value);
      } catch(err) {
        console.warn(`failed to parse JSON-like value`, value);
      }
    }

    return normalized;
  }
});
