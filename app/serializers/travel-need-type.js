import DS from 'ember-data';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

const { JSONAPISerializer } = DS;




export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'travelNeedType';

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
      const data = makeArray(payload.data);

      for (let i = 0; i < data.length; i++) {
          data[i].type = 'travel-need-type';
      }

      if (!isNone(payload.included) && !isNone(payload.included.length)) {
        for (let i = 0; i < payload.included.length; i++) {
          const include = payload.included[i];

          if (include.type === 'travel-need-type') {
            include.type = 'travel-need-type';
          }
        }
      }

      return payload;
    }

});
