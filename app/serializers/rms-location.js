import RiderManagementSerializer from './-rms-riderManagement';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'location';

    if (!isNone(json.data.relationships) && !isNone(json.data.relationships.address)) {
      const addressRelationships = json.data.relationships.address;

      if (!isNone(addressRelationships)) {
        delete json.data.relationships.address;
      }
    }
    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },


  normalizeResponse: function(store, primaryModelClass, payload) {
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
      data[i].type = 'rms-location';
    }

    if (!isNone(payload.included) && !isNone(payload.included.length)) {
      for (let i = 0; i < payload.included.length; i++) {
        const include = payload.included[i];

        if (include.type === 'address') {
            include.type = 'rms-address';
        }
      }
    }

    return payload;
  }

});
