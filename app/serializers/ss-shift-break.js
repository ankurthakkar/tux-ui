import SchedulingServiceSerializer from './-ss-schedulingService';

export default SchedulingServiceSerializer.extend({
  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    // startDate and endDate should be set to start and end of day.
    let startDate = new Date(json.data.attributes.startTime);
    let endDate = new Date(json.data.attributes.endTime);

    var difference = startDate - endDate;
    var diff_result = new Date(difference);

    var hourDiff = diff_result.getHours();

    json.data.attributes.promisedStart = startDate.toISOString();
    json.data.attributes.plannedDuration = hourDiff;
    json.data.type = 'shift-break';

    return json;
  }
});
