import DS from 'ember-data';
import { camelize, dasherize } from '@ember/string';
import { singularize } from 'ember-inflector';
import { isPresent } from '@ember/utils';

const { JSONAPISerializer } = DS;

export default JSONAPISerializer.extend({
  modelPrefix: '',

  keyForAttribute(key) {
    return key;
  },

  keyForRelationship(key) {
    return key;
  },

  // deprecated; will change to `payloadTypeFromModelName`
  payloadKeyFromModelName(modelName) {
    let modelPrefix = this.get('modelPrefix');
    if (isPresent(modelPrefix) && modelName.startsWith(`${modelPrefix}-`)) {
      modelName = modelName.slice(modelPrefix.length + 1);
    }

    return camelize(modelName);
  },

  // deprecated; will change to `modelNameFromPayloadType`
  modelNameFromPayloadKey(key) {
    let modelPrefix = this.get('modelPrefix');
    let modelName = dasherize(singularize(key));

    if (isPresent(modelPrefix) && !modelName.startsWith(`${modelPrefix}-`)) {
      modelName = `${modelPrefix}-${modelName}`;
    }
    return modelName;
  }
});
