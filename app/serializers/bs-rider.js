import BookingServiceSerializer from './-bs-bookingService';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default BookingServiceSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-rider',
  iqModelForeignKey: 'riderId',
  iqModelRelationship: 'iqRider'
});
