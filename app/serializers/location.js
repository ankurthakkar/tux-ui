import DS from 'ember-data';

const { JSONAPISerializer } = DS;
import { isNone } from '@ember/utils';
import TravelNeedSerializer from './bs-rider-travel-need';
import RiderEligibilitySerializer from './bs-rider-eligibility';
import AddressSerializer from './bs-address';
import { makeArray } from '@ember/array';





export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    serialize(snapshot, options) {
        const json = this._super(snapshot, options);

        json.data.attributes.address.room = null;
        json.data.attributes.address.country = 'US';
        json.data.attributes.address.countryAbbreviation = 'US';
        json.data.attributes.address.stateAbbreviation = 'WA';
        json.data.attributes.pointOfInterest.alias = null;

        delete json.data.relationships['leg-destination'];
        delete json.data.relationships['leg-origin'];
        delete json.data.relationships.stop;
        delete json.data.relationships.addresses;

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
      const data = makeArray(payload.data);
      const travelNeedSerializer = new TravelNeedSerializer();
      const riderEligibilitySerializer = new RiderEligibilitySerializer();
      const addressSerializer = new AddressSerializer();


      for (let i = 0; i < data.length; i++) {
        data[i].type = 'bs-location';

        if (!isNone(data[i].relationships)) {

          for (let j = 0; j < data[i].relationships.booking.data.length; j++) {
            const booking = data[i].relationships.booking.data[j];

            booking.type = 'bs-booking';
          }
          for (let j = 0; j < data[i].relationships.location.data.length; j++) {
            const location = data[i].relationships.location.data[j];

            location.type = 'bs-location';
          }
          for (let j = 0; j < data[i].relationships.addresses.data.length; j++) {
            const address = data[i].relationships.addresses.data[j];

            address.type = 'bs-address';
          }
        }
      }
        if (!isNone(payload.data.length)) {
            for (let i = 0; i < payload.data.length; i++) {
                if (!isNone(payload.data[i].relationships)) {
                    payload.data[i].relationships.rider.data.type = 'bs-rider';
                }
            }
        } else if (!isNone(payload.data.relationships)) {
            payload.data.relationships.rider.data.type = 'bs-rider';
        }

        if (!isNone(payload.included) && !isNone(payload.included.length)) {
            for (let i = 0; i < payload.included.length; i++) {
                const include = payload.included[i];

                if (include.type === 'segment') {
                    include.type = 'bs-segment';
                    include.attributes.fare = include.attributes.fare / 100;
                }

            }
        }
        if (!isNone(payload.included) && !isNone(payload.included.length)) {
          for (let i = 0; i < payload.included.length; i++) {
              const include = payload.included[i];

              try {
                if (include.type === 'address') {
                  addressSerializer.normalizeResponse(store, primaryModelClass, { data: include });
                }

                if (include.type === 'riderTravelNeed') {
                  travelNeedSerializer.normalizeResponse(store, primaryModelClass, { data: include });
                }

                if (include.type === 'travelNeedType') {
                  include.type = 'bs-travel-need-type';
                }

                if (include.type === 'passengerType') {
                  include.type = 'bs-passenger-type';
                }

                if (include.type === 'riderEligibility') {
                  riderEligibilitySerializer.normalizeResponse(store, primaryModelClass, { data: include });
                }

                if (include.type === 'eligibilityType') {
                  include.type = 'bs-eligibility-type';
                }

                if (include.type === 'location') {
                  include.type = 'bs-location';
                }
                  if (include.type === 'rider') {
                      include.type = 'bs-rider';
                  }

                  if (
                      include.relationships &&
                      include.relationships.rider &&
                      include.relationships.rider.data &&
                      include.relationships.rider.data.type === 'rider'
                  ) {
                      include.relationships.rider.data.type = 'bs-rider';
                  }

                  if (include.type === 'segment') {
                      include.attributes.fare = include.attributes.fare / 100;
                      include.type = 'bs-segment';
                  }
                  if (include.type === 'leg') {
                    include.type = 'bs-leg';
                  }

                  if (include.type === 'location') {
                    include.type = 'bs-location';
                  }

                  if (include.type === 'legTravelNeed') {
                    include.type = 'bs-leg-travel-need';
                  }

                  if (include.type === 'stop') {

                    include.type = 'bs-stop';
                  }

                  if (include.type === 'riderTravelNeed') {

                    include.type = 'bs-rider-travel-need';
                  }

                  if (include.type === 'rider') {

                    include.type = 'bs-rider';
                  }

                  if (include.type === 'booking') {

                    include.type = 'bs-booking';
                  }

                  if (include.type === 'address') {

                    include.type = 'bs-addresss';
                  }

              } catch (e) {
                  console.warn(e); // eslint-disable-line no-console
              }
          }
      }

        return payload;
    }
});
