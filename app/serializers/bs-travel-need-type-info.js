import DS from 'ember-data';
import { makeArray } from '@ember/array';

const { JSONAPISerializer } = DS;



export default JSONAPISerializer.extend({
  keyForAttribute: function(attr) {
    return attr;
},

keyForRelationship(key) {
  return key;
},


    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'travelNeedType';

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
        const data = makeArray(data.payload);

        for (let i = 0; i < data.length; i++) {
            data[i].type = 'bs-travel-need-type-info';
        }

        return payload;
    }
});
