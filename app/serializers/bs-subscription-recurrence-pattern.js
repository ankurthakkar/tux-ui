import BookingServiceSerializer from './-bs-bookingService';

export default BookingServiceSerializer.extend({
  attrs: {
    everyweekday: { serialize: false },
    pattern: { serialize: false },
    recurring: { serialize: false },
    dailyConfig: { serialize: false }
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    return json;
  },

  normalize(modelClass, resourceHash) {
    let normalized = this._super(modelClass, resourceHash);


    return normalized;
  }

});
