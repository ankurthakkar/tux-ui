import RiderManagementSerializer from './-rms-riderManagement';
import RiderSerializer from './rms-rider';

import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'address';

    if (!isNone(json.data.relationships)) {
        const locationsRelationships = json.data.relationships.locations;

        if (!isNone(locationsRelationships)) {
            locationsRelationships.data.map((object) => {
                object.type = 'location';
            });
        }

        const ridersPrimaryRelationships = json.data.relationships.ridersPrimary;

        if (!isNone(ridersPrimaryRelationships)) {
          delete json.data.relationships.ridersPrimary;
            // ridersPrimaryRelationships.data.map((object) => {
            //     object.type = 'rider';
            // });
        }

        const ridersFavoriteRelationships = json.data.relationships.ridersFavorite;

        if (!isNone(ridersFavoriteRelationships)) {
          delete json.data.relationships.ridersFavorite;
            // ridersFavoriteRelationships.data.map((object) => {
            //     object.type = 'rider';
            // });
        }
    }

    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const riderSerializer = new RiderSerializer();

    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
      data[i].type = 'rms-address';

      for (let j = 0; j < data[i].relationships.locations.data.length; j++) {
        const location = data[i].relationships.locations.data[j];

        location.type = 'rms-location';
      }
    }

    if (!isNone(payload.included) && !isNone(payload.included.length)) {
      for (let i = 0; i < payload.included.length; i++) {
        const include = payload.included[i];

        if (include.type === 'rider') {
            riderSerializer.normalizeResponse(store, primaryModelClass, { data: include });
        }
        if (include.type === 'location') {
            include.type = 'rms-location';
        }
      }
    }

    return payload;
  }
});
