import DS from 'ember-data';
import { isNone } from '@ember/utils';

const { JSONAPISerializer } = DS;


export default JSONAPISerializer.extend({
    keyForAttribute: function(attr) {
        return attr;
    },

    keyForRelationship(key) {
        return key;
    },

    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'legTravelNeed';

        if (!isNone(json.data.relationships)) {
            const travelNeedTypeRelationship = json.data.relationships.travelNeedType;

            if (!isNone(travelNeedTypeRelationship)) {
                travelNeedTypeRelationship.data.type = 'travelNeedType';
            }

            const legRelationship = json.data.relationships.leg;

            if (!isNone(legRelationship)) {
                legRelationship.data.type = 'leg';
            }
        }

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
        if (!isNone(payload.data.relationships)) {
            const travelNeedTypeRelationship = payload.data.relationships.travelNeedType;

            if (!isNone(travelNeedTypeRelationship)) {
                travelNeedTypeRelationship.data.type = 'bs-travel-need-type';
            }
        }
        if (!isNone(payload.included) && !isNone(payload.included.length)) {
            for (let i = 0; i < payload.included.length; i++) {
                const include = payload.included[i];

                try {

                  if (include.type === 'travelNeedType') {
                    include.type = 'bs-travel-need-type';
                  }

                  if (include.type === 'passengerType') {
                    include.type = 'bs-passenger-type';
                  }



                  if (include.type === 'eligibilityType') {
                    include.type = 'bs-eligibility-type';
                  }


                    if (include.type === 'rider') {
                        include.type = 'bs-rider';
                    }

                    if (
                        include.relationships &&
                        include.relationships.rider &&
                        include.relationships.rider.data &&
                        include.relationships.rider.data.type === 'rider'
                    ) {
                        include.relationships.rider.data.type = 'bs-rider';
                    }

                    if (include.type === 'segment') {
                        include.attributes.fare = include.attributes.fare / 100;
                        include.type = 'bs-segment';
                    }
                    if (include.type === 'leg') {
                      include.type = 'bs-leg';
                    }

                    if (include.type === 'location') {
                      include.type = 'bs-location';
                    }

                    if (include.type === 'legTravelNeed') {
                      include.type = 'bs-leg-travel-need';
                    }

                    if (include.type === 'stop') {

                      include.type = 'bs-stop';
                    }

                    if (include.type === 'riderTravelNeed') {

                      include.type = 'bs-rider-travel-need';
                    }

                    if (include.type === 'rider') {

                      include.type = 'bs-rider';
                    }

                    if (include.type === 'booking') {

                      include.type = 'bs-booking';
                    }

                    if (include.type === 'address') {

                      include.type = 'bs-addresss';
                    }

                } catch (e) {
                    console.warn(e); // eslint-disable-line no-console
                }
            }
        }

        return payload;
    }
});
