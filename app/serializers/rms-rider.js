import RiderManagementSerializer from './-rms-riderManagement';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';
import moment from 'moment';
import { isNone } from '@ember/utils';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-rider',
  iqModelForeignKey: 'id',
  iqModelRelationship: 'iqRider',

  attrs: {
    riderId: { serialize: false },
    iqRider: { serialize: false }
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.attributes.dateOfBirth =
      moment(json.data.attributes.dateOfBirth).format('YYYY-MM-DD');

    return json;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
      if (!isNone(data[i].attributes.dateOfBirth)) {
        data[i].attributes.dateOfBirth = moment(data[i].attributes.dateOfBirth).format();
      }
    }

    this._super(...arguments);

    return payload;
  }
});
