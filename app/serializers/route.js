import DispatchSerializer from './-dispatch';
import { typeOf } from '@ember/utils';

export default DispatchSerializer.extend({
  attrs: {
    vehicleName: { serialize: false },
    driverExternalId: { serialize: false },
    routeDate: { serialize: false },
    lifoDepth: { serialize: false },
    routeFactor: { serialize: false },
    isBooleanExample: { serialize: false },
    enumExamples: { serialize: false },
    enumExample: { serialize: false },
    odometer: { serialize: false },
    timestamp: { serialize: false },
    selectedVehicle: { serialize: false },
    selectedDriver: { serialize: false },
    navigationPolyline: { serialize: false},
    pulloutPolyline: { serialize: false},
    status: { serialize: false},
    otp: { serialize: false}
  },

  normalizeResponse: function(store, primaryModelClass, payload, id, requestType) {
    // this is because of inconsistensies in api
    // returns an array instead of an object after a POST
    if (requestType === 'createRecord' && typeOf(payload.data) === 'array') {
      if (payload.data.length === 1) {
        payload.data = payload.data[0];
      }
    }

    return this._super(...arguments);
  }
});
