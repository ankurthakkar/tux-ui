
import SSOServiceSerializer from './-sso-ssoService';

export default SSOServiceSerializer.extend({
    serialize(snapshot, options = {}) {
        options.includeId = true;

        const json = this._super(snapshot, options);

        json.data.type = 'role';

        return json;
    },

    normalizeResponse: function(store, primaryModelClass, payload) {
      let currentPayload = {
        data: []
      };

      const data = payload.length ? payload : [payload];

      for (let i = 0; i < data.length; i++) {
        currentPayload.data.push({
          id: data[i].roleName,
          type: 'sso-role',
          attributes: data[i]
        });
      }

      return currentPayload;
    }
});
