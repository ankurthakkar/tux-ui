import DispatchSerializer from './-dispatch';
import { typeOf } from '@ember/utils';

export default DispatchSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    if (requestType === 'createRecord' && typeOf(payload.data) === 'array') {
      if (payload.data.length === 1) {
        payload.data = payload.data[0];
      }
    }

    return this._super(...arguments);
  }
});
