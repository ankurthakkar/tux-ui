import ApplicationSerializer from './application'
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default ApplicationSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-vehicle',
  iqModelForeignKey: 'vehicleId',
  iqModelRelationship: 'iqVehicle',

  normalize(typeClass, hash) {
    // incoming payload has ID of vehicle
    let vehicleId = hash.id;

    // generate a unique compound id (vehicleName, timestamp)
    let time = (new Date(hash.attributes.timestamp)).getTime();
    hash.id = `${vehicleId}-${time}`;

    // flatten POJO structures
    let { location, mov } = hash.attributes;
    delete hash.attributes.location;
    delete hash.attributes.mov;

    hash.attributes.vehicleId = vehicleId;
    hash.attributes.lat = location.coord.lat;
    hash.attributes.lng = location.coord.lng;
    hash.attributes.heading = mov.heading;
    hash.attributes.speed = mov.speed;

    return this._super(...arguments);
  }
});
