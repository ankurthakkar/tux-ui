import BookingServiceSerializer from './-bs-bookingService';
import { isNone } from '@ember/utils';

export default BookingServiceSerializer.extend({
  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    if (!isNone(json.data.relationships)) {
      if (!isNone(json.data.relationships.passengerType)) {
        delete json.data.relationships.passengerType;
      }
    }

    return json;
  },

  normalize(modelClass, resourceHash) {
    let normalized = this._super(modelClass, resourceHash);


    return normalized;
  }

});
