import RiderManagementSerializer from './-rms-riderManagement';
import { makeArray } from '@ember/array';

export default RiderManagementSerializer.extend({
  attrs: {
  },

  serialize(/* snapshot, options */) {
    let json = this._super(...arguments);

    json.data.type = 'passengerType';

    return json;
  },

  payloadKeyFromModelName(modelName) {
    return modelName;
  },

  normalizeResponse: function(store, primaryModelClass, payload) {
    const data = makeArray(payload.data);

    for (let i = 0; i < data.length; i++) {
        data[i].type = 'rms-passenger-type';
    }

    return payload;
  }
});
