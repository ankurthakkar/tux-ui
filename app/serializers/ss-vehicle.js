
import SchedulingServiceSerializer from './-ss-schedulingService';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';
import { isNone } from '@ember/utils';

export default SchedulingServiceSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-vehicle',
  iqModelForeignKey: 'name',
  iqModelRelationship: 'iqVehicle',

  serialize(snapshot, options = {}) {
    options.includeId = true;

    const json = this._super(snapshot, options);

    json.data.type = 'vehicle';

    if (!isNone(json.data.relationships)) {

        const startGaragesRelationships = json.data.relationships.startGarages;

        if (!isNone(startGaragesRelationships)) {
          startGaragesRelationships.data.map((object) => {
                object.type = 'location';
            });
        }
        json.data.relationships.startGarage = new Object();
        json.data.relationships.startGarage.data =  startGaragesRelationships.data.firstObject;
        delete json.data.relationships.startGarages;

        const endGaragseRelationships = json.data.relationships.endGarages;

        if (!isNone(endGaragseRelationships)) {
          endGaragseRelationships.data.map((object) => {
                object.type = 'location';
            });
        }

        json.data.relationships.endGarage = new Object();
        json.data.relationships.endGarage.data = endGaragseRelationships.data.firstObject;

        delete json.data.relationships.endGarages;
        delete json.data.relationships.iqVehicle;

        const vehicleTypeRelationship = json.data.relationships.vehicleType;

        if (!isNone(vehicleTypeRelationship) && !isNone(vehicleTypeRelationship.data)) {
            vehicleTypeRelationship.data.type = 'vehicleType';
        }

        const driverRelationship = json.data.relationships.driver;

        if (!isNone(driverRelationship) && !isNone(driverRelationship.data)) {
          driverRelationship.data.type = 'driver';
        }

        const vehicleCapacityTypeRelationship = json.data.relationships.vehicleCapacityType;

      if (!isNone(vehicleCapacityTypeRelationship) && !isNone(vehicleCapacityTypeRelationship.data)) {
        vehicleCapacityTypeRelationship.data.type = 'vehicleCapacityType';
      }
      const vehicleCapacityConfigRelationship = json.data.relationships.vehicleCapacityConfig;

      if (!isNone(vehicleCapacityConfigRelationship) && !isNone(vehicleCapacityConfigRelationship.data)) {
        vehicleCapacityConfigRelationship.data.type = 'vehicleCapacityConfig';
      }


    }

    return json;
  }
});
