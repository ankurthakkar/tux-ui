import TripManagementSerializer from './-tm-tripManagement';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default TripManagementSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-driver',
  iqModelForeignKey: 'badgeNr',
  iqModelRelationship: 'iqDriver',

  // TODO: change to 'belongsTo' when data is fixed
  iqModelRelationshipType: 'hasMany'
});
