import DispatchSerializer from './-dispatch';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default DispatchSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-rider',
  iqModelForeignKey: 'externalId',
  iqModelRelationship: 'iqRider'
});
