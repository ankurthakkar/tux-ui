import DispatchSerializer from './-dispatch';
import IQModelIdentification from 'adept-iq/mixins/iq-model-identification';

export default DispatchSerializer.extend(IQModelIdentification, {
  iqModelName: 'iq-driver',
  iqModelForeignKey: 'externalId',
  iqModelRelationship: 'iqDriver'
});