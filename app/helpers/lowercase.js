import { helper } from '@ember/component/helper';

export function lowercase(str) {
  let s = str[0].replace(/\s+/g, '-').toLowerCase();
  return s;
}

export default helper(lowercase);
