// this is deprecated; used for generating RQL for active context queries
export const serverRelationships = {
  route: [
    { type: 'hasMany', modelName: 'trip', path: 'clusters.trips' },
    { type: 'hasMany', modelName: 'stop', path: 'clusters.trips.stops' }
  ],
  trip: [
    { type: 'belongsTo', modelName: 'route', path: 'cluster.route' },
    { type: 'hasMany', modelName: 'stop', path: 'stops' }
  ],
  stop: [
    { type: 'belongsTo', modelName: 'route', path: 'trip.cluster.route' },
    { type: 'belongsTo', modelName: 'trip', path: 'trip' }
  ],
  schedule: [],
  'rms-rider': [],
  'ss-driver': [],
  'ss-schedule': [],
  'ss-vehicle': [],
  'bs-booking': [],
  'bs-leg': [],
  'sso-user': [],
  'bs-subscription':[]
};
