// TODO: make this JSON-only

import IconPaths from 'adept-iq/config/icon-paths';

import RoutesWidgetConfig from 'adept-iq/pods/components/iq-widgets/routes-widget/config';
import TripsWidgetConfig from 'adept-iq/pods/components/iq-widgets/trips-widget/config';
import StopsWidgetConfig from 'adept-iq/pods/components/iq-widgets/stops-widget/config';
import VehiclesWidgetConfig from 'adept-iq/pods/components/iq-widgets/vehicles-widget/config';

// TODO: it would be better to look up values from static config at time of use,
// but for now this handles the differences in config format
const extractLabelsFromWidgetConfig = (widgetConfig) => {
  return widgetConfig.columns.map((columnConfig) => {
    let { id, type, label, valuePath, valuePreview, searchable, isMapLabelVisible } = columnConfig;

    return {
      id,
      type,
      label,
      valuePath,
      valuePreview,
      isSearchable: searchable,
      isVisible: isMapLabelVisible
    };
  })
};

let layers = [
  {
    id: 'agency',
    type: 'markers',
    label: 'Agency',
    modelName: 'agency-marker',
    isVisible: false,
    isActive: false, // does not respond to active context
    opacity: 1,
    types: [{
      id: 'location',
      label: 'Agency Location',
      style: 'location',
      isVisible: true
    }],
    labelDisplay: 'select',
    labels: []
  },
  {
    id: 'stops',
    type: 'markers',
    label: 'Stops',
    modelName: 'iq-stop',
    isVisible: true,
    isActive: true,
    opacity: 1,
    types: [{
      id: 'break',
      label: 'Break',
      valueKey: 'type',
      style: 'break',
      isVisible: true,
      iconPath: IconPaths.stops.break
    },{
      id: 'pick',
      label: 'Pickup',
      valueKey: 'bsStop.type',
      style: 'pick',
      isVisible: true,
      iconPath: IconPaths.stops.future
    }, {
      id: 'drop',
      label: 'Drop Off',
      valueKey: 'bsStop.type',
      style: 'drop',
      isVisible: true,
      iconPath: IconPaths.stops.future
    }],
    labelDisplay: 'select',
    labels: extractLabelsFromWidgetConfig(StopsWidgetConfig)
  },
  {
  id: 'routes',
  type: 'polylines',
  label: 'Routes',
  modelName: 'route',
  isVisible: true,
  isActive: true,
  opacity: 1,
  types: [
    {
      id: 'performed',
      label: 'Day of Service - Performed',
      style: 'solid',
      isVisible: false,
      iconPath: IconPaths.routes.performed,
      extraLegend: {
        label: 'w/ slack (no passenger in vehicle)',
        iconPath: IconPaths.routes.performedSlack,
      }
    },
    {
      id: 'planned',
      label: 'Day of Service - Planned',
      style: 'dotted',
      isVisible: true,
      iconPath: IconPaths.routes.planned,
      extraLegend: {
        label: 'w/ slack (no passenger in vehicle)',
        iconPath: IconPaths.routes.plannedSlack,
      }
    },
    {
      id: 'navigation',
      label: 'Navigation',
      style: 'dotted',
      isVisible: true
    }
  ],
  labelDisplay: 'select',
  labels: extractLabelsFromWidgetConfig(RoutesWidgetConfig)
},
{
  id: 'trips',
  type: 'polylines',
  label: 'Trips',
  modelName: 'iq-trip',
  isVisible: true,
  isActive: true,
  opacity: 1,
  types: [{
    id: 'Waitlist',
    label: 'Waitlist',
    valueKey: 'bsBooking.status',
    style: 'dotted',
    isVisible: true,
    iconPath: IconPaths.trips.planned
  }, {
    id: 'Scheduled',
    label: 'Scheduled',
    valueKey: 'bsBooking.status',
    style: 'solid',
    isVisible: true,
    iconPath: IconPaths.trips.performed
  }],
  labelDisplay: 'select',
  labels: extractLabelsFromWidgetConfig(TripsWidgetConfig)
}, {
  id: 'vehicles',
  type: 'markers',
  label: 'Vehicles',
  modelName: 'iq-vehicle',
  isVisible: true,
  isActive: true,
  opacity: 1,
  types: [{
    id: 'iq-vehicle',
    label: 'Vehicle',
    valueKey: 'constructor.modelName',
    style: 'vehicle',
    isVisible: true,
    iconPath: IconPaths.vehicles.atWork
  }],
  labelDisplay: 'select',
  labels: extractLabelsFromWidgetConfig(VehiclesWidgetConfig)
}];

export default layers;
