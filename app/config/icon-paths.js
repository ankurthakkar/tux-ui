export default {
  stops: {
    future: '/stops-icons/stop_unfinished.png',
    futureLate: '/stops-icons/stop_added_red.png',
    futureInDanger: '/stops-icons/stop_added_orange.png',
    next: '/stops-icons/stop_next.png',
    nextLate: '/stops-icons/stop_next_red.png',
    nextInDanger: '/stops-icons/stop_next_orange.png',
    completed: '/stops-icons/stop_done.png',
    break: '/stops-icons/break_next_fixed.png'
  },
  vehicles: {
    vehicle: '/vehicles-icons/car_icon.png',
    onBreak: '/vehicles-icons/car-on-break0.png',
    atWork: '/vehicles-icons/car-at-work.png'
  },
  tripArrow: {
    ontime: '/arrows-icons/arrow.svg',
    danger: '/arrows-icons/arrow-danger.svg',
    late: '/arrows-icons/arrow-late.svg'
  },
  tripArrowOffset: {
    ontime: '/arrows-icons/arrow_offset.svg',
    danger: '/arrows-icons/arrow_offset-danger.svg',
    late: '/arrows-icons/arrow_offset-late.svg'
  },
  routes: {
    planned: '/lines-icons/line-planned.png',
    plannedSlack: '/lines-icons/line-planned-slack.png',
    performed: '/lines-icons/line-performed.png',
    performedSlack: '/lines-icons/line-performed-slack.png',
  },
  trips: {
    planned: '/lines-icons/line-planned.png',
    performed: '/lines-icons/line-performed.png',
  }
}
