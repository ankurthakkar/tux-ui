import {
  nullFilter,
  emptyFilter,
  uuidEq,
  stringEq,
  stringNotEq,
  stringContains,
  booleanEq,
  booleanNe,
  numberEq,
  numberNotEq,
  numberLt,
  numberLte,
  numberGt,
  numberGte,
  numberBetween,
  numberStrictlyBetween,
  integerEq,
  dateEq,
  dateNe,
  dateGt,
  dateGte,
  dateLt,
  dateLte,
  dateBetween,
  dateStrictlyBetween,
} from './filter-types';

import {
  stringCompare,
  booleanCompare,
  numberCompare,
  dateCompare
} from './comparison-functions'

export const uuidColumnType = {
  id: 'uuid',
  filterTypes: [
    nullFilter,
    uuidEq,
    emptyFilter
  ],
  searchFilterType: uuidEq,
  compare: numberCompare
};

export const booleanColumnType = {
  id: 'boolean',
  filterTypes: [
    nullFilter,
    booleanEq,
    booleanNe,
    emptyFilter
  ],
  searchFilterType: booleanEq,
  compare: booleanCompare
};

export const stringColumnType = {
  id: 'string',
  filterTypes: [
    nullFilter,
    stringEq,
    stringNotEq,
    stringContains,
    emptyFilter
  ],
  searchFilterType: stringContains,
  compare: stringCompare
};

// alias for string
export const textColumnType = {
  id: 'text',
  filterTypes: stringColumnType.filterTypes,
  searchFilterType: stringColumnType.searchFilterType,
  compare: stringColumnType.compare
};

export const phoneColumnType = {
  id: 'phone',
  filterTypes: [
    nullFilter,
    stringEq,
    stringContains,
    emptyFilter
  ],
  searchFilterType: stringContains,
  compare: stringCompare
};

export const numberColumnType = {
  id: 'number',
  filterTypes: [
    nullFilter,
    numberEq,
    numberNotEq,
    numberLt,
    numberLte,
    numberGt,
    numberGte,
    numberBetween,
    numberStrictlyBetween,
    emptyFilter
  ],
  searchFilterType: numberEq,
  compare: numberCompare
};

export const integerColumnType = {
  id: 'integer',
  filterTypes: [
    nullFilter,
    integerEq,
    numberNotEq,
    numberLt,
    numberLte,
    numberGt,
    numberGte,
    numberBetween,
    numberStrictlyBetween,
    emptyFilter
  ],
  searchFilterType: integerEq,
  compare: numberCompare
};

export const floatColumnType = {
  id: 'float',
  filterTypes: [
    nullFilter,
    numberEq,
    numberNotEq,
    numberLt,
    numberLte,
    numberGt,
    numberGte,
    numberBetween,
    numberStrictlyBetween,
    emptyFilter
  ],
  searchFilterType: numberEq,
  compare: numberCompare
};

export const dateColumnType = {
  id: 'date',
  filterTypes: [
    nullFilter,
    dateEq,
    dateNe,
    dateLt,
    dateLte,
    dateGt,
    dateGte,
    dateBetween,
    dateStrictlyBetween,
    emptyFilter
  ],
  searchFilterType: dateEq,
  compare: dateCompare
};

// alias for date
export const dateTimeColumnType = {
  id: 'datetime',
  filterTypes: dateColumnType.filterTypes,
  searchFilterType: dateColumnType.searchFilterType,
  compare: dateColumnType.compare
};

// alias for date
export const timeColumnType = {
  id: 'time',
  filterTypes: dateColumnType.filterTypes,
  searchFilterType: dateColumnType.searchFilterType,
  compare: dateColumnType.compare
};

export const enumColumnType = {
  id: 'enum',
  filterTypes: [
    nullFilter,
    stringEq,
    stringNotEq,
    emptyFilter
  ],
  searchFilterType: dateEq,
  compare: stringCompare
};

export const columnTypes = [
  uuidColumnType,
  booleanColumnType,
  stringColumnType,
  textColumnType,
  phoneColumnType,
  numberColumnType,
  integerColumnType,
  floatColumnType,
  timeColumnType,
  dateColumnType,
  dateTimeColumnType,
  enumColumnType
];

export const columnTypesHash = columnTypes.reduce((obj, columnType) => {
  obj[columnType.id] = columnType;
  return obj;
}, {});

export default columnTypes;
