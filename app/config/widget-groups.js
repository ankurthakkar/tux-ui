import {
  messagesWidgetType,
  alertsWidgetType,
  routesWidgetType,
  stopsWidgetType,
  tripsWidgetType,
  driversWidgetType,
  vehiclesWidgetType,
  mapWidgetType,
  passengersWidgetType,
  boxWidgetType,
  metricsWidgetType,
  donutChartWidgetType,
  barChartWidgetType,
  gaugeWidgetType,
  box1WidgetType,
  box2WidgetType,
  box3WidgetType,
  box4WidgetType,
  settingsWidgetType,
  usersWidgetType,
  subscriptionsWidgetType
} from './widget-types'

export default [
  {
    id: 'widgets-1',
    title: 'Resources',
    widgetTypes: [
      passengersWidgetType,
      driversWidgetType,
      vehiclesWidgetType
    ]
  },
  {
    id: 'widgets-2',
    title: 'Work',
    widgetTypes: [
      mapWidgetType,
      messagesWidgetType,
      alertsWidgetType,
      routesWidgetType,
      tripsWidgetType,
      stopsWidgetType,
      subscriptionsWidgetType
    ]
  },
  {
    id: 'metrics',
    title: 'Metrics & Graphics',
    widgetTypes: [
      metricsWidgetType,
      gaugeWidgetType,
      boxWidgetType,
      donutChartWidgetType,
      barChartWidgetType
    ]
  },
  {
    id: 'config',
    title: 'Configuration',
    widgetTypes: [
      settingsWidgetType,
      usersWidgetType
    ]
  }
];
