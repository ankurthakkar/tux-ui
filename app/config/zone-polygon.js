// holds the zone polygon for walla walla
// used by zone-validation.js
export const zone_polygon = [
  [ 46.070180, -118.414163 ],
  [ 46.093803, -118.307092 ],
  [ 46.108669, -118.296996 ],
  [ 46.104824, -118.246489 ],
  [ 46.082908, -118.247049 ],
  [ 46.027789, -118.303751 ],
  [ 46.032074, -118.412725 ],
  [ 46.076793, -118.422319 ]
];


export default zone_polygon;
