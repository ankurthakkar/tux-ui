import {
  isNonEmptyString,
  isEncodedBoolean,
  isEncodedNumber,
  isEncodedDate,
  parseDate,
  parseBoolean,
  parseNumber,
  parseInteger,
  parseString
} from 'adept-iq/utils/encoding';

import {
  nullRQL,
  emptyRQL,
  eqRQL,
  neRQL,
  ltRQL,
  lteRQL,
  gtRQL,
  gteRQL,
  betweenRQL,
  strictlyBetweenRQL,
  inRQL,
  stringEqRQL,
  stringNeRQL,
  stringLikeRQL,
  dateGtRQL,
  dateGteRQL,
  dateLtRQL,
  dateLteRQL,
  dateEqRQL,
  dateNeRQL,
  dateBetweenRQL,
  dateStrictlyBetweenRQL
} from 'adept-iq/utils/rql-generators';

import {
  nullFn,
  emptyFn,
  eqFn,
  neFn,
  includesFn,
  ltFn,
  lteFn,
  gtFn,
  gteFn,
  betweenFn,
  strictlyBetweenFn,
  dateEqFn,
  dateNeFn,
  dateLtFn,
  dateLteFn,
  dateGtFn,
  dateGteFn,
  dateBetweenFn,
  dateStrictlyBetweenFn,
  stringEqIFn,
  stringNeIFn,
  stringIncludesIFn
} from 'adept-iq/utils/filter-functions';

export const nullFilter = {
  id: 'null',
  label: '&nbsp;',
  arity: '*',

  isValue: () => true,
  parseValue: (x) => x,

  fn: nullFn,
  rql: nullRQL
};

export const emptyFilter = {
  id: 'empty',
  label: 'Empty',
  arity: 0,

  isValue: () => true,
  parseValue: (x) => x,

  fn: emptyFn,
  rql: emptyRQL
};

export const uuidEq = {
  id: 'uuidEq',
  label: 'Equals',
  arity: 1,

  isValue: isNonEmptyString,
  parseValue: parseString,

  fn: eqFn,
  rql: eqRQL
};

export const uuidIn = {
  id: 'uuidIn',
  label: 'In',
  arity: '+',

  isValue: isNonEmptyString,
  parseValue: parseString,

  fn: includesFn,
  rql: inRQL
};

export const stringEq = {
  id: 'stringEq',
  label: '=',
  arity: 1,

  isValue: isNonEmptyString,
  parseValue: parseString,

  fn: stringEqIFn,
  rql: stringEqRQL
};

export const stringNotEq = {
  id: 'stringNotEq',
  label: '&ne;',
  arity: 1,

  isValue: isNonEmptyString,
  parseValue: parseString,

  fn: stringNeIFn,
  rql: stringNeRQL
};

export const stringContains = {
  id: 'stringContains',
  label: 'Contains',
  arity: 1,

  isValue: isNonEmptyString,
  parseValue: parseString,

  fn: stringIncludesIFn,
  rql: stringLikeRQL
};

export const booleanEq = {
  id: 'booleanEq',
  label: 'Is',
  arity: 1,

  isValue: isEncodedBoolean,
  parseValue: parseBoolean,

  fn: eqFn,
  rql: eqRQL  // TODO: double check
};

export const booleanNe = {
  id: 'booleanNe',
  label: 'Is not',
  arity: 1,

  isValue: isEncodedBoolean,
  parseValue: parseBoolean,

  fn: neFn,
  rql: neRQL  // TODO: double check
};

export const numberEq = {
  id: 'numberEq',
  label: '=',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: eqFn,
  rql: eqRQL
};

export const numberNotEq = {
  id: 'numberNotEq',
  label: '&ne;',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: neFn,
  rql: neRQL
};

export const numberLt = {
  id: 'numberLt',
  label: '&lt;',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: ltFn,
  rql: ltRQL
};

export const numberLte = {
  id: 'numberLte',
  label: '&le;',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: lteFn,
  rql: lteRQL
};

export const numberGt = {
  id: 'numberGt',
  label: '&gt;',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: gtFn,
  rql: gtRQL
};

export const numberGte = {
  id: 'numberGte',
  label: '&ge;',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: gteFn,
  rql: gteRQL
};

export const numberBetween = {
  id: 'numberBetween',
  label: 'Between',
  arity: 2,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: betweenFn,
  rql: betweenRQL
};

export const numberStrictlyBetween = {
  id: 'numberStrictlyBetween',
  label: 'Strictly Between',
  arity: 2,

  isValue: isEncodedNumber,
  parseValue: parseNumber,

  fn: strictlyBetweenFn,
  rql: strictlyBetweenRQL
};

export const integerEq = {
  id: 'integerEq',
  label: '=',
  arity: 1,

  isValue: isEncodedNumber,
  parseValue: parseInteger,

  fn: eqFn,
  rql: eqRQL
};

export const dateLt = {
  id: 'dateLt',
  label: 'Before',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateLtFn,
  rql: dateLtRQL
};

export const dateLte = {
  id: 'dateLte',
  label: 'At or Before',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateLteFn,
  rql: dateLteRQL
};

export const dateGt = {
  id: 'dateGt',
  label: 'After',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateGtFn,
  rql: dateGtRQL
};

export const dateGte = {
  id: 'dateGte',
  label: 'At or After',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateGteFn,
  rql: dateGteRQL
};

export const dateEq = {
  id: 'dateEq',
  label: '=',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateEqFn,
  rql: dateEqRQL,
};

export const dateNe = {
  id: 'dateNe',
  label: '&ne;',
  arity: 1,

  isValue: isEncodedDate,
  parseValue: parseDate,

  fn: dateNeFn,
  rql: dateNeRQL,
};

export const dateBetween = {
  id: 'dateBetween',
  label: 'Between',
  arity: 2,

  isValue: isEncodedDate,
  parseValue: parseDate,
  validateValues: ([t1, t2]) => t1.getTime() <= t2.getTime(),

  fn: dateBetweenFn,
  rql: dateBetweenRQL,
};

export const dateStrictlyBetween = {
  id: 'dateStrictlyBetween',
  label: 'Strictly Between',
  arity: 2,

  isValue: isEncodedDate,
  parseValue: parseDate,
  validateValues: ([t1, t2]) => t1.getTime() <= t2.getTime(),

  fn: dateStrictlyBetweenFn,
  rql: dateStrictlyBetweenRQL
};

export const filterTypes = [
  nullFilter,
  emptyFilter,
  uuidEq,
  uuidIn,
  stringEq,
  stringNotEq,
  stringContains,
  booleanEq,
  booleanNe,
  numberEq,
  numberNotEq,
  numberLt,
  numberLte,
  numberGt,
  numberGte,
  numberBetween,
  integerEq,
  dateEq,
  dateLt,
  dateLte,
  dateGt,
  dateGte,
  dateBetween,
  dateStrictlyBetween
];

export const filterTypesHash = filterTypes.reduce((acc, filter) => {
  acc[filter.id] = filter;
  return acc;
}, {});
