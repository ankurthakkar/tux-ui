
// this file is for "public" widget config required for listing available
// widget types and computing widget placements; most other config should be
// placed in the `config.js` adjacent to the specific widgets' components

// `name` reflects how the widget appears in a list (e.g. add-widget sidebar).
// For widget title bar, set `title` in widget's `config.js`.

// this would be a good place to specify icons for add-widget sidebar

export const messagesWidgetType = {
  id: 'messages',
  name: 'Messages',
  componentName: 'iq-widgets/message-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const settingsWidgetType = {
  id: 'settings',
  name: 'System',
  componentName: 'iq-widgets/edit-settings-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const routesWidgetType = {
  id: 'routes',
  name: 'Routes',
  componentName: 'iq-widgets/routes-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const stopsWidgetType = {
  id: 'stops',
  name: 'Stops',
  componentName: 'iq-widgets/stops-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const tripsWidgetType = {
  id: 'trips',
  name: 'Trips',
  componentName: 'iq-widgets/trips-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const mapWidgetType = {
  id: 'map',
  name: 'Map',
  componentName: 'iq-widgets/map-widget',

  defaultWidth: 3,
  defaultHeight: 3,
  minWidth: 2,
  minHeight: 2
};

export const subscriptionsWidgetType = {
  id: 'subscriptions',
  name: 'Subscriptions',
  componentName: 'iq-widgets/subscriptions-widget',

  defaultWidth: 4,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const passengersWidgetType = {
  id: 'passengers',
  name: 'Passenger',
  componentName: 'iq-widgets/passengers-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const driversWidgetType = {
  id: 'drivers',
  name: 'Drivers',
  componentName: 'iq-widgets/drivers-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const vehiclesWidgetType = {
  id: 'vehicles',
  name: 'Vehicles',
  componentName: 'iq-widgets/vehicles-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const boxWidgetType = {
  id: 'box',
  name: 'Box',
  componentName: 'iq-widgets/box-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const metricsWidgetType = {
  id: 'metrics',
  name: 'Metrics',
  componentName: 'iq-widgets/metrics-column-widget',

  defaultWidth: 8,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const donutChartWidgetType = {
  id: 'donut-chart',
  name: 'Donut Chart',
  componentName: 'iq-widgets/donut-chart-widget',

  defaultWidth: 3,
  defaultHeight: 2,
  minWidth: 3,
  minHeight: 2
};

export const barChartWidgetType = {
  id: 'bar-chart',
  name: 'Bar Chart',
  componentName: 'iq-widgets/bar-chart-widget',

  defaultWidth: 4,
  defaultHeight: 2,
  minWidth: 4,
  minHeight: 2
};

export const gaugeWidgetType = {
  id: 'gauge',
  name: 'Gauge',
  componentName: 'iq-widgets/gauge-widget',

  defaultWidth: 3,
  defaultHeight: 2,
  minWidth: 3,
  minHeight: 2
};

export const box1WidgetType = {
  id: 'box-1',
  name: 'Box 1',
  componentName: 'iq-widgets/box-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const box2WidgetType = {
  id: 'box-2',
  name: 'Box 2',
  componentName: 'iq-widgets/box-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const box3WidgetType = {
  id: 'box-3',
  name: 'Box 3',
  componentName: 'iq-widgets/box-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const box4WidgetType = {
  id: 'box-4',
  name: 'Box 4',
  componentName: 'iq-widgets/box-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const alertsWidgetType = {
  id: 'alerts',
  name: 'Alerts',
  componentName: 'iq-widgets/alert-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

export const usersWidgetType = {
  id: 'users',
  name: 'Users',
  componentName: 'iq-widgets/users-widget',

  defaultWidth: 1,
  defaultHeight: 2,
  minWidth: 1,
  minHeight: 1
};

const widgetTypes = [
  messagesWidgetType,
  settingsWidgetType,
  routesWidgetType,
  stopsWidgetType,
  tripsWidgetType,
  mapWidgetType,
  passengersWidgetType,
  driversWidgetType,
  vehiclesWidgetType,
  boxWidgetType,
  metricsWidgetType,
  donutChartWidgetType,
  barChartWidgetType,
  gaugeWidgetType,
  box1WidgetType,
  box2WidgetType,
  box3WidgetType,
  box4WidgetType,
  alertsWidgetType,
  usersWidgetType,
  subscriptionsWidgetType
];

export const widgetTypesHash = widgetTypes.reduce((obj, widgetType) => {
  obj[widgetType.id] = widgetType;
  return obj;
}, {});

export default widgetTypes;
