import { isNone, typeOf } from '@ember/utils';

export function nullCompare() {
  return 0;
}

export function stringCompare(a, b) {
  if (typeOf(a) !== 'string' && typeOf(b) !== 'string') return 0;
  if (typeOf(a) !== 'string') return 1;
  if (typeOf(b) !== 'string') return -1;
  return a.localeCompare(b);
}

export function numberCompare(a, b) {
  let aIsNone = isNone(a);
  let bIsNone = isNone(b);

  // can't compare two null things
  if (aIsNone && bIsNone) return 0;

  // null things come after non-null
  if (aIsNone) return 1;
  if (bIsNone) return -1;

  let x = parseFloat(a);
  let y = parseFloat(b);
  let xIsNaN = isNaN(x);
  let yIsNaN = isNaN(y);

  // can't compare two non-numeric things
  if (xIsNaN && yIsNaN) return 0;

  // non-numeric things come after numeric things
  if (xIsNaN) return 1;
  if (yIsNaN) return -1;

  return x - y;
}

export function booleanCompare(a, b) {
  if (typeOf(a) !== 'boolean' && typeOf(b) !== 'boolean') return 0;
  if (typeOf(a) !== 'boolean') return 1;
  if (typeOf(b) !== 'boolean') return -1;
  if ((a && b) || (!a && !b)) return 0;
  if (a) return -1;
  return 1;
}

export function dateCompare(a, b) {
  if (typeOf(a) !== 'date' && typeOf(b) !== 'date') return 0;
  if (typeOf(a) !== 'date') return 1;
  if (typeOf(b) !== 'date') return -1;
  return a.getTime() - b.getTime();
}

export default [
  nullCompare,
  stringCompare,
  numberCompare,
  booleanCompare,
  dateCompare
];
