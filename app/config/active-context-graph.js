// used to traverse local data model
export const dsRouteNode = {
  id: 'ds-route',
  modelName: 'route',
  links: [
    {
      type: 'hasMany',
      nodeId: 'ds-cluster',
      path: 'clusters'
    },
    {
      type: 'hasMany',
      nodeId: 'ds-vehicle-route',
      path: 'vehicleRoutes'
    }
  ],
  isActive: true,
  leftTimeConstraint: 'plannedStartTime',
  rightTimeConstraint: 'plannedEndTime'
};

export const dsClusterNode = {
  id: 'ds-cluster',
  modelName: 'cluster',
  links: [
    {
      type: 'belongsTo',
      nodeId: 'ds-route',
      path: 'route'
    },
    {
      type: 'hasMany',
      nodeId: 'ds-trip',
      path: 'trips'
    }
  ],
  isActive: true,
  leftTimeConstraint: 'startTime',
  rightTimeConstraint: 'endTime'
};

export const dsTripNode = {
  id: 'ds-trip',
  modelName: 'trip',
  links: [
    {
      type: 'belongsTo',
      nodeId: 'ds-cluster',
      path: 'cluster'
    },
    {
      type: 'hasMany',
      nodeId: 'ds-stop',
      path: 'stops'
    },
    {
      type: 'belongsTo',
      nodeId: 'ds-rider',
      path: 'rider'
    },
    {
      type: 'belongsTo',
      nodeId: 'iq-trip',
      path: 'iqTrip'
    }
  ],
  isActive: true,
  leftTimeConstraint: 'requestedTime',
  rightTimeConstraint: 'requestedTime'
};

export const dsStopNode = {
  id: 'ds-stop',
  modelName: 'stop',
  links: [
    {
      type: 'belongsTo',
      nodeId: 'ds-trip',
      path: 'trip'
    }, {
      type: 'belongsTo',
      nodeId: 'iq-stop',
      path: 'iqStop'
    }
  ],
  isActive: true,
  leftTimeConstraint: 'trip.requestedTime',
  rightTimeConstraint: 'trip.requestedTime'
};

export const dsDriverNode = {
  id: 'ds-driver',
  modelName: 'driver',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-driver',
    path: 'iqDriver'
  }, {
    type: 'hasMany',
    nodeId: 'ds-driver-shift',
    path: 'driverShifts'
  }]
};

export const dsDriverShiftNode = {
  id: 'ds-driver-shift',
  modelName: 'driver-shift',
  links: [{
    type: 'belongsTo',
    nodeId: 'ds-driver',
    path: 'driver'
  }, {
    type: 'belongsTo',
    nodeId: 'ds-vehicle-route',
    path: 'vehicleRoute'
  }],
  isActive: true,
  leftTimeConstraint: 'plannedStartTime',
  rightTimeConstraint: 'plannedEndTime'
};

export const dsVehicleNode = {
  id: 'ds-vehicle',
  modelName: 'vehicle',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-vehicle',
    path: 'iqVehicle'
  }, {
    type: 'hasMany',
    nodeId: 'ds-vehicle-route',
    path: 'vehicleRoutes'
  }]
};

export const dsVehicleRouteNode = {
  id: 'ds-vehicle-route',
  modelName: 'vehicle-route',
  links: [{
    type: 'belongsTo',
    nodeId: 'ds-route',
    path: 'route'
  }, {
    type: 'belongsTo',
    nodeId: 'ds-vehicle',
    path: 'vehicle'
  }, {
    type: 'hasMany',
    nodeId: 'ds-driver-shift',
    path: 'driverShifts'
  }],
  isActive: true,
  leftTimeConstraint: 'plannedStartTime',
  rightTimeConstraint: 'plannedEndTime'
};

export const dsRiderNode = {
  id: 'ds-rider',
  modelName: 'rider',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-rider',
    path: 'iqRider'
  },
  {
    type: 'hasMany',
    nodeId: 'ds-trip',
    path: 'trips'
  }]
};

export const ssDriverNode = {
  id: 'ss-driver',
  modelName: 'ss-driver',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-driver',
    path: 'iqDriver'
  }]
};

export const ssVehicleNode = {
  id: 'ss-vehicle',
  modelName: 'ss-vehicle',
  // this isn't used yet, but it will be when we expand initial loader
  includes: [
    'startGarage',
    'endGarage',
    'vehicleType',
    'vehicleType.vehicleCapacityConfigs',
    'vehicleType.vehicleCapacityConfigs.vehicleCapacityType',
    'availability'
  ],
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-vehicle',
    path: 'iqVehicle'
  }]
};

export const bsBookingNode = {
  id: 'bs-booking',
  modelName: 'bs-booking',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-trip',
    path: 'iqTrip'
  }, {
    type: 'hasMany',
    nodeId: 'bs-leg',
    path: 'legs'
  }, {
    // this is a hack for streaming; better to go through `legs`
    type: 'hasMany',
    nodeId: 'bs-stop',
    path: 'stops'
  }, {
    type: 'belongsTo',
    nodeId: 'bs-rider',
    path: 'rider'
  }],
  isActive: true,
  leftTimeConstraint: 'requestedTime',
  rightTimeConstraint: 'requestedTime'
};

export const bsLegNode = {
  id: 'bs-leg',
  modelName: 'bs-leg',
  links: [{
    type: 'belongsTo',
    nodeId: 'bs-booking',
    path: 'booking'
  }, {
    type: 'hasMany',
    nodeId: 'bs-segment',
    path: 'segments'
  }],
  isActive: true,
  leftTimeConstraint: 'requestTime',
  rightTimeConstraint: 'requestTime'
};

export const bsSegmentNode = {
  id: 'bs-segment',
  modelName: 'bs-segment',
  links: [{
    type: 'belongsTo',
    nodeId: 'bs-leg',
    path: 'leg'
  }, {
    type: 'hasMany',
    nodeId: 'bs-stop',
    path: 'stops'
  }],
  isActive: true,
  leftTimeConstraint: 'promiseTime',
  rightTimeConstraint: 'promiseTime'
};

export const bsStopNode = {
  id: 'bs-stop',
  modelName: 'bs-stop',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-stop',
    path: 'iqStop'
  }, {
    // this is a hack for streaming; better to go through `segment`
    type: 'belongsTo',
    nodeId: 'bs-booking',
    path: 'booking'
  }, {
    type: 'belongsTo',
    nodeId: 'bs-segment',
    path: 'segment'
  }],
  isActive: true,
  leftTimeConstraint: 'booking.requestedTime',
  rightTimeConstraint: 'booking.requestedTime'
};

export const bsRiderNode = {
  id: 'bs-rider',
  modelName: 'bs-rider',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-rider',
    path: 'iqRider'
  }, {
    type: 'hasMany',
    nodeId: 'bs-booking',
    path: 'bookings'
  }]
};

export const bsSubscriptionNode = {
  id: 'bs-subscription',
  modelName: 'bs-subscription'
};

export const rmsRiderNode = {
  id: 'rms-rider',
  modelName: 'rms-rider',
  links: [{
    type: 'belongsTo',
    nodeId: 'iq-rider',
    path: 'iqRider'
  }]
};

export const ssoUserNode = {
  id: 'sso-user',
  modelName: 'sso-user'
};

export const tmCannedMessageNode = {
  id: 'tm-canned-message',
  modelName: 'tm-canned-message',
  links: [{
    type: 'belongsTo',
    nodeId: 'tm-driver',
    path: 'driver'
  }],
  isActive: true,
  leftTimeConstraint: 'createdTime',
  rightTimeConstraint: 'createdTime'
};

export const tmDriverNode = {
  id: 'tm-driver',
  modelName: 'tm-driver',
  links: [{
    type: 'hasMany',
    nodeId: 'tm-canned-message',
    path: 'cannedMessages'
  }, {
    type: 'belongsTo',
    nodeId: 'iq-driver',
    path: 'iqDriver'
  }]
};

export const iqDriverNode = {
  id: 'iq-driver',
  modelName: 'iq-driver',
  links: [{
    // TODO: change to `belongsTo` when data is fixed
    type: 'hasMany',
    nodeId: 'tm-driver',
    path: 'tmDrivers'
  },
  {
    type: 'belongsTo',
    nodeId: 'ss-driver',
    path: 'ssDriver'
  },
  {
    type: 'belongsTo',
    nodeId: 'ds-driver',
    path: 'dsDriver'
  }]
};

export const iqVehicleNode = {
  id: 'iq-vehicle',
  modelName: 'iq-vehicle',
  links: [{
    type: 'belongsTo',
    nodeId: 'ds-vehicle',
    path: 'dsVehicle',
  },
  {
    type: 'belongsTo',
    nodeId: 'ss-vehicle',
    path: 'ssVehicle',
  }]
};

export const iqRiderNode = {
  id: 'iq-rider',
  modelName: 'iq-rider',
  links: [{
    type: 'belongsTo',
    nodeId: 'ds-rider',
    path: 'dsRider',
  }, {
    type: 'belongsTo',
    nodeId: 'bs-rider',
    path: 'bsRider'
  }, {
    type: 'belongsTo',
    nodeId: 'rms-rider',
    path: 'rmsRider',
  }]
};

export const iqTripNode = {
  id: 'iq-trip',
  modelName: 'iq-trip',
  links: [{
    type: 'belongsTo',
    nodeId: 'ds-trip',
    path: 'dsTrip'
  }, {
    type: 'belongsTo',
    nodeId: 'bs-booking',
    path: 'bsBooking'
  }],
  isActive: true,
  leftTimeConstraint: 'bsBooking.requestedTime',
  rightTimeConstraint: 'bsBooking.requestedTime'
};

export const iqStopNode = {
  id: 'iq-stop',
  modelName: 'iq-stop',
  links: [{
    type: 'belongsTo',
    nodeId: 'bs-stop',
    path: 'bsStop'
  }, {
    type: 'belongsTo',
    nodeId: 'ds-stop',
    path: 'dsStop'
  }],
  isActive: true,
  leftTimeConstraint: 'bsStop.booking.requestedTime',
  rightTimeConstraint: 'bsStop.booking.requestedTime'
};

export const activeContextNodes = [
  // dispatch graph
  dsRouteNode,
  dsClusterNode,
  dsTripNode,
  dsStopNode,
  dsDriverNode,
  dsDriverShiftNode,
  dsVehicleNode,
  dsVehicleRouteNode,
  dsRiderNode,

  // scheduling graph
  ssDriverNode,
  ssVehicleNode,

  // trip management graph
  tmCannedMessageNode,
  tmDriverNode,

  // rider management graph
  rmsRiderNode,

  // sign-on
  ssoUserNode,

  // unification nodes
  iqTripNode,
  iqStopNode,
  iqDriverNode,
  iqVehicleNode,
  iqRiderNode,

  // booking service graph
  bsBookingNode,
  bsLegNode,
  bsSegmentNode,
  bsStopNode,
  bsRiderNode,
  bsSubscriptionNode
];

export const activeContextGraph = activeContextNodes.reduce((obj, node) => {
  obj[node.id] = node;
  return obj;
}, {});

export default activeContextGraph;
