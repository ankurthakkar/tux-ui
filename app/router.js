import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('register');
  this.route('login');
  this.route('forgot-password');

  this.route('dashboard', { path: '/dashboard/:id' }, function() {
    this.route('modals', function() {
      this.route('edit-form');
    });
  });

  this.route('maximized');

  this.route('index', { path: '/' }, function() {
    this.route('modals', function() {
      this.route('create-dashboard');
      this.route('settings');
    });
  });

  this.route('terms-of-service');
});

export default Router;
