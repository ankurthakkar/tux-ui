export default function (records) {
  return Promise.all(records.map(record => record.save()));
}
