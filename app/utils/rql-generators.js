import moment from 'moment';

// compound RQL generators
export const andRQL = (...args) => `and(${args.join(',')})`;
export const orRQL = (...args) => `or(${args.join(',')})`;

// elementary RQL generators
export const nullRQL = () => null;
export const emptyRQL = (p) => `eq(${p},null)`;

export const eqRQL = (p, [v]) => `eq(${p},${v})`;
export const neRQL = (p, [v]) => `ne(${p},${v})`;
export const ltRQL = (p, [v]) => `lt(${p},${v})`;
export const lteRQL = (p, [v]) => `le(${p},${v})`;
export const gtRQL = (p, [v]) => `gt(${p},${v})`;
export const gteRQL = (p, [v]) => `ge(${p},${v})`;
export const betweenRQL = (p, [v, w]) => andRQL(gteRQL(p, [v]), lteRQL(p, [w]));

export const strictlyBetweenRQL = (p, [v, w]) => {
  return andRQL(gtRQL(p, [v]), ltRQL(p, [w]));
};

export const inRQL = (p, values) => {
  return `in(${p},(${values.join(',')}))`;
};

// quoted values! API will break if types don't match
export const stringEqRQL = (p, [v]) => `eq(${p},'${v}')`;
export const stringNeRQL = (p, [v]) => `ne(${p},'${v}')`;
export const stringLtRQL = (p, [v]) => `lt(${p},'${v}')`;
export const stringGtRQL = (p, [v]) => `gt(${p},'${v}')`;
export const stringLikeRQL = (p, [v]) => `ilike(${p},'%${v}%')`;  // need %...%
export const stringBetweenRQL = (p, [v, w]) => {
  return andRQL(stringGtRQL(p, [v]), stringLtRQL(p, [w]));
};

export const dateGtRQL = (p, [v]) => `gt(${p},'${moment(v).format()}')`;
export const dateGteRQL = (p, [v]) => `ge(${p},'${moment(v).format()}')`;
export const dateLtRQL = (p, [v]) => `lt(${p},'${moment(v).format()}')`;
export const dateLteRQL = (p, [v]) => `le(${p},'${moment(v).format()}')`;
export const dateEqRQL = (p, [v]) => `eq(${p},'${moment(v).format()}')`;
export const dateNeRQL = (p, [v]) => `ne(${p},'${moment(v).format()}')`;

export const dateBetweenRQL = (p, [v, w]) => {
  return andRQL(dateGteRQL(p, [v]), dateLteRQL(p, [w]));
};

export const dateStrictlyBetweenRQL = (p, [v, w]) => {
  return andRQL(dateGtRQL(p, [v]), dateLtRQL(p, [w]));
};
