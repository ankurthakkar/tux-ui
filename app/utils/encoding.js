import { typeOf, isPresent } from '@ember/utils';
import moment from 'moment';

const TIME_PARSING_FORMATS = ['h:mm A', 'HH:mm'];

// strict type validations
const isString = (s) => typeOf(s) === 'string';
const isNumber = (x) => typeOf(x) === 'number' && !isNaN(x);
const isDate = (date) => {
  let prototypeString = Object.prototype.toString.call(date);
  return prototypeString === '[object Date]' && !isNaN(date);
};

// used to check if a user-supplied string represents a valid filter value
const isNonEmptyString = (s) => isString(s) && isPresent(s);
const isEncodedBoolean = (s) => s === 'true' || s === 'false';
const isEncodedNumber = (s) => isNonEmptyString(s) && !isNaN(parseFloat(s));
const isEncodedDate = (s) => isNonEmptyString(s) && moment(s).isValid();
const isEncodedTime = (s) => {
  return isNonEmptyString(s) && moment(s, TIME_PARSING_FORMATS).isValid();
};

// use to coerce a string to the given type
const parseString = (str) => str;
const parseNumber = (str) => parseFloat(str);
const parseInteger = (str) => parseInt(str);
const parseBoolean = (str) => str === 'true';
const parseDate = (str) => moment(str).toDate();
const parseTime = (str) => moment(str, TIME_PARSING_FORMATS).toDate();

export {
  isNumber,
  isString,
  isDate,
  isNonEmptyString,
  isEncodedBoolean,
  isEncodedNumber,
  isEncodedDate,
  isEncodedTime,
  parseString,
  parseBoolean,
  parseNumber,
  parseInteger,
  parseDate,
  parseTime
};
