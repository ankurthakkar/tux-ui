export function breadthFirstSearch(params) {
  let {
    queue = [],
    visited = [],
    getNeighbours,
    visitNode
  } = params;

  while (queue.length > 0) {
    let v = queue.shift();
    if (visitNode) visitNode(v);
    visited.push(v);

    getNeighbours(v)
      .reject((w) => visited.includes(w))
      .forEach((w) => queue.push(w))
  }
}
