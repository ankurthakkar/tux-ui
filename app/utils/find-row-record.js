import { isBlank } from '@ember/utils';

export function findRowRecordByElement(targetElement, tableRows) {
  let rowElement = findRowElement(targetElement);
  if (isBlank(rowElement)) {
    return;
  }

  let node = rowElement.attributes.getNamedItem('data-row-id');
  if (isBlank(node)) {
    return;
  }
  return tableRows.findBy('rowId', node.value);
}

export function findRowElement(targetElement) {
  while (targetElement.tagName.toLowerCase() !== 'tr' && targetElement.tagName.toLowerCase() !== 'body') {
    targetElement = targetElement.parentElement;
  }

  return targetElement.tagName.toLowerCase() === 'tr' ? targetElement : null;
}
