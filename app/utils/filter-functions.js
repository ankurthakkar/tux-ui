import { typeOf, isBlank } from '@ember/utils';

export const nullFn = () => true;
export const emptyFn = isBlank;

export const eqFn = (a, [b]) => a === b;
export const neFn = (a, [b]) => a !== b;

export const includesFn = (a, [b]) => {
  if (!a || !a.includes || !b) return false;
  return a.includes(b);
}

export const ltFn = (a, [b]) => a < b;
export const lteFn = (a, [b]) => a <= b;
export const gtFn = (a, [b]) => a > b;
export const gteFn = (a, [b]) => a >= b;
export const betweenFn = (a, [b, c]) => b <= a && a <= c;
export const strictlyBetweenFn = (a, [b, c]) => b < a && a < c;

export const dateEqFn = (a, [b]) => a && b && a.getTime() === b.getTime();
export const dateNeFn = (a, [b]) => a && b && a.getTime() !== b.getTime();
export const dateLtFn = (a, [b]) => a && b && a.getTime() < b.getTime();
export const dateLteFn = (a, [b]) => a && b && a.getTime() <= b.getTime();
export const dateGtFn = (a, [b]) => a && b && a.getTime() > b.getTime();
export const dateGteFn = (a, [b]) => a && b && a.getTime() >= b.getTime();

export const dateBetweenFn = (t, [t0, t1]) => {
  return dateGteFn(t, [t0]) && dateLteFn(t, [t1]);
};

export const dateStrictlyBetweenFn = (t, [t0, t1]) => {
  return dateGtFn(t, [t0]) && dateLtFn(t, [t1]);
};

// case-insensitive string functions
export const stringEqIFn = (a, [b]) => {
  if (typeOf(a) !== 'string' || typeOf(b) !== 'string') return false;
  return eqFn(a.toLowerCase(), [b.toLowerCase()]);
};

export const stringNeIFn = (a, [b]) => {
  if (typeOf(a) !== 'string' || typeOf(b) !== 'string') return false;
  return neFn(a.toLowerCase(), [b.toLowerCase()]);
};

export const stringIncludesIFn = (a, [b]) => {
  if (typeOf(a) !== 'string' || typeOf(b) !== 'string') return false;
  return includesFn(a.toLowerCase(), [b.toLowerCase()]);
}
