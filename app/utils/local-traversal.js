import { isEmpty } from '@ember/utils';
import { makeArray } from '@ember/array';
import { breadthFirstSearch } from './graph';

import {
  activeContextGraph,
  activeContextNodes
} from 'adept-iq/config/active-context-graph';

const unwrapPossibleProxy = (possibleProxy) => {
  if (!possibleProxy) return null;
  if (possibleProxy.hasOwnProperty('content')) return possibleProxy.content;
  return possibleProxy;
};

// helper for converting flattened data set to a structured record set
const makeRecordSet = (data) => {
  return data.reduce((obj, { modelName, record }) => {
    let arr = obj[modelName];
    if (!arr) {
      arr = [];
      obj[modelName] = arr;
    }
    arr.push(record);
    return obj;
  }, {});
};

/**
 * Computes the Active Context graph induced by a given selection of entities.
 * @param {RecordSet} allData - the complete data set
 * @param {Object[]} selectedData - the entities that have been "checked"
 */
export function computeActiveContext(allData, selectedData) {
  let selectedRecordSet = makeRecordSet(selectedData);

  // compute induced subgraphs of each entity type group
  let recordSets = Object.keys(selectedRecordSet).map((modelName) => {
    let modelNode = activeContextNodes.findBy('modelName', modelName);

    let recordSet = {};

    // for *full* active context, we seed with all data of this type
    recordSet[modelName] = allData[modelName];

    let queue = [];

    makeArray(modelNode.links).forEach((link) => {
      // get related records of _selected_ items only
      let records = getRelatedRecords(selectedRecordSet, modelName, link);

      let linkModelName = activeContextGraph[link.nodeId].modelName;
      recordSet[linkModelName] = records;

      let node = activeContextGraph[link.nodeId];
      queue.push(node);
    });

    let visited = [modelNode];

    // push related records as far as they will go
    let expandedRecordSet = expandGraph(recordSet, queue, visited);

    // zero out any unvisited nodes *in this graph component*
    breadthFirstSearch({
      queue,
      visited,
      getNeighbours: ({ links }) => {
        return makeArray(links).map(({ nodeId }) => activeContextGraph[nodeId]);
      },
      visitNode: ({ modelName }) => {
        expandedRecordSet[modelName] = expandedRecordSet[modelName] || [];
      }
    });

    return expandedRecordSet;
  });

  let intersection = intersectRecordSets(allData, ...recordSets);

  // restore any unconstrained model types; this is necessary when an isolated
  // graph component has no selected items
  activeContextNodes.forEach(({ modelName }) => {
    if (intersection[modelName]) return;
    intersection[modelName] = allData[modelName];
  });

  // add the selected records back in where missing
  let union = unionRecordSets(intersection, selectedRecordSet);
  return union;
}

/**
 * Computes the Implicit Context graph induced by a given selection of entities.
 * @param {RecordSet} allData - the complete data set
 * @param {Object[]} selectedData - the entities that have been "checked"
 */
export function computeImplicitContext(allData, selectedData) {
  // implicit context is empty until something explicitly selected
  if (isEmpty(selectedData)) return {};

  let selectedRecordSet = makeRecordSet(selectedData);

  // compute induced subgraphs of each entity type group
  let recordSets = Object.keys(selectedRecordSet).map((modelName) => {
    let modelNode = activeContextNodes.findBy('modelName', modelName);

    let recordSet = {};

    // for *implicit* data, we seed with only the selected records; this results
    // in a smaller data set when only one type is selected
    recordSet[modelName] = selectedRecordSet[modelName];

    let queue = [];

    makeArray(modelNode.links).forEach((link) => {
      // get related records of _selected_ items only
      let records = getRelatedRecords(selectedRecordSet, modelName, link);

      let linkModelName = activeContextGraph[link.nodeId].modelName;
      recordSet[linkModelName] = records;

      let node = activeContextGraph[link.nodeId];
      queue.push(node);
    });

    let visited = [modelNode];

    // push related records as far as they will go
    let expandedRecordSet = expandGraph(recordSet, queue, visited);

    // zero out any unvisited nodes *in this graph component*
    breadthFirstSearch({
      queue,
      visited,
      getNeighbours: ({ links }) => {
        return makeArray(links).map(({ nodeId }) => activeContextGraph[nodeId]);
      },
      visitNode: ({ modelName }) => {
        expandedRecordSet[modelName] = expandedRecordSet[modelName] || [];
      }
    });

    return expandedRecordSet;
  });

  let intersection = intersectRecordSets(allData, ...recordSets);

  // restore any unconstrained model types; this is necessary when an isolated
  // graph component has no selected items
  activeContextNodes.forEach(({ modelName }) => {
    if (intersection[modelName]) return;
    intersection[modelName] = allData[modelName];
  });

  // add the selected records back in where missing
  let union = unionRecordSets(intersection, selectedRecordSet);

  return union;
}

/**
 * Traverses the domain relationship graph; this is not a true BFS!
 * @param {RecordSet} recordSet - the set of records to augment
 * @param {ModelNode[]} q - queue of model nodes to visit
 * @param {ModelNode[]} blackList - model nodes to never visit
 */
const expandGraph = (recordSet={}, q=[], blackList=[]) => {
  // don't mutate input queue
  q = q.slice();

  // for each node, keep track of which nodes we came from
  let predecessors = {};

  while (q.length > 0) {
    let sourceNode = q.shift();
    if (blackList.includes(sourceNode)) continue;

    makeArray(sourceNode.links).forEach((link) => {
      let targetNode = activeContextGraph[link.nodeId];

      // don't backtrack across hasManys
      if (link.type === 'hasMany') {
        let ids = predecessors[sourceNode.id] || [];
        if (ids.includes(targetNode.id)) return;
      }

      let records = getRelatedRecords(recordSet, sourceNode.modelName, link);
      let count = augmentRecordSet(recordSet, targetNode.modelName, records);

      // only [re-]visit target if we actually added something new
      if (count > 0) {
        q.addObject(targetNode);

        predecessors[targetNode.id] = predecessors[targetNode.id] || [];
        predecessors[targetNode.id].push(sourceNode.id);
      }
    });
  }

  return recordSet;
};

const getRelatedRecords = (recordSet, modelName, link) => {
  return recordSet[modelName].reduce((arr, record) => {
    let relation = unwrapPossibleProxy(record.get(link.path));
    if (!relation) return arr;

    switch (link.type) {
      case 'belongsTo': {
        arr.push(relation);
        break;
      }
      case 'hasMany':
        relation.forEach((record) => {
          record = unwrapPossibleProxy(record);
          if (record) {
            arr.push(record);
          }
        });
        break;
      default:
        throw `unknown relationship type '${link.type}'`;
    }

    return arr;
  }, []);
};

/**
 * Safely adds records of a single type to a record set in place.
 * @param {RecordSet} recordSet
 * @param {string} modelName
 * @param {Array} records
 * @returns {Boolean} whether or not records were added
 */

const augmentRecordSet = (recordSet, modelName, records) => {
  let collection = recordSet[modelName];
  if (!collection) {
    collection = [];
    recordSet[modelName] = collection;
  }

  let lengthBefore = collection.length;

  records.forEach((record) => {
    collection.addObject(record);
  });

  return collection.length - lengthBefore;
};

/**
 * Computes the union of two or more record sets.
 * @param {RecordSet[]} recordSets - record sets to combine
 */
const unionRecordSets = function mergeRecordSets(...recordSets) {
  return recordSets.reduce((obj, recordSet) => {
    Object.entries(recordSet).forEach(([modelName, records]) => {
      let collection = obj[modelName];
      if (!collection) {
        collection = [];
        obj[modelName] = collection;
      }

      if (isEmpty(records)) return;

      records.forEach((record) => {
        if (collection.includes(record)) return;
        collection.push(record);
      });
    });
    return obj;
  }, {});
};

/**
 * Computes the intersection of two record sets.
 * @param {RecordSet[]} recordSets - record sets to intersect
 */
const intersectRecordSets = function intersectRecordSets(...recordSets) {
  return recordSets.reduce((obj, recordSet, i) => {
    if (i === 0) {
      Object.entries(recordSet).forEach(([modelName, records]) => {
        obj[modelName] = records.slice();
      });
      return obj;
    }

    Object.entries(recordSet).forEach(([modelName, records]) => {
      let collection = obj[modelName];
      if (!collection) {
        collection = [];
        obj[modelName] = collection;
      }

      // find existing records that are not in the new set
      let missingRecords = collection.reduce((arr, record) => {
        if (records.includes(record)) return arr;
        arr.push(record);
        return arr;
      }, []);

      // remove those from the existing set
      missingRecords.forEach((record) => {
        let index = collection.indexOf(record);
        collection.splice(index, 1);
      });
    });

    return obj;
  }, {});
};
