import { isEmpty } from '@ember/utils';
import { nullCompare } from 'adept-iq/config/comparison-functions';

export function buildCompareFunction(params) {
  if (isEmpty(params)) return nullCompare;

  return (a, b) => {
    return params.reduce((acc, { path, asc, compare }) => {
      if (acc !== 0) return acc;
      let sign = asc ? 1 : -1;
      return sign * compare(a.get(path), b.get(path));
    }, 0);
  };
}
