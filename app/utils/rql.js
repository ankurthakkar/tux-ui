import { isPresent, isEmpty, typeOf } from '@ember/utils';
import { filterTypesHash } from 'adept-iq/config/filter-types';

export const buildQueryParams = ({ sorts, filter, includes, page }) => {
  let query = {};

  if (!isEmpty(sorts)) {
    query.sort = renderSortsRQL(sorts);
  }

  if (filter) {
    let filterRQL = renderFilterRQL(filter);
    if (filterRQL) {
      query.filter = filterRQL;
    }
  }

  if (!isEmpty(includes)) {
    query.include = renderIncludesRQL(includes);
  }

  if (isPresent(page)) {
    query['page[offset]'] = page.offset;
    query['page[limit]'] = page.limit;
  }

  return query;
};

const renderSortsRQL = (sorts) => {
  let rqlFragments = sorts.map(({ path, asc }) => {
    let sign = asc ? '+' : '-';
    return `${sign}${path}`;
  });

  return rqlFragments.uniq().join(',');
};

const renderFilterRQL = (node) => {
  if (!node) return null;

  // allow pre-rendered fragments
  if (typeOf(node) === 'string') return node;

  switch(node.type) {
    case 'and':
    case 'or':
      return renderCompoundFilter(node.type, node.args);
    default: {
      let { rql } = filterTypesHash[node.type];
      return rql(node.rql.path, node.rql.values);
    }
  }
};

const renderCompoundFilter = (type, args) => {
  if (!args) return null;

  let renderedArgs = args.map(renderFilterRQL).compact();

  if (renderedArgs.length === 0) return null;
  if (renderedArgs.length === 1) return renderedArgs[0];

  return `${type}(${renderedArgs.join(',')})`;
};

const renderIncludesRQL = (includes) => {
  // TODO: handle configurable inner joins
  return includes.sort().uniq().join(',');
};
