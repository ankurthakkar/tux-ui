import EmberObject from '@ember/object';
import { get, computed } from '@ember/object';

export default EmberObject.extend({
  cache: null,
  nodes: null,

  init() {
    this._super(...arguments);
    this.set('cache', {});
    this.set('nodes', []);
  },

  clear() {
    this.join([]);

    let cache = this.get('cache');
    Object.entries(cache).forEach(([key, node]) => {
      delete cache[key];
      this.release(node);
    });
  },

  // override initialize, enter, update, and exit with custom behaviour
  initialize: (data) => data,
  enter(/* node, i */) {},
  update(/* node, data, i */) {},
  exit(/* node */) {},
  release(/* node */) {},
  keyForNode: (node) => get(node, 'id'),
  keyForData: (data) => get(data, 'id'),

  join(collection) {
    let cache = this.get('cache');
    let nodes = this.get('nodes');
    let oldKeys = nodes.map(this.keyForNode);

    // build lookup tables
    let wasInNodes = {}
    for (let i = 0; i < nodes.length; i++) {
      wasInNodes[this.keyForNode(nodes[i])] = true;
    }

    let isInCollection = {};
    for (let i = 0; i < collection.length; i++) {
      isInCollection[this.keyForNode(collection[i])] = true;
    }

    // exit outgoing nodes first
    for (let i = nodes.length - 1; i >= 0; i--) {
      let node = nodes[i];
      let key = this.keyForNode(node);
      if (!isInCollection[key]) {
        nodes.splice(i, 1);
        this.exit(node);
      }
    }

    collection.forEach((data, i) => {
      let key = this.keyForData(data);
      let node = cache[key];

      if (!node) {
        node = this.initialize(data);
        cache[key] = node;
      }

      if (wasInNodes[key]) {
        this.update(node, data, i);

        if (i !== oldKeys.indexOf(key)) {
          nodes.splice(i, 1, node);
        }

        return;
      }

      this.enter(node, i);

      if (i < nodes.length) {
        let oldNode = nodes[i];
        let oldKey = this.keyForNode(oldNode);

        if (key !== oldKey) {
          nodes.splice(i, 1, node);
        }

        return;
      }

      nodes.push(node);
    });

    return this;
  }
});
