import { get, getProperties } from '@ember/object';
import { isEmpty } from '@ember/utils';
import moment from 'moment';

export  function isRouteAssignable(route, time) {

    const vehicleRoutes = get(route, 'vehicleRoutes');

    if(get(route,('assignedVehicle')) == null || get(route,('assignedVehicle')) === undefined) {
      return false;
    }

    if(get(route,('assignedDriver')) == null || get(route,('assignedDriver')) === undefined) {
      return false;
    }

    if(route.get('clusters') === null ||
      route.get('clusters') === undefined ||
      route.get('clusters').length === 0) {
        return false;
    }
    //TODO need to see how we can get promise date

    /*if (!isEmpty(vehicleRoutes)) {
        const currentVR = vehicleRoutes.find((vr) => {
            const { plannedStartTime, plannedEndTime } = getProperties(
                vr,
                'plannedStartTime',
                'plannedEndTime'
            );

            return moment(plannedEndTime).isAfter(time) && moment(plannedStartTime).isBefore(time);
        });

        if (currentVR) {
            const driverShifts = get(currentVR, 'driverShifts');

            if (!isEmpty(driverShifts)) {
                const currentDS = driverShifts.find((ds) => {
                    const { plannedStartTime, plannedEndTime } = getProperties(
                        ds,
                        'plannedStartTime',
                        'plannedEndTime'
                    );

                    return (
                        moment(plannedEndTime).isAfter(time) &&
                        moment(plannedStartTime).isBefore(time)
                    );
                });

                if (!isEmpty(currentDS)) {
                    return false;
                }
            }
            return true;
        }
    }*/
    return true;
}
