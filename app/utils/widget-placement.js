/**
 * Computes a list of possible locations that will fit a widget of specified
 * minimum width and height; scans left to right, then down the rows.
 *
 * @param {Dashboard} dashboard
 * @param {integer} gridWidth
 * @param {integer} minWidth
 * @param {integer} minHeight
 */
export function computeWidgetPlacements(
  dashboard,
  gridWidth,
  minWidth,
  minHeight
) {
  let mask = computeMask(dashboard, gridWidth);
  let freeSpacePairs = computeMaxSpacePairs(mask);

  let locations = [];
  let height = mask[0].length;

  for (let j = 0; j < height; j++) {
    for (let i = 0; i < gridWidth; i++) {
      let [maxWidth, maxHeight] = freeSpacePairs[i][j];
      if (maxWidth >= minWidth && maxHeight >= minHeight) {
        locations.push({
          x: i,
          y: j,
          maxWidth,
          maxHeight
        });
      }
    }
  }

  return locations;
}

/**
 * Computes max width and height for a widget placed at each location in mask.
 *
 * @param {Array} mask
 */
export function computeMaxSpacePairs(mask) {
  let width = mask.length;
  let pairs = [];

  for (let i = width - 1; i >= 0; i--) {
    let height = mask[i].length;
    pairs[i] = [];

    for (let j = height - 1; j >= 0; j--) {
      let maxWidth, maxHeight;

      if (mask[i][j]) {
        maxWidth = 0;
      } else if (i === width - 1) {
        maxWidth = 1;
      } else {
        maxWidth = pairs[i + 1][j][0] + 1;
      }

      if (mask[i][j]) {
        maxHeight = 0;
      } else if (j === height - 1) {
        maxHeight = Infinity;
      } else {
        maxHeight = pairs[i][j + 1][1] + 1;
      }

      pairs[i][j] = [maxWidth, maxHeight];
    }
  }

  return pairs;
}

/**
 * Computes an array mask with non-zero value iff grid location is occupied.
 *
 * @param {Dashboard} dashboard
 * @param {integer} gridWidth
 */
export function computeMask(dashboard, gridWidth) {
  let widgets = dashboard.get('widgets');

  // find horiz limit of existing widgets
  let xMax = widgets.reduce((max, widget) => {
    let x = widget.get('x');
    let width = widget.get('width');
    return Math.max(max, x + width);
  }, 0);

  // grid may be wider than right-most widget's edge
  xMax = Math.max(xMax, gridWidth);

  // allow one extra row below bottom-most widget
  let yMax = 1 + widgets.reduce((max, widget) => {
    let y = widget.get('y');
    let height = widget.get('height');
    return Math.max(max, y + height);
  }, 0);

  // allow one extra row in mask
  let mask = [];
  for (let i = 0; i < xMax; i++) {
    mask[i] = [];
    for (let j = 0; j < yMax; j++) {
      mask[i][j] = 0;
    }
  }

  // shade values for existing widgets in layout
  widgets.forEach((widget) => {
    let { x, y, height, width } =
      widget.getProperties(['x', 'y', 'height', 'width']);

    for (let i = x; i < x + width; i++) {
      for (let j = y; j < y + height; j++) {
        mask[i][j] = 1;
      }
    }
  });

  return mask;
}
