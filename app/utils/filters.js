import { nullFilter } from 'adept-iq/config/filter-types';
import { isEmpty, typeOf, isNone } from '@ember/utils';
import { filterTypesHash } from 'adept-iq/config/filter-types';

const alwaysTrue = () => true;

export function testFilterValues(filterType, values) {
  if (!filterType || filterType === nullFilter) return false;

  let { arity, isValue, parseValue, validateValues } = filterType;

  if (!values) {
    if (arity === '+') return false;
    if (typeOf(arity) === 'number' && arity > 0) return false;
    if (validateValues) return validateValues([]);
    return true;
  }

  if (arity === '+' && values.length === 0) return false;
  if (typeOf(arity) === 'number' && values.length < arity) return false;

  let args = values.slice(0, arity);

  if (isValue && !args.every(isValue)) return false;

  if (parseValue) {
    args = args.map(parseValue);
  }

  if (validateValues) return validateValues(args);

  return true;
}

export const buildValueFilterNode = (filterType, path, values) => {
  if (!filterType || filterType === nullFilter) return null;

  if (isNone(values)) {
    values = [];
  } else if (typeOf(values) !== 'array') {
    values = [values];
  }

  return {
    type: filterType.id,
    rql: {
      path,
      values
    },
    local: {
      path,
      values
    }
  }
};

export const buildCompoundFilterNode = (type, args) => {
  if (isEmpty(args)) return null;

  args = args.compact();

  if (args.length === 0) return null;
  if (args.length === 1) return args[0];

  return {
    type,
    args
  }
};

export function buildFilterFunction(node) {
  if (!node) return alwaysTrue;

  switch(node.type) {
    case 'and':
    case 'or':
      return buildCompoundFilterFunction(node);
    default:
      return buildValueFilterFunction(node);
  }
}

const buildCompoundFilterFunction = ({ type, args }) => {
  if (!args) return alwaysTrue;

  let functions = args.map(buildFilterFunction).compact();

  if (functions.length === 0) return alwaysTrue;
  if (functions.length === 1) return functions[0];

  switch(type) {
    case 'and':
      return (x) => functions.every((f) => f(x));
    case 'or':
      return (x) => functions.any((f) => f(x));
    default:
      throw 'invalid compound filter type';
  }
};

const buildValueFilterFunction = (node) => {
  if (!node) return alwaysTrue;

  let {
    type,
    local: {
      path,
      values
    }
  } = node;

  let filterType = filterTypesHash[type];
  let fn = filterType.fn;
  let filterFunction = (x) => {
    let targetValue = x.get(path);
    return fn(targetValue, values);
  };

  return filterFunction;
};
