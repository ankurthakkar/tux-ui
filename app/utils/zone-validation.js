import zonePolygon from 'adept-iq/config/zone-polygon';

export function insideZone(point) {
  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

  let x = point[0], y = point[1];

  let inside = false;
  for (let i = 0, j = zonePolygon.length - 1; i < zonePolygon.length; j = i++) {
      let xi = zonePolygon[i][0], yi = zonePolygon[i][1];
      let xj = zonePolygon[j][0], yj = zonePolygon[j][1];

      let intersect = ((yi > y) != (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) inside = !inside;
  }

  return inside;
}
