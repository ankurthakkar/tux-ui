import DS from 'ember-data';
import moment from 'moment';

// scheduling API uses a slightly different date format
const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ssZZ';

export default DS.DateTransform.extend({
  serialize(date) {
    if (date instanceof Date && !isNaN(date)) {
      return moment(date).format(DATE_FORMAT);
    } else {
      return null;
    }
  }
});
