// import Ember from 'ember';
import DS from 'ember-data';

const { Transform } = DS;

// function copy(source) {
//     let keys = Object.keys(source);
//     let dest = {};
//     for(let i = 0; i < keys.length; i++) {
//         if(keys[i].charAt(0) !== '_') {
//             dest[keys[i]] = source[keys[i]];
//         }
//     }

//     return dest;
// }

export default Transform.extend({
    deserialize: function(serialized) {
        return serialized;
    },
    serialize: function(deserialized) {
        return deserialized;
    }
});
