import DS from 'ember-data';
import polyline from '@mapbox/polyline';
import { isEmpty } from '@ember/utils';

export default DS.Transform.extend({
  deserialize(serialized) {
    if (isEmpty(serialized)) return null;
    return polyline.decode(serialized);
  },

  serialize(deserialized) {
    if (isEmpty(deserialized)) return null;
    return polyline.encode(deserialized);
  }
});
