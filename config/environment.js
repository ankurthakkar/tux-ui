'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'adept-iq',
    podModulePrefix: 'adept-iq/pods',
    environment,
    rootURL: '/',
    locationType: 'auto',
    'ember-light-table': {
      enableSync: true
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created

      localFilteringEnabled: true,
      paginationEnabled: false
    },

    API: {
      avlmService: {
        host: 'https://11gpajx0kl.execute-api.us-west-2.amazonaws.com'
      },
      avlmBookingService: {
        host: 'https://wj5xdf8phg.execute-api.us-east-1.amazonaws.com'
      },
      bookingService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/booking'
      },
      configService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/config'
      },
      dispatchService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/dispatch'
      },
      geocodeService: {
        host: 'http://35.161.159.100:7744'
      },
      riderManagementService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/rms'
      },
      schedulingService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/scheduling'
      },
      ssoService: {
        host: 'https://iq.awsstage.ddsdeploytest.com/sso',
        useMirage: false
      },
      streamingService: {
        host: 'ws://34.220.193.73:9999'
      },
      tripManagementService: {
        host: 'http://54.187.123.200/driver/latest/v1.0.2/api'
      }
    },

    tomtom: {
      search: {
        searchKey: 'eunxxmsmsjypvqycvwamhw7b',
        countrySet: 'US',
        center: {lat: 46.064878, lon: -118.337848},
        radius: 10000
      },

      key: 'adafjeecjbaq29u5wds7kg2c',
      basePath: '/assets/tomtom',

      // basePath not used on styleUrlMapping values
      styleUrlMapping: {
        main: {
           basic: '/assets/tomtom-dds/styles/basic_main.json',
           hybrid: '/assets/tomtom/styles/hybrid_main.json',
           labels: '/assets/tomtom/styles/labels_main.json'
        },
        night: {
           basic: '/assets/tomtom/styles/basic_night.json',
           hybrid: '/assets/tomtom/styles/hybrid_night.json',
           labels: '/assets/tomtom/styles/labels_night.json'
        }
      },
      source: 'vector'
    },

    // format for moment must have equivalent flatpickr format
    // https://momentjs.com/docs/#/displaying/
    // https://flatpickr.js.org/formatting/
    dateTimeFormat: {
      dateTimeMoment: 'YYYY-MM-DD hh:mm A',
      dateTimeFlatPickr: 'Y-m-d h:i K',

      dateMoment: 'YYYY-MM-DD',
      dateFlatPickr: 'Y-m-d',

      timeMoment: 'hh:mm A',
      timeFlatPickr: 'h:i K'
    },

    moment: {
      includeTimezone: 'all'
    }
  };

  if (environment === 'mirage') {
    ENV['ember-cli-mirage'] = {
      enabled: true
    };
    
    ENV.API.ssoService.useMirage = true;
  }

  if (environment === 'development') {
    ENV['ember-cli-mirage'] = {
      enabled: true
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {}

  return ENV;
};
