import ENV from '../config/environment';
import sso from './routes/sso';
import invitations from './routes/invitations';
import dispatch from './routes/dispatch';
import riderManagement from './routes/riderManagement';
import agencyMarkers from './routes/agency-marker';
import configItems from './routes/config-items';
import config from './routes/config';
import scheduling from './routes/scheduling';
import booking from './routes/booking';


export default function() {
  this.timing = 50;
  this.logging = false;

  // we need to load all of the fixtures into the database before anything else
  if (ENV.API.ssoService.useMirage === false) {
    let { host, namespace } = ENV.API.ssoService;
    this.passthrough(host + namespace + '/**');
  } else {
    sso(this);
  }
  invitations(this);

  agencyMarkers(this);

  // config API
  configItems(this);
  config(this);

  if (ENV.environment === 'mirage' || ENV.environment === 'test'){

    dispatch(this);
    scheduling(this);
    riderManagement(this);
    booking(this);

    // let geocode passthrough for geonodes
    this.passthrough(`${ENV.API.geocodeService.host}/**`);
  }

  if (ENV.environment === 'development') {
    this.passthrough(`${ENV.API.dispatchService.host}/**`);
    this.passthrough(`${ENV.API.riderManagementService.host}/**`);
    this.passthrough(`${ENV.API.schedulingService.host}/**`);
    this.passthrough(`${ENV.API.bookingService.host}/**`);
    this.passthrough(`${ENV.API.geocodeService.host}/**`);
    this.passthrough(`${ENV.API.ssoService.host}/**`);
    this.passthrough(`${ENV.API.avlmBookingService.host}/**`);
    this.passthrough(`${ENV.API.tripManagementService.host}/**`);
    this.passthrough(`${ENV.API.avlmService.host}/**`);
    this.passthrough(`${ENV.API.configService.host}/**`);

    // TODO: enable passthrough when config-item API is done for dashboards
    // this.passthrough(`${ENV.API.dispatch.config}/**`);
  }

  this.passthrough('/assets/**');
}
