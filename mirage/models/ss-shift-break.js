import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
    vehicle: belongsTo('ss-vehicle'),
    address: belongsTo('ss-location'),
    breakType: belongsTo('ss-break-type'),
});
