import moment from 'moment';
import { faker } from 'ember-cli-mirage';

export default [
  {
    name: 'ambulatory',
    description: faker.random.number({min: 1, max: 2}),
    loadTime: faker.random.number({min: 1, max: 5}),
    unloadTime: faker.random.number({min: 1, max: 5}),
    displayName: 'ambulatory',
    displayOrder: faker.random.number({min: 1, max: 2})
  },
  {
    name: 'wheel chair',
    description: faker.random.number({min: 1, max: 2}),
    loadTime: faker.random.number({min: 1, max: 5}),
    unloadTime: faker.random.number({min: 1, max: 5}),
    displayName: 'Wheel Chair',
    displayOrder: faker.random.number({min: 1, max: 2})
  },
  {
    name: 'wide wheel chair',
    description: faker.random.number({min: 1, max: 2}),
    loadTime: faker.random.number({min: 1, max: 5}),
    unloadTime: faker.random.number({min: 1, max: 5}),
    displayName: 'Wide Wheel Chair', 
    displayOrder: faker.random.number({min: 1, max: 2})
  },  
  {
    name: 'wide flex wheel chair',
    description: faker.random.number({min: 1, max: 2}),
    loadTime: faker.random.number({min: 1, max: 5}),
    unloadTime: faker.random.number({min: 1, max: 5}),
    displayName: 'Wide Flex Wheel Chair',
    displayOrder: faker.random.number({min: 1, max: 2})
  }
];
