import moment from 'moment';
import { faker } from 'ember-cli-mirage';

export default [
  {
    "id":1,
    "name": "route 1",  
    "plannedStartTime": moment().add(10,'min'),
    "plannedEndTime": moment().add(4,'hour'),
    "routeDate": moment().subtract(1,'hour'),
    "clusterIds": [1],
    stopPolylines: [
      {
        type: "pick",
        tripId: 7,
        stopId: 1,
        polyline: null
      }
    ]
    
  }
];
