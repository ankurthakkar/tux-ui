export default [
    {
        "type": "driver",
        "id": "1",
        "firstName": "Bruce",
        "lastName": "Wayne",
        "active": true
    },
    {
        active:true,
        areaCode:"555",
        availabilityIds:["1"],
        firstName:"Buritto",
        id:"2",
        iqDriverId:null,
        lastName:"Man",
        middleName:null,
        phoneNumber:"3245940",
        preferredVehicleName:"10",
        vehicleIds:null
    }
];