import moment from 'moment';
export default [
  {
    "id":1, 
    "distance": 200,
    "endTime": moment().add(4,'hour'),
    "ordinal": 1,
    "tripIds": [1], 
  } 
];
