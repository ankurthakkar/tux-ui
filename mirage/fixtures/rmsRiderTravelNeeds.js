import { faker } from 'ember-cli-mirage';

export default [
  {
    count: 1,
    riderId: '1',
    travelNeedTypeId: '1',
    passengerTypeId: '1',
  },
  {
    count: 1,
    riderId: '2',
    travelNeedTypeId: '2',
    passengerTypeId: '1',
  },
  {
    count: 1,
    riderId: '3',
    travelNeedTypeId: '3',
    passengerTypeId: '1',
  }
];
