export default [
    {
        "type": "vehicleAvailability",
        "id": "1",
        "startTime": "2015-12-31T23:00:00-08:00",
        "endTime": "3015-12-31T22:59:59-08:00",
        "shiftStart": "07:15:00",
        "shiftEnd": "23:59:00",
        "sunday": true,
        "monday": true,
        "tuesday": true,
        "wednesday": true,
        "thursday": true,
        "friday": true,
        "saturday": true
    }
];