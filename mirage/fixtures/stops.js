import moment from 'moment';
export default [
    {
        "type": "pick",
        "id": "1",
        "distanceToNext": 105,
        "dwell": 10,
        "eta": moment().add(1,'hour'), 
        "odometer": 10,
        "state": 'dispatched',
        "BreakType": 'rest',
        "BreakCategory":'category 1',
        "plannedDepartTime": moment().add(30,'min'),
        "phone1":'88888888888',
        "breakStartTime":moment().add(2,'hour'), 
        "breakEndTime":moment().add(3,'hour'), 
    },
    {
        "type": "drop",
        "id": "2",
        "distanceToNext": 105,
        "dwell": 10,
        "eta": moment().add(2,'hour'), 
        "odometer": 50, 
        "state": 'arrived',
        "BreakType": 'nap' ,
        "BreakCategory":'category 1',
        "plannedDepartTime": moment().add(30,'min'),
        "phone1":'88888888888',
        "breakStartTime":moment().add(2,'hour'), 
        "breakEndTime":moment().add(3,'hour'), 
  
    }
];