import moment from 'moment';
import { faker } from 'ember-cli-mirage';

export default [
  {
    alias : '', 
    country : 'US', 
    id : '1', 
    locality : 'Walla Walla', 
    locationIds : ['1'], 
    notes : 'hi' ,
    postOfficeBoxNumber : '', 
    postalCode : '99362', 
    region : 'Washington', 
    ridersFavoriteIds : ['1', '2'], 
    ridersPrimaryIds : ['1', '2'], 
    streetAddress : 'S 1st Ave', 
    streetNumber : '202', 
    subRegion : 'Walla Walla'
  },
  {
    alias : '', 
    country : 'US', 
    id : '2',
    locality : 'Walla Walla', 
    locationIds : ['2'], 
    notes : 'Pharmancy', 
    postOfficeBoxNumber : '', 
    postalCode : '99362', 
    region : 'Washington', 
    ridersFavoriteIds : ['1','3'], 
    ridersPrimaryIds : ['3'], 
    streetAddress : 'E Main St', 
    streetNumber : '51', 
    subRegion : 'Walla Walla'
  }
];
