export default [
    {
        driverId:"2",
        endTime:"2018-09-19T06:59:59.999Z",
        friday:true,
        id:"1",
        monday:true,
        saturday:false,
        shiftEnd:"17:00:00",
        shiftStart:"06:00:00",
        startTime:"2018-09-18T07:00:00.000Z",
        sunday:false,
        thursday:true,
        tuesday:true,
        wednesday:true
    }
];
