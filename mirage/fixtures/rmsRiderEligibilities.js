export default [
    {
        eligibilityTypeId : '2',
        from : '2018-09-12T07:00:00.000Z',
        id : '1' ,
        riderId : '1',
        to : '2019-04-20T06:59:59.999Z'
    },
    {
        eligibilityTypeId  : '1',
        from : '2018-09-12T07:00:00.000Z',
        id : '2' ,
        riderId : '2',
        to : '2019-04-20T06:59:59.999Z'
    },
    {
        eligibilityTypeId  : '3',
        from : '2018-09-12T07:00:00.000Z',
        id : '3' ,
        riderId : '3',
        to : '2019-04-20T06:59:59.999Z'
    }
];
  