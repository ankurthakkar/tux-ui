export default function(mirage) {
  mirage.get('/agency-marker', function(/* db, request */) {
    return {
      data: [
        {
          id: '0',
          type: 'agencyMarker',

          attributes: {
            lat: 46.0479,
            lng: -118.3537,
            options: {
              opacity: 1,
              label: 'This is an agency marker',
              icon: {
                icon: 'fa fa-flag',
                iconSize: [32, 37],
                iconAnchor: [16, 2],
                style: {
                  color: '#fff'
                },
                noPlainSVG: true,
              }
            }
          },
          relationships: {}
        },
        {
          id: '1',
          type: 'agencyMarker',

          attributes: {
            lat: 46.067414,
            lng: -118.315995,
            options: {
              opacity: 1,
              label: 'This is an agency marker',
              icon: {
                icon: 'fa fa-flask',
                iconSize: [32, 37],
                iconAnchor: [16, 2],
                style: {
                  color: '#fff'
                },
                noPlainSVG: true,
              }
            }
          },
          relationships: {}
        },
        {
          id: '2',
          type: 'agencyMarker',
          attributes: {
            lat: 46.073666,
            lng: -118.325684,
            options: {
              opacity: 1,
              label: 'This is an agency marker',
              icon: {
                icon: 'fa fa-fire',
                iconSize: [32, 37],
                iconAnchor: [16, 2],
                style: {
                  color: '#fff'
                },
                noPlainSVG: true,
              }
            }
          },
          relationships: {}
        }
      ],
      included: [

      ]
    };
  });
}
