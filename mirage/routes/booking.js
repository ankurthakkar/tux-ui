import _ from "lodash";
import ENV from "../../config/environment";
// import booking from "../../app/services/booking";

const bookingHost = ENV.API.bookingService.host;

export default function(mirage) {
  mirage.get(`${bookingHost}/booking`, function(db) {
    return db.bsBooking.all();
  });

  mirage.get(`${bookingHost}/leg`, function(db) {
    return db.bsLeg.all();
  });

  mirage.get(`${bookingHost}/subscription`, 'bs-subscription');

  mirage.get(`${bookingHost}/travel-need-type`, 'bs-travel-need-type');
  mirage.get(`${bookingHost}/passenger-type`, 'bs-passenger-type');
  mirage.get(`${bookingHost}/fare-type`, 'fare-type');

  mirage.get(`${bookingHost}/subscription-exclusion`, 'bs-subscription-exclusion');

  mirage.get(`${bookingHost}/subscription-travel-need`, 'bs-subscription-travel-need');
  mirage.get(`${bookingHost}/subscription-recurrence-pattern`, 'bs-subscription-recurrence-pattern');


}
