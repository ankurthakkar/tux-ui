import ENV from "../../config/environment";

const { host } = ENV.API.configService;

export default function(mirage) {
  mirage.get(`${host}/config-item`, (db /*, request */) => {
    return db.configItems.all();
  });

  mirage.post(`${host}/config-item`, function(db /*, request */) {
    let attrs = this.normalizedRequestAttrs();
    return db.configItems.create(attrs);
  });

  mirage.get(`${host}/config-item/:id`, (db, request) => {
    let { id } = request.params;
    return db.configItems.find(id);
  });

  mirage.patch(`${host}/config-item/:id`, function(db, request) {
    let { id } = request.params;
    let configItem = db.configItems.find(id);

    let attrs = this.normalizedRequestAttrs();
    configItem.update(attrs);

    return configItem;
  });

  mirage.delete(`${host}/config-item/:id`, function(db, request) {
    let { id } = request.params;
    let configItem = db.configItems.find(id);
    configItem.destroy();
  });
}