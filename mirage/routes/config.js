import { isBlank } from '@ember/utils';
import Response from 'ember-cli-mirage/response';
import ENV from '../../config/environment';

const { host } = ENV.API.tripManagementService;
const  configHost = ENV.API.configService.host;

export default function(mirage){

      mirage.get(`${configHost}/config/:id`, 'cs-config-item');

      mirage.get(`${configHost}/config/workspaces-default`, 'cs-config-item');

      mirage.get(`${configHost}/config/workspaces-users`, 'sso-user');

      mirage.get(`${configHost}/config/workspaces-default/default`, 'cs-config-item');


      mirage.get(`${host}/canned-message`, function(db) {
        return db.tmCannedMessages.all();
      });
    
      mirage.get('/travel-need-type', function (db){
        return db.travelNeedTypes.all();
      });
    
      mirage.get('/passenger-type', function (db){
        return db.bsPassengerTypes.all();
      });

      mirage.get('/vehicle-capacity-type', function (db){
        return db.vehicleCapacityTypes.all();
      });

      mirage.get('/vehicle-capacity-config', function (db){
        return db.vehicleCapacityConfigs.all();
      });

      mirage.get('/vehicle-type', function (db){
        return db.vehicleTypes.all();
      });

      mirage.get('/break-type', function (db){
        return db.breakTypes.all();
      });

      mirage.get('/shift-break', function (db){
        return db.ssShiftBreaks.all();
      });

}