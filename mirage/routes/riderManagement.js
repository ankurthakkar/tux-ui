import ENV from '../../config/environment';

const rmHost = ENV.API.riderManagementService.host;

export default function(mirage) {
  mirage.passthrough(`${ENV.API.geocodeService.host}/**`);


  mirage.get(`${rmHost}/rider-travel-need/`, function (schema){
    return schema.rmsRiderTravelNeeds.all();
  })
  mirage.post(`${rmHost}/rider-travel-need/`, 'rms-rider-travel-need');
  mirage.patch(`${rmHost}/rider-travel-need/:id`, 'rms-rider-travel-need');

  mirage.get(`${rmHost}/rider-travel-need/:id`, function (schema, request){
    return schema.rmsRiderTravelNeeds.find(request.params.id);
  })
  mirage.get(`${rmHost}/rider-eligibility/`, function (schema){
    return schema.rmsRiderEligibilities.all();
  })
  
  mirage.get(`${rmHost}/rider-eligibility/:id`, 'rms-rider-eligibility');
  mirage.patch(`${rmHost}/rider-eligibility/:id`, 'rms-rider-eligibility');

  mirage.get(`${rmHost}/rider`, function(db, request) {
    let query = db.rmsRiders.all();

    // Dispatch API requires normally-forbidden characters like +, -;
    // Mirage masks these with spaces in `request.queryParams`, so this hack
    // ensures that sorting works. We will need to solve this for other cases.
    let regExp = /sort=([+-])([^&]+)/;
    let matches = regExp.exec(request.url);
    if (matches) {
      let sign = matches[1];
      let sort = matches[2];

      query.models = query.models.sortBy(sort);

      if (sign === '-') {
        query.models.reverse();
      }
    }

    let json = this.serialize(query, 'rms-rider');

    return json;
  });

  mirage.patch(`${rmHost}/rider/:id`, 'rms-rider');

  mirage.post(`${rmHost}/rider`, 'rms-rider');

  mirage.get(`${rmHost}/location/:id`, 'rms-location');

  mirage.post(`${rmHost}/location`, 'rms-location');

  mirage.patch(`${rmHost}/location/:id`, 'rms-location');

  mirage.post(`${rmHost}/address`, 'rms-address');
  mirage.patch(`${rmHost}/address/:id`, 'rms-address');

  mirage.get(`${rmHost}/address/:id`, 'rms-address');
  mirage.get(`${rmHost}/address`, 'rms-address');


  mirage.post(`${rmHost}/rider-eligibility`, 'rms-rider-eligibility');

  mirage.get(`${rmHost}/travel-need-type`, function (db) {
    return db.rmsTravelNeedTypes.all();
  });

  mirage.post(`${rmHost}/travel-need-type`, 'rmsTravelNeedTypes');

  mirage.get(`${rmHost}/passenger-type`, function (db) {
    return db.rmsPassengerTypes.all();
  });

  mirage.get(`${rmHost}/eligibility-type`, 'rms-eligibility-type');

}
