import { isBlank } from '@ember/utils';
import Response from 'ember-cli-mirage/response';
import ENV from '../../config/environment';

const scheduleHost = ENV.API.schedulingService.host;

export default function(mirage) {
    mirage.get(`${scheduleHost}/schedule`, function ({ schedules }, request) {
      let query = schedules.all();
  
      // Dispatch API requires normally-forbidden characters like +, -;
      // Mirage masks these with spaces in `request.queryParams`, so this hack
      // ensures that sorting works. We will need to solve this for other cases.
      let regExp = /sort=([+-])([^&]+)/;
      let matches = regExp.exec(request.url);
      if (matches) {
        let sign = matches[1];
        let sort = matches[2];
  
        query.models = query.models.sortBy(sort);
  
        if (sign === '-') {
          query.models.reverse();
        }
      }
      return query;
    });

    mirage.get(`${scheduleHost}/vehicle-type`, 'ss-vehicle-type');

    mirage.get(`${scheduleHost}/vehicle-capacity-config`, 'ss-vehicle-capacity-config');

    mirage.get(`${scheduleHost}/vehicle-capacity-type`, 'ss-vehicle-capacity-type');
    mirage.get(`${scheduleHost}/driver`, 'ss-driver');
    mirage.get(`${scheduleHost}/shift-break`, 'ss-shift-break');

    mirage.get(`${scheduleHost}/break-type`, 'break-type');
    
    mirage.get(`${scheduleHost}/vehicle`, function(db) {
      return db.ssVehicles.all();
    });
    mirage.get(`${scheduleHost}/driver`, function(db) {
      return db.ssDrivers.all();
    });

    mirage.post(`${scheduleHost}/driver`, 'ss-driver');

    mirage.post(`${scheduleHost}/location`, 'ss-location');

    mirage.post(`${scheduleHost}/vehicle-availability`, 'ss-vehicle-availability');

    mirage.get(`${scheduleHost}/driver-availability/:id`, 'ss-driver-availability');

    mirage.post(`${scheduleHost}/driver-availability`, 'ss-driver-availability');

    mirage.post(`${scheduleHost}/vehicle`, 'ss-vehicle');
  }
  
