import { isBlank } from '@ember/utils';
import Response from 'ember-cli-mirage/response';
import ENV from '../../config/environment';

const { host } = ENV.API.ssoService;


export default function(mirage) {

  mirage.post(host + '/signup', (schema, request) => {
    let params = JSON.parse(request.requestBody);

    if (params.password === 'error') {
      return new Response(422, { 'Content-Type': 'application/json' }, {
        message: 'API Error (From Mirage)'
      });
    }

    return {
      token: 'token_string'
    }
  });

  mirage.get(host + '/login', (schema, request) => {
    let [ userName, password ] = parseBasicAuth(request.requestHeaders);

    if (password === 'incorrect') {
      return new Response(400, { 'Content-Type': 'application/json' }, {
        message: 'Unauthorized'
      });
    }

    if (password === 'disabled') {
      return new Response(403, { 'Content-Type': 'application/json' }, {
        message: 'Disabled'
      });
    }

    let header = {
      alg: 'RS256',
      typ: 'JWT'
    }

    let payload = {
      userId: 'a056b87e-754b-40f4-9683-7b49d7f238db',
      userName: userName,
      displayName: userName,
      sequenceNumber: 1,
      roleNames: [
        'admin',
        'user'
      ]
    }

    let base64Header = btoa(JSON.stringify(header));
    let base64Payload = btoa(JSON.stringify(payload));

    return {
      token: `${base64Header}.${base64Payload}`
    }
  });
}

function parseBasicAuth({ authorization }) {
  if (isBlank(authorization)) return;
  if (!/Basic /.test(authorization)) return;

  let base64Token = authorization.split(' ')[1];

  return atob(base64Token).split(':');
}
