import _ from 'lodash';
import ENV from '../../config/environment';

const dispatchHost = ENV.API.dispatchService.host;

export default function(mirage) {

  mirage.get('/user', function(db) {
    return db.ssoUsers.all().models;
  });

  mirage.get(`${dispatchHost}/rider`, function ({ riders }, request) {
    let query = riders.all();

    // Dispatch API requires normally-forbidden characters like +, -;
    // Mirage masks these with spaces in `request.queryParams`, so this hack
    // ensures that sorting works. We will need to solve this for other cases.
    let regExp = /sort=([+-])([^&]+)/;
    let matches = regExp.exec(request.url);
    if (matches) {
      let sign = matches[1];
      let sort = matches[2];

      query.models = query.models.sortBy(sort);

      if (sign === '-') {
        query.models.reverse();
      }
    }
    return query;
  });

  mirage.get(`${dispatchHost}/route`, function ({ routes }, request) {
    let query = routes.all();

    // Dispatch API requires normally-forbidden characters like +, -;
    // Mirage masks these with spaces in `request.queryParams`, so this hack
    // ensures that sorting works. We will need to solve this for other cases.
    let regExp = /sort=([+-])([^&]+)/;
    let matches = regExp.exec(request.url);
    if (matches) {
      let sign = matches[1];
      let sort = matches[2];

      query.models = query.models.sortBy(sort);

      if (sign === '-') {
        query.models.reverse();
      }
    }
    return query;
  });

  mirage.get(`${dispatchHost}/schedule`, function(db) {
    return db.schedules.all();
  });

  mirage.get(`${dispatchHost}/driver`, function(schema, request) {
    console.log(schema);
    console.log(request);
    let regExp = /\(([^)]+)\)/;
    let matches = regExp.exec(request.queryParams.filter);
    console.log(matches);
    // console.log(schema.db.ssDrivers.findBy(request.queryParams));
  });

  mirage.get(`${dispatchHost}/vehicle`, function(db) {
    return db.vehicles.all();
  });

  mirage.get(`${dispatchHost}/route`, function(db) {
    return db.routes.all();
  }); 

  // @TODO figure out how to use query pararmeters for complex searches 
  mirage.get(`${dispatchHost}/trip`, 'trip');
  mirage.get(`${dispatchHost}/trip/:id`, 'trip');

  mirage.get(`${dispatchHost}/no-show-reason-code`, 'no-show-reason-code');
  mirage.get(`${dispatchHost}/cancel-type`, 'cancel-type');
  mirage.get(`${dispatchHost}/agency-marker`, 'agency-marker');
  mirage.get(`${dispatchHost}/travel-need-type`, 'travel-need-type');
  mirage.get(`${dispatchHost}/provider`, 'provider');

  mirage.get(`${dispatchHost}/stop`, function ({ stops }, request) {
    let query = stops.all();

    // Dispatch API requires normally-forbidden characters like +, -;
    // Mirage masks these with spaces in `request.queryParams`, so this hack
    // ensures that sorting works. We will need to solve this for other cases.
    let regExp = /sort=([+-])([^&]+)/;
    let matches = regExp.exec(request.url);
    if (matches) {
      let sign = matches[1];
      let sort = matches[2];

      query.models = query.models.sortBy(sort);

      if (sign === '-') {
        query.models.reverse();
      }
    }
    return query;
  });


  mirage.get('/schedule', function(db) {
    return db.schedules.all();
  });






  

}
