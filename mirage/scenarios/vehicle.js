export default function(server) {
  server.create('cs-config-item', {
    id: "workspaces-default/default",
    name: "default",
    category: "workspaces-default",
    value: {
        "data":{
        "id": "workspaces-default/default",
        "attributes": {
          "name": "Default Dashboard",
          "role": "Booking",
          "category": "workspaces-users",
          "startDate": "2018-09-28T07:00:00.000Z",
          "endDate": "2018-09-29T07:00:00.000Z",
          "referenceDate": "2018-09-28T07:00:00.000Z",
          "widgets": [
            {
              "id": "7",
              "typeId": "vehicles",
              "width": 4,
              "height": 2,
              "x": 0,
              "y": 4,
              "state": {}
            }
          ]
        },
        "type": "dashboard"
      }
    }
  });
    // Setup for state of database
  server.loadFixtures();
  }