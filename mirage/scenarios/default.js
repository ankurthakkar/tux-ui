export default function(server) {

  server.create('cs-config-item', {
    id: "workspaces-default/default",
    name: "default",
    category: "workspaces-default",
    value: {
        "data":{
        "id": "workspaces-default/default",
        "attributes": {
          "name": "Default Dashboard",
          "role": "Booking",
          "category": "workspaces-users",
          "startDate": "2018-09-28T07:00:00.000Z",
          "endDate": "2018-09-29T07:00:00.000Z",
          "referenceDate": "2018-09-28T07:00:00.000Z",
          "widgets": [
            {
              "id": "2",
              "typeId": "routes",
              "width": 4,
              "height": 2,
              "x": 0,
              "y": 0,
              "state": {}
            },
            {
              "id": "3",
              "typeId": "stops",
              "width": 4,
              "height": 2,
              "x": 4,
              "y": 0,
              "state": {}
            },
            {
              "id": "4",
              "typeId": "trips",
              "width": 4,
              "height": 2,
              "x": 4,
              "y": 2,
              "state": {}
            },
            {
              "id": "5",
              "typeId": "passengers",
              "width": 4,
              "height": 2,
              "x": 4,
              "y": 4,
              "state": {}
            },
            {
              "id": "6",
              "typeId": "drivers",
              "width": 4,
              "height": 2,
              "x": 0,
              "y": 2,
              "state": {}
            },
            {
              "id": "7",
              "typeId": "vehicles",
              "width": 4,
              "height": 2,
              "x": 0,
              "y": 4,
              "state": {}
            },
            {
              "id": "8",
              "typeId": "map",
              "width": 4,
              "height": 6,
              "x": 8,
              "y": 0,
              "state": {}
            }
          ]
        },
        "type": "dashboard"
      }
    }
  });
/*
  server.create('cs-config-item', {
    id: 'workspaces-default/default2',
    name: 'Another Dashboard Configuration Item',
    value: JSON.stringify({
      data: {
        type: 'dashboard',

        attributes: {
          name: 'Another Dashboard',
          role: 'Dispatch',
          widgets: [
            { id: '1', typeId: 'routes', x: 0, y: 0, width: 4, height: 2 }
          ]
        }
      }
    })
  });
*/
  console.log(server);
  server.loadFixtures();

  server.create('invitation', {
    id: 'e3208800e4defb62c21b1b0a458ad190f1fca347aa5215cef0c3d8d14cea85e7',
    email: 'alex@isleofcode.com',
    isAdmin: true
  });

  server.create('invitation', {
    id: 'f65adb2b6a5d71571877bec4793cca2ff79527401ac96c4dbdf3a7e698d2e7aa',
    email: 'isleofcode1',
    isAdmin: true
  });

  server.create('invitation', {
    id: '8ac2d129a430e68eb5af4573554138662359b854fa47c24a81ab437ce81f5fbd',
    email: 'isleofcode2',
    isAdmin: true
  });

  server.create('invitation', {
    id: '992d45851abfd4e3febc7868f5f414a522049162f96a4d4e42de9722bace22f0',
    email: 'isleofcode3',
    isAdmin: true
  });

  server.create('invitation', {
    id: '3c6d9fa3855d9020c4496d9bad25637b38650126067be396dca416a88e129c6d',
    email: 'isleofcode4',
    isAdmin: true
  });

  server.create('invitation', {
    id: '57475bac4c5d9a58924a025765769475a5b475d8014802a4614fb66ab2dd541d',
    email: 'isleofcode5',
    isAdmin: true
  });

  server.create('invitation', {
    id: 'e8e6419f79c5aa574aba18825cb1d3338d26e4cb6fd18beb890f05da50994759',
    email: 'isleofcode6',
    isAdmin: true
  });

  server.create('invitation', {
    id: '1d44e646220e1d6029b81a91dd2c34ce502c460eafb7d7fef45ad7fb471ddef2',
    email: 'isleofcode7',
    isAdmin: true
  });

  server.create('invitation', {
    id: '3dfacba575edf36a5613f4e74c07c072ad9499c8f824c370d87f909a0222aa81',
    email: 'isleofcode8',
    isAdmin: true
  });

  server.create('invitation', {
    id: '646d660f16b1854d24d04a8bc8a73cebe26b3c1ba65e5c26c58e1a32a02aaf78',
    email: 'isleofcode9',
    isAdmin: true
  });

  server.create('invitation', {
    id: '22a351b86940338d68f1575758840416810e286a827f2a3ffe4f5078eb347348',
    email: 'isleofcode10',
    isAdmin: true
  });

  server.create('invitation', {
    id: '9ff605e0dcea60562a8135740596059f867d3814c40b29a9467657280b7986e5',
    email: 'jordan@isleofcode.com'
  });
}
