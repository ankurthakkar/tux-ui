import { Factory, faker, association} from 'ember-cli-mirage';
import moment from 'moment';
export default Factory.extend({
    state(){
        return faker.list.random('planned', 'unAssigned');   
    },
    promisedStart: null,
    plannedDuration: faker.random.number({min:100, max: 600}),
    notes: '23432',
    startTime: moment().format(),
    endTime: moment().add(30, 'minutes').format(),
    vehicle: association(),
    address: association(),
    breakType: association(),
});
