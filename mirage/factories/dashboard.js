import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return faker.random.word();
  },

  height() {
    return 12;
  },

  width() {
    return 8;
  }
});
