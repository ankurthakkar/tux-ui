import { Factory, faker, trait, association } from 'ember-cli-mirage';

export default Factory.extend({
  distanceToNext() {
    return faker.random.number({ min: 0, max: 500 });
  },

  dwell() {
    return faker.random.number({ min: 0, max: 50 });
  },

  eta() {
    return faker.date.future();
  },

  onboardCount() {
    return faker.random.number({ min: 1, max: 10 });
  },

  ordinal() {
    return faker.random.number({ min: 0, max: 100 });
  },

  phone1() {
    return faker.phone.phoneNumber();
  },

  notes() {
    return faker.lorem.words().toUpperCase();
  },
  timestamp() {
    return new Date()
  },
  // place: association()
  // defaultScenario: trait({
  //   afterCreate(stop, server){
  //   }
  // })


});
