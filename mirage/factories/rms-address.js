import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    alias: '',
    streetNumber: faker.address.streetPrefix(),
    streetAddress: faker.address.streetAddress(),
    locality: faker.address.county(),
    subRegion: '',
    region: '',
    postOfficeBoxNumber: faker.random.number({min: 1, max:999}),
    postalCode: faker.address.zipCode(),
    country: 'USA',
    notes: 'Random Notes'
});
