import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    type: 'place',
    lat () {
        return faker.address.latitude();
    },
    lng () {
        return faker.address.longitude();   
    },
    geonode() {
        return faker.random.number({min: 45, max: 50});    
    },
    streetNumber(){
        return faker.address.streetPrefix();
    },
    streetAddress(){
        return faker.address.streetAddress();   
    },
    locality(){
        return '';   
    },
    subRegion(){
        return '';
    },
    region(){
        return 'Washington'
    },
    postOfficeNumber(){
        return faker.address.zipCode();
    },
    postalCode(){
        return faker.address.zipCode();
    },
    country: 'USA',
    name(){
        return faker.address.city();   
    },
    notes: 'no clue',

});
