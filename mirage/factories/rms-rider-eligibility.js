import { Factory, faker } from 'ember-cli-mirage';
import moment from 'moment';
export default Factory.extend({
    from: moment(),
      to: moment().add(1, 'year')
});
