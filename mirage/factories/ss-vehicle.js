import { Factory, faker, trait } from 'ember-cli-mirage';

export default Factory.extend({
    name: 'bus',
    active: true,
    
    withTwoBreaks: trait({
        afterCreate(vehicle, server){
            server.createList('ss-shift-break', 2, {vehicle});
        }
    })
});
