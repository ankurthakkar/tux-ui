import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    count: faker.random.number({min: 1, max: 4})
});
