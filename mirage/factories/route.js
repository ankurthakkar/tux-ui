import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return `${faker.random.number({ min: 10000, max: 99999 })}`;
  },

  plannedStartTime() {
    let now = new Date();
    let hourFromNow = new Date(now.getTime() + 60*60*1000);
    return faker.date.between(now, hourFromNow);
  },

  plannedEndTime() {
    let now = new Date();
    let hourFromNow = new Date(now.getTime() + 60 * 60 * 1000);
    let twoHoursFromNow = new Date(now.getTime() + 2 * 60 * 60 * 1000);
    return faker.date.between(hourFromNow, twoHoursFromNow);
  },

  lifoDepth() {
    return faker.random.number({ min: 0, max: 100 });
  },

  routeFactor() {
    return faker.random.number({ min: 0, max: 100 });
  },
  stopPolylines(){
    return  [{
      type: "pick",
      tripId: 7,
      stopId: 1,
      polyline: null
    }];
  }

});
