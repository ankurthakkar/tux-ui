import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  minHeight() {
    return 2;
  },

  minWidth() {
    return 2;
  }
});

