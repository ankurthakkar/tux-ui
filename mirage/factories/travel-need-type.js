import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    name: faker.list.cycle('ambulatory', 'WheelChair', 'WideWheelChair', 'WideFlexWheelChair'),
    vehicleCapacityCount () {
        return faker.random.number({min: 1, max: 2});
    },
    loadTime() {
        return faker.random.number({min: 1, max: 5});
    },
    unloadTime() {
        return faker.random.number({min: 1, max: 5});
    },
    displayName: faker.list.cycle('ambulatory', 'WheelChair', 'WideWheelChair', 'WideFlexWheelChair'),
    displayOrder() {
        return faker.random.number({min: 1, max: 2});
    },
    
});
