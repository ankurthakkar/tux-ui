import { Factory, faker, trait, association } from "ember-cli-mirage";
export default Factory.extend({
  anchor() {
    return faker.list.random("pick", "drop");
  }, // wait to figure out booking later // externalId: attr('string'),
  fare() {
    return faker.random.number({min: 1, max: 10});
  },
  notes() {
    return "notes about stuff";
  }, 
  // promisedTime: attr('date'), // requestedTime: attr('date'), // noShowNotes: attr('string'), // cancelNotes: attr('string')
  timestamp() {
    return new Date();
  },
  // defaultScenario: trait({
  //   afterCreate(trip, server){
  //       server.createList('stop', 2, {trip}, 'defaultScenario');
  //   }
  // })  

});
