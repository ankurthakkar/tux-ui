import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  email() {
    return faker.internet.email();
  },

  isAdmin: false,
  isUsed: false,
  isExpired: false
});
