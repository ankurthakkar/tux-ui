import { Factory, faker } from "ember-cli-mirage";
import moment from "moment";
export default Factory.extend({
  firstName() {
    return faker.name.firstName();
  },
  middleName() {
    return faker.name.firstName();
  },
  lastName() {
    return faker.name.lastName();
  },
  areaCode() {
    return "360";
  },
  phoneNumber() {
    return faker.phone.phoneNumberFormat().split('-').join('');
  },
  extension() {
    return faker.random.number({ min: 1000, max: 9999 });
  },
  dateOfBirth() {
    return moment().subtract(30, "years");
  },
  loadTime() {
    return faker.random.number({ min: 1, max: 9 });
  },
  unloadTime() {
    return faker.random.number({ min: 1, max: 9 });
  },
  notes() {
    return "these are notes";
  },
  fullPhoneNumber(){
    return this.areaCode + this.phoneNumber;
  },
  eligibilityFrom(){
    return moment().format();
  },
  eligibilityTo(){
    return moment().add(1, 'year').format();
  }
});
