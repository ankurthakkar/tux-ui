import { Factory, faker } from 'ember-cli-mirage';
import moment from 'moment';

export default Factory.extend({
  firstName() {
    return faker.name.firstName();
  },
  middleName() {
    return faker.name.firstName();
  },
  lastName() {
    return faker.name.lastName();
  },
  areaCode() {
    return "360";
  },
  phoneNumber() {
    return faker.phone.phoneNumber();
  },
  extension() {
    return faker.random.number({ min: 1000, max: 9999 });
  },
  dateOfBirth() {
    return moment().subtract(30, "years");
  },
  loadTime() {
    return faker.random.number({ min: 1, max: 9 });
  },
  unloadTime() {
    return faker.random.number({ min: 1, max: 9 });
  },
  notes() {
    return "these are notes";
  },

  placeName() {
    return `${faker.address.city()}`;
  },

  searchAddress() {
    return `${faker.address.streetAddress()}`;
  },

  unit() {
    return `${faker.address.streetPrefix()}`;
  },

  city() {
    return `${faker.address.city()}`;
  },

  locality() {
    return `${faker.address.state()}`;
  },

  postalCode() {
    return `${faker.address.zipCode()}`;
  },

  latitude() {
    return `${faker.address.latitude()}`;
  },

  longitude() {
    return `${faker.address.longitude()}`;
  },

  eligibilityfrom() {
    return `${faker.date.recent()}`;
  },

  eligibilityTo() {
    return `${faker.date.future(
      1,
      new Date()
        .toJSON()
        .slice(0, 10)
        .replace(/-/g, "/")
    )}`;
  },



});
