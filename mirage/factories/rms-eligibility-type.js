import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    name: faker.list.cycle('senior', 'medical', 'emergency', 'drop-off')
});
