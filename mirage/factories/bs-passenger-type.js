import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    name: faker.list.cycle('client', 'pca', 'companion')
});
