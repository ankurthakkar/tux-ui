import { Factory, faker, trait, association } from "ember-cli-mirage";
import moment from "moment";
export default Factory.extend({
  distance() {
    return faker.random.number({ min: 10, max: 40 });
  },
  endTime() {
    return moment(this.startTime).add(30, "minutes");
  },
  headCount() {
    return faker.random.number({ min: 1, max: 4 });
  },
  ordinal() {
    return 0;
  },
  startTime() {
    return moment()
      .add(15, "minutes")
      .format();
  },
  state() {
    return "Washington";
  }
});
