import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return `${faker.random.number({ min: 10000, max: 99999 })}`;
  },

  type() {
    return `${faker.random.number({ min: 10000, max: 99999 })}`;
  },

  ambulatorySeats() {
      return `${faker.random.number({ min: 5, max: 20})}`
  }

});
