import { JSONAPISerializer } from 'ember-cli-mirage';
import { camelize } from '@ember/string';
import { isPresent } from '@ember/utils';

export default JSONAPISerializer.extend({
  modelPrefix: '',
  alwaysIncludeLinkageData: true,

  keyForModel(modelName) {
    return modelName;
  },

  keyForCollection(modelName) {
    return modelName;
  },

  keyForAttribute(attr) {
    return attr;
  },

  keyForRelationship(key) {
    return key;
  },

  typeKeyForModel(model) {
    
    let { modelName } = model;
    let modelPrefix = this.modelPrefix;
    if (isPresent(modelPrefix) && modelName.startsWith(`${modelPrefix}-`)) {
      modelName = modelName.slice(modelPrefix.length + 1);
    }

    return camelize(modelName);
  }
});