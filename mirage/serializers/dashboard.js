import DispatchSerializer from './-dispatch';

const INCLUDE = ['widgets'];

export default DispatchSerializer.extend({
  include: INCLUDE
});
