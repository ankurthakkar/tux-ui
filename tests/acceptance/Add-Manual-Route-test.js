import {module, test} from 'qunit';
import {visit, currentURL, fillIn, click, find, findAll, pauseTest} from '@ember/test-helpers';
import {setupApplicationTest} from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import routeScenario from '../../mirage/scenarios/route';
import {currentSession, authenticateSession} from 'ember-simple-auth/test-support';
import _ from 'lodash';
import {fillInForm, mockSocketService} from 'adept-iq/tests/util';
import moment from 'moment';
import socketService from 'adept-iq/services/socket';
import {run} from '@ember/runloop';


const routeDataEntryStub = {
    name: 'route batman',
    start: moment().add(15, 'minutes').format('hh:mm'),
    end: moment().add(2, 'hours').format('hh:mm'),
    date: moment().format('YYYY-MM-DD')
};

module('Acceptance | Add Manual Route', function(hooks) {
    setupApplicationTest(hooks);
    setupMirage(hooks);

    hooks.beforeEach(function() {
        this.owner.register('service:socket', socketService);
        this.socketService = this.owner.lookup('service:socket');
        this.socketService.connect = mockSocketService.connect;
        this.socketService.toJSON = mockSocketService.toJSON;
        this.startMockServer = mockSocketService.startMockServer.bind(this.socketService);
    });

    test('Add Manual Route', async function(assert) {

        // this.socketService.connect = () => 'funny stuff';
        routeScenario(server);
        run(() => this.startMockServer(server));

        await authenticateSession({
            userId: 1,
            user: 'admin',
            token: 'test jwt'
        });

        assert.equal(currentSession(this.application).get('isAuthenticated'), true, 'user is authenticated');

        await visit('dashboard/workspaces-default%2Fdefault');

        await pauseTest();

        assert.ok(/dashboard\//.test(currentURL()), 'Checking that the user is redirected to some dashboard page.');

        const routeWidget = await find('.data-test-routes-widget');

        assert.ok(routeWidget, 'Confirming the Route Widget has Rendered');

        // check the passenger widget loaded with the 3 default buttons
        const buttons = await this.element.querySelectorAll('.data-test-routes-widget .column-widget-button');

        assert.equal(buttons.length, 3, 'confirming the search, plus, and filter buttons are available');

        await click('.data-test-plus-button');

        const dropdownActions = await find('.data-test-dropdown').children;

        assert.ok(dropdownActions, 'Confirm the drop down menu has rendered');
        assert.equal(dropdownActions.length, 1, 'Confirm plus button only has an action for only creating a new route');

        const createActionButton = _.find(dropdownActions, (action) => action.innerText === 'New Route');

        await click(createActionButton);

        const someEditForm = currentURL();

        assert.ok(/edit-form/.test(someEditForm), 'Confirm Routing for Opening up an Edit Form');

        const routeForm = await find('.data-test-route-form-widget');

        assert.ok(routeForm, 'Confirm the Route Data Entry Form Rendered');

        await fillInForm(routeDataEntryStub);

        // await pauseTest();

        await pauseTest();


    });
});
