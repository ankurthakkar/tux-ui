import { module, test } from 'qunit';
import { visit, currentURL, fillIn, click, find, findAll} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { Promise } from 'rsvp';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import passengerScenario from '../../mirage/scenarios/passenger';
import { currentSession, authenticateSession } from 'ember-simple-auth/test-support';
import _ from 'lodash';
import { fillInForm } from 'adept-iq/tests/util';




module('Acceptance | create new passenger', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('create new passenger', async function(assert) {
    // assert.expect(20); 
    // 'server' is a global variable in the test environment.
    passengerScenario(server);

    const rider = server.db.rmsRiders.find('1');

    rider.firstName = 'Link';
    rider.lastName = 'Hyrule';

    await authenticateSession({
      userId: 1,
      user: 'admin',
      token: 'test jwt'
    });

    assert.equal(currentSession(this.application).get('isAuthenticated'), true, 'user is authenticated');

    
    await visit('/dashboard/1');

    let passengerRows = await findAll('.passengers-widget vertical-collection tr');


    assert.equal(passengerRows.length, 1, 'Confirm only one Rider is in the table currently');

    // Move to Dashboard Page
    let someDashboard = currentURL();
    assert.ok(/dashboard\//.test(someDashboard), 'Checking that the user is redirected to some dashboard page.');

    // check the passenger widget is there
    let passenger = await this.element.querySelectorAll('.passengers-widget');
    assert.equal(passenger.length, 1, 'confirming there is only one passenger widget');

    // check the passenger widget loaded with the 3 default buttons
    let buttons = await this.element.querySelectorAll('.passengers-widget .column-widget-button');
    assert.equal(buttons.length, 3, 'confirming the search, plus, and filter buttons are available');

    // Click plus button to open drop down menu
    await click('.data-test-plus-button');
    
    // find the drop down 
    let dropdown = await find('.data-test-dropdown');

    // get all of the drop down children
    let dropdownActions = dropdown.children;


    // currently there is only one option for the plus button
    assert.equal(dropdownActions.length, 1, 'confirming the drop down currently has one action for the plus button');

    // get the Create New Passenger Action Button
    let createActionButton = _.find(dropdownActions, (action) => action.innerText === 'New Passenger');
    
    // Click to Open the Data Entry Form for Passenger Column Widget
    await click(createActionButton);

    let someEditForm = currentURL();
    assert.ok(/edit-form/.test(someEditForm), 'Confirm Routing for Opening up an Edit Form' )

    // assert the data entry form rendered
    let passengerForm = await find('.data-test-passenger-form-widget');
    assert.ok(passengerForm, 'Confirming the Passenger Form is opened');

    // get the form control buttons
    let formHeaderButtons =  await findAll('[data-test-form-header-button]');
    assert.equal(formHeaderButtons.length, 4, 'Confirm for the generic form control buttons a close, search, undo, and apply buttons');

    let formAddButtons = await findAll('.data-test-passenger-form-widget .fa.fa-plus');

    assert.equal(formAddButtons.length, 4, 'Passenger Data Entry Widget Currently has 4 buttons for adding additional information');  

    // Click every add component form button
    await _.reduce(formAddButtons, async (acc, formAddButton) => acc.then(await click(formAddButton)), Promise.resolve());

    await fillInForm(rider);

    let searchAddressFields = await findAll('[data-test-search-address-field] .ember-power-select-trigger');
    await click(searchAddressFields[0]);

    let powerSearch = await find('.ember-power-select-search-input');
    assert.ok(powerSearch, 'Power Search Rendered from clicking');

    await fillIn(powerSearch, 'main');

    // small delay
    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    })

    // only way to get address list items
    let listItems = await findAll('li');
    assert.ok(listItems, 'auto complete renders addresses on the screen');

    await click(listItems[1]);
    
    await click(searchAddressFields[1]);

    powerSearch = await find('.ember-power-select-search-input');
    assert.ok(powerSearch, 'Power Search Rendered from clicking');

    await fillIn(powerSearch, 'main');

    // small delay
    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    })

    // only way to get address list items
    listItems = await findAll('li');
    assert.ok(listItems, 'auto complete renders addresses on the screen');

    await click(listItems[2]);


    await fillIn('.data-test-form-travel-need input', '1');

        // small delay
    await new Promise((resolve) => {
          setTimeout(() => resolve(), 1000);
    })

    await click('[data-test-form-apply-button]');

    await click('[data-test-header-cancel-button]');

    await click('[data-test-form-apply-button]');

    await click('[data-test-header-commit-button]');

    // find close button
    let closeButton = await find('[data-test-form-close-button]');
    // close data entry
    await click(closeButton);

    // Click plus button to open drop down menu
    await click('.data-test-plus-button');
    // click create new passenger
    await click(createActionButton);

    formAddButtons = await findAll('.data-test-passenger-form-widget .fa.fa-plus');

    // Click every add component form button for calendar input button to appear
    await _.reduce(formAddButtons, async (acc, formAddButton) => acc.then(await click(formAddButton)), Promise.resolve());

    await fillInForm(rider);

    // click to open flatpicker calendar
    await click('.flatpickr-input');
    
    // Currently why this.element.query selector or find isn't working needs investigation
    let calendar = await document.querySelector('.dayContainer');

    assert.ok(calendar, 'confirm calendar had rendered on screen');

    await click('[data-test-form-undo-button]');

    // Gather all of the rows that are not disabled
    let rows = _.compact(_.map(await findAll('.edit-cell input'), (cell) => {
    if (!cell.disabled){
      return cell.closest('tr');
    }
    return null;
    }));

    let textRows = {
      labels: [],
      cells: []
    };
    _.forEach(rows, (row) => {     
      textRows.labels.push(row.cells[0].innerText);
      textRows.cells.push(row.cells[1].innerText); 
    });

    assert.deepEqual(textRows.cells, Array(textRows.cells.length).join(".").split("."), `Confirmed Each Data Entry Input Cell ${textRows.labels.join(', ')} are Empty`);

    await click(closeButton);

    // get all of the new passenger rows
    passengerRows = await findAll('.passengers-widget vertical-collection tr');

    assert.equal(passengerRows.length, 2, 'Confirms a new Rider has been added to the table');

    const recentRider = server.db.rmsRiders.find('2');

    assert.ok(recentRider, 'Confirm the database has added a recent rider');

    assert.equal(rider.firstName, recentRider.firstName, 'Confirmed Recent Rider"s First Name');

    assert.equal(rider.lastName, recentRider.lastName, 'Confirmed Recent Rider"s Last Name');

  });
});
