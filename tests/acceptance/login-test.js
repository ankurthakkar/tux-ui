import { module, test } from 'qunit';
import { visit, currentURL, fillIn, click, findAll } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { Promise } from 'rsvp';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import defaultScenario from '../../mirage/scenarios/default';

module('Acceptance | login', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('visiting /login, user can login and get directed to some dashboard', async function(assert) {
    assert.expect(5);
    // 'server' is a global variable in the test environment.
    defaultScenario(server);
    let numWidgets = JSON.parse(server.db.configItems[0].value).data.attributes.widgets.length;

    await visit('/login');

    assert.equal(currentURL(), '/login', 'Confirming that the login page loads');

    await fillIn('[data-test-email-input]', 'iq-admin@ddswireless.com');
    await fillIn('[data-test-password-input]', '4|)M1n');

    // PROBLEM: ember-simple-auth does not have all its async activites monitored by the test environment.
    await click('[data-test-login-button]');

    // This delay allows the unmonitored stuff to run.
    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    });

    let someDashboard = currentURL();
    assert.ok(/dashboard\//.test(someDashboard), 'Checking that the user is redirected to some dashboard page.');

    let widgets = await findAll('.grid-stack-item');
    assert.equal(widgets.length, numWidgets, 'Checking that the correct number of widgets are displayed in the dashboard.');

    let mapFilter = await findAll('[data-test-map-filter]');
    assert.notOk(mapFilter.length, 'Confirming that the map filter is not yet displayed.');

    await click('[data-test-map-filter-button]');
    mapFilter = await findAll('[data-test-map-filter]');
    assert.ok(mapFilter.length > 0, 'Confirming that the map filter appears after the map filter button is clicked.');
  });


  test('visiting /login, user can get to the /forgot-password page', async function(assert) {
    assert.expect(1);
    await visit('/login');

    await click('.data-test-forgot-password');

    assert.equal(currentURL(), '/forgot-password', 'Checking the the user got was directed to /forgot-password.');
  });
});
