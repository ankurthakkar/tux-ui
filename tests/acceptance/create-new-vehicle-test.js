import { module, test} from 'qunit';
import { visit, currentURL, fillIn, click, find, findAll, pauseTest} from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { Promise } from 'rsvp';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import vehicleScenario from '../../mirage/scenarios/vehicle';
import { currentSession, authenticateSession } from 'ember-simple-auth/test-support';
import _ from 'lodash';
import {fillInForm, mockSocketService} from 'adept-iq/tests/util';
import socketService from 'adept-iq/services/socket';
import {run} from '@ember/runloop';

module('Acceptance | create new vehicle', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.owner.register('service:socket', socketService);
    this.socketService = this.owner.lookup('service:socket');
    this.socketService.connect = mockSocketService.connect;
    this.socketService.toJSON = mockSocketService.toJSON;
    this.startMockServer = mockSocketService.startMockServer.bind(this.socketService);
});

  test('create new vehicle', async function(assert) {
    assert.expect(16);
    vehicleScenario(server);
    run(() => this.startMockServer(server));
    let newVehicle = server.db.ssVehicles.find('1');

    // add data for filling with fillInForm function
    newVehicle.name = "11";

    await authenticateSession({
      userId: 1,
      user: 'admin',
      token: 'test jwt'
    });
    assert.equal(currentSession(this.application).get('isAuthenticated'), true, 'user is authenticated');

    await visit('/dashboard/workspaces-default%2Fdefault');
    
    let vehicleRows = await findAll('.vehicles-widget vertical-collection tr');

    assert.ok(vehicleRows.length > 0, 'Confirm at least 1 vehicle is in the table');

    // Move to Dashboard Page
    let someDashBoard = currentURL();
    console.log(someDashBoard);
    assert.ok(/dashboard\//.test(someDashBoard), 'visiting some dashboard');

    // check the vehicle widget is there
    let vehicle = await this.element.querySelectorAll('.vehicles-widget');
    assert.equal(vehicle.length, 1, 'confirming there is only one vehicle widget');

    // check the vehicle widget loaded with the 3 default buttons
    let buttons = await this.element.querySelectorAll('.vehicles-widget .column-widget-button');
    assert.equal(buttons.length, 3, 'confirming the search, plus, and filter buttons are available');


    // Click plus button to open drop down menus
    await click('.data-test-plus-button');
    
    // find the drop down 
    let dropdown = await find('.data-test-dropdown');

    // get all of the drop down children
    let dropdownActions = dropdown.children;


    // currently there is only one option for the plus button
    assert.equal(dropdownActions.length, 1, 'confirming the drop down currently has one action for the plus button');

    // get the Create New vehicle Action Button
    let createActionButton = _.find(dropdownActions, (action) => action.innerText === 'New Vehicle');
    
    // Click to Open the Data Entry Form for vehicle Column Widget
    await click(createActionButton);

    let someEditForm = currentURL();
    assert.ok(/edit-form/.test(someEditForm), 'Confirm Routing for Opening up an Edit Form' )

    // assert the data entry form rendered
    let vehicleForm = await find('.data-test-vehicle-form-widget');
    assert.ok(vehicleForm, 'Confirming the vehicle Form is opened');

    // get the form control buttons
    let formControls = await findAll('[data-test-form-header-button]');

    assert.equal(formControls.length, 4, 'Confirm for the generic form control buttons a close button and search button');

    let formAddButtons = await findAll('.data-test-vehicle-form-widget .fa.fa-plus');

    assert.equal(formAddButtons.length, 1, 'vehicle Data Entry Widget Currently has 1 button for adding additional information');

    // Click add new Availability (keeping function for clicking all buttons for future tests)
    await _.reduce(formAddButtons, async (acc, formAddButton) => acc.then(await click(formAddButton)), Promise.resolve());

    await fillInForm(newVehicle);

    let formType = await findAll('.ember-power-select-trigger');

    await click(formType[0]);

    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    })

    let garageFields = await findAll('[data-test-address-field] .ember-power-select-trigger');
    console.log(garageFields);
    await click(garageFields[0]);

    let powerSearch = await find('.ember-power-select-search-input');
    assert.ok(powerSearch, 'Power Search Rendered from clicking');

    await fillIn(powerSearch, 'main');

    // small delay
    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    })

    let listItems = await findAll('li');
    console.log(listItems);
    assert.ok(listItems, 'auto complete renders addresses on the screen');

    await click(listItems[1]);
    await pauseTest();
    await click(garageFields[1]);

    powerSearch = await find('.ember-power-select-search-input');
    assert.ok(powerSearch, 'Power Search Rendered from clicking');

    await fillIn(powerSearch, 'main');

    await new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    })

    listItems = await findAll('li');
    assert.ok(listItems, 'auto complete renders addresses on the screen');

    await click(listItems[1]); 

    await click(formType[3]);

    let dayOptions = await findAll('.ember-power-select-option');

    //click all seven days
    dayOptions.forEach(async (element) => {
      await click(element);
    });

    await click('[data-test-form-apply-button]');

    await click('[data-test-header-cancel-button]');

    await click('[data-test-form-apply-button]');

    await click('[data-test-header-commit-button]');

    // find close button
    let closeButton = await find('[data-test-form-close-button]');

    // close vehicle data entry form
    await click(closeButton);

    vehicleRows = await findAll('.vehicles-widget vertical-collection tr');
    assert.equal(vehicleRows.length, 2, 'Confirm two vehicles are in the table now');

    newVehicle = server.db.ssVehicles.find('2');
    
    assert.equal(11, newVehicle.name, 'Confirm new vehicle has expected values in the ember store');

  });

});
