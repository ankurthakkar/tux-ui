import { module, test } from 'qunit';
import { run } from '@ember/runloop';
import { setupRenderingTest } from 'ember-qunit';
import { render, click, waitFor } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { startMirage } from 'adept-iq/initializers/ember-cli-mirage';
import widget from 'adept-iq/classes/widget';
import _ from 'lodash';
import { getDataFromTablePerColumn } from 'adept-iq/tests/util';

const passengersWidgetStub = {
  id: '1', 
  typeId: 'passengers', 
  x: 0, 
  y: 2,
  width: 4, 
  height: 2,
  state: {
    sortAsc: false,
    sortId: "otp"
  },
  dashboard: '1' 
};

module('Integration | Component | iq-widgets/passengers-widget', function(hooks) {
  setupRenderingTest(hooks);

  // Start the Mirage Server before Each Test
  hooks.beforeEach(function() {
    this.server = startMirage();
  })

  // Shut Down the Mirage Server After Each Test
  hooks.afterEach(function() {
    this.server.shutdown();
  });

  test('Passenger Sort Columns', async function(assert) {
    // Create a Dashboard model Instance
    const dashboard =  run(() => this.owner.lookup('service:store').createRecord('dashboard'));
    // Create 10 riders
    server.loadFixtures();
    const riders = server.db.rmsRiders;
    // Attach dashboard model instance to the Passenger Widget Stub Class Instance
    passengersWidgetStub.dashboard = dashboard;
    const passengerObj = widget.create(passengersWidgetStub);

    // Set any properties with this.set('myProperty', 'value');
    // Set a Property for the ember view to input widget properties into component
    this.set('passenger', passengerObj);
    
    // Ex. Handle any actions with this.set('myAction', function(val) { ... });
    // render the Passengers Column Widget
    await render(hbs`{{iq-widgets/passengers-widget widget=passenger }}`);

    // Check if the Widget Renders
    const passengerWidget = this.element.querySelector('.passengers-widget');
    assert.ok(passengerWidget, 'Confirm Passenger Column Widget Renders')

    // Check if the Widget's Data Renders
    const passengerTableData = getDataFromTablePerColumn(this, '.passengers-widget');
    assert.equal(passengerTableData.ID.length, riders.length, `Confirm the Data has ${passengerTableData.ID.length} Rows`);

    let passengerHeaderControls = this.element.querySelectorAll('.column-controls');
    assert.equal(passengerHeaderControls.length, 11, 'Confirm There are 11 Controls for a Passenger Widget');


    /* Check for Sorted Ascending First Name */
    let label = this.element.querySelectorAll('.column-label');
  
    await click(label[0]);

    await waitFor('vertical-collection');

    let currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    let labelText = label[0].innerText.replace(/\s/g,'');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');
  
    let headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    let sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);

    /* Check for Sorted Descending First Name */

    label = this.element.querySelectorAll('.column-label');
  
    await click(label[0]);

    await waitFor('vertical-collection');

    currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    labelText = label[0].innerText.replace(/\s/g,'');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');
  
    headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);


        /* Check for Sorted Ascending Phone Number */
    label = this.element.querySelectorAll('.column-label');
  
    await click(label[3]);

    await waitFor('vertical-collection');

    currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    labelText = label[3].innerText.replace(/\s/g, '');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');

    headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);

    /* Check for Sorted Descending Phone Number */

    label = this.element.querySelectorAll('.column-label');

    await click(label[3]);

    await waitFor('vertical-collection');

    currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    labelText = label[3].innerText.replace(/\s/g, '');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');

    headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);

        
    /* Check for Sorted Ascending OTP */
    label = this.element.querySelectorAll('.column-label');
  
    await click(label[label.length - 1]);

    await waitFor('vertical-collection');

    currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    labelText = label[label.length - 1].innerText.replace(/\s/g,'');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');
  
    headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);

    /* Check for Sorted Descending OTP */

    label = this.element.querySelectorAll('.column-label');
  
    await click(label[label.length - 1]);

    await waitFor('vertical-collection');

    currentTableState = getDataFromTablePerColumn(this, '.passengers-widget');

    labelText = label[label.length - 1].innerText.replace(/\s/g,'');
    passengerHeaderControls = this.element.querySelectorAll('.column-controls');
  
    headerControl = _.find(passengerHeaderControls, (control) => control.children.length > 0);
    sortOrder = headerControl.children[0].classList.contains('fa-sort-asc') ? 'asc' : 'desc';

    passengerTableData[labelText] = _.orderBy(passengerTableData[labelText], [], [sortOrder]);

    assert.deepEqual(currentTableState[labelText], passengerTableData[labelText], `Confirming sorted column: ${labelText} Current Column Data: ${currentTableState[labelText]} is equilvant to ${passengerTableData[labelText]}`);



  });



});


