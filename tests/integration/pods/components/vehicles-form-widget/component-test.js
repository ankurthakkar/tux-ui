import { module, test } from 'qunit';
import { run } from '@ember/runloop';
import { setupRenderingTest } from 'ember-qunit';
import { render, click, fillIn } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { startMirage } from 'adept-iq/initializers/ember-cli-mirage';
import _ from 'lodash';


module('Integration | Component | vehicles-form-widget', function(hooks) {
  setupRenderingTest(hooks);

  // Start the Mirage Server before Each Test
  hooks.beforeEach(function() {
    this.server = startMirage();
  })

  // Shut Down the Mirage Server After Each Test
  hooks.afterEach(function() {
    this.server.shutdown();
  });

  test('Search editable Fields', async function(assert) {
    server.loadFixtures();
    const vehicles = server.db.ssVehicles;
    const vehicle = run(() => this.owner.lookup('service:store').createRecord('ss-vehicle', vehicles[0]));

    this.set('editableRecords', [vehicle]);

    await render(hbs`{{iq-widgets/vehicles-form-widget editableRecords=editableRecords}}`);

    // Check to ensure the form widget rendered
    let vehicleForm = this.element.querySelector('.data-test-vehicle-form-widget');
    assert.ok(vehicleForm, 'Confirming the vehicle Form is opened');

    let formControls = this.element.querySelectorAll('[data-test-form-header-button]');
    assert.equal(formControls.length, 4, 'Confirm for the generic form control buttons a close button and search button');

    let searchButton = this.element.querySelector('[data-test-form-search-button]');

    await click(searchButton);

    let searchInput = this.element.querySelector('[data-test-form-search-input]');

    await click(searchInput);

    /*   Search validation for Type Cell   */
    await fillIn(searchInput, 'Type');

    let cellLabel = this.element.querySelectorAll('.cell-label');

    assert.equal(cellLabel.length, 1, 'Confirm only one cell is available');
    assert.equal(cellLabel[0].textContent.replace(/\s/g, ''), 'Type', 'Confirm only Type cell is available');

    /*   Search validation for Active Cell   */
    await fillIn(searchInput, 'Active');

    cellLabel = this.element.querySelectorAll('.cell-label');

    assert.equal(cellLabel.length, 1, 'Confirm only one cell is available');
    assert.equal(cellLabel[0].textContent.replace(/\s/g, ''), 'Active', 'Confirm only Active cell is available');

    /*   Search validation for Vehicle Cell   */
    await fillIn(searchInput, 'Vehicle');

    cellLabel = this.element.querySelectorAll('.cell-label');

    assert.equal(cellLabel.length, 1, 'Confirm only one cell is available');
    assert.equal(cellLabel[0].textContent.replace(/\s/g, ''), 'VehicleId', 'Confirm only Vehicle Id cell is available');

    /*   Search validation for Whellchair Spaces Cell   */
    await fillIn(searchInput, 'Wheelchair');

    cellLabel = this.element.querySelectorAll('.cell-label');

    assert.equal(cellLabel.length, 1, 'Confirm only one cell is available');
    assert.equal(cellLabel[0].textContent.replace(/\s/g, ''), 'Wheelchairspaces', 'Confirm only Wheelchair Spaces cell is available');

    /*   Search validation for Ambulatory Seats Cell   */
    await fillIn(searchInput, 'Ambulatory');

    cellLabel = this.element.querySelectorAll('.cell-label');

    assert.equal(cellLabel.length, 1, 'Confirm only one cell is available');
    assert.equal(cellLabel[0].textContent.replace(/\s/g, ''), 'AmbulatorySeats', 'Confirm only Ambulatory Seats cell is available');

  });
});
