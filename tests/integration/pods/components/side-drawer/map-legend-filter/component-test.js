import Service from '@ember/service';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { click, pauseTest } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

const layer = {
  types: [{
    label: 'first',
    isVisible: true
  },{
    label: 'second',
    isVisible: false
  }]
};

let flag = false;

const mapContextStub = Service.extend({
  setLayerTypeVisibility() {
    flag = true;
  }
});

module('Integration | Component | side-drawer/map-legend-filter', function(hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function() {
    this.owner.register('service:map-context', mapContextStub);
  });

  test('it displays all labels with their info', async function(assert) {
    assert.expect(5);

    this.set('layer', layer);
    await render(hbs`{{side-drawer/map-legend-filter layer=layer}}`);

    let rows = this.element.querySelectorAll('.data-test-type-visibility-checkbox');
    assert.equal(rows.length, layer.types.length, 'Checking that all layer types are rendered.');

    let displayedText = this.element.textContent;
    layer.types.forEach((type, index) => {
      let checkbox = this.element.querySelectorAll('.data-test-type-visibility-checkbox');
      assert.equal(checkbox[index].checked, type.isVisible, 'Checking that the check box value matches the type\'s visibility.');

      let pattern = new RegExp(type.label);
      assert.ok(pattern.test(displayedText), 'Checking that the type\'s label is displayed.');
    });
  });

  test('it acts when the checkbox is clicked', async function(assert) {
    assert.expect(1);

    this.set('layer', layer);
    await render(hbs`{{side-drawer/map-legend-filter layer=layer}}`);

    let checkBox = this.element.querySelector('.data-test-type-visibility-checkbox');
    await click(checkBox);

    assert.ok(flag, 'Checking that the mapContext\'s setLayerTypeVisibility method was called.');
  });
});
