/* eslint-disable */
import { visit, currentURL, fillIn, click, find, findAll} from '@ember/test-helpers';
import _ from 'lodash';
import {WebSocket, Server} from 'mock-socket';
import ENV from 'adept-iq/config/environment';
import RSVP from 'rsvp';
import EmberObject  from '@ember/object';
import { run, cancel, later } from '@ember/runloop';


// ----------- Mocked Service Stubs for Tests ------------------ //
const SUBSCRIPTION_TOPICS = [
  { topic: 'routeWidget', loadAsync: true },
  { topic: 'tripWidget', loadAsync: true },
  { topic: 'stopWidget', loadAsync: true },
  { topic: 'avlWidget', loadAsync: true },
  { topic: 'navigationInfoWidget', loadAsync: true },
  { topic: 'routeExecEventsWidget', loadAsync: true }
];

const mockSocketService = {

/**
 * @summary : Summary: Utility Function for creating JSON object per each Socket Message 
 * @param {server} server : An object associated with the relational database of Mirage and to use the serailize property to format response payloads
 * @return JSON response payload 
 */
  toJSON(models, server) {
    // I then map each route model and serialize each to have a format of {data: object}
    models = models.map((model) => server.serializerOrRegistry.serialize(model));
    // I then mock the included property; @TODO Delete When Included is functional for most fixtures
    models = _.map(models, (model) => {
      model.included = []
      return model;
    });
    // Then I Stringify the entire array of routes and make a data property for the entire payload to post
    return {data: JSON.stringify(models)};
  },

/**
 * @summary : Summary: Utility Function for Creating a Web Socket Server and Simulating Kafka data through set interval function
 * @param {server} server : An object associated with the relational database of Mirage to create payloads and simulate actual changes
 */
  startMockServer(server) {
      SUBSCRIPTION_TOPICS.map(({topic}) => {
        let payload = null;
        switch (topic){
          case SUBSCRIPTION_TOPICS[0].topic:
          payload = this.toJSON(server.schema.routes.all().models, server);
          this.onSocketMessage(topic, payload, RSVP.defer());
            break;
          // case SUBSCRIPTION_TOPICS[1].topic:
          // payload = this.toJSON(server.schema.trips.all().models, server);
          // this.onSocketMessage(topic, payload, RSVP.defer());
          //   break;
          // case SUBSCRIPTION_TOPICS[2].topic:
          // payload = this.toJSON(server.schema.stops.all().models, server);
          // this.onSocketMessage(topic, payload, RSVP.defer());
          //   break;
          // case SUBSCRIPTION_TOPICS[3].topic:
          // payload = this.toJSON(server.schema.avl.all().models, server);
          // this.onSocketMessage(topic, payload, RSVP.defer());
          //   break;
          // case SUBSCRIPTION_TOPICS[4].topic:
          // payload = this.toJSON(server.schema.routes.all().models, server);
          // this.onSocketMessage(topic, payload, RSVP.defer());
          //   break;
          // case SUBSCRIPTION_TOPICS[5].topic:
          // payload = this.toJSON(server.schema.routes.all().models, server);
          // this.onSocketMessage(topic, payload, RSVP.defer());
          // break;
          default:
            console.log(`Topic not available ${topic}`);
        }
        });
  },
  /**
   * @summary : this function can be anything but here's a simple stub overriding the connection function used in the socket service
   * @param {*} startDate : param does not matter
   * @param {*} endDate  : param does not matter
   */
  connect(startDate, endDate) {
    return;
  },
}

// ----------- Utility Functions for Acceptance Tests ------------------ //

/**
 * @summary : Summary: Need a form rendered; A utility functions for filling out form(s) could be improved
 * @param {model} model : An object associated with properties similar to the cell labels to fill in each cell with releastic data
 * @return : returns a promise
 */
async function fillInForm(model){
    
    return Promise.all(_.map(_.keys(model), async (key) => {
      let cellInput = await find(`.data-test-${key}-input input`);
      if (cellInput){
        console.log(cellInput);
        return await fillIn(cellInput, model[key]);
      }
      return Promise.resolve();
    }))
  }

// ----------- Utility Functions for Integration Tests ------------------ //


 /**
 * @summary : Summary: Need a form rendered; A utility functions for filling out form(s) could be improved
 * @param {app} app : An object associated with the test context for an integration test or acceptance test
 * @param {tableSelector} tableSelector: a associated css selector for selecting the rendered component on the dom exclusively a table
 * @return : returns a the table data dependent on the table selector
 */ 
const getDataFromTablePerColumn = (app, tableSelector) => {
    // get all cells except for check box cell
    const cells = _.filter(app.element.querySelectorAll(`${tableSelector} vertical-collection .table-base-cell`), (cell) => {
        return !cell.classList.contains(`table-check-box-cell`);
    });
    // Obtain each Label and condense the text
    const labels = _.map(app.element.querySelectorAll(`${tableSelector} .column-label`), (label) => label.innerText.replace(/\s/g, ''));
    // Create a table Object with the current state of each column
    const table = {};
    _.forEach(labels, (label) => {
    
      table[label] = [];
    });
  
    _.forEach(cells, (cell, index) => {
      index = index % labels.length;
      if (cell){
        table[labels[index]].push(cell.innerText.replace(/\s/g,''));
      }
    });
  
    return table;
  
  }

export {
    fillInForm, 
    getDataFromTablePerColumn,
    mockSocketService
  };