'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const pickFiles = require('broccoli-static-compiler');
const lodash = require('lodash');

const { concat } = lodash;

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    'ember-bootstrap': {
      'bootstrapVersion': 4,
      'importBootstrapFont': false,
      'importBootstrapCSS': false
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  // mapbox polyline decoder
  app.import('node_modules/@mapbox/polyline/index.js', {
    using: [
      { transformation: 'cjs', as: '@mapbox/polyline' }
    ]
  });

  // tomtom
  app.import('vendor/tomtom/tomtom.min.js');
  app.import('vendor/shims/tomtom.js');
  app.import('vendor/tomtom/map.css')

  // leaflet plugins
  app.import('vendor/leaflet/leaflet.rotatedMarker.js')

  const tomtomFolders = ['glyphs', 'images', 'sprites', 'styles'];
  const tomtomAssets = tomtomFolders.map((folder) => {
    return pickFiles('vendor', {
      srcDir: `/tomtom/${folder}`,
      destDir: `/assets/tomtom/${folder}`
    });
  });

  const tomtomDDSFolders = ['styles'];
  const tomtomDDSAssets = tomtomDDSFolders.map((folder) => {
    return pickFiles('vendor', {
      srcDir: `/tomtom-dds/${folder}`,
      destDir: `/assets/tomtom-dds/${folder}`
    });
  });

  const assets = concat(tomtomAssets, tomtomDDSAssets);

  return app.toTree(assets);
};
