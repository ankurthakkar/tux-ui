(function() {
  function vendorModule() {
    'use strict';

    return {
      'default': self['tomtom'],
      __esModule: true,
    };
  }

  define('tomtom', [], vendorModule);
})();
